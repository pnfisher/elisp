(provide 'my-python)

(require 'my-globals)
(require 'my-glimpse)

(require 'compile)
(require 'python)
(require 'gud)

(defvar my-python-compile-use-comint-mode nil)

(defun my-python-toggle-compile-comint-mode ()
  (interactive)
  (setq my-python-compile-use-comint-mode
        (not my-python-compile-use-comint-mode)))

(defun my-python-compile (&optional arg)
  (interactive "P")
  (let ((command
         (compilation-read-command
          (concat
           "python "
           (when buffer-file-name
             (file-name-nondirectory buffer-file-name))))))
    ;; comint mode allows us to provide input to running python
    ;; script if it needs it
    (compile command (when (or arg my-python-compile-use-comint-mode) t))))

(add-hook
 'python-mode-hook
 #'(lambda ()
     (font-lock-mode t)
     (setq python-indent-offset 2)
     (setq python-shell-interpreter "ipython"
           python-shell-interpreter-args "-i")))

;; (defadvice pdb (before pdb-advice activate compile)
;;   (let* ((fname (car (cdr (split-string-and-unquote command-line))))
;;          (bname (concat "*gud-" fname "*"))
;;          (proc (get-buffer-process bname)))
;;     (when (and proc
;;                (y-or-n-p (format "%s already exists. Restart? " bname)))
;;       (switch-to-buffer bname)
;;       (let* ((src gud-target-name)
;;              (buf (find-buffer-visiting src)))
;;         (when buf
;;           (switch-to-buffer buf))
;;         (kill-process proc)
;;         (while (get-buffer-process bname)
;;           (sleep-for 0.1))
;;         (kill-buffer bname)))))

;; (add-hook
;;  'pdb-mode-hook
;;  #'(lambda ()
;;      (let* ((src gud-target-name)
;;             (buf (find-buffer-visiting src)))
;;        (when (not buf)
;;          (error (concat "no source buffer for " src)))
;;        (delete-other-windows)
;;        (split-window-below)
;;        (other-window 1)
;;        (switch-to-buffer buf)
;;        (other-window 1))))

(defun my-python-docs (version)
  (interactive "nversion (2 or 3): ")
  (when (not (or (= version 2) (= version 3)))
    (error "version must be either 2 or 3"))
  (let* ((basedir (concat my-home-directory "/opt/python/docs"))
         (dir (format "%s/%d" basedir version)))
    (my-glimpse dir)))

(define-key python-mode-map (kbd "C-c C-b") 'python-shell-send-buffer)
(define-key python-mode-map (kbd "C-c C-c") 'comment-region)
(define-key python-mode-map (kbd "C-c C-SPC") 'gud-break)
;; (define-key python-mode-map (kbd "<C-tab>") 'completion-at-point)
;; (define-key python-mode-map (kbd "<M-down>") 'python-nav-forward-sexp)
;; (define-key python-mode-map (kbd "<M-up>") 'python-nav-backward-sexp)
(define-key python-mode-map (kbd "C-c C-d") 'my-python-docs)
(define-key python-mode-map (kbd "<f9>") 'my-python-compile)

;;(add-hook
;; 'jython-mode-hook
;; '(lambda ()
;;    (setq python-jython-command "/h/phil/opt/jython2.5.2/bin/jython")))

;;(defadvice run-python
;;  (around run-python-advice compile activate)
;;  (if (eq major-mode 'jython-mode)
;;      (let ((process-environment
;;             (cons (concat "JYTHONPATH=" data-directory)
;;                   process-environment)))
;;        ad-do-it)
;;    ad-do-it))

;;(defadvice python-send-buffer
;;  (after python-send-buffer-advice compile activate)
;;  (let* ((bname (buffer-name python-buffer))
;;         (buf (get-buffer bname))
;;         (nw (count-windows)))
;;    (unless buf
;;      (error (concat "couldn't find " bname " buffer")))
;;    (when (= nw 1)
;;      (split-window))
;;    (let ((pop-up-windows t))
;;      (pop-to-buffer buf t)
;;      (other-window -1))))

;;(defun python-send-buffer-with-args (args)
;;  (interactive "sargs: ")
;;  (let ((source-buffer (current-buffer)))
;;    (with-temp-buffer
;;      (insert "import sys; sys.argv = '''" args "'''.split()\n")
;;      (insert-buffer-substring source-buffer)
;;      (python-send-buffer))))
