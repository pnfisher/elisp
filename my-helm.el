(provide 'my-helm)

(require 'my-submodules)
(require 'my-keymaps)
;; because we're going to change the ido-mode from the default ('both)
;; given in my-ido.el (change it to just 'file and then use helm for
;; buffer switching. We need to make sure 'my-ido loads before we make
;; the switch, otherwise our setting could get clobbered)
(require 'my-ido)
(ido-mode 'file)

(require 'cl-lib)
(require 'vc-hooks)

(defvar my-helm-do-grep-ag-history nil)

(defun my-helm-do-grep-ag ()
  (interactive)
  (let ((helm-grep-ag-command
         (concat
          "ag "
          (read-from-minibuffer
           "Run ag with args: "
           (concat
            (replace-regexp-in-string
             "\\(\\s-*ag\\s-*\\)\\|\\(\\s-*%s?\\)\\{3\\}\\s-*$"
             ""
             helm-grep-ag-command)
            " ")
           nil
           nil
           'my-helm-do-grep-ag-history)
          " %s %s %s")))
    (call-interactively 'helm-do-grep-ag)))

(defun my-helm-with-helm-mode (func)
  (point-to-register (string-to-char ".") t)
  (let ((restore-state (if helm-mode 1 -1)))
    (if current-prefix-arg
        (unwind-protect
            (progn
              (helm-mode -1)
              (call-interactively func))
          (helm-mode restore-state))
      (unwind-protect
          (progn
            (helm-mode 1)
            (call-interactively func))
        (helm-mode restore-state)))))

(my-global-set-key (kbd "M-x") 'helm-M-x)
(my-global-set-key (kbd "C-c M-x") 'execute-extended-command)

(defun my-helm-describe-variable ()
  (interactive)
  (my-helm-with-helm-mode 'describe-variable))
(my-global-set-key [remap describe-variable] 'my-helm-describe-variable)

(defun my-helm-describe-function ()
  (interactive)
  (my-helm-with-helm-mode 'describe-function))
(my-global-set-key [remap describe-function] 'my-helm-describe-function)

(defun my-helm-info-lookup-symbol ()
  (interactive)
  (my-helm-with-helm-mode 'info-lookup-symbol))
(my-global-set-key [remap info-lookup-symbol] 'my-helm-info-lookup-symbol)

(defun my-helm-info-goto-emacs-command-node ()
  (interactive)
  (my-helm-with-helm-mode 'Info-goto-emacs-command-node))
(my-global-set-key [remap Info-goto-emacs-command-node]
                'my-helm-info-goto-emacs-command-node)

(defun my-helm-helm-man-woman ()
  (interactive)
  (my-helm-with-helm-mode 'helm-man-woman))
(my-global-set-key [remap man] 'my-helm-helm-man-woman)

(defun my-helm-find-variable ()
  (interactive)
  (my-helm-with-helm-mode 'find-variable))

(defun my-helm-find-function ()
  (interactive)
  (my-helm-with-helm-mode 'find-function))

(defun my-helm-apropos ()
  (interactive)
  (if current-prefix-arg
      (call-interactively 'apropos-command)
    (call-interactively 'helm-apropos)))
(my-global-set-key [remap apropos-command] 'my-helm-apropos)

(defun my-helm-switch-to-buffer ()
  (interactive)
  (if current-prefix-arg
      (call-interactively 'ido-switch-buffer)
    (call-interactively 'helm-mini)))
(global-set-key [remap switch-to-buffer] 'my-helm-switch-to-buffer)
(my-global-set-key (kbd "<f6>") 'my-helm-switch-to-buffer)

(defun my-helm-describe-bindings ()
  (interactive)
  (if current-prefix-arg
      (call-interactively 'describe-bindings)
    (call-interactively 'helm-descbinds)))
(my-global-set-key [remap describe-bindings] 'my-helm-describe-bindings)

(defun my-helm-filtered-bookmarks ()
  (interactive)
  (if current-prefix-arg
      (call-interactively 'bookmark-jump)
    (call-interactively 'helm-filtered-bookmarks)))
(my-global-set-key [remap bookmark-jump] 'my-helm-filtered-bookmarks)

;; if we're in a directory that is itself in
;; vc-directory-exclusion-list, it must be that we really want to
;; search that directory, so in such cases, lets set helm's skip
;; boring files variable to nil
(defun my-helm-find-advice (ofunc &rest args)
  (let ((dir (file-name-base (directory-file-name default-directory))))
    (if (member dir vc-directory-exclusion-list)
        (let ((helm-findutils-skip-boring-files nil))
          (apply ofunc args))
      (apply ofunc args))))
(advice-add #'helm-find :around #'my-helm-find-advice)
(advice-add #'helm-find-files :around #'my-helm-find-advice)

(defun my-helm-help-advice (ofunc &rest args)
  (let ((full (frame-parameter nil 'fullscreen)))
    (when (not full)
      (toggle-frame-fullscreen))
    (apply ofunc args)
    (when (not full)
      (toggle-frame-fullscreen))))
(advice-add #'helm-help :around #'my-helm-help-advice)

(defun my-helm-advice-default (ofunc &rest plist)
  (when (string-equal (plist-get plist :buffer) "*helm-descbinds*")
    (setq plist (plist-put plist :default my-keymaps-prefix-regexp)))
  (apply ofunc plist))
(advice-add #'helm :around #'my-helm-advice-default)

(defun my-helm-advice-input (ofunc &rest plist)
  (when (string-equal (plist-get plist :buffer) "*helm-descbinds*")
    (setq plist (plist-put plist :input my-keymaps-prefix-regexp))
    (cl-dolist (source (plist-get plist :sources))
      (when (string-equal (cdr (car source)) "Global Bindings:")
        (setq plist (plist-put plist :sources (list source)))
        (cl-return))))
  (apply ofunc plist))

(defun my-helm-describe-bindings-custom ()
  (interactive)
  (advice-remove #'helm #'my-helm-advice-default)
  (advice-add #'helm :around #'my-helm-advice-input)
  (call-interactively #'helm-descbinds)
  (advice-remove #'helm #'my-helm-advice-input)
  (advice-add #'helm :around #'my-helm-advice-default))

(helm-autoresize-mode 1)

(custom-set-variables
 '(helm-move-to-line-cycle-in-source t)
 '(helm-ag-always-set-extra-option t)
 ;;'(helm-google-suggest-default-browser-function 'browse-url-text-emacs)
 '(helm-surfraw-default-browser-function 'browse-url-text-emacs)
 ;;'(helm-autoresize-max-height 80)
 '(helm-findutils-search-full-path t)
 '(helm-mode-fuzzy-match t)
 '(helm-completion-in-region-fuzzy-match t)
 '(helm-buffers-fuzzy-matching t)
 '(helm-recentf-fuzzy-match t)
 '(helm-M-x-fuzzy-match t)
 '(helm-apropos-fuzzy-match t)
 '(helm-lisp-fuzzy-completion t)
 ;;'(helm-semantic-fuzzy-match t)
 '(helm-imenu-fuzzy-match t)
 )
