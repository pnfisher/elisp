(provide 'my-gnus)

(require 'smtpmail)
(require 'starttls)
(require 'message)
(require 'gnus)
(require 'gnus-msg)
(require 'nnir)

(custom-set-variables
 '(gnus-home-directory "~/gnus")
 '(gnus-startup-file (concat gnus-home-directory "/.newsrc"))
 '(gnus-use-cache t)
 ;; '(gnus-select-method
 ;;   '(nntp "news.eternal-september.org" (nntp-port-number 80)))
 '(gnus-select-method '(nntp "news.gmane.org"))
 ;; default was 200, with nil gnus won't prompt for how many articles
 ;; to fetch no matter how large the number of articles (let's try
 ;; 20000 and see how that goes
 '(gnus-large-newsgroup 20000)
 ;; '(setq gnus-thread-hide-subtree nil)
 ;; '(gnus-mark-unpicked-articles-as-read nil)
 ;; '(gnus-fetch-old-headers t)
 '(gnus-confirm-mail-reply-to-news t)
 ;; '(gnus-summary-thread-gathering-function 'gnus-gather-threads-by-subject)
 ;; '(gnus-ignored-newsgroups
 ;;  "^to\\.\\|^[0-9. ]+\\( \\|$\\)\\|^[\"]\"[#'()]")
 '(gnus-header-face-alist
   '(("From" nil gnus-header-from)
     ("Subject" nil my-gnus-header-subject)
     ("Newsgroups:.*," nil gnus-header-newsgroups)
     ("" gnus-header-name gnus-header-content)))
 )

(defface my-gnus-header-subject
  '((default :inherit 'gnus-header-subject)
    (((class color) (background light)) :foreground "magenta4")
    (((class color) (background dark)) :foreground "violet"))
  "My face for gnus article subject highlighting."
  :group 'gnus-article-headers
  :group 'gnus-article-highlight)

(add-hook 'gnus-group-mode-hook 'gnus-topic-mode)
(add-hook
 'gnus-startup-hook
 #'(lambda ()
     ;; show all subscribed groups, even if they have 0 messages
     (gnus-group-list-all-groups 5)))

(add-to-list
 'gnus-secondary-select-methods
 '(nnimap
   "gmail"
   (nnimap-address "imap.gmail.com")
   (nnimap-server-port "imaps")
   (nnimap-stream ssl)
   (nnir-search-engine imap)
   (nnmail-expiry-target "nnimap+gmail:[Gmail]/Trash")
   (nnmail-expiry-wait 90)
   )
 )

;;(defvar smtpmail-auth-credentials (expand-file-name "~/.authinfo.gpg"))
;;(defvar smtpmail-starttls-credentials '(("smtp.gmail.com" 587 nil nil)))
(custom-set-variables
 '(send-mail-function 'smtpmail-send-it)
 '(message-send-mail-function 'smtpmail-send-it)
 '(smtpmail-default-smtp-server "smtp.gmail.com")
 '(smtpmail-smtp-server "smtp.gmail.com")
 '(smtpmail-smtp-service 587)
 ;;'(smtpmail-debug-info t)
 '(user-mail-address "philipnfisher@gmail.com")
 '(user-full-name "Philip Fisher")
 '(message-subscribed-address-functions '(gnus-find-subscribed-addresses))
 )

(eval-after-load 'mailcap '(mailcap-parse-mailcaps))
