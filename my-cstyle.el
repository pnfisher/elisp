(provide 'my-cstyle)

(defvar my-cstyle-eb t)

(defun my-set-eb-style ()
  (interactive)
  (c-set-style "my-eb")
  (setq tab-width 4
        indent-tabs-mode t))

;; NOTE: debug indentation styles using
;; c-show-syntactic-information. Also, you can look at all the
;; preconfigured syles by checking the variable c-style-alist
(add-hook
 'c-mode-common-hook
 '(lambda ()
    (if (not (boundp 'google-c-style))
        (progn
          ;;(setq c-default-style "gnu")
          (c-set-style "gnu"))
      (google-set-c-style)
      (c-add-style "my-eb"
                   '("Google"
                     (c-basic-offset . 4)
                     (c-offsets-alist
                      (label . 0))))
      (c-add-style "my-modified-google"
                   '("Google"
                     (c-basic-offset . 2)
                     (c-offsets-alist
                      (member-init-intro . 2)
                      (statement-cont . +)
                      (arglist-intro . 2)
                      (case-label . 2)
                      (label . 1)))))
    (if my-cstyle-eb
        (my-set-eb-style)
      (set (make-local-variable 'tab-width) 2)
      (set (make-local-variable 'indent-tabs-mode) nil)
      (c-set-style "my-modified-google"))))
