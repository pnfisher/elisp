(provide 'my-rg)

(require 'my-tools)
(require 'my-grep)

(eval-and-compile (defconst my-rg-binary "rg"))
(defvar my-rg-default-command (format "%s --no-heading" my-rg-binary))
;; unfortunately, ripgrep doesn't support 01;31 term type color codes
(defvar my-rg-color-params nil)
(defvar my-rg-history nil)
(defvar my-rgrc-top-directory "/")

(eval-and-compile
  (my-grep-define-compilation-mode
    'my-rg-mode
    my-rg-binary
    ;; highlight matches
    t))

(cl-assert my-rg-highlight-matches)

(defun my-rg-types ()
  (interactive)
  (shell-command (concat my-rg-binary " --type-list")))
(defalias 'my-rg-filters 'my-rg-types)

(defun my-rg-find-rgrc (path)
  (let ((dir (directory-file-name (expand-file-name path)))
        (p nil))
    (if (string-equal dir my-rgrc-top-directory)
        nil
      (setq p (concat dir "/.rgrc"))
      (if (file-exists-p p)
          p
        (my-rg-find-rgrc
         (directory-file-name
          (file-name-directory
           (directory-file-name dir))))))))

(defun my-rg-get-params-rgrc (rgrc)
  (let (params)
    (when rgrc
      (with-temp-buffer
        (insert-file-contents rgrc)
        (goto-char (point-min))
        (while (not (eobp))
          (beginning-of-line)
          (when (and (not (looking-at "\\s-*#"))
                     (looking-at "\\s-*\\(.*\\)"))
            (setq params (concat params " " (match-string 1))))
          (forward-line 1))))
    params))

(defun my-rg ()
  (interactive)
  (let* ((backup (nth 1 (assoc my-tools-id my-tags-directory-table)))
         (rgrc (my-rg-find-rgrc default-directory))
         (rgdir (if rgrc (file-name-directory rgrc) nil))
         (params (my-rg-get-params-rgrc rgrc))
         (default
           (replace-regexp-in-string
            "\\s-\\s-+"
            " "
            (concat my-rg-binary " " params " ")))
         (index (+ (string-bytes default) 1))
         cmd)
    (when (not (file-exists-p backup))
      (setq backup nil))
    (when (or (not rgdir) current-prefix-arg)
      (setq rgdir (read-directory-name
                   "directory: "
                   backup
                   backup)))
    (setq cmd
          (replace-regexp-in-string
           (format "^%s" my-rg-binary)
           (concat my-rg-default-command
                   (when my-rg-highlight-matches
                     (concat " " my-rg-color-params)))
           (read-from-minibuffer
            (format "Run %s (like this): " my-rg-binary)
            (cons (concat default (thing-at-point 'symbol)) index)
            nil
            nil
            'rg-history)))
    (with-temp-buffer
      (cd rgdir)
      (compilation-start
       cmd
       'my-rg-mode
       #'(lambda (mode) (concat "*" my-rg-binary "*"))))))
