(provide 'my-comint)

(require 'my-scroll)
(require 'comint)

;; (custom-set-faces
;;   ;; custom-set-faces was added by Custom.
;;   ;; If you edit it by hand, you could mess it up, so be careful.
;;   ;; Your init file should contain only one such instance.
;;   ;; If there is more than one, they won't work right.
;;  '(comint-highlight-input
;;    ((((class color) (background dark)) (:foreground "Gray85"))))
;;  '(comint-highlight-prompt
;;    ((((class color) (background dark)) (:foreground "Gray89"))))
;;  '(minibuffer-prompt ((((background dark)) nil)))
;;  '(query-replace ((t (:inherit highlight)))))

(custom-set-variables
 '(comint-scroll-show-maximum-output nil)
 '(comint-scroll-to-bottom-on-output nil)
 '(comint-scroll-to-bottom-on-input t))

(add-hook
 'comint-mode-hook
 '(lambda ()
    (setq tab-width 8)))

(define-key comint-mode-map (kbd "C-<S-up>") 'comint-previous-input)
(define-key comint-mode-map (kbd "C-<S-down>") 'comint-next-input)
(define-key comint-mode-map (kbd "C-<up>") 'my-scroll-n-lines-behind)
(define-key comint-mode-map (kbd "C-<down>") 'my-scroll-n-lines-ahead)
