(provide 'my-text)

(require 'cl-lib)
(require 'files)

(defvar my-text-mode-map
  (let ((map (make-sparse-keymap)))
    (set-keymap-parent map text-mode-map)
    (define-key map (kbd "TAB") 'my-indent-for-tab-command)
    map))

(setq
 auto-mode-alist
 (append
  '(
    ("\\.te?xt\\'" . my-text-mode)
    )
  auto-mode-alist))

(define-derived-mode my-text-mode text-mode "Text+"
  (turn-on-auto-fill)
  (use-local-map my-text-mode-map)
  ;; (paragraph-indent-minor-mode)
  ;; change how fill-region handles bullets.
  ;; (set (make-local-variable 'paragraph-start)
  ;;     (concat paragraph-start "\\|[*] +.+$"))
  (set (make-local-variable 'sentence-end-double-space) nil)
  (set (make-local-variable 'tab-width) 4)
  ;; change how fill-region handles lines prefixed with
  ;; '; -*-'
  (set (make-local-variable 'paragraph-start)
       (concat paragraph-start "\\|;[;]? \\-\\*\\-.+$"))
  (set (make-local-variable 'tab-always-indent) 'complete))

(cl-defun my-count-words (&optional arg)
  (interactive "P")
  (when mark-active
    (save-restriction
      (narrow-to-region (region-beginning) (region-end))
      (let ((mark-active nil))
        (my-count-words)))
    (cl-return-from my-count-words))
  (when arg
    (save-excursion
      (push-mark (point) nil t)
      (goto-char (point-max))
      (my-count-words))
    (cl-return-from my-count-words))
  (let ((count 0))
    (save-excursion
      (goto-char (point-min))
      (cl-loop

       (while (looking-at "^\\(;\\|#\\)")
         (when (> (forward-line 1) 0)
           (cl-return)))

       (beginning-of-line)

       (let ((rstart (point)))
         (end-of-line)
         (setq count
               (- (+ count (count-matches "\\w+" rstart (point)))
                  (count-matches "<\\w+\\>" rstart (point)))))

       (when (> (forward-line 1) 0)
         (cl-return))))

    (message (format "%d words" count))))

;;(defalias 'count-words 'my-count-words)

(cl-defun my-indent-for-tab-command (&optional arg)
  (interactive "P")
  (when (or current-prefix-arg (looking-at "^."))
    (indent-for-tab-command arg)
    (cl-return-from my-indent-for-tab-command))
  (let ((regular-indent nil))
    (save-excursion
      (backward-char 1)
      (when (looking-at " \\( \\|\\w\\)")
        (setq regular-indent t)))
    (when regular-indent
      (indent-for-tab-command arg)
      (cl-return-from my-indent-for-tab-command))
    (save-excursion
      (let ((current (point)))
        (beginning-of-line)
        (re-search-forward "^ +" nil t)
        (when (= current (point))
          (setq regular-indent t))))
    (when regular-indent
      (indent-for-tab-command arg)
      (cl-return-from my-indent-for-tab-command)))
  (ispell-complete-word))

(cl-defun my-fill-region ()
  (interactive)
  (when mark-active
    (save-restriction
      (narrow-to-region (region-beginning) (region-end))
      (let ((mark-active nil))
        (my-fill-region)))
    (cl-return-from my-fill-region))
  (save-excursion
    (let ((fill-column (read-number "Set fill-column to: " (current-column))))
      (when (= fill-column 0)
        (setq fill-column (point-max)))
      (if (not (string-equal major-mode "message-mode"))
          (fill-region (point-min) (point-max))
        (save-excursion
          (goto-char (point-min))
          (when (re-search-forward "--text follows this line--" nil t)
            (let (p1)
              (cl-loop
               ;; exit loop when end of buffer
               (when (> (forward-line 1) 0)
                 (cl-return))
               ;; process text that isn't quoted email
               (when (not (looking-at "^>"))
                 (setq p1 (point))
                 (cl-loop
                  ;; skip forward over quoted email; exit when quote
                  ;; or buffer ends
                  (when (or (> (forward-line 1) 0)
                            (looking-at "^>"))
                    (cl-return)))
                 (fill-region p1 (point)))))))))))

(cl-defun my-insert-blank-lines ()
  (interactive)
  (let ((relocate (point)))
    (save-excursion
      (goto-char (point-min))
      (cl-loop
       (when (> (forward-paragraph 1) 0) (cl-return))
       (when (not (looking-at "^\\([ \\t]*\\|[ \\t]*\\)$"))
         (newline)
         (when (> relocate (point))
           (setq relocate (1+ relocate))))))
    (goto-char relocate)))

(cl-defun my-delete-blank-lines ()
  (interactive)
  (let ((relocate (point)))
    (save-excursion
      (goto-char (point-min))
      (cl-loop
       (when (> (forward-paragraph 1) 0) (cl-return))
       (when (looking-at "^[ \\t]*$")
         (let ((kill-whole-line t))
           (kill-line))
         (when (> relocate (point))
           (setq relocate (1- relocate))))))
    (goto-char relocate)))

;; xxx is this really necessary? fill-region should take care of this
;; already
(cl-defun my-find-whitespace (&optional arg)
  (interactive "P")
  (when mark-active
    (save-restriction
      (narrow-to-region (region-beginning) (region-end))
      (let ((mark-active nil))
        (my-find-whitespace)))
    (cl-return-from my-find-whitespace))
  (when arg
    (save-excursion
      (push-mark (point) nil t)
      (goto-char (point-max))
      (my-find-whitespace))
    (cl-return-from my-find-whitespace))
  (let ((relocate (point)))
    (save-excursion
      (goto-char (point-min))
      (cl-loop
       (when (not (re-search-forward "[ ][ ]" nil t))
         (cl-return))
       (if (<= (current-column) 4)
           (backward-char 1)
         (isearch-highlight (- (point) 2) (point))
         (when (y-or-n-p "replace whitespace with single space ")
           (replace-match " " nil t)
           (when (> relocate (point))
             (setq relocate (1- relocate)))
           (backward-char 2))
         (isearch-dehighlight))))
    (goto-char relocate)))
