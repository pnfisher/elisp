(provide 'my-uniquify)

(require 'uniquify)

(custom-set-variables
 '(uniquify-buffer-name-style 'post-forward)
 '(uniquify-separator ":")
 '(uniquify-after-kill-buffer-p nil))
