(provide 'my-ccprog)

(require 'my-globals)
(require 'my-cstyle)
(require 'my-utils)
(require 'my-edit)

(require 'cl-lib)
(require 'cc-mode)
(require 'font-lock)
(require 'c-comment-edit)
(require 'my-compile)

(eval-and-compile
  (when my-opt-install-path
    (let ((clang-format-path (concat my-opt-install-path "/share/clang")))
      (when (file-exists-p clang-format-path)
        (add-to-list 'load-path clang-format-path)
        (dir-locals-set-directory-class
         clang-format-path
         'my-elisp-source-class)))))

(require 'clang-format nil :noerror)

(defconst my-cc-clang-format-styles
  (list
   (when (file-exists-p (concat my-home-directory "/.clang-format")) "file")
   "Google"
   "Chromium"
   "LLVM"
   "Mozilla"
   "WebKit"))

;; suppress compiler warnings when clang-format.el fails to load
(eval-when-compile
  (when (not (boundp 'clang-format-style))
    (defun clang-format-region (&rest args) nil)))

(eval-and-compile
  (if (boundp 'clang-format-style)
      (advice-add #'clang-format-region
                  :before
                  #'(lambda (&rest args)
                      (let ((default (car my-cc-clang-format-styles)))
                        (set (make-local-variable 'clang-format-style)
                             (completing-read
                              (format
                               "style (default %s): "
                               (if (string-equal default "file")
                                   "~/.clang-format"
                                 default))
                              my-cc-clang-format-styles
                              nil t nil nil
                              default)))))
    (message "WARNING: my-ccprog.el failed to load clang-format")))

(dolist (map (list c-mode-map c++-mode-map))
  (when (functionp 'clang-format-buffer)
    (define-key map (kbd "C-<S-f9>") 'clang-format-buffer))
  ;; (define-key map (kbd "C-c E") 'c-comment-edit)
  (define-key map (kbd "C-c C-SPC") 'gud-break)
  (define-key map (kbd "C-<S-f2>") 'c-toggle-auto-hungry-state)
  (define-key map (kbd "C-c |") 'my-cc-align-params))

;; common to both c and c++
(add-hook
 'c-mode-common-hook
 '(lambda ()

    ;; (c-toggle-auto-state 1)
    ;; (c-toggle-hungry-state 1)
    ;; (c-toggle-auto-hungry-state 1)

    ;; (face-remap-add-relative
    ;;  'font-lock-keyword-face '((:foreground "cyan3")))
    ;; (face-remap-add-relative
    ;;  'font-lock-type-face '((:foreground "lime green")))
    ;; (face-remap-add-relative
    ;;  'font-lock-variable-name-face '((:foreground "white")))
    ;;
    ;; letting google-c-style take care of these
    ;;
    ;; (setq c-indent-level global-c-indent-level)
    ;; (setq tab-width global-c-indent-level)
    ;; (c-set-offset 'case-label '+)
    ;; (c-set-offset 'arglist-intro global-c-indent-level)
    ;; (c-set-offset 'substatement-open 0)
    (font-lock-mode t)))

(setq
 c-comment-edit-hook
 #'(lambda ()
     (if (y-or-n-p "multi-line: ")
         (progn
           (if (not (equal c-comment-leader " *"))
               (setq c-comment-leader " *"))
           (set-buffer-modified-p t)
           (goto-char (point-min))
           (insert "\n"))
       (if (not (equal c-comment-leader "  "))
           (setq c-comment-leader "  "))
       (set-buffer-modified-p t))))

;;(setq c-comment-edit-end-hook 'c-comment-edit-end-aux)
;;(defun c-comment-edit-end-aux ()
;;  (if (equal c-comment-leader "  ")
;;      (save-excursion
;;	  (if (search-backward "/*" (point-min) t)
;;	      (progn
;;		(forward-char 2)
;;		(my-delete-vertical-whitespace)
;;		(if (search-forward "*/" (point-max) t)
;;		    (progn
;;		      (progn
;;			(forward-char -3)
;;			(my-delete-vertical-whitespace)))))))))

(cl-defun my-cc-process-params (align-type)

  (when (not (cl-member align-type '(align unalign)))
    (error "unrecognized alignment type"))

  ;; I could use cl-flet here, but I don't like the way emacs formats
  ;; things when using cl-flet. Also, I avoid and extra level of
  ;; indentation by not defining get-end as a local function using
  ;; cl-flet. On the other hand, this requires using funcall...
  (let* ((get-end #'(lambda (&optional backup-required)
                      (save-excursion
                        (when backup-required
                          (backward-up-list 1))
                        (forward-list)
                        (point))))
         (align (eq align-type 'align))
         (start
          (let ((current (point)))
            (save-excursion
              ;; `backward-up-list' won't find a balanced list if
              ;; point is on the parenthesis or brace marking the
              ;; list's beginning, but will if we're on the
              ;; parenthesis or brace marking its end. This tweak just
              ;; ensures the behaviour we see when requesting
              ;; alignment is uniform whether we're at the very start
              ;; (or end) of a list when we request alignment of that
              ;; list
              (when (looking-at "[({]")
                (forward-char 1))
              ;; starting at the current point, loop backward one
              ;; space at a time, checking each time to see if we're
              ;; in a valid balanaced expression. This way, if we're
              ;; currently in a string or a comment which is itself
              ;; contained within a balanced expression, this will
              ;; enable us to wiggle our way out of the comment or
              ;; string and find the beginning and end of the
              ;; containing expression
              (cl-loop
               (condition-case nil
                   (progn
                     (backward-up-list 1)
                     ;; we've found a balanced expression
                     (let ((end (funcall get-end)))
                       ;; but only return its start point if our
                       ;; current point lies between that start point
                       ;; and the expression's end point. This way we
                       ;; ensure we haven't searched backward so far
                       ;; that we've found a balanced expression which
                       ;; entirely precedes our current point
                       (cl-return (if (> end current) (point) nil))))
                 (error
                  (when (bobp)
                    (cl-return nil))
                  (forward-char -1)))))))
         end)

    (when (not start)
      (error "sorry, can't find balanced expression to %salign"
             (if align "" "un")))

    (save-excursion

      (goto-char start)

      ;; highlight region to be aligned or unaligned and prompt user
      ;; whether to continue with alignment or unalignment
      (when (not (my-overlay-and-prompt
                  (+ (point) 1) (- (funcall get-end) 1)
                  (concat (when (not align) "un")
                          "align region? ")))
        (cl-return-from my-cc-process-params))

      ;; deal with whitespace between opening parenthesis and first
      ;; argument
      (when (looking-at "[({]\\(\\(\n\\|\\s-\\)*\\)")
        (replace-match "" nil nil nil 1)
        (forward-char -1)
        (when (looking-at "{")
          (forward-char 1)
          (insert (if align "\n" " "))
          (indent-for-tab-command)
          (forward-char -1)))

      (forward-char)
      (setq end (funcall get-end t))

      (while (not (= (point) end))

        (cond

         ;; forward over nested parenthetical expressions
         ((looking-at "[({]")
          (forward-list))

         ;; forward over strings
         ((looking-at "[\"']")
          (forward-sexp)
          ;; and deal with adjoining strings as if they were separate
          ;; arguments
          (when (looking-at "\\(\\(\n\\|\\s-\\)*\\)[\"']")
            (replace-match (if align "\n" "") nil nil nil 1)
            (indent-for-tab-command)
            (setq end (funcall get-end t))))

         ;; deal with whitespace and comma between parameters
         ((looking-at "\\(\\(\n\\|\\s-\\)*,\\(\n\\|\\s-\\)*\\)")
          (replace-match (if align ",\n" ", ") nil nil nil 1)
          (indent-for-tab-command)
          (setq end (funcall get-end t)))

         ;; deal with whitespace between final param and closing
         ;; parenthesis
         ((looking-at "\\(\\(\n\\|\\s-\\)*\\)[)}]")
          (replace-match "" nil nil nil 1)
          (when (looking-at "}")
            (insert (if align "\n" " "))
            (indent-for-tab-command))
          (setq end (funcall get-end t))
          (forward-char 1))

         ;; deal with excess whitespace in general
         ((looking-at "\\s-\\(\\s-+\\)")
          (replace-match "" nil nil nil 1)
          (indent-for-tab-command)
          (setq end (funcall get-end t)))

         (t
          ;; `forward-comment' jumps over both whitespace and
          ;; comments. If it encouters just whitespace, it will jump
          ;; over it but at the same time return nil. If it doesn't
          ;; find a comment, we don't want it to jump over any
          ;; whitespace it encounters before deciding there is no
          ;; comment, so we use a `save-excursion'.
          (if (save-excursion (forward-comment 1))
              (forward-comment 1)
            (forward-char 1))))))))

(defun my-cc-unalign-params ()
  (interactive)
  (my-cc-process-params 'unalign))

(defun my-cc-align-params ()
  (interactive)
  (if current-prefix-arg
      (my-cc-unalign-params)
    (my-cc-process-params 'align)))
