(provide 'my-hacks)

(eval-and-compile (require 'ispell))

(defun ispell-complete-word (&optional interior-frag)
  "Try to complete the word before or under point (see `lookup-words').
If optional INTERIOR-FRAG is non-nil then the word may be a character
sequence inside of a word.

Standard ispell choices are then available."
  (interactive "P")
  (let ((cursor-location (point))
        (case-fold-search-val case-fold-search)
        (word (ispell-get-word nil "\\*")) ; force "previous-word" processing.
        start end possibilities replacement)
    (setq start (car (cdr word))
          end (car (cdr (cdr word)))
          word (car word)
          possibilities
          (or (string= word "")		; Will give you every word
              (ispell-lookup-words (concat (and interior-frag "*") word
                                    (if (or interior-frag (null ispell-look-p))
                                        "*"))
                            (or ispell-complete-word-dict
                                ispell-alternate-dictionary))))
    (cond ((eq possibilities t)
           (message "No word to complete"))
          ((null possibilities)
           (message "No match for \"%s\"" word))
          (t				; There is a modification...
           (setq case-fold-search nil)	; Try and respect case of word.
           (cond
            ((string-equal (upcase word) word)
             (setq possibilities (mapcar 'upcase possibilities)))
            ((eq (upcase (aref word 0)) (aref word 0))
             (setq possibilities (mapcar (function
                                          (lambda (pos)
                                            (if (eq (aref word 0) (aref pos 0))
                                                pos
                                              (capitalize pos))))
                                         possibilities))))
           (setq case-fold-search case-fold-search-val)
           (save-window-excursion
             (setq replacement
                   (ispell-command-loop possibilities nil word start end)))
           (cond
            ((equal 0 replacement)	; BUFFER-LOCAL ADDITION
             (ispell-add-per-file-word-list word))
            (replacement		; REPLACEMENT WORD
             (setq word (if (atom replacement) replacement (car replacement))
                   cursor-location (+ (- (length word) (- end start))
                                      cursor-location))
             ;; xxx this needs to follow adjustment to cursor-location,
             ;; since delete-region appears to have side-effects on
             ;; value stored in end variable
             (delete-region start end)
             (insert word)
             (if (not (atom replacement)) ; recheck spelling of replacement.
                 (progn
                   (goto-char cursor-location)
                   (ispell-word nil t)))))
           (if (get-buffer ispell-choices-buffer)
               (kill-buffer ispell-choices-buffer))))
    (ispell-pdict-save ispell-silently-savep)
    (goto-char cursor-location)))
