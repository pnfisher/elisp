(provide 'my-timers)

(defun my-ring-a-ding (type i secs msg noisy)
  (message (concat
            type
            " expired ("
            secs
            ")"
            (if (> (length msg) 0)
                (concat ": " msg))))
  (while (> i 0)
    (if noisy
        (let ((vol (shell-command-to-string
                    "xset q | grep bell | cut -d \' \' -f 6"))
              (resp (shell-command-to-string "xset b on")))
          (ding t)
          (sit-for 0.2)
          (let ((resp (shell-command-to-string (concat "xset b " vol))))))
      (let ((visible-bell t))
        (ding t)
        (sit-for 0.2)))
    (setq i (- i 1))))

(defun my-ring-flash (secs)
  "Setup timer to flash mode line upon expiration. To cancel call
cancel-ring-dings."
  (interactive "sseconds: ")
  (if (equal secs "")
      (error "no seconds specified"))
  (let ((msg (read-string "message: "))
        (i (string-to-number (read-buffer "flashes: " "20"))))
    (run-with-timer
     secs
     nil
     'my-ring-a-ding
     "timer"
     i
     secs
     msg
     nil)))

(defun my-ring-ding (secs)
  "Setup timer to beep upon expiration. To cancel call cancel-ring-dings."
  (interactive "sseconds: ")
  (if (equal secs "")
      (error "no seconds specified"))
  (let ((msg (read-string "message: "))
        (i (string-to-number (read-buffer "beeps: " "10"))))
    (run-with-timer
     secs
     nil
     'my-ring-a-ding
     "timer"
     i
     secs
     msg
     t)))

(defun my-ring-alarm-bell (i time msg)
  (my-ring-a-ding "alarm" i time msg t))

(defun my-ring-alarm (time)
  "Setup timer to beep upon expiration. To cancel call cancel-alarm-bell."
  (interactive "stime: ")
  (if (equal time "")
      (error "no time specified"))
  (let ((msg (read-string "message: "))
        (i (string-to-number (read-buffer "beeps: " "10"))))
    (run-at-time
     time
     nil
     'my-ring-alarm-bell
     i
     time
     msg)))

(defun my-alarm (time)
  "Setup timer to beep upon expiration. To cancel call cancel-alarm."
  (interactive "stime: ")
  (my-ring-alarm time))

(defun my-timers-left ()
  (interactive)
  (let ((timers timer-list)
        (ring-dings nil)
        (tstr nil))
    (if (y-or-n-p "ring-dings: ")
        (setq ring-dings 'my-ring-a-ding))
    (while (> (length timers) 0)
      (let ((func (aref (car timers) 5))
            (time (timer-until (car timers) (current-time))))
        (if (not ring-dings)
            (setq tstr (concat tstr (format " %d/%S" time func)))
          (if (equal func ring-dings)
              (setq tstr (concat tstr (format " %d" time))))))
      (setq timers (cdr timers)))
    (message (concat (if ring-dings "ring-dings:" "timers:")
                     (or tstr " none")))))

(defun my-cancel-ring-dings ()
  (interactive)
  (cancel-function-timers 'my-ring-a-ding))

(defun my-cancel-ring-alarm ()
  (interactive)
  (cancel-function-timers 'my-ring-alarm-bell))

(defun my-cancel-alarm ()
  (interactive)
  (my-cancel-ring-alarm))
