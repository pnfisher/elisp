(provide 'my-submodules)

(require 'my-globals)
(require 'my-keymaps)

(require 'info)
(require 'color)

(eval-and-compile
  (defconst my-submodules-directory
    (expand-file-name (concat my-elisp-directory "/submodules")))
  (when (not (file-exists-p my-submodules-directory))
    (error (concat my-submodules-directory " directory is missing")))
  ;; all needed by magit
  (add-to-list 'load-path (concat my-submodules-directory "/magit/lisp"))
  (add-to-list 'load-path (concat my-submodules-directory "/dash.el"))
  (add-to-list 'load-path (concat my-submodules-directory "/with-editor")))

(require 'magit)
(require 'my-magit)

(info-initialize)
(add-to-list
 'Info-directory-list
 (concat my-submodules-directory "/magit/Documentation/"))

;; smarttabs
(let ((smartdir (concat my-submodules-directory "/smarttabs")))
  (when (file-exists-p smartdir)
    (add-to-list 'load-path smartdir)
    (autoload 'smart-tabs-mode "smart-tabs-mode"
      "Intelligently indent with tabs, align with spaces!")
    (autoload 'smart-tabs-mode-enable "smart-tabs-mode")
    (autoload 'smart-tabs-advice "smart-tabs-mode")
    (autoload 'smart-tabs-insinuate "smart-tabs-mode")
    (smart-tabs-insinuate
     'c 'c++ 'java 'javascript 'cperl 'python 'ruby 'nxml)))

;; magit-svn
;;(add-to-list 'load-path (concat my-submodules-directory "/magit-svn"))
;;(require 'magit-svn)

;; go-mode
(let ((godir (concat my-submodules-directory "/go-mode.el")))
  (when (file-exists-p godir)
    (add-to-list 'load-path godir)
    (autoload 'go-mode "go-mode" nil t)
    (add-to-list 'auto-mode-alist '("\\.go\\'" . go-mode))
    (add-hook 'go-mode-hook #'(lambda () (font-lock-mode t)))))

;; typescript-mode is javascript-mode
(add-to-list 'auto-mode-alist '("\\.ts\\'" . javascript-mode))

;; markdown-mode
(autoload 'markdown-mode
  (concat my-submodules-directory "/markdown-mode/markdown-mode")
  "Major mode for editing Markdown files" t)
(add-to-list 'auto-mode-alist '("\\.text\\'" . markdown-mode))
(add-to-list 'auto-mode-alist '("\\.markdown\\'" . markdown-mode))
(add-to-list 'auto-mode-alist '("\\.md\\'" . markdown-mode))

;; web-mode
(add-to-list 'load-path (concat my-submodules-directory "/web-mode"))
(require 'my-web-mode)

;; ssh
(add-to-list 'load-path (concat my-submodules-directory "/ssh-el"))
(require 'ssh)

;; google c style guide
(add-to-list 'load-path (concat my-submodules-directory "/styleguide"))
(require 'google-c-style)
(require 'my-cstyle)

;; yasnippet
(eval-and-compile
  (add-to-list 'load-path (concat my-submodules-directory "/yasnippet"))
  (require 'yasnippet))
(yas-global-mode 1)
(my-global-set-key (kbd "C-c y") 'yas-insert-snippet)
(define-key yas-minor-mode-map (kbd "<backtab>") 'yas-expand)
(define-key yas-minor-mode-map [(tab)] nil)
(define-key yas-minor-mode-map (kbd "TAB") nil)
;;(define-key yas-minor-mode-map (kbd "<tab>") nil)

;; helm
(add-to-list 'load-path (concat my-submodules-directory "/emacs-async"))
(add-to-list 'load-path (concat my-submodules-directory "/helm"))
(require 'helm-config)
;;(helm-mode 1)
(add-to-list 'load-path (concat my-submodules-directory "/helm-ls-git"))
(require 'helm-ls-git)
(add-to-list 'load-path (concat my-submodules-directory "/emacs-helm-ag"))
(require 'helm-ag)
(custom-set-variables
 '(helm-ag-always-set-extra-option t))
;; (add-to-list 'load-path (concat my-submodules-directory "/helm-rg"))
;; (require 'helm-rg)
(add-to-list 'load-path (concat my-submodules-directory "/helm-descbinds"))
(require 'helm-descbinds)
(add-to-list 'load-path (concat my-submodules-directory "/emacs-helm-gtags"))
(require 'helm-gtags)
(custom-set-variables
 ;;'(helm-gtags-display-style 'detail)
 ;;'(helm-gtags-use-input-at-cursor t)
 '(helm-gtags-pulse-at-cursor t))

;; psvn
(add-to-list 'load-path (concat my-submodules-directory "/psvn"))
(require 'psvn)
(require 'my-svn)

;; my stuff
(require 'my-helm)

;; ;; dtrt-indent (guess buffer indentation scheme)
;;(add-to-list 'load-path (concat my-submodules-directory "/dtrt-indent"))
;;(require 'dtrt-indent)

;; ;; flycheck
;; (add-to-list 'load-path (concat my-submodules-directory "/dash.el"))
;; (add-to-list 'load-path (concat my-submodules-directory "/seq.el"))
;; (add-to-list 'load-path (concat my-submodules-directory "/flycheck"))
;; (require 'flycheck)

;;
;; using company-mode instead of auto-complete
;;

;; ;; auto-complete
;; (add-to-list 'load-path (concat my-submodules-directory "/auto-complete"))
;; (add-to-list 'load-path (concat my-submodules-directory "/popup-el"))
;; (require 'auto-complete-config)
;; (add-to-list 'ac-dictionary-directories (concat
;;                                          my-submodules-directory
;;                                          "/auto-complete/ac-dict"))
;; (ac-config-default)

;; company-mode
(add-to-list 'load-path (concat my-submodules-directory "/company-mode"))
(require 'company)
;; (add-hook 'after-init-hook 'global-company-mode)
(my-global-set-key (kbd "<C-return>") 'company-complete)
(define-key company-active-map (kbd "C-n") 'company-select-next)
(define-key company-active-map (kbd "C-p") 'company-select-previous)
(define-key company-active-map (kbd "C-j") 'company-show-location)
