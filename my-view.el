(provide 'my-view)

(require 'view)

(custom-set-variables
 '(view-read-only t))

;; my hack of original view-search (eliminates the paging aspect of
;; the original version)
(defun view-search (times regexp)
  ;; This function does the job for all the View-search- commands.
  ;; Search for the TIMESth match for REGEXP.  If TIMES is negative
  ;; search backwards.  If REGEXP is nil use `view-last-regexp'.
  ;; Characters "!" and "@" have a special meaning at the beginning of
  ;; REGEXP and are removed from REGEXP before the search "!" means
  ;; search for lines with no match for REGEXP.  "@" means search in
  ;; the whole buffer, don't start searching from the present point.
  (let (where no end ln)
    (cond
     ((and regexp (> (length regexp) 0)
           (or (not (memq (string-to-char regexp) '(?! ?@)))
               (progn
                 (if (member (substring regexp 0 2) '("!@" "@!"))
                     (setq end t no t ln 2)
                   (setq no (not (setq end (eq ?@ (string-to-char regexp))))
                         ln 1))
                 (> (length (setq regexp (substring regexp ln))) 0))))
      (setq view-last-regexp (if no (list regexp) regexp)))
     ((consp view-last-regexp)
      (setq regexp (car view-last-regexp))
      (unless (setq no (not no)) (setq view-last-regexp regexp)))
     (view-last-regexp (setq regexp view-last-regexp)
                       (if no (setq view-last-regexp (list regexp))))
     (t (error "No previous View-mode search")))
    (save-excursion
      (if end
          (goto-char (if (< times 0) (point-max) (point-min)))
        ;; xxx I commented out this
        ;;(move-to-window-line (if (< times 0) 0 -1)))
        )
      (if (if no (view-search-no-match-lines times regexp)
            (re-search-forward regexp nil t times))
          (setq where (point))))
    (if where
        (progn
          (push-mark)
          (goto-char where)
          (if view-overlay
              (move-overlay view-overlay (match-beginning 0) (match-end 0))
            (setq view-overlay
                  (make-overlay (match-beginning 0) (match-end 0))))
          (overlay-put view-overlay 'face view-highlight-face)
          ;; xxx I commented out this
          ;;(beginning-of-line)
          (view-recenter))
      (message "Can't find occurrence %d of %s%s"
               times (if no "no " "") regexp)
      (sit-for 4))))
