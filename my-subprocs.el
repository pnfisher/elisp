(provide 'my-subprocs)

(require 'my-globals)
(require 'my-utils)

(defconst my-emacs-binary (my-shell-command-to-string "which emacs"))

(defun my-devhelp ()
  (interactive)
  (when (not (= (shell-command "which devhelp >& /dev/null") 0))
    (error "devhelp missing"))
  (call-process "devhelp" nil 0 nil))

(defun my-htop ()
  (interactive)
  (when (not (and (= (shell-command "which xterm >& /dev/null") 0)
                  (= (shell-command "which htop >& /dev/null") 0)))
    (error "xterm or htop missing"))
  (call-process "xterm" nil 0 nil "-maximized" "-e" "htop"))
(defalias 'htop 'my-htop)

(defun my-emacs (host)
  (interactive (list (read-buffer "host: " my-hostname)))
  (when (or (string-equal host "tp")
            (string-equal host "TP"))
    (setq host "TP-20141493"))
  (let ((title (if (string-match "@" host)
                   host
                 (concat my-username "@" host))))
    (if (equal host my-hostname)
        (call-process my-emacs-binary nil 0 nil "-title" title)
      ;; (if (= (shell-command "which xrdb >& /dev/null") 0)
      ;;     (let ((xrdb (my-shell-command-to-string "which xrdb")))
      ;;       (call-process
      ;;        xrdb
      ;;        nil 0 nil
      ;;        "-load"
      ;;        (concat my-home-directory "/.Xdefaults"))))
      (call-process
       "ssh"
       nil 0 nil
       "-o"
       "compression=no"
       "-f"
       host
       (concat
        ". "
        my-home-directory
        "/.profile; "
        "test -e "
        default-directory
        " && cd "
        default-directory
        "; "
        (concat "X11CLIENT=" my-hostname " emacs -title " title)))
      (message (concat "starting emacs on " host)))))
(defalias 'emacs 'my-emacs)

(defun my-chrome (url)
  (interactive (list
                (if (eq major-mode 'dired-mode)
                    (concat
                     "file://"
                     (expand-file-name default-directory)
                     "/"
                     (thing-at-point 'filename))
                  (thing-at-point 'url))))
  (if (not (and url (y-or-n-p (concat url " "))))
      (setq url nil))
  (cond
   ((file-exists-p "/usr/bin/google-chrome")
    (if (null url)
        (call-process "google-chrome" nil 0 nil)
      (call-process "google-chrome" nil 0 nil "--new-window" url)))
   ((file-exists-p "/usr/bin/chromium-browser")
    (if (null url)
        (call-process "chromium-browser" nil 0 nil)
      (call-process "chromium-browser" nil 0 nil "--new-window"  url)))
   (t
    (error "can't find chrome browswer binary"))))

(defconst my-firefox-binary "firefox")

(defun my-firefox (url)
  (interactive (list
                (if (eq major-mode 'dired-mode)
                    (concat
                     "file://"
                     (expand-file-name default-directory)
                     "/"
                     (thing-at-point 'filename))
                  (thing-at-point 'url))))
  (if (not (and url (y-or-n-p (concat url " "))))
      (setq url nil))
  (if (file-exists-p "/usr/bin/firefox")
      (if (null url)
          (call-process my-firefox-binary nil 0 nil)
        (call-process my-firefox-binary nil 0 nil url))
    (let ((host (read-buffer "ssh host: ")))
      (message (concat "using ssh to call firefox on " host))
      (call-process
       "ssh"
       nil 0 nil
       "-o"
       "compression=no"
       "-f"
       host
       (concat
        ". "
        my-home-directory
        "/.profile; "
        my-firefox-binary
        " "
        url)))))
(defalias 'ff 'my-firefox)

(defun my-google (terms)
  (interactive
   (list
    (replace-regexp-in-string
     "\\(^\\s-+\\|\\s-+$\\)"
     ""
     (read-from-minibuffer
      "search terms: "
      (thing-at-point 'symbol)
      nil
      nil))))
  (when (string-equal terms "")
    (error "no search terms"))
  (call-process
   my-firefox-binary
   nil
   0
   nil
   (concat
    "http://www.google.ca/search?q="
    (replace-regexp-in-string "\\s-+" "\+" terms))))

(defun my-www (url)
  (interactive (list
                (if (eq major-mode 'dired-mode)
                    (concat
                     "file://"
                     (expand-file-name default-directory)
                     "/"
                     (thing-at-point 'filename))
                  (thing-at-point 'url))))
  (when (not url)
    (error "no url at point"))
  (if (y-or-n-p (concat url " "))
      (call-process my-firefox-binary nil 0 nil url)
    (message "skipping %s" url)))

(defun my-root ()
  (interactive)
  (if (and (file-exists-p "/usr/bin/gksu")
           (not current-prefix-arg))
      (call-process
       "gksu" nil 0 nil "-k"
       (concat
        "PATH="
        (getenv "PATH")
        " "
        my-emacs-binary " -title root@" my-hostname))
    (when (not current-prefix-arg)
      (message "gksu missing; running sudo -i in xterm instead")
      (sleep-for 2))
    (call-process
     "xterm" nil 0 nil
     "-T" "sudo -i"
     "-geometry" "80x55"
     "-e"
     "bash -c 'sudo -i'")))
