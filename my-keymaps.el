(provide 'my-keymaps)

(require 'cl-lib)
(require 'help-macro)

;; for use in my-helm.el
(defconst my-keymaps-prefix-regexp " \\(^C-c\\s-\\|f[0-9]+>\\) ")
(defconst my-key-binding-header
  (concat
   "key             binding\n"
   "---             -------\n\n"))
(defconst my-funckeys-header
  "List of my function key bindings\n\n")
(defconst my-global-setkeys-header
  "List of my global set key bindings\n\n")
(defconst my-keymaps-header
  "List of my custom keymap bindings\n\n")
(defvar my-keymaps-help-screens-alist nil)
(defvar my-global-set-keys nil)
(defvar my-keymaps-alist nil)

(defun my-global-set-key (key func)
  (global-set-key key func)
  (push (cons key func) my-global-set-keys))

(defun my-keymaps-make-help-screen (help-screen doc keymap-sym)
  (eval
   `(make-help-screen
     ,help-screen
     nil
     ,(concat
       doc
       "\n\n"
       (mapconcat #'(lambda (entry)
                      (let ((key (car entry))
                            (func (cdr entry))
                            prefix)
                        (when (not (symbolp func))
                          (setq prefix key
                                entry (cl-caddr entry)
                                key (car entry)
                                func (cdr entry)))
                        (cl-assert (symbolp func))
                        (format "%-16s%s"
                                (concat (when prefix
                                          (concat
                                           (key-description (list prefix))
                                           "-"))
                                        (key-description (list key)))
                                func)))
                  (reverse (cdr (symbol-value keymap-sym)))
                  "\n"))
     ,keymap-sym)))

(defun my-keymaps-make-keymap (keymap-sym doc keymap key)
  (let ((help-screen (intern (format "%s-help-screen" keymap-sym))))
    (define-key keymap (kbd "<RET>") help-screen)
    (eval `(defvar ,keymap-sym keymap))
    (global-set-key key (symbol-value keymap-sym))
    (my-keymaps-make-help-screen help-screen doc keymap-sym)
    (add-to-list 'my-keymaps-alist (cons key keymap-sym))
    (add-to-list 'my-keymaps-help-screens-alist (cons key help-screen))))

(my-keymaps-make-keymap
 'my-keymaps-description-commands-keymap
 "Help screen for listing my custom key bindings"
 (let ((map (make-sparse-keymap)))
   (define-key map (kbd "a") 'my-descbinds)
   (define-key map (kbd "d") 'my-descbinds)
   (define-key map (kbd "f") 'my-funckeys)
   (define-key map (kbd "g") 'my-global-setkeys)
   (define-key map (kbd "h") 'my-helm-describe-bindings-custom)
   (define-key map (kbd "k") 'my-keymaps)
   map)
 (kbd "C-c C-k"))

(my-keymaps-make-keymap
 'my-keymaps-ag-commands-keymap
 "Help screen for ag commands"
 (let ((map (make-sparse-keymap)))
   (define-key map (kbd "A") 'helm-do-ag)
   (define-key map (kbd "a") 'helm-ag)
   (define-key map (kbd "B") 'helm-ag-buffers)
   (define-key map (kbd "b") 'helm-do-ag-buffers)
   (define-key map (kbd "h") 'my-helm-do-grep-ag)
   (define-key map (kbd "m") 'my-ag)
   (define-key map (kbd "P") 'helm-ag-project-root)
   (define-key map (kbd "p") 'helm-do-ag-project-root)
   map)
 (kbd "C-c a"))

(my-keymaps-make-keymap
 'my-buffer-commands-keymap
 "Help screen for buffer manipulation commands"
 (let ((map (make-sparse-keymap)))
   (define-key map (kbd "a") 'my-async-shell-command-output-other-window)
   (define-key map (kbd "b") 'bury-buffer)
   (define-key map (kbd "i") 'ibuffer)
   (define-key map (kbd "k") 'my-kill-buffer-other-window)
   (define-key map (kbd "m") 'buffer-menu)
   (define-key map (kbd "o") 'my-open-compile-window)
   (define-key map (kbd "R") 'my-rotate-window-buffers-forward)
   (define-key map (kbd "r") 'rename-buffer)
   (define-key map (kbd "s") 'my-shell-command-output-other-window)
   (define-key map (kbd "u") 'unbury-buffer)
   map)
 (kbd "C-c b"))

(my-keymaps-make-keymap
 'my-edit-commands-keymap
 "Help screen for file editing commands"
 (let ((map (make-sparse-keymap)))
   (define-key map (kbd "b") 'delete-blank-lines)
   (define-key map (kbd "c") 'my-check-spaces)
   (define-key map (kbd "g") 'goto-line)
   (define-key map (kbd "j") 'just-one-space)
   (define-key map (kbd "K") 'my-kill-line-backward)
   (define-key map (kbd "k") 'kill-word)
   (define-key map (kbd "l") 'my-copy-line)
   (define-key map (kbd "n") 'my-delete-until-next-text)
   (define-key map (kbd "p") 'my-delete-until-previous-text)
   (define-key map (kbd "T") 'delete-trailing-whitespace)
   (define-key map (kbd "t") 'my-set-tab-width)
   (define-key map (kbd "W") 'my-delete-all-whitespace)
   (define-key map (kbd "w") 'my-delete-all-whitespace-but-one)
   (define-key map (kbd "x") 'my-delete-region)
   (define-key map (kbd "^") 'delete-indentation)
   (define-key map (kbd "C-q") 'my-prog-tidy-function)
   (define-key map (kbd "M-q") 'my-prog-tidy-region)
   (define-key map (kbd "\\") 'delete-horizontal-space)
   map)
 (kbd "C-c e"))

(my-keymaps-make-keymap
 'my-git-commands-keymap
 "Help screen for git and magit commands"
  (let ((map (make-sparse-keymap)))
    (define-key map (kbd "E") 'magit-ediff-compare)
    (define-key map (kbd "e") 'my-magit-ediff-current-buffer)
    (define-key map (kbd "G") 'helm-grep-do-git-grep)
    (define-key map (kbd "g") 'my-magit-git-grep)
    (define-key map (kbd "h") 'helm-ls-git-ls)
    (define-key map (kbd "l") 'my-magit-log-all)
    (define-key map (kbd "S") 'magit-status)
    (define-key map (kbd "s") 'my-magit-file-status-display)
    (define-key map (kbd "v") 'my-magit-visit-file)
    map)
  (kbd "C-c g"))

(my-keymaps-make-keymap
 'my-helm-command-keymap
 "Help screen for my favourite helm commands"
 (let ((map (make-sparse-keymap)))
   (define-key map (kbd "a") 'my-helm-do-grep-ag)
   (define-key map (kbd "b") 'helm-browse-project)
   (define-key map (kbd "c") 'helm-colors)
   (define-key map (kbd "d") 'helm-documentation)
   ;; this is bound to "C-h b"
   ;; (define-key map (kbd "D") 'helm-descbinds)
   (define-key map (kbd "F") 'my-helm-find-function)
   (define-key map (kbd "f") 'helm-find-files)
   (define-key map (kbd "G") 'helm-gid)
   (define-key map (kbd "g") 'helm-grep-do-git-grep)
   (define-key map (kbd "i") 'helm-info-emacs)
   (define-key map (kbd "I") 'helm-info-at-point)
   (define-key map (kbd "K") 'my-helm-describe-bindings-custom)
   (define-key map (kbd "k") 'my-helm-describe-bindings)
   (define-key map (kbd "l") 'helm-ls-git-ls)
   (define-key map (kbd "m") 'helm-man-woman)
   (define-key map (kbd "P") 'helm-list-emacs-process)
   (define-key map (kbd "p") 'helm-apropos)
   (define-key map (kbd "r") 'helm-regexp)
   (define-key map (kbd "R") 'helm-register)
   (define-key map (kbd "t") 'helm-top)
   (define-key map (kbd "V") 'my-helm-find-variable)
   (define-key map (kbd ".") 'helm-etags-select)
   (define-key map (kbd "/") 'helm-find)
   (define-key map (kbd ",") 'helm-lisp-completion-at-point)
   map)
 (kbd "C-c h"))

(my-keymaps-make-keymap
 'my-misc-commands-keymap
 "Help screen for miscellaneous commands"
 (let ((map (make-sparse-keymap)))
   (define-key map (kbd "D") 'desktop-read)
   (define-key map (kbd "d") 'desktop-save)
   (define-key map (kbd "f") 'my-funckeys)
   (define-key map (kbd "g") 'goto-line)
   (define-key map (kbd "j") 'my-append-to-junk)
   (define-key map (kbd "m") 'man)
   (define-key map (kbd "p") 'find-file-at-point)
   (define-key map (kbd "R") 'remember-notes)
   (define-key map (kbd "r") 'remember)
   (define-key map (kbd "S") 'ssh)
   (define-key map (kbd "s") 'shell)
   (define-key map (kbd "T") 'my-set-tab-width)
   (define-key map (kbd "t") 'my-tramp-host)
   map)
 (kbd "C-c m"))

(my-keymaps-make-keymap
 'my-ediff-commands-keymap
 "Help screen for ediff commands"
 (let ((map (make-sparse-keymap)))
   (define-key map (kbd "b") 'ediff-buffers)
   (define-key map (kbd "h") 'my-ediff-host)
   (define-key map (kbd "E") 'my-ediff-vc-rev)
   (define-key map (kbd "e") 'my-ediff-vc)
   map)
 (kbd "C-c q"))

(my-keymaps-make-keymap
 'my-keymaps-elisp-commands-keymap
 "Help screen for emacs lisp commands"
 (let ((map (make-sparse-keymap)))
   (define-key map (kbd "e") 'my-elisp)
   (define-key map (kbd "f") 'my-helm-find-function)
   (define-key map (kbd "g") 'my-elisp-grep)
   (define-key map (kbd "i") 'my-elisp-insert)
   (define-key map (kbd "P") 'my-elisp-pp-eval-last-sexp)
   (define-key map (kbd "p") 'my-elisp-find-symbol-at-point)
   (define-key map (kbd "S") 'my-scratch-insert)
   (define-key map (kbd "s") 'my-scratch)
   (define-key map (kbd "v") 'my-helm-find-variable)
   (define-key map (kbd "z") 'my-elisp-zrgrep)
   (define-key map (kbd ".") 'my-elisp-find-symbol-at-point)
   map)
 (kbd "C-c x"))

(my-keymaps-make-keymap
 'my-file-search-commands-keymap
 "Help screen for file searching commands"
 (let ((map (make-sparse-keymap)))
   (define-key map (kbd "a") 'my-ag)
   (define-key map (kbd "c") 'my-cscope)
   (define-key map (kbd "D") 'find-dired)
   (define-key map (kbd "d") 'find-name-dired)
   (define-key map (kbd "E") 'my-elisp-zrgrep)
   (define-key map (kbd "e") 'my-elisp-grep)
   (define-key map (kbd "F") 'grep-find)
   (define-key map (kbd "f") 'find-grep-dired)
   (define-key map (kbd "G") 'grep)
   (define-key map (kbd "g") 'my-magit-git-grep)
   (define-key map (kbd "h") 'my-helm-do-grep-ag)
   (define-key map (kbd "I") 'my-fnid)
   (define-key map (kbd "i") 'my-gid)
   (define-key map (kbd "l") 'my-glimpse)
   ;; (define-key map (kbd "P") 'helm-rg)
   (define-key map (kbd "p") 'my-rg)
   (define-key map (kbd "R") 'rgrep)
   (define-key map (kbd "r") 'my-rgrep)
   (define-key map (kbd "T") 'my-gtags-cscope)
   (define-key map (kbd "t") 'my-gtags)
   (define-key map (kbd "z") 'my-zrgrep)
   (define-key map (kbd ".") 'my-reset-tools-id)
   map)
 (kbd "C-c z"))

(my-keymaps-make-keymap
 'my-keymaps-keymap
 "Help screens available for my keymaps"
 (let ((map (make-sparse-keymap)))
   (dolist (entry (cl-sort
                   my-keymaps-help-screens-alist
                   #'string-lessp
                   :key #'car))
     (define-key map (kbd (substring (car entry) -1)) (cdr entry)))
   map)
 (kbd "C-c u"))

(my-keymaps-make-help-screen
 'ctl-x-r-map-help-screen
 "My custom help screen for the ctl-x-r-map keymap"
 'ctl-x-r-map)
(define-key ctl-x-r-map (kbd "<RET>") 'ctl-x-r-map-help-screen)

(defun my-keymaps-description ()
  (let (description)
    ;; process all my keymaps (sorted in REVERSE alphabetical order
    ;; based on prefix key because the `push' call below will reverse
    ;; our reverse sorted order and put the final list into
    ;; alphabetical order)
    (dolist (alist-entry (cl-sort
                          my-keymaps-alist
                          ;; reverse alphabetical order intended (see
                          ;; above)
                          #'(lambda (prefix1 prefix2)
                              (not (string-lessp prefix1 prefix2)))
                          :key #'(lambda (alist-entry)
                                   (key-description (car alist-entry)))))
      ;; process each entry in the current keymap. The cdr of each
      ;; alist-entry is a keymap variable name. The symbol-value of
      ;; that variable gives its contents, and the cdr of the contents
      ;; gives the gives us a list of each keymap-entry
      (dolist (keymap-entry (cdr (symbol-value (cdr alist-entry))))
        (let ((key (car keymap-entry))
              (func (cdr keymap-entry))
              prefix)
          (when (not (symbolp func))
            (setq prefix key
                  keymap-entry (cl-caddr keymap-entry)
                  key (car keymap-entry)
                  func (cdr keymap-entry)))
          (cl-assert (symbolp func))
          (unless (equal (kbd "<RET>") (char-to-string key))
            (push
             (format "%-16s%s"
                     (concat
                      (key-description (car alist-entry))
                      " "
                      (concat (when prefix
                                (concat
                                 (key-description (list prefix))
                                 "-"))
                              (key-description (list key))))
                     func)
             description)))))
    (mapconcat #'identity description "\n")))

(defun my-keymaps ()
  (interactive)
  (let ((help-window-select t))
    (help-setup-xref
     (list #'my-keymaps)
     (called-interactively-p 'interactive))
    (with-help-window (help-buffer)
      (with-current-buffer standard-output
        (erase-buffer)
        (insert my-keymaps-header)
        (insert my-key-binding-header)
        (insert (my-keymaps-description))
        (goto-char (point-min))
        (view-mode)))))

(defun my-global-setkeys-description ()
  (mapconcat
   #'identity
   (let (key-func-strings)
     (dolist (keymap my-global-set-keys key-func-strings)
       (let ((key-desc (key-description (car keymap))))
         (unless (string-match "\\(^<remap>\\|f[0-9]+\\)" key-desc)
           (push
            (format "%-16s%s" key-desc (cdr keymap))
            key-func-strings))))
     (sort key-func-strings #'string-lessp))
   "\n"))

(defun my-global-setkeys ()
  (interactive)
  (let ((help-window-select t))
    (help-setup-xref
     (list #'my-global-setkeys)
     (called-interactively-p 'interactive))
    (with-help-window (help-buffer)
      (with-current-buffer standard-output
        (erase-buffer)
        (insert my-global-setkeys-header)
        (insert my-key-binding-header)
        (insert (my-global-setkeys-description))
        (goto-char (point-min))
        (view-mode)))))

;; both my-funckeys-description and my-funckeys can't go in
;; my-funckeys.el, because my-funckeys.el has to require this file for
;; its calls to my-global-set-key, and if these functions were in
;; my-funckeys.el, then this file would have to require that file in
;; order for this file to call my-funckeys-description in my-descbinds
(defun my-funckeys-description ()
  (let (key-settings)
    (dolist (keymap my-global-set-keys) ;; or (cddr global-map) if you
                                        ;; want all function key
                                        ;; bindings
      (let ((key (format "%s" (car keymap)))
            (func (format "%s" (cdr keymap)))
            (case-fold-search nil))
        (when (string-match "f[0-9]+" key)
          (push (cons key func) key-settings))))
    (mapconcat
     #'(lambda (key-setting) (format "%-16s%s"
                                     (car key-setting)
                                     (cdr key-setting)))
     (cl-sort
      key-settings
      #'(lambda (f1 f2)
          (cond ((= (length f1) (length f2)) (string-lessp f1 f2))
                ((< (length f1) (length f2)) t)
                (t nil)))
      :key #'(lambda (key-setting)
               (let ((key (car key-setting)))
                 (string-match "\\(f[0-9]+\\)" key)
                 (match-string 1 key))))
     "\n")))

(defun my-funckeys ()
  (interactive)
  (let ((help-window-select t))
    (help-setup-xref
     (list #'my-funckeys)
     (called-interactively-p 'interactive))
    (with-help-window (help-buffer)
      (with-current-buffer standard-output
        (erase-buffer)
        (insert my-funckeys-header)
        (insert my-key-binding-header)
        (insert (my-funckeys-description))
        (goto-char (point-min))
        (view-mode)))))

(defun my-descbinds ()
  (interactive)
  (let ((help-window-select t))
    (help-setup-xref
     (list #'my-descbinds)
     (called-interactively-p 'interactive))
    (with-help-window (help-buffer)
      (with-current-buffer standard-output
        (erase-buffer)
        (insert my-keymaps-header)
        (insert my-key-binding-header)
        (insert (my-keymaps-description))
        (insert "\n\n")
        (insert my-global-setkeys-header)
        (insert my-key-binding-header)
        (insert (my-global-setkeys-description))
        (insert "\n\n")
        (insert my-funckeys-header)
        (insert my-key-binding-header)
        (insert (my-funckeys-description))
        (goto-char (point-min))
        (view-mode)))))
