;; wanderlust
(let* ((wl-dir "/usr/share/emacs/site-lisp/wl")
       (wl-wl-dir (concat wl-dir "/wl"))
       (wl-elmo-dir (concat wl-dir "/elmo")))
  (when (and (file-exists-p wl-wl-dir)
             (file-exists-p wl-elmo-dir))
    (add-to-list 'load-path wl-dir)
    (add-to-list 'load-path wl-wl-dir)
    (add-to-list 'load-path wl-elmo-dir)
    (autoload 'wl "wl" "wanderlust" t)
    (autoload 'wl-other-frame "wl" "Wanderlust on new frame." t)
    (autoload 'wl-draft "wl-draft" "Write draft with Wanderlust." t)))

;; IMAP

;;(require 'elmo-search)
(setq elmo-imap4-default-server "imap.gmail.com")
(setq elmo-imap4-default-user "philipnfisher@gmail.com")
(setq elmo-imap4-default-authenticate-type 'clear)
(setq elmo-imap4-default-port '993)
(setq elmo-imap4-default-stream-type 'ssl)
(setq elmo-imap4-use-modified-utf7 t)


;; network

(setq elmo-network-session-idle-timeout 300)


;; SMTP

(setq wl-smtp-connection-type 'starttls)
(setq wl-smtp-posting-port 587)
(setq wl-smtp-authenticate-type "plain")
(setq wl-smtp-posting-user "philipnfisher")
(setq wl-smtp-posting-server "smtp.gmail.com")
(setq wl-local-domain "gmail.com")
(setq wl-from "Philip Fisher <philipnfisher@gmail.com>")


;; headers

(setq
 wl-message-ignored-field-list '("^.*:")
 wl-message-visible-field-list '("^\\(To\\|Cc\\):"
                                 "^Subject:"
                                 "^\\(From\\|Reply-To\\):"
                                 "^Organization:"
                                 ;;"^Message-Id:"
                                 "^\\(Posted\\|Date\\):"
                                 ;;"^[xX]-[Ff]ace:"
                                 )
 wl-message-sort-field-list '("^From"
                              "^Organization:"
                              "^X-Attribution:"
                              "^Subject"
                              "^Date"
                              "^To"
                              "^Cc")
 )


;; user-agents

;;(setq wl-generate-mailer-string-function
;;      (function
;;       (lambda ()
;;         (concat "User-Agent: "
;;                 (wl-generate-user-agent-string-1 nil)))))
(setq mime-edit-insert-user-agent-field nil)
(setq wl-generate-mailer-string-function
      (function
       (lambda ()
         (format "X-Mailer: %s" (product-string-1 'wl-version)))))


;; folders

(setq wl-default-folder "%inbox")
;; auto completing folder names
(setq wl-default-spec "%")
(setq wl-draft-folder "%[Gmail]/Drafts") ;; Gmail IMAP
(setq wl-trash-folder "%[Gmail]/Trash")
;;(setq wl-fcc          "%[Gmail]/Sent Mail")

;; mark sent messages as read (sent messages get sent back to you and
;; placed in the folder specified by wl-fcc)
(setq wl-fcc-force-as-read t)
;;(setq wl-folder-check-async t)


;; emacs mail agent

;; so we can send mail using C-x m
(autoload 'wl-user-agent-compose "wl-draft" nil t)
(if (boundp 'mail-user-agent)
    (setq mail-user-agent 'wl-user-agent))
(if (fboundp 'define-mail-user-agent)
    (define-mail-user-agent
      'wl-user-agent
      'wl-user-agent-compose
      'wl-draft-send
      'wl-draft-kill
      'mail-send-hook))


;; notifications

(setq wl-biff-check-interval 60)
;;(setq wl-biff-check-folder-list '("%inbox"))
(setq wl-biff-check-folder-list nil)

;;(setq wl-biff-check-folder-list
;;      '(
;;        "-gmane.comp.version-control.git.magit@news.gmane.org"
;;        "-gmane.comp.sysutils.systemd.devel@news.gmane.org"
;;        "-gmane.mail.wanderlust.general@news.gmane.org"
;;        )
;;      )

;;;; Set mail-icon to be shown universally in the modeline.
;;(setq global-mode-string
;;      (cons
;;       '(wl-modeline-biff-status
;;         wl-modeline-biff-state-on
;;         wl-modeline-biff-state-off)
;;       global-mode-string))


;; reply stuff

(setq wl-subscribed-mailing-list
      '("systemd-devel@lists.freedesktop.org"))

(setq wl-draft-reply-buffer-style 'keep)

;; original default
(when 0
  (setq wl-draft-reply-without-argument-list
        '(
          ("Followup-To"
           ("Mail-Followup-To" "Mail-Reply-To" "Reply-To")
           nil
           ("Followup-To"))
          ("Mail-Followup-To"
           ("Mail-Followup-To")
           nil nil)
          ("Newsgroups"
           ("Mail-Reply-To" "Reply-To" "To")
           ("Cc")
           ("Newsgroups"))
          ("Mail-Reply-To"
           ("Mail-Reply-To" "Reply-To")
           ("To" "Cc")
           nil)
          ("Reply-To"
           ("Reply-To")
           ("To" "Cc")
           nil)
          (wl-draft-self-reply-p
           ("To")
           ("Cc")
           nil)
          ("From"
           ("From")
           ("To" "Cc")
           nil)
          )
        )
  )

;; took out newsgroups
(setq wl-draft-reply-without-argument-list
      '(
        ("Followup-To"
         ("Mail-Followup-To" "Mail-Reply-To" "Reply-To")
         nil
         ("Followup-To"))
        ("Mail-Followup-To"
         ("Mail-Followup-To")
         nil nil)
        ("Mail-Reply-To"
         ("Mail-Reply-To" "Reply-To")
         ("To" "Cc")
         nil)
        ("Reply-To"
         ("Reply-To")
         ("To" "Cc")
         nil)
        (wl-draft-self-reply-p
         ("To")
         ("Cc")
         nil)
        ("From"
         ("From")
         ("To" "Cc")
         nil)
        )
      )

;;(setq wl-draft-reply-without-argument-list
;;      '(("Mail-Followup-To" . (("Mail-Followup-To") nil ("Newsgroups")))
;;        ("Followup-To" . (nil nil ("Followup-To")))
;;        (("X-ML-Name" "Reply-To") . (("Reply-To") nil nil))
;;        ("From" . (("From") ("To" "Cc") ("Newsgroups")))))

;;(setq wl-draft-reply-without-argument-list
;;      '(
;;        ;;("Followup-To" .
;;         ;;(("Mail-Followup-To" "Mail-Reply-To" "Reply-To")
;;         ;;nil
;;         ;;("Followup-To")))
;;        ("Followup-To" .
;;         (("Mail-Followup-To" "Mail-Reply-To" "Reply-To") nil nil))
;;        ("Mail-Followup-To" .
;;         (("Mail-Followup-To") nil nil))
;;        ;;("Newsgroups" .
;;         ;;(("Mail-Reply-To" "Reply-To" "To") ("Newsgroups")))
;;        ("Mail-Reply-To" .
;;         (("Mail-Reply-To" "Reply-To") nil nil))
;;        ("Reply-To" .
;;         (("Reply-To") nil nil))
;;        (wl-draft-self-reply-p . (("To") nil))
;;        ("From" . (("From") nil nil))
;;        )
;;      )


;; threading

(setq wl-thread-indent-level 2
      wl-thread-have-younger-brother-str "+"
      wl-thread-youngest-child-str       "+"
      wl-thread-vertical-str             "|"
      wl-thread-horizontal-str           "-"
      wl-thread-space-str                " ")


;; misc control

;;Only save draft when I tell it to (C-x C-s or C-c C-s):
(setq wl-auto-save-drafts-interval nil)
(setq wl-demo-display-logo "ascii")
;; sorry, but I don't like the green background
(defadvice wl-demo-setup-properties
  (around wl-demo-setup-properties-advice activate compile)
  nil)
