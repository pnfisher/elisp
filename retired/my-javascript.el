(provide 'my-javascript)

(require 'js-comint)

;; using io.js node command; see node_repl.js for dependencies and
;; their setup
(let ((homedir (getenv "HOME")))
  (setq
   inferior-js-program-command
   (concat "env NODE_PATH="
           homedir "/opt/io.js/lib/node_modules "
           "node "
           homedir "/src/elisp/node_repl.js")))

;; node_repl.js expects an commandline argument specifying directory
;; to cd into
(defadvice run-js (before run-js-advice activate compile)
  (set (make-local-variable 'inferior-js-program-command)
       (concat inferior-js-program-command " " default-directory)))

(add-hook
 'js-mode-hook
 '(lambda ()
    (font-lock-mode t)
    (set (make-local-variable 'js-indent-level) 2)
    (local-set-key "\C-c\C-p" 'run-js)
    (local-set-key (kbd "C-c #") 'comment-region)
    (local-set-key "\C-x\C-e" 'js-send-last-sexp)
    (local-set-key "\C-\M-x" 'js-send-last-sexp-and-go)
    (local-set-key "\C-cb" 'js-send-buffer)
    (local-set-key "\C-c\C-b" 'js-send-buffer-and-go)
    (local-set-key "\C-cl" 'js-load-file-and-go)))
