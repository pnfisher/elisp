// installed shelljs as follows
//   npm install -g shelljs
// see: https://github.com/arturadib/shelljs
//
// this script called by my-javascript.el
require("shelljs/global")
var repl = require("repl");

repl.start({
  //prompt: "> ",
  input: process.stdin,
  output: process.stdout,
  terminal: false,
  useColors: true
});

// my-javascript.el should pass in default-directory so
// we know to cd into it
if (process.argv.length > 2 && test("-f ", process.argv[2]))
  cd(process.argv[2])
