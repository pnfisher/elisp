;;; To install, merely put this file somewhere GNU Emacs will find it,
;;; then add the following lines to your .emacs file:
;;;
;;;   (autoload 'cscope "cscope")
;;;
;;; You may also adjust some customizations variables, below, by defining
;;; them in your .emacs file.

(require 'compile)
(require 'font-lock)
(provide 'cscope)

(font-lock-set-defaults)

(defcustom cscope-id nil
  "Key to index cscope-reffile-table"
  :group 'my-tools-id)
(defvar cscope-mode-hook nil)
(defvar cscope-face-hook nil)
(defvar cscope-reffile-table nil)
(defvar cscope-history nil)
(defvar cscope-regexp-alist
  '(("^\\([^ \n]+\\) [^ \n]+ \\([0-9]+\\) [^ ]+" 1 2))
  "Regexp used to match cscope hits.  See `compilation-error-regexp-alist'.")

(defvar cscope-default-face 'default)
(defvar cscope-file-face font-lock-warning-face)
(defvar cscope-function-face font-lock-function-name-face)
(defvar cscope-line-face font-lock-variable-name-face)
(defvar cscope-unknown-face font-lock-constant-face)
(defvar cscope-global-face font-lock-keyword-face)
(defvar cscope-info-face compilation-info-face)
(defvar cscope-error-face compilation-error-face)

(defvar
  cscope-font-lock-keywords
  (font-lock-compile-keywords
   '(("^\\([^\n ]+\\)\\( \\)\\([^<>\n ]+\\)\\( \\)\\([0-9]+\\)\\( [^ ].*\\)$"
      (1 cscope-file-face)
      (2 cscope-default-face)
      (3 cscope-function-face)
      (4 cscope-default-face)
      (5 cscope-line-face)
      (6 cscope-default-face))
     ("^\\([^\n ]+\\)\\( \\)\\(<unknown>\\)\\( \\)\\([0-9]+\\)\\( [^ ].*\\)$"
      (1 cscope-file-face)
      (2 cscope-default-face)
      (3 cscope-unknown-face)
      (4 cscope-default-face)
      (5 cscope-line-face)
      (6 cscope-default-face))
     ("^\\([^\n ]+\\)\\( \\)\\(<global>\\)\\( \\)\\([0-9]+\\)\\( [^ ].*\\)$"
      (1 cscope-file-face)
      (2 cscope-default-face)
      (3 cscope-global-face)
      (4 cscope-default-face)
      (5 cscope-line-face)
      (6 cscope-default-face))
     ("\\(error\\)"
      (1 cscope-error-face)))))

(defvar cscope-prompt
  "c-[s]ym [d]ef [r]ef-by [c]alling [t]ext [g]rep [e]grep [f]ile #[i]nclude")
(defvar cscope-command-table
  '(("s" ("0" "c-symbol"))
    ("d" ("1" "global definition"))
    ("c" ("2" "functions called by"))
    ("r" ("3" "functions calling"))
    ("t" ("4" "text string"))
    ("g" ("5" "grep pattern"))
    ("e" ("6" "egrep pattern"))
    ("f" ("7" "file"))
    ("i" ("8" "file including"))))

(defun cscope-find-arg (command-num table)
  (if (null table)
      nil
    (let* ((entry (nth 0 table))
           (command-letter (nth 0 entry)))
      (if (string-equal (nth 0 (nth 1 entry)) command-num)
          (string-to-char command-letter)
        (cscope-find-arg command-num (cdr table))))))

(defun cscope (&optional command-num)
  (interactive)
  (run-hooks 'cscope-mode-hook)
  (let ((arg (if command-num
                 (cscope-find-arg command-num cscope-command-table)
               (read-char cscope-prompt nil))))
    (if arg
        (let ((command (nth 1 (assoc (char-to-string arg)
                                     cscope-command-table))))
          (if command
              (cscope-find-aux (nth 0 command) (nth 1 command))
            (error (format "Not a valid command: %s" (char-to-string arg)))))
      (error (format "Not a valid command: %s" (if command-num
                                                   command-num
                                                 "?"))))))

(defun cscope-find-c-symbol ()
  (interactive)
  (cscope "0"))

(defun cscope-find-global-definition ()
  (interactive)
  (cscope "1"))

(defun cscope-find-functions-calling ()
  (interactive)
  (cscope "3"))

(defun cscope-find-file ()
  (interactive)
  (cscope "7"))

(defun cscope-find-files-including ()
  (interactive)
  (cscope "8"))

(defun cscope-find-aux (command prompt)
  (let ((token
         (read-from-minibuffer
          (concat "Find " prompt ": ")
          (cons (or (thing-at-point 'symbol) "") 1)
          nil
          nil
          'cscope-history))
        (reffile
         (format
          "%s/%s"
          (nth 1 (assoc cscope-id cscope-reffile-table))
          "cscope.out")))
    (if (and (not (string-equal token "")) reffile)
        (let ((compilation-process-setup-function 'cscope-process-setup)
              (compilation-error-regexp-alist cscope-regexp-alist)
              (compilation-scroll-output nil)
              (compilation-mode-hook
               '(lambda ()
                  (set
                   (make-local-variable 'compilation-highlight-regexp)
                   nil)
                  (font-lock-remove-keywords
                   nil
                   compilation-mode-font-lock-keywords)
                  (font-lock-add-keywords
                   nil
                   cscope-font-lock-keywords)
                  (run-hooks 'cscope-face-hook))))
          (compilation-start
           (concat
            "cscope -f "
            reffile
            " -d -L"
            command
            " "
            token)
           'compilation-mode
           '(lambda (mode) "*cscope*")
           nil)))))

(defun cscope-process-setup ()
  "Set up `compilation-exit-message-function' for `cscope'."
  (set (make-local-variable 'compilation-exit-message-function)
       (lambda (status code msg)
         (if (eq status 'exit)
             (cond ((zerop code)
                    '("finished\n" . "matched"))
                   ((= code 1)
                    '("error\n" . "no match"))
                   (t
                    (cons msg code)))
           (cons msg code)))))
