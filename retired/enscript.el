;; Print Emacs buffer on line printer.
;; Copyright (C) 1985, 1988 Free Software Foundation, Inc.

;; This file is part of GNU Emacs.

;; GNU Emacs is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY.  No author or distributor
;; accepts responsibility to anyone for the consequences of using it
;; or for whether it serves any particular purpose or works at all,
;; unless he says so in writing.  Refer to the GNU Emacs General Public
;; License for full details.

;; Everyone is granted permission to copy, modify and redistribute
;; GNU Emacs, but only under the conditions described in the
;; GNU Emacs General Public License.   A copy of this license is
;; supposed to have been given to you along with GNU Emacs so you
;; can know your rights and responsibilities.  It should be in a
;; file named COPYING.  Among other things, the copyright notice
;; and this notice must be preserved on all copies.


(defconst enscript-switches (cons "-G" nil)
  "*List of strings to pass as extra switch args to lpr when it is invoked.")

(defvar enscript-command "enscript"  "Shell command for printing a file")

(defun enscript-buffer ()
  "Print buffer contents using enscript.
`enscript-switches' is a list of extra switches (strings) to pass to enscript."
  (interactive)
  (enscript-region-1 (point-min) (point-max) enscript-switches))

(defun enscript-region (start end)
  "Print region contents using enscript.
`lpr-switches' is a list of extra switches (strings) to pass to enscript."
  (interactive "r")
  (enscript-region-1 start end enscript-switches))

(defun enscript-code-buffer ()
  "Print buffer contents using enscript.
`enscript-switches' is a list of extra switches (strings) to pass to enscript."
  (interactive)
  (enscript-region-1 (point-min) (point-max) (cons "-2r" enscript-switches)))

(defun enscript-code-region (start end)
  "Print region contents using enscript.
`lpr-switches' is a list of extra switches (strings) to pass to enscript."
  (interactive "r")
  (enscript-region-1 start end (cons "-2r" enscript-switches)))

(defun enscript-region-1 (start end switches)
  (let ((name (concat (buffer-name) " Emacs buffer"))
	(width tab-width))
    (save-excursion
     (message "Spooling...")
     (if (/= tab-width 8)
	 (let ((oldbuf (current-buffer)))
	  (set-buffer (get-buffer-create " *spool temp*"))
	  (widen) (erase-buffer)
	  (insert-buffer-substring oldbuf start end)
	  (setq tab-width width)
	  (untabify (point-min) (point-max))
	  (setq start (point-min) end (point-max))))
     (apply 'call-process-region
	    (nconc (list start end enscript-command
			 nil nil nil) switches))
     (message "Spooling...done"))))


