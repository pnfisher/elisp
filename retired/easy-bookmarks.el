;; Easy bookmarks management
;;
;; Author: Anthony Fairchild
;;

;;;; Keymaping examples
(global-set-key [?\C-c ?\C-b]  'easy-bookmark-set)
(global-set-key [(control f2)]  'easy-bookmark-cycle-forward)
(global-set-key [?\C-c f2]  'easy-bookmark-cycle-reverse)
(global-set-key [(control shift f2)]  'easy-bookmark-clear-all)

;; Include common lisp stuff
(with-no-warnings
  (require 'cl))
(require 'bookmark)
(defvar easy-current-bookmark nil)

(defun easy-bookmark-make-name ()
  "makes a bookmark name from the buffer name and cursor position"
  (concat (buffer-name (current-buffer))
          "-" (number-to-string (point))))

(defun easy-bookmark-set ()
  "remove a bookmark if it exists, create one if it doesnt exist"
  (interactive)
  (let ((bm-name (easy-bookmark-make-name)))
    (if (bookmark-get-bookmark bm-name t)
        (progn (bookmark-delete bm-name)
               (message "bookmark removed"))
      (progn (bookmark-set bm-name)
             (setf easy-current-bookmark bm-name)
             (message "bookmark set")))))

(defun easy-bookmark-cycle (i)
  "Cycle through bookmarks by i.  'i' should be 1 or -1"
  (if bookmark-alist
      (progn (unless easy-current-bookmark
               (setf easy-current-bookmark (first (first bookmark-alist))))
             (let ((cur-bm (assoc easy-current-bookmark bookmark-alist)))
               (setf easy-current-bookmark
                     (if cur-bm
                         (first
                          (nth
                           (mod
                            (+ i (with-no-warnings
                                  (position cur-bm bookmark-alist)))
                            (length bookmark-alist))
                           bookmark-alist))
                       (first (first bookmark-alist))))
               (bookmark-jump easy-current-bookmark)
               ;; Update the position and name of the bookmark.  We
               ;; only need to do this when the bookmark has changed
               ;; position, but lets go ahead and do it all the time
               ;; anyway.
               (bookmark-set-position easy-current-bookmark (point))
               (let ((new-name (easy-bookmark-make-name)))
                 (bookmark-set-name easy-current-bookmark new-name)
                 (setf easy-current-bookmark new-name))))
    (message "There are no bookmarks set!")))

(defun easy-bookmark-cycle-forward ()
  "find the next bookmark in the bookmark-alist"
  (interactive)
  (easy-bookmark-cycle 1))

(defun easy-bookmark-cycle-reverse ()
  "find the next bookmark in the bookmark-alist"
  (interactive)
  (easy-bookmark-cycle -1))

(defun easy-bookmark-clear-all()
  "clears all bookmarks"
  (interactive)
  (setf bookmark-alist nil))
