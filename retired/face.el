(defun custom-make-face (fg bg)
  "Return a face with background set to bg, and foreground set to fg"
  (let ((face (make-face (intern (concat "custom-" fg "-" bg "-face"))
                         (format "Face using fg %s and bg %s" fg bg))))
    (modify-face face fg bg)
    face))

(custom-make-face "skyblue" "black")


(defun custom-make-face (fg bg)
  (cons 'foreground-color fg)
  (cons 'background-color bg))


(defface custom-lightgoldenrod-black-face
  '((t (:foreground "lightgoldenrod") (:background "black")))
  "Face using fg: light golden rod, bg: black")

(defface custom-skyblue-black-face
  '((t (:foreground "skyblue") (:background "black")))
  "Face using fg: light golden rod, bg: black")

(defface custom-skyblue-black-face
  '((t (:foreground "skyblue")))
  "Face using sky blue.")

(defface custom-mediumspringgreen-face
  '((t (:foreground "mediumspringgreen")))
  "Face using medium spring green.")

(defface custom-tomato-face
  '((t (:foreground "tomato")))
  "Cscope font lock face for <unknown>s.")

(defun make-custom-face (name)
  (defface 