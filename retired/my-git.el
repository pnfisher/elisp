(eval-and-compile (require 'magit))
;;(load-library "rebase-mode")

(with-no-warnings (require 'cl))
(require 'grep)
(require 'view)
(require 'git nil 'noerror)
(require 'dired)

;;(eval-after-load 'info
;;  '(progn (info-initialize)
;;          (add-to-list 'Info-directory-list "~/src/elisp/magit")))

(defvar my-git-log-cutoff nil)
(defvar my-git-window-configuration nil)
(defvar my-git-current-file-log-name nil)
(defvar my-git-fake-buffer-suffix "@git<")
(defvar my-git-buffer-alist nil)
(defvar my-git-grap-args-history nil)
(defvar my-git-grep-history nil)
(defvar my-git-grep-default-args "--no-color -nI")
(defvar my-git-log-command "log --format=%H --topo-order ")
(defvar my-git-sha1-regexp "^\\([0-9a-f]+\\)$")
(defcustom my-git-absolute-ref nil
  "tell my-git-find-file to find files by absolute ref"
  :group 'my-tools-id)

(defstruct my-git-buffer-alist-entry fname sha1 id real-ref user-ref timestamp)

(defvar my-git-buffer-mode-map
  (let ((map (make-sparse-keymap)))
    (suppress-keymap map)
    (set-keymap-parent map view-mode-map)
    (define-key map "E" '(lambda ()
                           (interactive)
                           (my-git-ediff-predecessor)))
    (define-key map "F" 'my-git-find-file)
    (define-key map "G" 'my-magit-file-log-grep)
    (define-key map "X" 'my-git-kill-all-fakes)
    (define-key map "K" '(lambda ()
                           (interactive)
                           (my-git-purge-buffer-alist)
                           (my-git-next-buffer t)
                           (my-git-next-buffer nil)
                           (kill-buffer)))
    (define-key map "O" '(lambda ()
                           (interactive)
                           (my-git-find-current-file)))
    (define-key map "L" 'magit-file-log)
    (define-key map "N" '(lambda ()
                           (interactive)
                           (my-git-next-buffer t)))
    (define-key map "P" '(lambda ()
                           (interactive)
                           (my-git-next-buffer nil)))
    (define-key map "R" '(lambda ()
                           (interactive)
                           (my-git-rename-buffer)))
    (define-key map "T" '(lambda ()
                           (interactive)
                           (my-git-buffer-timestamp)))
    (define-key map "Z" '(lambda ()
                           (interactive)
                           (my-git-ediff-current-ref)))
    map))

(defun my-git-buffer-mode ()
  (kill-all-local-variables)
  (setq mode-name "my-git")
  (setq major-mode 'my-git-buffer-mode)
  ;;(use-local-map my-git-buffer-mode-map)
  (view-mode)
  (set (make-local-variable 'view-no-disable-on-exit) t)
  ;; With Emacs 22 `view-exit-action' could delete the selected window
  ;; disregarding whether the help buffer was shown in that window at
  ;; all.  Since `view-exit-action' is called with the help buffer as
  ;; argument it seems more appropriate to have it work on the buffer
  ;; only and leave it to `view-mode-exit' to delete any associated
  ;; window(s).
  (setq view-exit-action
        (lambda (buffer)
          ;; Use `with-current-buffer' to make sure that `bury-buffer'
          ;; also removes BUFFER from the selected window.
          (with-current-buffer buffer
            (bury-buffer))))
  (set (make-local-variable 'minor-mode-overriding-map-alist)
       (list (cons 'view-mode my-git-buffer-mode-map))))

(defun my-git-in-fake-buffer (&optional error)
  (let ((entry (assoc (current-buffer) my-git-buffer-alist)))
    (if entry
        (cdr entry)
      (when error
        (error "sorry, no buffer alist entry for current buffer")))))

(defun my-git-validate-repository ()
  (let ((topdir (magit-get-top-dir default-directory)))
    (when (not topdir)
      (error "not currently in git repository"))
    topdir))

(defun* my-git-file-to-use ()
  (my-git-validate-repository)
  (let ((entry (my-git-in-fake-buffer)))
    (when entry
      (return-from my-git-file-to-use
        (my-git-buffer-alist-entry-fname entry))))
  (when (and (equal (buffer-name) magit-log-buffer-name)
             my-git-current-file-log-name)
    (return-from my-git-file-to-use
                 my-git-current-file-log-name))
  (let ((fname (if (eq major-mode 'dired-mode)
                   (concat default-directory (thing-at-point 'filename))
                 (buffer-file-name))))
    (when (not fname)
      (error "current buffer is not visiting a file"))
    ;; magit-filename returns a filename relative to the
    ;; top of the git repository
    (magit-filename fname)))

(defun my-git-command-to-string (cmd)
  (shell-command-to-string (concat "cd "
                                   (my-git-validate-repository)
                                   "; git --no-pager "
                                   cmd)))

(defun my-git-log-all-cmd (fname)
  (concat
   my-git-log-command
   (when my-git-log-cutoff (concat "-n " my-git-log-cutoff " "))
   "--all --follow "
   fname))

(defun my-git-command (cmd)
  (shell-command (concat "cd "
                         (my-git-validate-repository)
                         "; PAGER=; git --no-pager "
                         cmd)))

(defun my-git-current-branch ()
  (with-temp-buffer
    (insert (my-git-command-to-string "status -uno"))
    (goto-char (point-min))
    (when (not (re-search-forward "^\\(# \\)?On branch \\([^ \n]+\\)" nil t))
      (error "coudn't find current branch name"))
    (match-string 2)))

;; clean out dead buffers
;;(defun my-git-purge-buffer-alist ()
;;  (setq my-git-buffer-alist
;;        (with-no-warnings
;;          (cl-delete-if '(lambda (buf)
;;                           (not (buffer-live-p buf)))
;;                        my-git-buffer-alist
;;                        :key 'car))))
(defun my-git-purge-buffer-alist ()
  (setq my-git-buffer-alist
        (loop for item in my-git-buffer-alist
              when (buffer-live-p (car item))
              collect item)))

(defun my-git-kill-all-fakes ()
  (interactive)
  (my-git-purge-buffer-alist)
  (setq my-git-window-configuration (current-window-configuration))
  (loop for item in my-git-buffer-alist
        when (buffer-live-p (car item))
        do (switch-to-buffer (car item))
        do (kill-buffer)))

(defun my-git-next-buffer-in-list (list fwd)
  (let ((len (length list))
        (buf nil))
    (when (> len 1)
      (let ((i (with-no-warnings (position (current-buffer) list :key 'car))))
        (if fwd
            (when (= (setq i (+ i 1)) len)
              (setq i 0))
          (when (= (setq i (- i 1)) -1)
            (setq i (- len 1))))
        (setq buf (car (nth i list)))
        (when (not (buffer-live-p buf))
          (error "unexpectedly encountered dead buffer in alist"))
        (switch-to-buffer buf)))))

(defun my-git-filter-alist-by-name (buffer-entry)
  (let* (gc
         (sublist
          (loop for item in my-git-buffer-alist
                with name = (my-git-buffer-alist-entry-fname
                             (cdr buffer-entry))
                with fname and alive
                do (setq fname (my-git-buffer-alist-entry-fname (cdr item))
                         alive (buffer-live-p (car item)))
                when (not alive) do (setq gc t)
                ;; bail out once item's name greater than
                ;; current buffer's name
                until (and (not (string-equal fname name))
                           (not (string-lessp fname name)))
                when (and alive (string-equal fname name))
                collect item)))
    (when gc (my-git-purge-buffer-alist))
    sublist))

(defun my-git-filter-alist-by-not-name (buffer-entry)
  (let* (gc
         (sublist
          (loop for item in my-git-buffer-alist
                with name = (my-git-buffer-alist-entry-fname
                             (cdr buffer-entry))
                with fname and alive and current
                do (setq fname (my-git-buffer-alist-entry-fname (cdr item))
                         alive (buffer-live-p (car item)))
                when (not alive) do (setq gc t)
                ;; make sure current buffer is in list so that
                ;; my-git-next-buffer-in-list knows where to start
                when (eq (car buffer-entry) (car item)) collect item
                when (and alive
                          (not (string-equal current fname))
                          (not (string-equal name fname)))
                do (setq current fname) and collect item)))
    (when gc (my-git-purge-buffer-alist))
    sublist))

(defun my-git-next-buffer (fwd)
  (let ((buffer-entry (assoc (current-buffer) my-git-buffer-alist))
        name)
    (when (not buffer-entry)
      (error "not in appropriate (fake) buffer"))
    (my-git-next-buffer-in-list
     (if current-prefix-arg
         (my-git-filter-alist-by-not-name buffer-entry)
       (my-git-filter-alist-by-name buffer-entry))
     fwd)))

(defun* my-git-add-new-buffer-alist
    (fname id sha1 &optional real-ref user-ref)

  (let*

      ;; when we find the right place to insert a new item, this
      ;; function will carry out that insertion. item is what we're
      ;; inserting, node is the first item in our existing list which
      ;; is greater than our new item (as defined by new-entry-greater
      ;; below)
      ((insert-before
        '(lambda (item node)
           (assert (consp node))
           (setf (cdr node) (cons (car node) (cdr node)))
           (setf (car node) item)))

       ;; function for comparing new item to items already in list.
       ;; We want to insert new item in list in increasing order as
       ;; defined by this function -- alphabetical when comparing
       ;; names, sequential when comparing log ids (i.e. youngest
       ;; first, oldest last)
       (new-entry-less-or-younger
        '(lambda (current)
           (let* ((entry (cdr current))
                  (curr-fname (my-git-buffer-alist-entry-fname entry)))
             (if (string-equal fname curr-fname)
                 (let ((new-id (string-to-number id))
                       (curr-id (string-to-number
                                 (my-git-buffer-alist-entry-id entry))))
                   ;; taking this out, things originally selected
                   ;; using a ref may have the same id as some
                   ;; already existing buffer
                   ;;(assert (not (= new-id curr-id)))
                   (< new-id curr-id))
               (string-lessp fname curr-fname)))))

       (buf (current-buffer))
       (curr my-git-buffer-alist)
       prev ts entry)

    ;; skip if buffer already in list
    (when (assoc buf my-git-buffer-alist)
      (return-from my-git-add-new-buffer-alist))

    ;; create timestamp for new item (no longer making real
    ;; use of timestamps, but I'll keep tracking them anyway
    ;; for informational purposes)
    (setq ts (my-git-command-to-string
              (concat "show -s --format=format:'%ct' " sha1)))
    (when (not (string-match "^[0-9]+$" ts))
      (error "sha1 timestamp makes no sense"))

    ;; find where to insert new item
    (loop while curr
          ;; do we need to reap dead buffers in list
          ;; and start again
          when (not (buffer-live-p (car (car curr))))
          do (progn
               (my-git-purge-buffer-alist)
               (return-from my-git-add-new-buffer-alist
                (my-git-add-new-buffer-alist fname id sha1 real-ref user-ref)))
          ;; compare new item with current item in list. We want sort
          ;; order to be alpha by name, youngest to oldest when names
          ;; are equal
          until (funcall new-entry-less-or-younger (car curr))
          do (setq prev curr
                   curr (cdr curr)))

    (setq entry (make-my-git-buffer-alist-entry
                 :fname fname
                 :sha1 sha1
                 :id id
                 :real-ref real-ref
                 :user-ref user-ref
                 :timestamp (string-to-number ts)))

    ;; if nothing in list, then make the list with new entry as it its
    ;; sole member
    (when (null my-git-buffer-alist)
      (setq my-git-buffer-alist (list (cons buf entry)))
      (return-from my-git-add-new-buffer-alist))

    (assert (or prev curr))

    (if curr
        (funcall insert-before (cons buf entry) curr)
      ;; new entry goes at end of list
      (assert (consp prev))
      (setf (cdr prev) (list (cons buf entry))))

  )
)

(defun my-git-create-fake-buffer-name
  (fname sha1 id &optional real-ref user-ref)
  (let ((munged-name
         (if (string-match "^\\(.*\\)/\\([^/]+\\)$" fname)
             ;; file names are stored relative to the top of the
             ;; current repo, but we don't want such long names used
             ;; as part of our buffer names
             (match-string 2 fname)
           fname)))
    (concat
     munged-name
     my-git-fake-buffer-suffix
     (substring sha1 0 7)
     "#"
     id
     (when real-ref (concat "#" real-ref))
     (when (and user-ref
                ;; user-ref can be #N where N = id when end-user has
                ;; specified a user-ref using this format; the code
                ;; will have interpreted this user-ref as and id
                ;; instead of a user-ref so we need to ignore the
                ;; user-ref (which is just a duplicate of the id
                (not (string-equal (concat "#" id) user-ref))
                (not (string-equal user-ref real-ref)))
       (concat "#[" user-ref "]"))
     ">")))

(defun my-git-rename-buffer ()
  (let* ((entry (my-git-in-fake-buffer t))
         (fname (my-git-buffer-alist-entry-fname entry))
         (sha1 (my-git-buffer-alist-entry-sha1 entry))
         (id (my-git-buffer-alist-entry-id entry))
         (real-ref (my-git-buffer-alist-entry-real-ref entry))
         (user-ref (my-git-buffer-alist-entry-user-ref entry))
         (bname1
          (my-git-create-fake-buffer-name fname sha1 id real-ref user-ref))
         (bname2 (if user-ref
                     (my-git-create-fake-buffer-name fname sha1 id real-ref)
                   bname1))
         (bname3 (my-git-create-fake-buffer-name fname sha1 id))
         (bname (cond ((string-equal bname1 (buffer-name))
                       (if (string-equal bname1 bname2)
                           bname3
                         bname2))
                      ((string-equal bname2 (buffer-name))
                       bname3)
                      (t
                       bname1)))
         buf)
    (if (not (setq buf (get-buffer bname)))
        (rename-buffer bname)
      (kill-buffer)
      (switch-to-buffer buf))))

(defun my-git-toggle-id ()
  (interactive)
  (setq my-git-log-cutoff (if my-git-log-cutoff nil "0"))
  (if (equal my-git-log-cutoff "0")
      (setq my-git-absolute-ref t)
    (setq my-git-absolute-ref nil))
  (message (concat
            "my-git [-log-cutoff] set to "
            (if my-git-log-cutoff my-git-log-cutoff "nil")
            ", [-absolute-ref] set to "
            (if my-git-absolute-ref "t" "nil"))))

(defun my-git-set-cutoff ()
  (interactive)
  (let ((cutoff (read-from-minibuffer "log -n cutoff: " my-git-log-cutoff)))
    (if (equal cutoff "nil")
        (setq my-git-log-cutoff nil)
      (when (and cutoff (not (string-match "^[0-9]+$" cutoff)))
        (error "sorry, cutoff is not a number"))
      (setq my-git-log-cutoff cutoff))))
(defalias 'my-git-toggle-cutoff 'my-git-set-cutoff)

(defun my-git-toggle-absolute ()
  (interactive)
  (setq my-git-absolute-ref (not my-git-absolute-ref))
  (message
   (concat
    "my-git-absolute-ref set to "
    (if my-git-absolute-ref "t" "nil"))))

(defun my-git-buffer-timestamp ()
  (let ((entry (my-git-in-fake-buffer t))
        ts)
    (setq ts (my-git-buffer-alist-entry-timestamp entry))
    (message (format "current buffer timestamp: %d" ts))))

(defun my-git-window-restore ()
  (when my-git-window-configuration
    (set-window-configuration my-git-window-configuration)
    (setq my-git-window-configuration nil))
  (remove-hook 'ediff-quit-hook 'my-git-window-restore))

(defun my-git-find-current-file ()
  (let ((entry (my-git-in-fake-buffer t)))
    (find-file (concat (my-git-validate-repository)
                       (my-git-buffer-alist-entry-fname entry)))))

(defun my-git-find-sha1-by-id (id)
  (let ((fname (my-git-file-to-use)))
    (when (not (string-match "^[0-9]+$" id))
      (error "sorry, id is not a number"))
    (setq id (string-to-number id))
    (with-temp-buffer
      (insert (my-git-command-to-string (my-git-log-all-cmd fname)))
      (goto-char (point-min))
      (forward-line (- id 1))
      (when (not (= (line-number-at-pos) id))
        (error (format "sorry, couldn't find log entry number %d" id)))
      (when (not (looking-at my-git-sha1-regexp))
        (error (format "sorry, can't determine sha1 for specified id %d" id)))
      (match-string 1))))

(defun* my-git-find-id-from-sha1 (fname sha1)
  (let ((id 1))
    (with-temp-buffer
      (insert (my-git-command-to-string (my-git-log-all-cmd fname)))
      (goto-char (point-min))
      (while (not (looking-at (concat "^" sha1 "$")))
        (when (> (forward-line 1) 0)
          (return-from my-git-find-id-from-sha1 "na"))
        (setq id (+ id 1)))
      (number-to-string id))))

(defun* my-git-ref-to-info (ref)
  (let ((fname (my-git-file-to-use))
        buf id ts sha1)
    (when (string-match "^#\\([0-9]+\\)$" ref)
      (setq id (match-string 1 ref))
      (setq sha1 (my-git-find-sha1-by-id id))
      (return-from my-git-ref-to-info
        (cl-values id sha1)))
    ;; sha1s generated by git log *all* for the file
    (with-temp-buffer
      (setq buf (current-buffer))
      (insert (my-git-command-to-string (my-git-log-all-cmd fname)))
      (goto-char (point-min))
      ;; sha1s generated by git log for the ref
      (with-temp-buffer
        (insert
         (my-git-command-to-string
          (concat my-git-log-command ref)))
        (goto-char (point-min))
        ;; now find the first sha1 for the ref also shared by
        ;; the file
        (while (with-current-buffer buf
                 (when (re-search-forward my-git-sha1-regexp nil t)
                   (setq sha1 (match-string 1))
                   (setq id (line-number-at-pos))))
          (when (re-search-forward sha1 nil t)
            (return-from my-git-ref-to-info
              (cl-values
               (number-to-string id)
               sha1)))))))
  (error (concat "couldn't find a base commit for ref " ref)))

(defun my-git-ref-to-sha1 (ref)
  (with-temp-buffer
    (insert
     (my-git-command-to-string (concat "rev-parse " ref)))
    (goto-char (point-min))
    (when (not (looking-at my-git-sha1-regexp))
      (error "sorry, couldn't find sha1 for specified ref"))
    (match-string 1)))

(defun my-git-family-shas (fname)
  (let (sha1 sha2)
    (with-temp-buffer
      (insert
       (my-git-command-to-string
        (concat my-git-log-command "-n 2 " fname)))
      (goto-char (point-min))
      (when (not (looking-at my-git-sha1-regexp))
        (error "couldn't determine current file's sha1"))
      (setq sha1 (match-string 1))
      (when (or (> (forward-line 1) 0)
                (not (looking-at my-git-sha1-regexp)))
        (error "couldn't find a parent commit"))
      (setq sha2 (match-string 1)))
    (cl-values sha1 sha2)))

(defun* my-git-find-parent (fname sha1)
  (with-temp-buffer
    (insert (my-git-command-to-string (my-git-log-all-cmd fname)))
    (goto-char (point-min))
    (when (not (re-search-forward sha1 nil t))
      (error "couldn't find specified sha1"))
    (when (or (> (forward-line 1) 0)
              (not (looking-at my-git-sha1-regexp)))
      (error "couldn't find a parent commit"))
    (return-from my-git-find-parent
      (cl-values (number-to-string (line-number-at-pos))
                 (match-string 1)))))

(defun my-git-find-file-aux (id sha1 &optional user-ref)
  (let* ((fname (my-git-file-to-use))
         (other-fname fname)
         (real-ref (magit-name-rev sha1))
         (done nil)
         buf)
    (when (not (find-file-existing (concat (my-git-validate-repository)
                                           fname)))
      (setq my-git-window-configuration nil)
      (error (concat "couldn't find file " fname)))
    (setq buf (get-buffer-create (my-git-create-fake-buffer-name fname
                                                                 sha1
                                                                 id
                                                                 real-ref
                                                                 user-ref)))
    (switch-to-buffer buf)
    (setq buffer-read-only nil)
    (erase-buffer)
    (insert (my-git-command-to-string (concat "show " sha1 ":./" fname)))
    ;;(setq buffer-read-only t)
    ;;(view-mode)
    (my-git-buffer-mode)
    (goto-char (point-min))
    (while (looking-at (concat "fatal: Path '"
                               fname
                               "'.*'"
                               sha1
                               "'.*$"))
      (setq other-fname (read-from-minibuffer
                         (concat "alternate path/name for " fname ": ")))
      (when (string-equal other-fname "")
        (kill-buffer)
        (error "can't create valid buffer to diff"))
      (setq buffer-read-only nil)
      (erase-buffer)
      (insert (my-git-command-to-string
               (concat "show " sha1 ":./" other-fname)))
      (goto-char (point-min)))
    (when (not (string-equal other-fname fname))
      (rename-buffer (replace-regexp-in-string
                      (replace-regexp-in-string
                       "^.*/"
                       ""
                       fname)
                      (replace-regexp-in-string
                       "^.*/"
                       ""
                       other-fname)
                      (buffer-name))))
    (my-git-add-new-buffer-alist fname id sha1 real-ref user-ref)
    buf))

(defun my-git-find-file-absolute ()
  (interactive)
  (let* ((ref (read-from-minibuffer "ref: " (my-git-current-branch)))
         (sha1 (my-git-ref-to-sha1 ref)))
    (my-git-find-file-aux
     (if (equal my-git-log-cutoff "0")
         "na"
       (my-git-find-id-from-sha1 (my-git-file-to-use) sha1))
     sha1
     ref)))

(defun* my-git-find-file ()
  (interactive)
  (when my-git-absolute-ref
    (my-git-find-file-absolute)
    (return-from my-git-find-file))
  (let ((ref (read-from-minibuffer "ref: " (my-git-current-branch))))
    (cl-multiple-value-bind (id sha1)
        (my-git-ref-to-info ref)
      (my-git-find-file-aux id sha1 ref))))

(defun my-git-ediff (id sha1 &optional ref)
  (let ((fname (my-git-file-to-use))
        original-buf id-buf)
    (setq my-git-window-configuration (current-window-configuration))
    (when (not (find-file-existing (concat (my-git-validate-repository)
                                           fname)))
      (setq my-git-window-configuration nil)
      (error (concat "couldn't find file " fname)))
    (setq original-buf (current-buffer)
          id-buf (my-git-find-file-aux id sha1 ref))
    (if current-prefix-arg
        (setq my-git-window-configuration nil)
      (add-hook 'ediff-quit-hook
                'my-git-window-restore
                'append))
    (ediff-buffers original-buf id-buf)))

(defun my-git-ediff-current (id sha1 &optional ref)
  (setq my-git-window-configuration (current-window-configuration))
  (let ((original-buf (current-buffer))
        (id-buf (my-git-find-file-aux id sha1 ref)))
    (if current-prefix-arg
        (setq my-git-window-configuration nil)
      (add-hook 'ediff-quit-hook
                'my-git-window-restore
                'append))
    (ediff-buffers original-buf id-buf)))

(defun my-git-ediff-ref-absolute ()
  (interactive)
  (let* ((ref (read-from-minibuffer "ref: " (my-git-current-branch)))
         (sha1 (my-git-ref-to-sha1 ref)))
    (my-git-ediff
     (if (equal my-git-log-cutoff "0")
         "na"
       (my-git-find-id-from-sha1 (my-git-file-to-use) sha1))
     sha1
     ref)))

(defun my-git-ediff-current-ref ()
  (let ((entry (my-git-in-fake-buffer t))
        id user-ref)
    (setq user-ref (my-git-buffer-alist-entry-user-ref entry))
    (setq user-ref (read-from-minibuffer
                    "ref: "
                    (if user-ref user-ref (my-git-current-branch))))
    (cl-multiple-value-bind (id sha1)
        (my-git-ref-to-info user-ref)
      (my-git-ediff-current id sha1 user-ref))))

(defun* my-git-ediff-ref ()
  (interactive)
  (when my-git-absolute-ref
    (my-git-ediff-ref-absolute)
    (return-from my-git-ediff-ref))
  (let ((ref (read-from-minibuffer "ref: " (my-git-current-branch))))
    (cl-multiple-value-bind (id sha1)
        (my-git-ref-to-info ref)
      (my-git-ediff id sha1 ref))))

(defun* my-git-ediff-predecessor ()
  (interactive)
  (my-git-validate-repository)
  (let* ((buffer-alist-entry (my-git-in-fake-buffer))
         (fname (my-git-file-to-use))
         id sha1 sha2)
    (when buffer-alist-entry
      (cl-multiple-value-bind
          (id sha2) (my-git-find-parent
                     fname
                     (my-git-buffer-alist-entry-sha1 buffer-alist-entry))
        (my-git-ediff id sha2))
      (return-from my-git-ediff-predecessor))
    (cl-multiple-value-setq
        ;; sha1 = current; sha2 = parent
        (sha1 sha2)
      (my-git-family-shas fname))
    ;; diff the first entry in the log if the source file is modified;
    ;; if it's unmodified, then it's the first entry in the log, and
    ;; we need to diff it against the second entry
    (with-temp-buffer
      (insert
       (my-git-command-to-string (concat "status --short " fname)))
      (goto-char (point-min))
      (if (not (looking-at "^ \\(M\\|\\U\\) "))
          (if (equal my-git-log-cutoff "0")
              (setq id "na")
            (cl-multiple-value-setq
                (id sha2)
              ;; yes, technically we already have the parent sha as
              ;; sha2, but we still need the id
              (my-git-find-parent fname sha1)))
        ;; we're diffing the index against the previous commit so
        ;; pretend the previous commit (i.e. sha1) is the parent
        (setq sha2 sha1)))
    (my-git-ediff
     (if id id "1")
     sha2)))

(defun my-git-grep ()
  (interactive)
  (let ((git-dir (magit-get-top-dir default-directory)))
    (if (or (not git-dir) current-prefix-arg)
        (if emacs24
            (with-no-warnings (execute-extended-command nil "grep"))
          (grep
           (progn
             (grep-compute-defaults)
             (read-shell-command
              "Run grep (like this): "
              (grep-default-command)
              'grep-history
              nil))))
      (let* ((prompt (concat "git grep " my-git-grep-default-args " "))
             (cmd
              (read-from-minibuffer
               "Run git grep (like this): "
               (cons
                (concat prompt (thing-at-point 'symbol))
                (+ (string-bytes prompt) 1))
               nil
               nil
               'my-git-grep-history)))
        (with-temp-buffer
          (cd git-dir)
          (set (make-local-variable 'grep-use-null-device) nil)
          ;; replace 'git grep ...' with 'git --no-pager grep ...'
          (grep (concat "git --no-pager" (substring cmd 3))))))))

(defun my-git-blame ()
  (interactive)
  (my-git-validate-repository)
  (let ((fname (my-git-file-to-use)))
    (my-git-command
     (concat
      "blame --root --show-stats "
      (when (yes-or-no-p "use current line number ")
        (concat "-L " (substring (what-line) 5) " "))
      fname
      " &"))))

;; patch -R -o - < patch.txt

;;
;; magit specific stuff
;;
;; desgined to alter magit's *magit-log* and *magit-commit* buffer
;; behaviour
;;

(defvar my-magit-section-search-string nil)
(defvar my-magit-overlay nil)
(defface my-magit-highlight-face
  '((t (:inherit 'isearch)))
  "Face for my-magit-highlight-section-search-string"
  :group 'isearch)
(set-face-background 'my-magit-highlight-face "magenta3")
;;(set-face-foreground 'my-magit-highlight-face "lightskyblue1")
(set-face-foreground 'my-magit-highlight-face "white")

(define-key magit-mode-map "j" 'my-magit-forward-to-search-string)
(define-key magit-mode-map "J" '(lambda ()
                                  (interactive)
                                  (my-magit-find-file)))
(define-key magit-mode-map "I" '(lambda ()
                                  (interactive)
                                  (cl-multiple-value-bind (id sha1)
                                      (my-magit-find-info-from-log)
                                    (message (concat
                                              "log id: "
                                              id)))))

(defadvice magit-refresh-log-buffer
  (before my-magit-refresh-log-buffer activate compile)
  (dolist (e magit-custom-options)
    (cond ((string-match "^-\\(G\\|S\\|-grep=\\)\\(.*\\)" e)
           (set (make-local-variable 'my-magit-section-search-string)
                (match-string 2 e)))
          ((string-match "^--\\(author\\|committer\\)=\\(.*\\)" e)
           (set (make-local-variable 'my-magit-section-search-string)
                (concat
                 "^\\(Author\\|Committer\\):[ \t]+"
                 (match-string 2 e)))))))

(defadvice magit-refresh-file-log-buffer
  (after my-magit-refresh-file-log-buffer activate compile)
  (local-set-key "G" 'my-magit-file-log-grep)
  (set (make-local-variable 'my-magit-section-search-string)
       (concat "^[ \t]*\\(Modified\\|New\\)[ \t]+" file))
  (set (make-local-variable 'my-git-current-file-log-name)
       file))

;; get the log-buffer local search string variable into the commit
;; buffer (as a local variable)
(defadvice magit-show-commit
  (after my-magit-show-commit compile activate)
  (let ((log-buf (get-buffer magit-log-buffer-name))
        (commit-buf (get-buffer magit-commit-buffer-name))
        tmp)
    (when (and log-buf commit-buf)
      (with-current-buffer commit-buf
        (when (not my-magit-section-search-string)
          (with-current-buffer log-buf
            (setq tmp my-magit-section-search-string))
          (set (make-local-variable 'my-magit-section-search-string) tmp))))))

(defadvice magit-file-log
  (around my-magit-file-log compile activate)
  (let* ((fname (my-git-file-to-use))
         (buffer-alist-entry (my-git-in-fake-buffer)))
   (if (not buffer-alist-entry)
        (progn
          ad-do-it
          (goto-char (point-min))
          (forward-line 1))
      (let ((sha1 (substring
                   (my-git-buffer-alist-entry-sha1 buffer-alist-entry)
                   0 7))
            (range "HEAD"))
        (magit-buffer-switch magit-log-buffer-name)
        (magit-mode-init (my-git-validate-repository)
                         'magit-log-mode
                         #'magit-refresh-file-log-buffer
                         fname
                         range
                         'oneline)
        (goto-char (point-min))
        (if (not (re-search-forward (concat "^" sha1) nil t))
            (message (concat
                      "sorry, couldn't find log entry for sha1 id "
                      sha1))
          (beginning-of-line)
          ;;(magit-goto-section (magit-current-section))
          )))))

(defun* my-magit-find-info-from-log ()
  (when (not my-git-current-file-log-name)
    (error (concat "not in " magit-log-buffer-name)))
  (let ((fname my-git-current-file-log-name)
        (regexp "^\\([0-9a-f]+\\) ")
        (id 1)
        sha1 sha1-regexp)
    (save-excursion
      (beginning-of-line)
      (when (not (looking-at regexp))
        (return-from my-magit-find-info-from-log))
      (setq sha1 (match-string 1))
      (setq sha1-regexp (concat "^" sha1))
      (with-temp-buffer
        (insert (my-git-command-to-string (my-git-log-all-cmd fname)))
        (goto-char (point-min))
        (while (not (looking-at sha1-regexp))
          (when (> (forward-line 1) 0)
            (error (concat
                    "couldn't find "
                    sha1
                    " in "
                    magit-log-buffer-name)))
          (setq id (+ id 1)))))
    (cl-values
     (number-to-string id)
     sha1)))

(defun* my-magit-find-file ()
  (when (not my-git-current-file-log-name)
    (return-from my-magit-find-file))
  (save-excursion
    (beginning-of-line)
    (when (not (looking-at "^\\([0-9a-f]+\\) "))
      (return-from my-magit-find-file)))
  (cl-multiple-value-bind (id sha1)
      (my-magit-find-info-from-log)
    (my-git-find-file-aux id sha1)))

;; modified version of isearch-overlay (from isearch.el)
(defun my-magit-highlight (beg end)
  (if my-magit-overlay
      ;; Overlay already exists, just move it.
      (move-overlay my-magit-overlay beg end (current-buffer))
    ;; Overlay doesn't exist, create it.
    (setq my-magit-overlay (make-overlay beg end))
    ;; 1001 is higher than lazy's 1000 and ediff's 100+
    (overlay-put my-magit-overlay 'priority 1001)
    (overlay-put my-magit-overlay 'face 'my-magit-highlight-face)))

(defun my-magit-highlight-section-search-string ()
  (save-excursion
    (let ((p1 (point)))
      (re-search-forward my-magit-section-search-string nil t)
      (my-magit-highlight p1 (point))))
  (recenter))

(defun my-magit-find-section-search-string (prefix)
  (if prefix
      (if (re-search-backward my-magit-section-search-string nil t)
          (my-magit-highlight-section-search-string)
        (goto-char (point-max))
        (when (re-search-backward my-magit-section-search-string nil t)
          (my-magit-highlight-section-search-string)))
    (when (looking-at my-magit-section-search-string)
      (re-search-forward my-magit-section-search-string nil t))
    (if (re-search-forward my-magit-section-search-string nil t)
        (progn
          (re-search-backward my-magit-section-search-string nil t)
          (my-magit-highlight-section-search-string))
      (goto-char (point-min))
      (when (re-search-forward my-magit-section-search-string nil t)
        (re-search-backward my-magit-section-search-string nil t)
        (my-magit-highlight-section-search-string)))))

(defun my-magit-forward-to-search-string ()
  (interactive)
  (when my-magit-section-search-string
    (if (string-equal (buffer-name) magit-commit-buffer-name)
        (my-magit-find-section-search-string current-prefix-arg)
      (when (string-equal (buffer-name) magit-log-buffer-name)
        (magit-visit-item)
        (my-magit-find-section-search-string current-prefix-arg)
        (other-window -1)))))

(defun my-magit-refresh-file-grep-log-buffer (file range style grepstr)
  (magit-configure-have-graph)
  (magit-configure-have-decorate)
  (magit-configure-have-abbrev)
  (setq magit-current-range range)
  (setq magit-file-log-file file)
  (set (make-local-variable 'my-git-current-file-log-name) file)
  (set (make-local-variable 'my-magit-section-search-string) grepstr)
  (magit-create-log-buffer-sections
    (apply #'magit-git-section nil
           (magit-rev-range-describe
            range
            (format "Commits for file %s (-G%s)" file grepstr))
           (apply-partially 'magit-wash-log style)
           `("log"
             ,(format "--max-count=%s" magit-log-cutoff-length)
             ,"--abbrev-commit"
             ,(format "--abbrev=%s" magit-sha1-abbrev-length)
             ,@(cond ((eq style 'long) (list "--stat" "-z"))
                     ((eq style 'oneline) (list "--pretty=oneline"))
                     (t nil))
             ,@(if magit-have-decorate (list "--decorate=full"))
             ,@(if magit-have-graph (list "--graph"))
             ,@(list (concat "-G" grepstr))
             "--"
             ,file))))

(defun my-magit-file-log-grep ()
  (interactive)
  (let ((topdir (magit-get-top-dir default-directory))
        (fname (my-git-file-to-use))
        (grepstr (read-from-minibuffer "grep for: "))
        (range "HEAD"))
    (magit-buffer-switch "*magit-log*")
    (magit-mode-init topdir
                     'magit-log-mode
                     #'my-magit-refresh-file-grep-log-buffer
                     fname
                     range
                     'oneline
                     grepstr)))

(global-set-key [?\C-c ?u] 'magit-status)
(global-set-key [?\C-c ?\C-l] 'magit-file-log)
(global-set-key [?\C-c ?/] 'my-git-find-file)
;;git-grep will call grep when we're not in a gip repo
(global-set-key [?\C-c ?\g] 'my-git-grep)
(global-set-key [?\C-c ?q] 'my-git-ediff-predecessor)
(global-set-key [?\C-c ?z] 'my-git-ediff-ref)
(global-set-key [?\C-c ??] 'magit-status)
(global-set-key [?\C-h ?j] 'magit-file-log)

;; git diff-tree --name-status maint_tweaks..maint
;; git diff-tree --name-only --pretty=format:files: $(git show-branch --merge-base)
