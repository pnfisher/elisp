(require 'cl)

(custom-set-variables
 '(compilation-search-path
   `("."
     "~/src/chromium/git/src/chrome/browser")))

(defvar chrome-git-dir (concat (getenv "HOME") "/src/chromium/git/src"))

(defun ch-cat ()
  (interactive)
  (let ((src chrome-git-dir))
    (when (not (file-exists-p src))
      (error (format "directory " src " doesn't exist")))
    (cd chrome-git-dir)
    (let* ((bname "*ch-cat*")
           (buf (get-buffer-create bname)))
      (when (shell-command
             "time ~/scripts/mk_cat.chrome &"
             bname)
        (pop-to-buffer buf)
        (other-window -1)))))

(defun ch-browser-aux (url)
  (let ((br "/usr/bin/google-chrome"))
    (when (not (file-exists-p br))
      (error (concat "can't find " br)))
    (call-process
     br
     nil 0 nil
     "--new-window"
     url)))

(defun ch-bots ()
  (interactive)
  (ch-browser-aux
   "https://chromium-build.appspot.com/p/chromium/console"))

(defun ch-falls ()
  (interactive)
  (ch-browser-aux
   "http://build.chromium.org/p/chromium/waterfall"))

(defun ch-search ()
  (interactive)
  (ch-browser-aux
   "http://code.google.com/p/chromium/source/search"))

(defun ch-prj ()
  (interactive)
  (ch-browser-aux "http://code.google.com/p/chromium/"))

(defun ch-issues ()
  (interactive)
  (ch-browser-aux "http://code.google.com/p/chromium/issues/list"))

(defun google (search-text)
  (interactive "ssearch text: ")
  (ch-browser-aux (concat "http://www.google.com/search?q= " search-text)))

(defun ch-files ()
  (let ((chfile (concat chrome-git-dir "/chrome.files")))
    (when (not (file-exists-p chfile))
      (error (concat "can't find " chfile)))
    (find-file chfile)))

(defun Xephyr ()
  (interactive)
  (call-process "Xephyr" nil 0 nil ":4" "-screen" "1024x768x24"))

(defun ch-gdb ()
  (interactive)
  (with-temp-buffer
    (insert
     (shell-command-to-string
      "xwininfo -display :4 -root"))
    (goto-char (point-min))
    (when (looking-at
           "^xwininfo: error: \\(unable to open display.*:[0-9]+\\)")
      (error (concat (match-string 1) "\" -- did you run Xephyr?"))))
  (let* ((chrome-binary-dir (concat chrome-git-dir "/out/Debug"))
         (chrome-binary (concat chrome-binary-dir "/chrome"))
         (gdbinit (concat chrome-binary-dir "/.gdbinit")))
    (when (not (file-exists-p chrome-binary))
      (error (concat chrome-binary " doesn't exist")))
    (when (file-exists-p gdbinit)
      (delete-file gdbinit))
    (shell-command-to-string
     (concat "cd " chrome-git-dir "/../..; make -f Makefile.git gdbinit"))
    (when (not (file-exists-p gdbinit))
      (error "couldn't make gdbinit"))
    ;;(write-region
    ;; (concat
    ;;  "set args chrome --profile-directory=Debug http://jaypeak\n"
    ;;  "set environment DISPLAY=:4\n")
    ;; nil
    ;; gdbinit)
    (let ((cmd
           (read-from-minibuffer
            "Run gdb (like this): "
            ;;(concat "gdb --annotate=3 " chrome-binary)
            (concat "gdb --fullname " chrome-binary)
            )))
      ;;(gdb cmd)
      (gud-gdb cmd)
      )))
