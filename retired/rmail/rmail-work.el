
;; setup mail stuff for work
(require 'rmail)
(load "rmailsum")

(setq mail-signature t)

(require 'smtpmail)
(setq send-mail-function 'smtpmail-send-it)
(setq smtpmail-smtp-server "exchange.mc.com")
(setq smtpmail-smtp-service 25)
(setq smtpmail-local-domain "mc.com")
(setq smtpmail-sendto-domain "mc.com")
(setq user-mail-address "pfisher@mc.com")

(setq rmail-file-name "~/rmail/RMAIL")
(setq rmail-default-rmail-file "~/rmail/rmail.current")
(setq rmail-delete-after-output nil)



(custom-set-variables
 '(rmail-mime-attachment-dirs-alist
   `(("text/.*" "~/rmail/detached")
    ("image/.*" "~/rmail/detached")
    (".*" "~/rmail/detached"))))

;;(define-key rmail-summary-mode-map "F" 'mime-forward)
;;(define-key rmail-summary-mode-map "D" 'detach)
(define-key rmail-mode-map [?\M-r] 'rmail-search-backwards)
;;(define-key rmail-mode-map "F" 'mime-forward)
;;(define-key rmail-mode-map "D" 'detach)
;;(define-key rmail-mode-map "N" 'find-next-detach-label)
;;(define-key rmail-mode-map "P" 'find-previous-detach-label)
;;(define-key rmail-mode-map "X" 'delete-detach-label)
(define-key rmail-mode-map "W" 'rmail-do-wrap-lines)
(define-key rmail-mode-map "H" 'rhtml2txt)
;;(define-key rmail-mode-map [?\C-c ?h] 'html2txt-detach-label)
;;(define-key rmail-mode-map [?\C-c ?H] 'html2txt-detach-label)
(define-key rmail-mode-map "R" 'rmail-resend-aux)
(define-key rmail-mode-map "L" 'rmail-add-label)
(define-key rmail-summary-mode-map "L" 'rmail-summary-add-label)
(define-key rmail-mode-map "V" 'rmail-toggle-html)

(define-key rmail-summary-mode-map "I"
  '(lambda ()
     (interactive nil)
     (rmail-summary-add-label "NB")))

(define-key rmail-mode-map "I"
  '(lambda ()
     (interactive nil)
     (rmail-add-label "NB")))

(define-key rmail-summary-mode-map "T"
  '(lambda ()
     (interactive nil)
     (rmail-summary-add-label "TEMP")))

(define-key rmail-mode-map "T"
  '(lambda ()
     (interactive nil)
     (rmail-add-label "TEMP")))

(define-key rmail-summary-mode-map "M"
  '(lambda ()
     (interactive nil)
     (rmail-summary-add-label "Attachments")))

(define-key rmail-mode-map "M"
  '(lambda ()
     (interactive nil)
     (rmail-add-label "Attachments")))

(define-key rmail-summary-mode-map "A"
  '(lambda ()
     (interactive nil)
     (let ((rmail-fetch-mail-from-server nil))
       (rmail-summary-input "~/rmail/rmail.archive"))))

(define-key rmail-mode-map "A"
  '(lambda ()
     (interactive nil)
     (let ((rmail-fetch-mail-from-server nil))
       (rmail-input "~/rmail/rmail.archive"))))

(define-key rmail-summary-mode-map "D"
  '(lambda ()
     (interactive nil)
     (let ((rmail-fetch-mail-from-server nil))
       (rmail-summary-input "~/rmail/rmail.deleted"))))

(define-key rmail-mode-map "D"
  '(lambda ()
     (interactive nil)
     (let ((rmail-fetch-mail-from-server nil))
       (rmail-input "~/rmail/rmail.deleted"))))

(define-key rmail-summary-mode-map "a"
  '(lambda ()
     (interactive nil)
     (let ((rmail-delete-after-output t))
       (rmail-summary-output "~/rmail/rmail.archive" 1))))

(define-key rmail-mode-map "a"
  '(lambda ()
     (interactive nil)
     (if (equal (buffer-name) "rmail.archive")
         (rmail-delete-forward)
       (let ((rmail-delete-after-output t))
         (rmail-output "~/rmail/rmail.archive")))))

(define-key rmail-summary-mode-map "d"
  '(lambda ()
     (interactive nil)
     (let ((rmail-delete-after-output t))
       (rmail-summary-output "~/rmail/rmail.deleted" 1))))

(define-key rmail-mode-map "d"
  '(lambda ()
     (interactive nil)
     (if (equal (buffer-name) "rmail.deleted")
         (rmail-delete-forward)
       (let ((rmail-delete-after-output t))
         (rmail-output "~/rmail/rmail.deleted")))))

(defvar rmail-strip-html t)
(defvar rmail-wrap-lines t)
(defvar rmail-html-mime-unhide t)

(defun rmail-toggle-html ()
  (interactive)
  (setq rmail-strip-html (not rmail-strip-html))
  (rmail-show-message))

(defun rmail-toggle-wrap ()
  (interactive)
  (setq rmail-wrap-message (not rmail-wrap-message)))

(defun rmail-toggle-mime-unhide ()
  (interactive)
  (setq rmail-html-mime-unhide (not rmail-html-mime-unhide)))

(defun rmail-do-wrap-lines ()
  (interactive)
  (if rmail-wrap-lines
      (save-excursion
        (set (make-local-variable 'fill-column) (window-width))
        (beginning-of-buffer)
        (let ((buffer-read-only))
          (when (re-search-forward "^[ ]*$")
            (while (not (eobp))
              (forward-line)
              (beginning-of-line)
              (let ((p1 (point)))
                (end-of-line)
                (let ((p2 (point)))
                  (save-excursion
                    (fill-region p1 p2 nil t))))))))))

(defadvice rmail-mime-toggle-button
  (around html-rmail-toggle-before activate compile)
  (if rmail-strip-html
      (let ((pos (point))
            tag)
        (end-of-line)
        (let ((end (point)))
          (beginning-of-line)
          (let ((beg (point)))
            (save-restriction
              (narrow-to-region beg end)
              (setq tag (buffer-string)))
            (if (not (string-match "^\\(\\[[^:]*:[^ ]* \\)" tag))
                (error (concat "search for sub-tag within " tag " failed")))
            (let ((tag2 (match-string 1 tag))
                  (rmail-strip-html nil))
              (rmail-show-message)
              (if (search-forward tag nil t)
                  (progn
                    (forward-char (- pos end))
                    (push-button))
                (if (not (search-forward tag2 nil t))
                    (error (concat "search for " tag2 " failed"))))
              (rhtml2txt)
              (rmail-do-wrap-lines)
              (beginning-of-line)
              (forward-char (- pos beg))))))
    ad-do-it
    (when rmail-strip-html
      (rhtml2txt)
      (rmail-do-wrap-lines))))

(defun rmail-do-html-mime-unhide ()
  (if rmail-html-mime-unhide
      (save-excursion
        (beginning-of-buffer)
        (while (re-search-forward
                "^\\[[^:]*:text/\\(html Show\\|calendar Hide\\)\\]"
                nil
                t)
          (beginning-of-line)
          (forward-button 1)
          (let ((rmail-strip-html nil))
            (push-button))
          (end-of-line)))))

(defun rmail-do-strip-html ()
  (rmail-do-html-mime-unhide)
  (rhtml2txt)
  (rmail-do-wrap-lines))

(defadvice rmail-summary-by-regexp
  (around rmail-summary-by-regexp-advice activate compile)
  (let ((rmail-enable-mime nil))
    ad-do-it))

(defadvice rmail-show-message-1
  (after html-rmail-show-message-1 activate compile)
  (if rmail-strip-html
      (rmail-do-strip-html)))

(defadvice rmail-resend (around safe-rmail-resend activate compile)
  (if (not (string-equal mode-name "RMAIL"))
      (error "Sorry: not in RMAIL buffer"))
  (let ((pos (point)))
    (let ((rmail-enable-mime nil)
          (rmail-strip-html nil))
      (rmail-show-message)
      ad-do-it)
    (rmail-show-message)
    (goto-char pos)))

(defun rmail-resend-aux ()
  (interactive)
  (let ((addr (read-buffer "rmail-resend address: " "pfisher@mc.com")))
    (rmail-resend addr)))

(defvar rmail-fetch-mail-from-server t)

(setq pop3-leave-mail-on-server nil)
(setq rmail-primary-inbox-list `("~/rmail/pop3"))
(require 'pop3)
;;(defadvice rmail (before rmail-pop3 activate compile)
;;  (if (y-or-n-p "Do you really want to open rmail: ")
;;      (progn
;;        (if (y-or-n-p "Fetch new mail from exchange: ")
;;            (progn
;;              (if (y-or-n-p "Delete mail on server: ")
;;                  (setq pop3-leave-mail-on-server nil)
;;                (setq pop3-leave-mail-on-server t))
;;              (setq pop3-password (rmail-get-remote-password 'POP))
;;              (pop3-movemail (car rmail-primary-inbox-list))
;;              (setq pop3-password nil))))
;;    (error "Not running rmail")))
(defadvice rmail (before rmail-pop3 activate compile)
  (if rmail-fetch-mail-from-server
      (let ((pop3-password (rmail-get-remote-password 'POP)))
        (pop3-movemail (car rmail-primary-inbox-list)))))
(setq
 ;;pop3-mailhost "CHM-EMAIL2.ad.mc.com"
 ;;pop3-mailhost "chm-email2.mc.com"
 pop3-mailhost "exchange.mc.com"
 pop3-maildrop "pfisher"
 pop3-password-required t
 pop3-port 110
 pop3-preserve-inbox nil)
(if (equal (getenv "USER") "phil")
    (load-library "pop-password"))

(defun rmail-without-fetch ()
  (interactive)
  (let ((rmail-fetch-mail-from-server nil))
    (rmail)))

;;(defadvice sendmail-user-agent-compose
;;  (after sendmail-user-agent-advice activate compile)
;;  (auto-fill-mode nil)
;;  (longlines-mode t))

;;(defadvice compose-mail (after compose-mail-advice activate compile)
;;  (auto-fill-mode nil)
;;  (longlines-mode t))

;;(defadvice mail-send (before mail-send-advice activate compile)
;;  (longlines-mode nil))


(defadvice rmail (after rmail-update activate compile)
  (disptime-update))

(defadvice rmail-input (before rmail-input-advice activate compile)
  (interactive
   (let ((rmail-fetch-mail-from-server nil))
     (list (read-file-name "RMAIL file: "
                           "~/rmail/"
                           nil
                           nil
                           "rmail.retired")))))

(defadvice mail-yank-original
  (after mail-yank-original-update activate compile)
  (goto-char 0)
  (when (search-forward mail-header-separator nil t)
    (newline)
    (newline)
    (goto-char (- (point) 1))))

(setq rmail-highlight-face emacs22-line-face)
(require 'etach)

(defun find-next-detach-label ()
  (interactive)
  (if (search-forward-regexp
       (concat
        "\\["
        etach-detached-file-label-separator-string
        "file:"
        "\\([^\]]+\\)\\]")
       nil
       t)
      (goto-char (match-beginning 1))))

(defun find-previous-detach-label ()
  (interactive)
  (if (search-backward-regexp
       (concat
        "\\["
        etach-detached-file-label-separator-string
        "file:"
        "\\([^\]]+\\)\\]")
       nil
       t)
      (goto-char (match-beginning 1))))

(defun delete-detach-label ()
  (interactive)
  (let ((fname
         (replace-regexp-in-string
          "^file:"
          ""
          (thing-at-point 'filename))))
    (if (and (file-readable-p fname)
             (y-or-n-p (concat "delete " fname ": ")))
        (delete-file fname))))

(defun html2txt-detach-label ()
  (interactive)
  (let ((filter (format "%s/scripts/html2txt.rb" (getenv "HOME")))
        (fname
         (replace-regexp-in-string
          "^file:"
          ""
          (thing-at-point 'filename))))
    (if (and (file-readable-p fname)
             (file-executable-p filter)
             (string-match "html$" fname))
        (if (y-or-n-p "insert html/text in current message: ")
            (let ((text
                   (shell-command-to-string (concat "html2txt.rb " fname))))
              (save-excursion
                (rmail-edit-current-message)
                (search-forward-regexp
                 (concat
                  "file:"
                  fname)
                 nil
                 t)
                (beginning-of-line)
                (kill-line)
                (insert text "\n")
                (rmail-cease-edit))
              (if (y-or-n-p (concat "delete " fname ": "))
                  (delete fname)))
          (shell-command (concat "html2txt.rb " fname))))))
