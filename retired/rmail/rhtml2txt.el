;;; rhtml2txt.el --- a simple html to plain text converter hacked by
;;; me and specifically targeted at rmail (still a work in progress)

;; Copyright (C) 2002, 2003, 2004, 2005, 2006, 2007, 2008, 2009, 2010, 2011, 2012 Free Software Foundation, Inc.

;; Author: Joakim Hove <hove@phys.ntnu.no>
;; hacked: me <pfisher@mc.com>

;; This file is part of GNU Emacs.

;; GNU Emacs is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; GNU Emacs is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with GNU Emacs.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;; These functions provide a simple way to wash/clean html infected
;; mails.  Definitely do not work in all cases, but some improvement
;; in readability is generally obtained.  Formatting is only done in
;; the buffer, so the next time you enter the article it will be
;; "re-htmlized".
;;
;; The main function is `rhtml2txt'.

;;; Code:

;;
;; <Global variables>
;;

(eval-when-compile
  (require 'cl))

(defvar rhtml2txt-format-single-element-list nil)

(setq
 rhtml2txt-format-single-element-list
 '(("hr" . rhtml2txt-clean-hr)
   ("base" . delete-region)
   ("col"  . delete-region)
   ("colgroup" . delete-region)
   ("!doctype" . delete-region)))

(defvar rhtml2txt-replace-list nil
   "The map of entity to text.

This is an alist were each element is a dotted pair consisting of an
old string, and a replacement string.  This replacement is done by the
function `rhtml2txt-substitute' which basically performs a
`replace-string' operation for every element in the list.  This is
completely verbatim - without any use of REGEXP.")

(setq
 rhtml2txt-replace-list
 '(("&acute;" . "`")
   ("&amp;" . "&")
   ("&apos;" . "'")
   ("&brvbar;" . "|")
   ("&cent;" . "c")
   ("&circ;" . "^")
   ("&copy;" . "(C)")
   ("&curren;" . "(#)")
   ("&deg;" . "degree")
   ("&divide;" . "/")
   ("&euro;" . "e")
   ("&frac12;" . "1/2")
   ("&gt;" . ">")
   ("&iquest;" . "?")
   ("&laquo;" . "<<")
   ("&ldquo" . "\"")
   ("&lsaquo;" . "(")
   ("&lsquo;" . "`")
   ("&lt;" . "<")
   ("&mdash;" . "--")
   ("&nbsp;" . " ")
   ("&ndash;" . "-")
   ("&permil;" . "%%")
   ("&plusmn;" . "+-")
   ("&pound;" . "£")
   ("&quot;" . "\"")
   ("&raquo;" . ">>")
   ("&rdquo" . "\"")
   ("&reg;" . "(R)")
   ("&rsaquo;" . ")")
   ("&rsquo;" . "'")
   ("&sect;" . "§")
   ("&sup1;" . "^1")
   ("&sup2;" . "^2")
   ("&sup3;" . "^3")
   ("&#43;" . "+")
   ("&#8217;" . "'")
   ("&#8230;" . "...")
   ("&#39;" . "'")
   ("&#8220;" . "\"")
   ("&#8221;" . "\"")
   ("&#8212;" . "--")
   ("&#8211;" . "-")
   ("&middot;" . "*")
   ("&#9642;" . "#")
   ;; another form of non-breaking space
   ("\u00A0" . " ")
   ;; it'd be nice to know the unicode string for this
   ("â€™" . "'")
   ("" . "")
   ("" . "*")
   ("&#8216;" . "'")
   ("&tilde;" . "~")))

(defvar rhtml2txt-remove-tag-list nil
  "A list of removable tags.

This is a list of tags which should be removed, without any
formatting.  Note that tags in the list are presented *without*
any \"<\" or \">\".  All occurrences of a tag appearing in this
list are removed, irrespective of whether it is a closing or
opening tag, or if the tag has additional attributes.  The
deletion is done by the function `rhtml2txt-remove-tags'.

For instance the text:

\"Here comes something <font size\"+3\" face=\"Helvetica\"> big </font>.\"

will be reduced to:

\"Here comes something big.\"

If this list contains the element \"font\".")

(setq
 rhtml2txt-remove-tag-list
 '(
   ;; remove "i" from here if you want <i>-tagged text to show up
   ;; underlined
   "i"
   "span"
   "tt"
   "sup"
   ;; now doing blockquote in the formatted list (see below)
   ;;"blockquote"
   "fieldset"
   ;; now doing html, body and p in the formatted list (see below)
   ;;"html"
   ;;"body"
   ;;"p"
   "img"
   "dir"
   "head"
   ;; now doing div and style in the formatted list (see below)
   ;;"div"
   ;;"style"
   "font"
   "title"
   "tbody"
   "td"
   "tr"
   "meta"))

(defvar rhtml2txt-format-tag-list nil
  "An alist of tags and processing functions.

This is an alist where each dotted pair consists of a tag, and then
the name of a function to be called when this tag is found.  The
function is called with the arguments p1, p2, p3 and p4. These are
demontrated below:

\"<b> This is bold text </b>\"
 ^   ^                 ^    ^
 |   |                 |    |
p1  p2                p3   p4

Then the called function will typically format the text somewhat and
remove the tags.")


(defvar rhtml2txt-format-tag-list2 nil
  "An alist of tags and processing functions.

This is an alist where each dotted pair consists of a tag, and then
the name of a function to be called when this tag is found.  The
function is called with the arguments p1, p2, p3 and p4. These are
demontrated below:

\"<b> This is bold text </b>\"
 ^   ^                 ^    ^
 |   |                 |    |
p1  p2                p3   p4

Then the called function will typically format the text somewhat and
remove the tags.")

(setq
 rhtml2txt-format-tag-list
 '(("pre"        . rhtml2txt-clean-pre)))

(setq
 rhtml2txt-format-tag-list2
 '(("b" 	       . rhtml2txt-clean-bold)
   ("strong"     . rhtml2txt-clean-bold)
   ("u" 	       . rhtml2txt-clean-underline)
   ("i" 	       . rhtml2txt-clean-italic)
   ("em"         . rhtml2txt-clean-italic)
   ("blockquote" . rhtml2txt-clean-blockquote)
   ("a"          . rhtml2txt-clean-anchor)
   ("ul"         . rhtml2txt-clean-ul)
   ("ol"         . rhtml2txt-clean-ol)
   ("dl"         . rhtml2txt-clean-dl)
   ("html"       . rhtml2txt-clean-body)
   ("body"       . rhtml2txt-clean-body)
   ("div"        . rhtml2txt-clean-div)
   ("p"          . rhtml2txt-clean-p)
   ("table"      . rhtml2txt-clean-table)
   ("script"     . rhtml2txt-clean-script)
   ;;("o:p"      . rhtml2txt-clean-OPs)
   ("style"      . rhtml2txt-clean-tags)
   ("center"     . rhtml2txt-clean-center)))

(defvar rhtml2txt-remove-tag-list2 nil
  "Another list of removable tags.

This is a list of tags which are removed similarly to the list
`rhtml2txt-remove-tag-list' - but these tags are retained for the
formatting, and then moved afterward.")

(setq rhtml2txt-remove-tag-list2
  '("li" "dt" "dd" "meta"
    ;; microsoft office generated xml tags
    "u[0-9]?:p"
    ;; this one risky, may cause problems xxx
    ;; in general looking for microsoft xml
    ;; tags (e.g. <sti:smarttags> etc.)
    ".[^: <>]*:.[^ <>]*"
    ))

;;
;; </Global variables>
;;

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;
;; <Utility functions>
;;

(defun rhtml2txt-replace-string (from-string to-string min max)
  "Replace FROM-STRING with TO-STRING in region from MIN to MAX."
  (goto-char min)
  (let ((delta (- (string-width to-string) (string-width from-string)))
	(change 0))
    (while (search-forward from-string max t)
      (replace-match to-string)
      (setq change (+ change delta)))
    change))

(defun rhtml2txt-replace-re-string (from-string to-string min max)
  "Replace regexp FROM-STRING with TO-STRING in region from MIN to MAX."
  (goto-char min)
  (let ((change 0))
    (while (re-search-forward from-string max t)
      (replace-match to-string)
      (setq change
            (+ change
               (- (string-width to-string)
                  (- (match-end 0) (match-beginning 0))))))
    change))

;;
;; </Utility functions>
;;

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;
;; <Functions related to attributes> i.e. <font size=+3>
;;

(defun rhtml2txt-attr-value (list attribute)
  "Get value of ATTRIBUTE from LIST."
  (nth 1 (assoc attribute list)))

(defun rhtml2txt-get-attr (p1 p2)
  (goto-char p1)
  (re-search-forward " +[^ ]" p2 t)
  (let* ((attr-string (buffer-substring-no-properties (1- (point)) (1- p2)))
         (tmp-list (split-string attr-string))
         (attr-list)
         (counter 0)
         (prev (car tmp-list))
         (this (nth 1 tmp-list))
         (next (nth 2 tmp-list))
         (index 1))

    (cond
     ;; size=3
     ((string-match "[^ ]=[^ ]" prev)
      (let* ((pos (1+ (string-match "[^ ]=[^ ]" prev)))
             (attr  (substring prev 0 pos))
             (value (substring prev (1+ pos))))
        (setq attr-list (cons (list attr value) attr-list))))
     ;; size= 3
     ((string-match "[^ ]=\\'" prev)
      (setq attr-list (cons (list (substring prev 0 -1) this) attr-list))))

    (while (< index (length tmp-list))
      (cond
       ;; size=3
       ((string-match "[^ ]=[^ ]" this)
        (let ((attr  (nth 0 (split-string this "=")))
              (value (nth 1 (split-string this "="))))
          (setq attr-list (cons (list attr value) attr-list))))
       ;; size =3
       ((string-match "\\`=[^ ]" this)
        (setq attr-list (cons (list prev (substring this 1)) attr-list)))
       ;; size= 3
       ((string-match "[^ ]=\\'" this)
        (setq attr-list (cons (list (substring this 0 -1) next) attr-list)))
       ;; size = 3
       ((string= "=" this)
        (setq attr-list (cons (list prev next) attr-list))))
      (setq index (1+ index))
      (setq prev this)
      (setq this next)
      (setq next (nth (1+ index) tmp-list)))

    ;;
    ;; Tags with no accompanying "=" i.e. value=nil
    ;;
    (setq prev (car tmp-list))
    (setq this (nth 1 tmp-list))
    (setq next (nth 2 tmp-list))
    (setq index 1)

    (when (and (not (string-match "=" prev))
               this
               (not (string= (substring this 0 1) "=")))
      (setq attr-list (cons (list prev nil) attr-list)))

    (while (< index (1- (length tmp-list)))
      (when (and this
                 (not (string-match "=" this))
                 (not (or (string= (substring next 0 1) "=")
                          (string= (substring prev -1) "="))))
        (setq attr-list (cons (list this nil) attr-list)))
      (setq index (1+ index))
      (setq prev this)
      (setq this next)
      (setq next (nth (1+ index) tmp-list)))

    (when (and this
               (not (string-match "=" this))
               (not (string= (substring prev -1) "=")))
      (setq attr-list (cons (list this nil) attr-list)))
    ;; return - value
    attr-list))

;;
;; </Functions related to attributes>
;;

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;
;; <Functions to be called to format a tag-pair>
;;
(defun rhtml2txt-clean-list-items (p1 p2 list-type)
  (goto-char p1)
  (let ((item-nr 0)
	(items   0))
    (while (search-forward "<li>" p2 t)
      (setq items (1+ items)))
    (goto-char p1)
    (while (< item-nr items)
      (setq item-nr (1+ item-nr))
      (search-forward "<li>" (point-max) t)
      (cond
       ((string= list-type "ul") (insert " o "))
       ((string= list-type "ol") (insert (format " %s: " item-nr)))
       (t (insert " x "))))))

(defun rhtml2txt-clean-dtdd (p1 p2)
  (goto-char p1)
  (let ((items   0)
	(item-nr 0))
    (while (search-forward "<dt>" p2 t)
      (setq items (1+ items)))
    (goto-char p1)
    (while (< item-nr items)
      (setq item-nr (1+ item-nr))
      (re-search-forward "<dt>\\([ ]*\\)" (point-max) t)
      (when (match-string 1)
	(delete-region (point) (- (point) (string-width (match-string 1)))))
      (let ((def-p1 (point))
	    (def-p2 0))
	(re-search-forward "\\([ ]*\\)\\(</dt>\\|<dd>\\)" (point-max) t)
	(if (match-string 1)
	    (progn
	      (let* ((mw1 (string-width (match-string 1)))
		     (mw2 (string-width (match-string 2)))
		     (mw  (+ mw1 mw2)))
		(goto-char (- (point) mw))
		(delete-region (point) (+ (point) mw1))
		(setq def-p2 (point))))
	  (setq def-p2 (- (point) (string-width (match-string 2)))))
	(put-text-property def-p1 def-p2 'face 'bold)))))

(defun rhtml2txt-delete-tags (p1 p2 p3 p4)
  (delete-region p1 p2)
  (delete-region (- p3 (- p2 p1)) (- p4 (- p2 p1))))

(defun rhtml2txt-delete-single-tag (p1 p2)
  (delete-region p1 p2))

(defun rhtml2txt-clean-table (p1 p2 p3 p4)
  (let ((str1 "<br>--[mangled table stuff starts]---<br>")
        (str2 "<br>--[mangled table stuff ends]-----<br>"))
    (goto-char p4)
    (insert str2)
    (rhtml2txt-delete-tags p1 p2 p3 p4)
    (goto-char p1)
    (insert str1)))

(defun rhtml2txt-clean-script (p1 p2 p3 p4)
  (let ((str1 "--[script deleted]---<br>"))
    (delete-region p1 p4)
    (goto-char p1)
    (insert str1)))

(defun rhtml2txt-clean-tags (p1 p2 p3 p4)
  (delete-region p1 p4))

(defun rhtml2txt-clean-OPs (p1 p2 p3 p4)
  (delete-region p1 p4)
  (newline 1))

(defun rhtml2txt-clean-hr (p1 p2)
  (rhtml2txt-delete-single-tag p1 p2)
  (goto-char p1)
  (newline 1)
  (insert (make-string fill-column ?-)))

(defun rhtml2txt-clean-ul (p1 p2 p3 p4)
  (rhtml2txt-delete-tags p1 p2 p3 p4)
  (rhtml2txt-clean-list-items p1 (- p3 (- p1 p2)) "ul"))

(defun rhtml2txt-clean-ol (p1 p2 p3 p4)
  (rhtml2txt-delete-tags p1 p2 p3 p4)
  (rhtml2txt-clean-list-items p1 (- p3 (- p1 p2)) "ol"))

(defun rhtml2txt-clean-dl (p1 p2 p3 p4)
  (rhtml2txt-delete-tags p1 p2 p3 p4)
  (rhtml2txt-clean-dtdd p1 (- p3 (- p1 p2))))

(defun rhtml2txt-clean-center (p1 p2 p3 p4)
  (rhtml2txt-delete-tags p1 p2 p3 p4)
  (center-region p1 (- p3 (- p2 p1))))

(defun rhtml2txt-clean-bold (p1 p2 p3 p4)
  (put-text-property p2 p3 'face 'bold)
  (rhtml2txt-delete-tags p1 p2 p3 p4))

(defun rhtml2txt-clean-title (p1 p2 p3 p4)
  (put-text-property p2 p3 'face 'bold)
  (rhtml2txt-delete-tags p1 p2 p3 p4))

(defun rhtml2txt-clean-underline (p1 p2 p3 p4)
  (put-text-property p2 p3 'face 'underline)
  (rhtml2txt-delete-tags p1 p2 p3 p4))

(defun rhtml2txt-clean-italic (p1 p2 p3 p4)
  (put-text-property p2 p3 'face 'italic)
  (rhtml2txt-delete-tags p1 p2 p3 p4))

(defun rhtml2txt-clean-font (p1 p2 p3 p4)
  (rhtml2txt-delete-tags p1 p2 p3 p4))

(defun rhtml2txt-clean-returns (p1 p2 p3 p4)
  (rhtml2txt-delete-tags p1 p2 p3 p4)
  ;; rhtml2txt-clean-fluff now does this in a single pass for the
  ;; entire region being converted
  (when 0
    (setq p4 (+ p1 (- p3 p2)))
    (goto-char p1)
    (while (re-search-forward "\\(^\\s-\\s-+\\)" p4 t)
      (let ((delta (- (match-end 0) (match-beginning 0))))
        (replace-match "")
        (setq p4 (- p4 delta))))
    (goto-char p1)
    ;;  (while (re-search-forward "\n" p4 t)
    ;;    (let ((delta (- (match-end 0) (match-beginning 0))))
    ;;      (replace-match "")
    ;;      (setq p4 (- p4 delta))))
    (while (re-search-forward "\\(\n\\|<o:p>[^<>]*<o:p>\\)" p4 t)
      (let ((delta (- (match-end 0) (match-beginning 0))))
        (replace-match "")
        (setq p4 (- p4 delta))))))

(defun rhtml2txt-clean-div (p1 p2 p3 p4)
  (goto-char p1)
  ;; xxx a real hack
  (while
      (re-search-forward
       "\\(margin-\\(top\\|bottom\\):\\s-*0+\\(.00+\\)?.*?\\(in\\|pt\\)\\)"
       p2
       t)
    (let ((delta (- (match-end 0) (match-beginning 0))))
      (replace-match "")
      (setq p2 (- p2 delta)
            p3 (- p3 delta)
            p4 (- p4 delta))))
  (goto-char p1)
  (let ((top (re-search-forward "margin-top" p2 t))
        need-br)
    (goto-char p2)
    ;; xxx an even bigger hack
    (let ((p3 (if (re-search-forward "<div" p3 t) (match-beginning 0) p3)))
      (goto-char p2)
      (setq need-br
            (not (re-search-forward "\\(<p\\( \\|>\\)\\)" p3 t))))
    (when need-br
      (goto-char p1)
      (when (re-search-forward "margin-bottom" p2 t)
        (goto-char p4)
        (insert "<br>")))
    (rhtml2txt-clean-returns p1 p2 p3 p4)
    (when need-br
      (goto-char p1)
      (insert "<br>")
      (when top
        (insert "<br>")))))

(defun rhtml2txt-clean-blockquote (p1 p2 p3 p4)
  (rhtml2txt-clean-returns p1 p2 p3 p4)
  (goto-char p1)
  (insert "<br>"))

(defun rhtml2txt-clean-p (p1 p2 p3 p4)
  (goto-char p2)
  (let ((need-br (not (re-search-forward "\\(<div\\)" p3 t))))
    ;;(when need-br
      ;;(goto-char p4)
      ;;(insert "<br>"))
    (rhtml2txt-clean-returns p1 p2 p3 p4)
    (when need-br
      (goto-char p1)
      (insert "<br>"))
    )
  )

(defun rhtml2txt-clean-pre (p1 p2 p3 p4)
  (rhtml2txt-delete-tags p1 p2 p3 p4)
  (setq p4 (+ p1 (- p3 p2)))
  (goto-char p1)
  (while (re-search-forward "\n" p4 t)
    (let ((delta (- (match-end 0) (match-beginning 0))))
      (replace-match "<br>")
      (setq p4 (- p4 delta))
      (setq p4 (+ p4 4))
      ))
  (goto-char p1)
  (while (re-search-forward " " p4 t)
    (let ((delta (- (match-end 0) (match-beginning 0))))
      (replace-match "&nbsp;")
      (setq p4 (- p4 delta))
      (setq p4 (+ p4 6))
      ))
  (goto-char p4)
  (insert "<br>")
  (goto-char p1)
  (insert "<br>"))

(defun rhtml2txt-clean-body (p1 p2 p3 p4)
  (rhtml2txt-clean-returns p1 p2 p3 p4))

(defun rhtml2txt-clean-anchor (p1 p2 p3 p4)
  ;; If someone can explain how to make the URL clickable I will surely
  ;; improve upon this.
  ;; Maybe `goto-addr.el' can be used here.
  (let* ((attr-list (rhtml2txt-get-attr p1 p2))
         (href (rhtml2txt-attr-value attr-list "href")))
    (if attr-list
        (delete-region p1 p4)
      (rhtml2txt-remove-tags '("a")))
    (when href
      (goto-char p1)
      (insert (substring href 1 -1 ))
      (put-text-property p1 (point) 'face 'bold))))

;;
;; </Functions to be called to format a tag-pair>
;;

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;
;; <Functions to be called to fix up paragraphs>
;;

(defun rhtml2txt-fix-paragraph (p1 p2)
  (goto-char p1)
  (let ((refill-start)
        (refill-stop))
    (when (re-search-forward "<br>$" p2 t)
      (goto-char p1)
      (when (re-search-forward ".+[^<][^b][^r][^>]$" p2 t)
        (beginning-of-line)
        (setq refill-start (point))
        (goto-char p2)
        (re-search-backward ".+[^<][^b][^r][^>]$" refill-start t)
        (forward-line 1)
        (end-of-line)
        ;; refill-stop should ideally be adjusted to
        ;; accommodate the "<br>" strings which are removed
        ;; between refill-start and refill-stop.  Can simply
        ;; be returned from my-replace-string
        (setq refill-stop (+ (point)
                             (rhtml2txt-replace-string
                              "<br>" ""
                              refill-start (point))))
        ;; (message "Point = %s  refill-stop = %s" (point) refill-stop)
        ;; (sleep-for 4)
        (fill-region refill-start refill-stop))))
  (rhtml2txt-replace-string "<br>" "" p1 p2))

;;
;; This one is interactive ...
;;
(defun rhtml2txt-fix-paragraphs ()
  "This _tries_ to fix up the paragraphs - this is done in quite a ad-hook
fashion, quite close to pure guess-work. It does work in some cases though."

  ;; xxx hack to deal with mid-line <br>s
  (goto-char (point-min))
  (let ((regexp "\\(<br[ ]?/?>\\)"))
    (while (re-search-forward (format ".%s." regexp) nil t)
      (replace-match "\n" nil nil nil 1)
      ;; if the original <br> was mid-line, and there was
      ;; a string of more than one of them, keep replacing
      ;; them with newlines
      (while (and (looking-at regexp)
                  (re-search-forward regexp nil t))
        (replace-match "\n" nil nil nil 1))))

  (goto-char (point-min))
  (while (re-search-forward "^<br>$" nil t)
    (delete-region (match-beginning 0) (match-end 0)))

  ;; Removing lonely <br> on a single line, if they are left intact we
  ;; dont have any paragraphs at all.
  (goto-char (point-min))
  (while (not (eobp))
    (let ((p1 (point)))
      (forward-paragraph 1)
      ;;(message "Kaller fix med p1=%s  p2=%s " p1 (1- (point))) (sleep-for 5)
      (rhtml2txt-fix-paragraph p1 (1- (point)))
      (goto-char p1)
      (when (not (eobp))
	(forward-paragraph 1)))))

;;
;; </Functions to be called to fix up paragraphs>
;;

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;
;; <Interactive functions>
;;

(defun rhtml2txt-remove-tags (tag-list)
  "Removes the tags listed in the list `rhtml2txt-remove-tag-list'.
See the documentation for that variable."
  (dolist (tag tag-list)
    (goto-char (point-min))
    (while (re-search-forward (format
                               "\\(</?%s[^>]*>\\)"
                               tag)
                              (point-max)
                              t)
      (delete-region (match-beginning 0) (match-end 0)))))

(defun rhtml2txt-format-tags (tag-list)
  "See the variable `rhtml2txt-format-tag-list' for documentation."
  (dolist (tag-and-function tag-list)
    (let ((tag      (car tag-and-function))
          (function (cdr tag-and-function)))
      (goto-char (point-min))
      (while (re-search-forward
              (format
               ;;"\\(<%s\\( [^>]*\\)?>\\)"
               "\\(<%s\\(\\($\\| \\)\\([^>]\\|\n\\)*\\)?>\\)"
               tag)
              (point-max) t)
        (let ((p1)
              (p2 (point))
              (p3) (p4))
          (search-backward "<" (point-min) t)
          (setq p1 (point))
          (unless (search-forward (format "</%s>" tag) (point-max) t)
            (goto-char p2)
            (insert (format "</%s>" tag)))
          (setq p4 (point))
          (search-backward "</" (point-min) t)
          (setq p3 (point))
          (funcall function p1 p2 p3 p4)
          (goto-char p1))))))

(defun rhtml2txt-substitute ()
  "See the variable `rhtml2txt-replace-list' for documentation."
  (interactive)
  (dolist (e rhtml2txt-replace-list)
    (goto-char (point-min))
    (let ((old-string (car e))
	  (new-string (cdr e)))
      (rhtml2txt-replace-string
       old-string new-string (point-min) (point-max)))))

(defun rhtml2txt-format-single-elements ()
  (dolist (tag-and-function rhtml2txt-format-single-element-list)
    (let ((tag      (car tag-and-function))
          (function (cdr tag-and-function)))
      (goto-char (point-min))
      (while (re-search-forward (format "\\(<%s\\( [^>]*\\)?>\\)" tag)
                                (point-max) t)
        (let ((p1)
              (p2 (point)))
          (search-backward "<" (point-min) t)
          (setq p1 (point))
          (funcall function p1 p2))))))

(defun rhtml2txt-remove-comments ()
  (goto-char (point-min))
  (while (re-search-forward "\\(<!--\\)" (point-max) t)
    (let ((p1 (match-beginning 0)))
      (if (re-search-forward "\\(-->\\)" (point-max) t)
          (delete-region p1 (match-end 0))))))

(defun rhtml2txt-odds-and-sods ()
  (let ((min (point-min))
        (max (point-max)))
    ;; this shouldn't be necessary. Tags like this,
    ;;
    ;;    <![if ...]>
    ;;    <![endif ...]>
    ;;
    ;; should only show up in properly formed comments (or within xml
    ;; regions). But they occassionly show up in the wrong place so we
    ;; have to be proactive.
    (rhtml2txt-replace-re-string
     "\\(<!\\[[^>]*>\\)"
     ""
     min max)))

(defun rhtml2txt-trim-blank-lines ()
  "replace multiple blank lines with a single one"
  (goto-char (point-min))
  ;; get rid of a first blank line
  (when (looking-at "^\\s-*\n")
    (re-search-forward "^\\s-*\n" nil t)
    (replace-match ""))
  (while (re-search-forward "\\(^\\s-*$\\)\n" (point-max) t)
    (replace-match "\n")))

(defun rhtml2txt-strip-blank-lines ()
  (goto-char (point-min))
  (when (search-forward "<html" nil t)
    (let ((p1 (match-end 0)))
      (when (search-forward "</html>" nil t)
        (let ((p2 (match-beginning 0)))
          (save-restriction
            (narrow-to-region p1 p2)
            (goto-char p1)
            (while (and (not (eobp))
                        (re-search-forward "\\(^\\s-*$\\)\n" (point-max) t))
              (replace-match "")
              (if (not (eobp))
                  (forward-char 1)))))))))

(defun rhtml2txt-strip-newlines ()
  (goto-char (point-min))
  (while (re-search-forward "\\(\n\\|\\s-\\s-+\\)" nil t)
    (replace-match "")))

;; remove whitespace at start and end of lines, newlines and microsoft
;; xml newline junk... I should probably see if I can make this more
;; efficient
(defun rhtml2txt-sanitize ()
  (goto-char (point-min))
  (while
      (re-search-forward
       ">?\\(\\s-*\n\\s-*\\|\\s-\\s-+\\|<o:p>[^<>]*<o:p>\\)"
       nil t)
    (let ((beg (match-beginning 0)))
      (if (equal (buffer-substring beg (+ beg 1)) ">")
          (replace-match ">")
        (replace-match " ")))))

(defun rhtml2txt-create-paragraphs()
  (rhtml2txt-replace-re-string "<br[^>]*>" "\n" (point-min) (point-max))
  ;;(rhtml2txt-replace-re-string "<br[ ]?/?>" "\n" (point-min) (point-max))
  ;;(rhtml2txt-replace-re-string "<br [^>]*>" "\n" (point-min) (point-max))
  )

;;
;; Main function
;;

;;;###autoload
(defun rhtml2txt ()
  "Convert HTML to plain text in the current buffer."
  (interactive)
  (save-excursion
    (let ((case-fold-search t)
          (buffer-read-only)
          (tagexp "<\\(html\\|head\\|body\\|meta\\|table\\|!doctype\\)"))
      (if (and
           (not
            (re-search-forward tagexp nil t))
           (not
            (re-search-forward "^\\[[0-9][^:]*:text/html Hide.*\\]$" nil t)))
          ;; even emails with no html can benfit from this
          (rhtml2txt-substitute)
        (goto-char (match-beginning 0))
        (while
            (or
             (re-search-forward tagexp nil t)
             (re-search-forward
              "^\\(\\[[0-9][^:]*:text/html Hide.*\\]\\)$"
              nil t))
          (save-restriction
            (let ((tag (buffer-substring (match-beginning 1) (match-end 1))))
              (narrow-to-region
               (match-beginning 0)
               (if (or
                    (and tag (search-forward (concat tag ">") nil t))
                    (re-search-forward "^\\[[0-9][^:]*:\\-w* .*\\]$" nil t))
                   (match-end 0)
                 (point-max)))

              ;;(rhtml2txt-strip-blank-lines)
              ;;(rhtml2txt-strip-newlines)

              (rhtml2txt-remove-comments)
              (rhtml2txt-format-tags rhtml2txt-format-tag-list)
              (rhtml2txt-remove-tags rhtml2txt-remove-tag-list)
              (rhtml2txt-sanitize)
              (rhtml2txt-format-tags rhtml2txt-format-tag-list2)
              (rhtml2txt-remove-tags rhtml2txt-remove-tag-list2)

              ;;must follow tag processing
              (rhtml2txt-substitute)
              (rhtml2txt-format-single-elements)
              (rhtml2txt-odds-and-sods)

              ;;(rhtml2txt-fix-paragraphs)
              ;; somtimes fix paragraphs misses a <br> or two; especially
              ;; ones like this <br xxxxx>, or ones that are broken
              ;; up across two lines

              ;; process <br>s (in all their various forms)
              (rhtml2txt-create-paragraphs)
              (rhtml2txt-trim-blank-lines)

              (goto-char (point-max))
              )))))))

;;
;; </Interactive functions>
;;
(provide 'rhtml2txt)
;; arch-tag: e9e57b79-35d4-4de1-a647-e7e01fe56d1e
;;; rhtml2txt.el ends here
