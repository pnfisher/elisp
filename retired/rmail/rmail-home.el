;; setup mail stuff for home
(require 'rmail)
(load "rmailsum")

(require 'smtpmail)
(setq smtpmail-smtp-server "localhost")
(setq smtpmail-smtp-service 2025)
;;(setq smtpmail-smtp-server "smtp1.sympatico.ca")
(setq smtpmail-local-domain "mc.com")
(setq smtpmail-sendto-domain "mc.com")
(setq user-mail-address "pfisher@mc.com")
(setq send-mail-function 'smtpmail-send-it)

(setq rmail-file-name "~/rmail/RMAIL")
(setq rmail-default-rmail-file "~/rmail/rmail.current")
(setq rmail-delete-after-output t)
(define-key rmail-mode-map [?\M-r] 'rmail-search-backwards)
(define-key rmail-mode-map "F" 'mime-forward)
(define-key rmail-summary-mode-map "F" 'mime-forward)
(define-key rmail-mode-map "D" 'detach)
(define-key rmail-summary-mode-map "D" 'detach)
(define-key rmail-mode-map "G" 'pop3-get-new-mail)
(define-key rmail-summary-mode-map "G" 'pop3-get-new-mail)
(define-key rmail-mode-map "O"
  '(lambda ()
     (interactive nil)
     (rmail-output-to-rmail-file "~/rmail/rmail.current")))
(define-key rmail-mode-map "d"
  '(lambda ()
     (interactive nil)
     (if (equal (buffer-name) "rmail.deleted")
         (rmail-delete-forward)
       (rmail-output-to-rmail-file "~/rmail/rmail.deleted"))))

(defadvice rmail-resend (before safe-rmail-resend activate compile)
  (if (not (string-equal mode-name "RMAIL"))
      (error "Sorry: not in RMAIL buffer")))

(defadvice rmail-input (before rmail-input-advice activate compile)
  (interactive
   (list (read-file-name "RMAIL file: "
                         "~/rmail/mercury/"
                         nil
                         nil
                         "rmail.retired"))))

(setq rmail-primary-inbox-list `("~/rmail/pop3"))
(require 'pop3)
(defadvice rmail (before rmail-pop3 activate compile)
  (pop3-get-new-mail))

(defadvice rmail (after rmail-update activate compile)
  (disptime-update))

(defadvice mail-yank-original
  (after mail-yank-original-update activate compile)
  (goto-char 0)
  (if (search-forward mail-header-separator)
      (progn
        (newline)
        (newline)
        (goto-char (- (point) 1)))))

(setq rmail-highlight-face emacs22-line-face)
(require 'etach)
