(require 'rmail)
;; so we can tweak the summary mode map below
(load "rmailsum")
(require 'smtpmail)
(require 'pop3)
(autoload 'rhtml2txt "rhtml2txt" nil t)

(if emacs-at-work
    (custom-set-variables
     '(pop3-leave-mail-on-server nil)
     '(pop3-mailhost "exchange.mc.com")
     '(pop3-maildrop "pfisher")
     '(pop3-password-required t)
     '(pop3-port 110))
  (custom-set-variables
   '(pop3-leave-mail-on-server t)
   '(pop3-mailhost "localhost")
   '(pop3-maildrop "pfisher")
   '(pop3-password-required t)
   '(pop3-port 2110)))

(if (equal (getenv "USER") "phil")
    (load-library "pop-password"))

(custom-set-variables
  ;; custom-set-variables was added by Custom.
  ;; If you edit it by hand, you could mess it up, so be careful.
  ;; Your init file should contain only one such instance.
  ;; If there is more than one, they won't work right.
 '(rmail-preserve-inbox nil)
 '(rmail-primary-inbox-list `("~/rmail/pop3"))
 '(mail-user-agent 'sendmail-user-agent)
 '(mail-self-blind t)
 '(mail-yank-ignored-headers nil)
 '(mail-signature t)
 '(message-yank-prefix "")
 '(mail-yank-prefix nil)
 '(mail-indentation-spaces 0)
 '(send-mail-function 'smtpmail-send-it)
 '(smtpmail-local-domain "mc.com")
 '(smtpmail-sendto-domain "mc.com")
 '(user-mail-address "pfisher@mc.com")
 ;;'(rmail-default-rmail-file "~/rmail/rmail.current")
 '(rmail-delete-after-output nil)
 '(rmail-file-name "~/rmail/RMAIL")
 '(rmail-highlight-face emacs22-line-face)
 '(rmail-mime-attachment-dirs-alist
   `(("text/.*" "~/rmail/detached")
    ("image/.*" "~/rmail/detached")
    (".*" "~/rmail/detached"))))

(if emacs-at-work
    (custom-set-variables
     '(smtpmail-smtp-server "exchange.mc.com")
     '(smtpmail-smtp-service 25))
  (custom-set-variables
   '(smtpmail-smtp-server "localhost")
   '(smtpmail-smtp-service 2025)))

(defvar rmail-fetch-mail-from-server t)
(defvar rmail-strip-html t)
(defvar rmail-wrap-lines t)
(defvar rmail-html-mime-unhide t)

(defun rmail-toggle-html ()
  (interactive)
  (setq rmail-strip-html (not rmail-strip-html))
  (when (equal major-mode 'rmail-mode)
    (rmail-show-message)))

(defun rmail-toggle-wrap ()
  (interactive)
  (setq rmail-wrap-message (not rmail-wrap-message)))

(defun rmail-toggle-mime-unhide ()
  (interactive)
  (setq rmail-html-mime-unhide (not rmail-html-mime-unhide)))

(defun rmail-do-wrap-lines ()
  (interactive)
  (if rmail-wrap-lines
      (save-excursion
        (set (make-local-variable 'fill-column) (- (window-width) 1))
        (beginning-of-buffer)
        (let ((buffer-read-only))
          (when (re-search-forward "^\\s-*$")
            (while (not (eobp))
              (forward-line)
              (beginning-of-line)
              (let ((p1 (point)))
                (end-of-line)
                (let ((p2 (point)))
                  (save-excursion
                    (fill-region p1 p2 nil t))))))))))

(defadvice rmail-mime-toggle-button
  (around html-rmail-toggle-button-advice activate compile)
  (if rmail-strip-html
      (let ((pos (point)))
        (end-of-line)
        (let ((end (point)))
          (beginning-of-line)
          (let* ((beg (point))
                 (tag (buffer-substring beg end)))
            ;; find the mime tag without the actual button
            ;; (i.e. Hide or Show)
            (if (not (string-match "^\\(\\[[^:]*:[^ ]* \\)" tag))
                (error (concat "search for sub-tag within " tag " failed")))
            (let ((tag2 (match-string 1 tag))
                  ;; suppress html stripping for the next call
                  ;; to rmail-show-message... rmailmm.el needs
                  ;; to see the buffer in it's non-stripped state
                  ;; to carry-out the next button push
                  (rmail-strip-html nil))
              (rmail-show-message)
              ;; search for the tag again, the call to
              ;; rmail-show-message may have changed the state of the
              ;; button (i.e.  from Hide to Show) because we've made a
              ;; previous call to rmail-do-html-mime-unhide which has
              ;; altered the buffer's default state, in which case
              ;; we're finished.  If the button is still in its
              ;; original state then we need to push it
              (if (search-forward tag nil t)
                  (progn
                    (forward-char (- pos end))
                    (push-button))
                ;; the button has already changed state, so just
                ;; search for the original tag without the button in
                ;; order to get us back to our original position in
                ;; the buffer (the call to rmail-show-message above
                ;; always takes us back to the start of the buffer)
                (if (not (search-forward tag2 nil t))
                    (error (concat "search for " tag2 " failed"))))
              (rhtml2txt)
              (rmail-do-wrap-lines)
              (beginning-of-line)
              (forward-char (- pos beg))))))
    ad-do-it
    (when rmail-strip-html
      (error "xxx how did we get here?")
      (rhtml2txt)
      (rmail-do-wrap-lines))))

(defadvice rmail-mime
  (before rmail-mime-advice activate compile)
  (when rmail-strip-html
    (let ((rmail-strip-html nil))
      (rmail-show-message))))

(defun rmail-do-html-mime-unhide ()
  (if rmail-html-mime-unhide
      (save-excursion
        (beginning-of-buffer)
        (while (re-search-forward
                "^\\[[^:]*:text/\\(html Show\\|calendar Hide\\)\\]"
                nil
                t)
          (beginning-of-line)
          (forward-button 1)
          (let ((rmail-strip-html nil))
            (push-button))
          (end-of-line)))))

(defun rmail-do-strip-html ()
  (rmail-do-html-mime-unhide)
  (rhtml2txt)
  (rmail-do-wrap-lines))

(defadvice rmail-summary-by-regexp
  (around rmail-summary-by-regexp-advice activate compile)
  (let ((rmail-enable-mime nil))
    ad-do-it))

(defadvice rmail-show-message-1
  (after html-rmail-show-message-1 activate compile)
  (if rmail-strip-html
      (rmail-do-strip-html)))

(defadvice rmail-resend (around safe-rmail-resend activate compile)
  (if (not (string-equal mode-name "RMAIL"))
      (error "Sorry: not in RMAIL buffer"))
  (let ((pos (point)))
    (let ((rmail-enable-mime nil)
          (rmail-strip-html nil))
      (rmail-show-message)
      ad-do-it)
    (rmail-show-message)
    (goto-char pos)))

(defun rmail-resend-aux ()
  (interactive)
  (let ((addr (read-buffer "rmail-resend address: " "pfisher@mc.com")))
    (rmail-resend addr)))

(defun rmail-without-fetch ()
  (interactive)
  (let ((rmail-fetch-mail-from-server nil))
    (rmail)))

;;(defadvice sendmail-user-agent-compose
;;  (after sendmail-user-agent-advice activate compile)
;;  (auto-fill-mode nil)
;;  (longlines-mode t))

;;(defadvice compose-mail (after compose-mail-advice activate compile)
;;  (auto-fill-mode nil)
;;  (longlines-mode t))

;;(defadvice mail-send (before mail-send-advice activate compile)
;;  (longlines-mode nil))

;; there seems to be a bug in 23.4's rmail. When it deletes the last
;; message in the RMAIL file, it leaves a blank line at the start of
;; the file; for rmail-convert-file-maybe, this means the file is
;; using an invalid mbox format
(defadvice rmail-convert-file-maybe
  (before rmail-convert-file-maybe-advice activate compile)
  (widen)
  (goto-char (point-min))
  (when (and (looking-at "\\(^\\s-*$\\)\n")
             (re-search-forward "\\(^\\s-*$\\)\n" nil t))
    (let ((buffer-read-only nil))
      (replace-match "")
      (goto-char (point-min))))
  (when (and (file-exists-p rmail-file-name)
             (not (equal (file-modes rmail-file-name) #o600)))
    (chmod rmail-file-name #o600)))

(defadvice rmail-input (before rmail-input-advice activate compile)
  (interactive
   (let ((rmail-fetch-mail-from-server nil))
     (list (read-file-name "RMAIL file: "
                           "~/rmail/"
                           nil
                           nil
                           "rmail.retired")))))

(defadvice rmail (before rmail-pop3-advice activate compile)
  (when (or emacs-at-work
            (y-or-n-p "Fetch new mail from exchange: "))
    (let ((pop3-password (rmail-get-remote-password 'POP)))
      (if emacs-at-work
          (pop3-movemail (car rmail-primary-inbox-list))
        (let ((pop3-leave-mail-on-server
               (not (y-or-n-p "Delete mail on server: "))))
          (pop3-movemail (car rmail-primary-inbox-list)))))))

(defadvice rmail (after rmail-update-advice activate compile)
  (disptime-update))

(defadvice mail-yank-original
  (around mail-yank-original-advice activate compile)
  (goto-char (point-max))
  (insert "\n\n-----Original Message-----\n")
  (let ((header (point)))
    ad-do-it
    (goto-char header))
  (let ((case-fold-search t))
    (while (not (looking-at "^\\s-*$"))
      (if (not (looking-at "\\(from\\|to\\|subject\\|date\\|cc\\): "))
          (let ((p1 (point)))
            (forward-line)
            (delete-region p1 (point)))
        (forward-line)
        (while (and (not (looking-at "\\s-*$"))
                    (looking-at "^\\s-"))
          (forward-line))))
    (goto-char (point-min))
    (when (search-forward mail-header-separator nil t)
      (forward-char 1))))

;;(define-key rmail-summary-mode-map "F" 'mime-forward)
;;(define-key rmail-summary-mode-map "D" 'detach)
(define-key rmail-mode-map [?\M-r] 'rmail-search-backwards)
;;(define-key rmail-mode-map "F" 'mime-forward)
;;(define-key rmail-mode-map "D" 'detach)
;;(define-key rmail-mode-map "N" 'find-next-detach-label)
;;(define-key rmail-mode-map "P" 'find-previous-detach-label)
;;(define-key rmail-mode-map "X" 'delete-detach-label)
(define-key rmail-mode-map "W" 'rmail-do-wrap-lines)
(define-key rmail-mode-map "H" 'rhtml2txt)
;;(define-key rmail-mode-map [?\C-c ?h] 'html2txt-detach-label)
;;(define-key rmail-mode-map [?\C-c ?H] 'html2txt-detach-label)
(define-key rmail-mode-map "R" 'rmail-resend-aux)
(define-key rmail-mode-map "L" 'rmail-add-label)
(define-key rmail-summary-mode-map "L" 'rmail-summary-add-label)
(define-key rmail-mode-map "V" 'rmail-toggle-html)
(define-key rmail-mode-map "O" 'rmail-output-body-to-file)
(define-key rmail-summary-mode-map "O" 'rmail-output-body-to-file)

(define-key rmail-summary-mode-map "I"
  '(lambda ()
     (interactive nil)
     (rmail-summary-add-label "NB")))

(define-key rmail-mode-map "I"
  '(lambda ()
     (interactive nil)
     (rmail-add-label "NB")))

(define-key rmail-summary-mode-map "T"
  '(lambda ()
     (interactive nil)
     (rmail-summary-add-label "TEMP")))

(define-key rmail-mode-map "T"
  '(lambda ()
     (interactive nil)
     (rmail-add-label "TEMP")))

(define-key rmail-summary-mode-map "M"
  '(lambda ()
     (interactive nil)
     (rmail-summary-add-label "Attachments")))

(define-key rmail-mode-map "M"
  '(lambda ()
     (interactive nil)
     (rmail-add-label "Attachments")))

(define-key rmail-summary-mode-map "A"
  '(lambda ()
     (interactive nil)
     (let ((rmail-fetch-mail-from-server nil))
       (rmail-summary-input "~/rmail/rmail.archive"))))

(define-key rmail-mode-map "A"
  '(lambda ()
     (interactive nil)
     (let ((rmail-fetch-mail-from-server nil))
       (rmail-input "~/rmail/rmail.archive"))))

(define-key rmail-summary-mode-map "D"
  '(lambda ()
     (interactive nil)
     (let ((rmail-fetch-mail-from-server nil))
       (rmail-summary-input "~/rmail/rmail.deleted"))))

(define-key rmail-mode-map "D"
  '(lambda ()
     (interactive nil)
     (let ((rmail-fetch-mail-from-server nil))
       (rmail-input "~/rmail/rmail.deleted"))))

(define-key rmail-summary-mode-map "a"
  '(lambda ()
     (interactive nil)
     (let ((rmail-delete-after-output t)
           (rmail-html-mime-unhide nil))
       (rmail-summary-output "~/rmail/rmail.archive" 1))))

(define-key rmail-mode-map "a"
  '(lambda ()
     (interactive nil)
     (if (equal (buffer-name) "rmail.archive")
         (rmail-delete-forward)
       (let ((rmail-delete-after-output t)
             (rmail-html-mime-unhide nil))
         (rmail-output "~/rmail/rmail.archive")))))

(define-key rmail-summary-mode-map "d"
  '(lambda ()
     (interactive nil)
     (let ((rmail-delete-after-output t)
           (rmail-html-mime-unhide nil))
       (rmail-summary-output "~/rmail/rmail.deleted" 1))))

(define-key rmail-mode-map "d"
  '(lambda ()
     (interactive nil)
     (if (equal (buffer-name) "rmail.deleted")
         (rmail-delete-forward)
       (let ((rmail-delete-after-output t)
             (rmail-html-mime-unhide nil))
         (rmail-output "~/rmail/rmail.deleted")))))
