;; -*-eshell-*- ;;
(require 'eshell)
(custom-set-variables
 '(eshell-prompt-function 
   '(lambda ()
      (concat (getenv "HOST") ":" (eshell/pwd) " % ")))
 '(eshell-prompt-regexp "^[^:]*:[^%]*% "))

(custom-set-faces
 '(eshell-prompt-face
   ((((class color) (background dark)) (:foreground "Gray85")))))

;;(defun eshell/ct (&rest args)
;;  (eshell-command-result (concat "cleartool " 
;;                                 (eshell-flatten-and-stringify args))))

;;(defun eshell/lsco ()
;;  (eshell-command-result "cleartool lsco -cview -short -avobs"))

