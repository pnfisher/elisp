(require 'view)

(defvar linuxce-mode-map
  (let ((map (make-sparse-keymap)))
    (suppress-keymap map)
    (set-keymap-parent map view-mode-map)
    (define-key map "n" 'linuxce-next-url)
    (define-key map "p" 'linuxce-prev-url)
    (define-key map "\n" 'linuxce-noprompt)
    (define-key map "\r" 'linuxce-noprompt)
    (define-key map "g" 'linuxce-noprompt)
    (define-key map "f" '(lambda ()
                           (interactive)
                           (when view-last-regexp
                             (re-search-forward view-last-regexp nil t))))
    (define-key map "r" '(lambda ()
                           (interactive)
                           (when view-last-regexp
                             (re-search-backward view-last-regexp nil t))))
    map))

(defun linuxce-mode ()
  (kill-all-local-variables)
  (setq mode-name "LinuxCE")
  (setq major-mode 'linuxce-mode)
  ;;(use-local-map linuxce-mode-map)
  (view-mode)
  (set (make-local-variable 'view-no-disable-on-exit) t)
  ;; With Emacs 22 `view-exit-action' could delete the selected window
  ;; disregarding whether the help buffer was shown in that window at
  ;; all.  Since `view-exit-action' is called with the help buffer as
  ;; argument it seems more appropriate to have it work on the buffer
  ;; only and leave it to `view-mode-exit' to delete any associated
  ;; window(s).
  (setq view-exit-action
        (lambda (buffer)
          ;; Use `with-current-buffer' to make sure that `bury-buffer'
          ;; also removes BUFFER from the selected window.
          (with-current-buffer buffer
            (bury-buffer))))
  (set (make-local-variable 'minor-mode-overriding-map-alist)
       (list (cons 'view-mode linuxce-mode-map))))

;; if you want to specify no url and you can't just enter
;; return because you're being prompted with a default
(defun linuxce-no-url (url)
  (if (string-match "^\\(\.\\|-\\)?$" url) t nil))

(defun linuxce-next-url ()
  (interactive)
  (if (not
       (re-search-forward
        "\\[\\(wiki\\|http\\):.*?\\]"
        nil t))
      (message "no more urls")
    (goto-char (match-beginning 1))))

(defun linuxce-prev-url ()
  (interactive)
  (if (not
       (re-search-backward
        "\\[\\(wiki\\|http\\):.*?\\]"
        nil t))
      (message "no more urls")
    (goto-char (match-beginning 1))))

(defun linuxce-fetch (url search)
  (let ((buf (get-buffer-create "*LinuxCE*")))
    (switch-to-buffer buf)
    (goto-char (point-min))
    (setq buffer-read-only nil)
    (delete-region (point-min) (point-max))
    (let ((proc (start-process-shell-command
                 "LinuxCE"
                 (current-buffer)
                 (concat
                  "/h/phil/code/python/linuxce.py"
                  (when (not (equal url ""))
                    (concat " -u " url))
                  (when (and search (not (equal search "")))
                    (concat " -s " search))))))
	    (set-process-filter proc 'linuxce-insertion-filter)
	    (set-process-sentinel proc 'linuxce-insertion-sentinel)
      (linuxce-mode))))

(defun linuxce-prompt (url)
  (interactive)
  (let ((new-url (read-buffer "url: " url)))
    (when (linuxce-no-url new-url)
      (let ((search (read-from-minibuffer "search: ")))
        (linuxce-fetch new-url search)))))

(defun linuxce-noprompt ()
  (interactive)
  (let ((line (thing-at-point 'line))
        url)
    (if (string-match "\\(http://.*?\\)\\( \\|\\]\\)" line)
        (setq url (match-string 1 line))
      (if (string-match "\\(wiki:.*?\\)\\( \\|\\]\\)" line)
          (setq url (match-string 1 line))))
    (when url
      (linuxce-fetch url nil))))

(defun linuxce ()
  (interactive)
  (let ((line (thing-at-point 'line))
        url)
    (if (string-match "\\(http://.*?\\)\\( \\|\\]\\)" line)
        (setq url (match-string 1 line))
      (if (string-match "\\(wiki:.*?\\)\\( \\|\\]\\)" line)
          (setq url (match-string 1 line))))
    (linuxce-prompt url)))

(defun linuxce-insertion-filter (proc string)
  ;;(display-buffer (process-buffer proc))
  (with-current-buffer (process-buffer proc)
    (save-excursion
      ;; Insert the text, advancing the process marker.
      (goto-char (process-mark proc))
      (let ((buffer-read-only nil))
        (insert string))
      (set-marker (process-mark proc) (point)))))

(defun linuxce-insertion-sentinel (proc msg)
  (if (memq (process-status proc) '(exit signal))
      (let ((buffer (process-buffer proc)))
        (if (null (buffer-name buffer))
            ;; buffer killed
            (set-process-buffer proc nil)
          (with-current-buffer buffer
            ;;(goto-char (point-min))
            ;;(setq buffer-read-only t)
            (delete-process proc))))))
