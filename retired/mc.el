(when emacs-at-work
  (load-library "clear-util.el")
  (if (file-exists-p "/usr/atria/bin/cleartool")
      (load-library "clear-local")
    (load-library "clear-remote")))


(let ((find-file-special-alist
       '(("elisp" . "~/elisp")
         ("html" . "~/elisp/rhtml2txt.el")
         ("scripts" . "~/scripts")
         ("bscripts" . "/vobs/os_components/scripts")
         ("specs" . "/vobs/release/rpm/MCP")
         ("vxworks" . "~/code/ics/vxworks")
         ("notes" . "~/text/org/notes.org")
         ("emacs-tips" . "~/elisp/emacs-tips")
         ("test-machines" . "~/elisp/test-machines")
         ("bashrc" . "~/mcp/etc/bashrc"))))
  (dolist (curr find-file-special-alist)
    (let ((func-name (car curr))
          (file-name (cdr curr)))
      (eval
       (list 'defun (intern func-name)
             `()
             `(interactive)
             `(if (file-exists-p ,file-name)
                  (find-file ,file-name)
                (error
                 (concat
                  "file or directory " ,file-name " does not exist")))))
      (defalias
        (intern (concat "load-" func-name))
        (intern-soft func-name)))))

(defun vxs.phil ()
  (interactive)
  (find-file "/ssh:sqa-marvin:/tftpboot/vxs.phil"))
(defalias 'load-vxs.phil 'vxs.phil)

(defun bout ()
  (interactive)
  (let ((bhost (read-buffer "build host: "))
        (view
         (if (not (file-exists-p "/usr/atria/bin/cleartool"))
             (read-buffer "view: ")
           (read-buffer "view: "
                        (substring
                         (shell-command-to-string
                          "/usr/atria/bin/cleartool pwv -short")
                         0 -1)))))
    (find-file
     (concat
      "/ssh:" bhost ":/home/phil/mcp/build/output/" view))))
(defalias 'load-bout 'bout)

(defun ppc ()
  (interactive)
  (if (get-buffer-process (current-buffer))
      (cd "~/code/ics/ppc")
    (find-file "~/code/ics/ppc")))
(defalias 'load-ppc 'ppc)

(defun intel ()
  (interactive)
  (if (get-buffer-process (current-buffer))
      (cd "~/code/ics/intel")
    (find-file "~/code/ics/intel")))
(defalias 'load-intel 'intel)

(defun unfiled ()
  (interactive)
  (let ((fname "~/bugs/bug.unfiled"))
    (find-file fname)
    (if (not (file-exists-p fname))
        (insert "Bugzilla ID: unfiled\n\n"))))
(defalias 'load-unfiled 'unfiled)

(defun* bugs ()
  (interactive)
  (let ((dir "~/bugs")
        (switches dired-listing-switches)
        (bug (thing-at-point 'word)))
    (when (not (file-exists-p dir))
      (error (concat dir " missing")))
    (when (and bug (string-match "^[0-9]+$" bug))
      (let ((fname (concat dir "/bug." bug)))
        (when (or (file-exists-p fname)
                  (y-or-n-p (format "create %s " fname)))
          (find-file fname)
          (return-from bugs))))
    (if (string-match "t" switches)
        (dired dir)
      ;; dired likes the -t added with a space. If you
      ;; don't do this it won't know how to remove the
      ;; -t switch when you toggle the sort
      (dired dir (concat switches " -t")))))
(defalias 'load-bugs 'bugs)

(defun mcssh ()
  (interactive)
  (let ((ip (thing-at-point 'url)))
    (when (not (and ip (string-match "\\(\\([0-9]+\.\\)\\{4\\}\\)$" ip)))
      (error "couldn't find IP"))
    (find-file (concat "/ssh:root@" (match-string 1 ip) ":/"))))

(defun mcbin ()
  (interactive)
  (let ((dir "/h/phil/mcp/bin"))
    (if (file-exists-p dir)
        (if (get-buffer-process (current-buffer))
            (progn
              (cd dir)
              (insert (concat "cd " dir))
              (comint-send-input))
          (find-file dir))
      (error (concat dir " missing")))))
(defalias 'load-mcbin 'mcbin)

(defun vspec-edit ()
  (interactive)
  (when (not (file-exists-p "/usr/atria/bin/cleartool"))
    (error "cleartool not installed on this host"))
  (with-temp-buffer
    (insert
     (shell-command-to-string "/usr/atria/bin/cleartool catcs"))
    (goto-char (point-min))
    (if (re-search-forward
         "^#\\s-*\\(/h/phil/config/specs/[^[:space:]]*\\)\\s-*$"
         nil t)
        (let ((spec-file (match-string 1)))
          (if (file-exists-p spec-file)
              (find-file spec-file)
            (error (concat "config spec file '" spec-file "' missing"))))
      (error "couldn't find '/h/phil/config/specs/.*' in output"))))
(defalias 'load-vspec-edit 'vspec-edit)

(defun bugify ()
  (interactive)
  (let ((fname (if (eq major-mode 'dired-mode)
                    (thing-at-point 'filename)
                  (buffer-name))))
    (if (file-exists-p fname)
        (let ((header
               (shell-command-to-string
                (concat "head -n 1 " fname))))
          (if (string-match "Bugzilla ID:[ ]*\\([0-9][0-9]*\\)" header)
              (let* ((bname (concat
                             "*bugify<"
                             (match-string 1 header)
                             ">*"))
                     (buf (get-buffer-create bname)))
                (if (shell-command
                     (concat "~/scripts/bugify " fname)
                     bname)
                    (progn
                      (pop-to-buffer buf)
                      (goto-char (point-min))
                      (when (re-search-forward "^Bugzilla ID: [0-9]+" nil t)
                        (beginning-of-line))
                      (other-window -1))))
            (error (concat fname " has incorrect first line"))))
      (error (concat "can't find " fname)))))

(defun mycalc (expr)
  (interactive "sexpr: ")
  (shell-command (concat "~/scripts/calc '" expr "'")))

(defun mount.phil ()
  (interactive)
  (if (get-buffer-process (current-buffer))
      (progn
        (insert "mkdir -p /h/phil")
        (comint-send-input)
        (insert "mount -t nfs 172.20.100.111:/econ_vol2/home/phil /h/phil")
        (comint-send-input)
        (insert "echo cd /h/phil/mcp/bin")
        (comint-send-input)
        (insert "echo cd /h/phil/mcp/sdf")
        (comint-send-input))
    (error "not in a process buffer")))
(defalias 'mount-phil 'mount.phil)

(defun mcsdf ()
  (interactive)
  (let ((view
         (if (not (file-exists-p "/usr/atria/bin/cleartool"))
             (read-buffer "View: ")
           (shell-command-to-string "/usr/atria/bin/cleartool pwv -short"))))
    (if (get-buffer-process (current-buffer))
        (let* ((bname (substring (buffer-name) 1 -1))
               (dname (concat "/h/phil/mcp/sdf/" bname)))
          (if (file-exists-p dname)
              (progn
                (cd dname)
                (insert (concat "cd " dname))
                (comint-send-input)
                (insert (concat "echo ./setup.sdf -v " view))
                (comint-send-input)
                (insert "echo ./restore.sdf")
                (comint-send-input))
            (error (concat "directory " dname " does not exist"))))
      (error "not in a process buffer"))))

(if emacs-at-work
    (defun current-directory ()
      (if (string-match "/vobs/" default-directory nil)
          default-directory
        (let ((dir (substring (shell-command-to-string "pwd") 0 -1)))
          (cd dir)
          dir)))
  (defun current-directory ()
    default-directory))

(defvar mercury-associates-file
  (format "%s/text/general/associates.txt"
          (if emacs-at-work homedir "/h/phil")))

(defun mercury-associates-directory (ass)
  (interactive "sassociate: ")
  (if (not (equal ass ""))
      (let ((bname "*associates*")
            (data
             (concat
              (shell-command-to-string
               (concat "associates | grep -i "
                       ass))
              (shell-command-to-string
               (concat "grep -i "
                       ass
                       " "
                       mercury-associates-file)))))
        (if (> (length data) 0)
            (let ((buf (get-buffer-create bname)))
              (pop-to-buffer buf)
              (delete-region (point-min) (point-max))
              (insert data)
              (other-window -1))
          (message "Associate not found")))
    (error "Did not provide associate for whom to grep")))

(defun outlook ()
  (interactive)
  (let ((num-days (read-buffer "number of days: " "7")))
    (if (not (string-match "^\\s-*\\([-]?[0-9]+\\)\\s-*$" num-days))
        (error (concat "number of days '" num-days "' formatted incorrectly"))
      (setq num-days (match-string 1 num-days)))
    (shell-command
     (concat "outlook"
             " -d "
             num-days
             (when (y-or-n-p "full listings: ") " -f")
             " &"))))

(defun phil_checkin ()
  (interactive)
  (emacs-aux "phil_checkin" "os-v240-7"))

(defun phil_checkin2 ()
  (interactive)
  (emacs-aux "phil_checkin2" "os-v240-7"))

(defun workbench ()
  (interactive)
  (let* ((host (getenv "HOST"))
         (view
          (if (not (file-exists-p "/usr/atria/bin/cleartool"))
              (read-buffer "View: ")
            (shell-command-to-string "/usr/atria/bin/cleartool pwv -short")))
         (bsp (read-buffer "BSP: "))
         (ver (cond ((equal host "lx-pfisher1")
                     (read-buffer "6.6 or 6.7: "))
                    ((equal host "os-x86-2")
                     "6.7")
                    ((equal host "lx-pcg-1")
                     "6.9")
                    (t
                     (error "running on unsupported host")))))
    (if (and (not (equal view ""))
             (not (equal bsp "")))
        (if (or (equal ver "6.6")
                (equal ver "6.7")
                (equal ver "6.9"))
            (call-process
             (format
              "~/workspace/%s/scripts/startWorkbench.sh"
              ver)
             nil 0 nil
             "-v"
             view
             "-bsp"
             bsp)
          (error "invalid vxworks version"))
      (error "missing argument"))))

(defun wb ()
  (interactive)
  (workbench))

(defun bugzilla ()
  (interactive)
  (call-process firefox-binary nil 0 nil "http://chm-bugzilla/bugzilla"))

(defun wiki ()
  (interactive)
  (call-process firefox-binary nil 0 nil "http://wiki.mc.com/LinuxCE"))

(defun scheduler ()
  (interactive)
  (call-process
   firefox-binary nil 0 nil "http://qe.mc.com/scheduler/schedule.php"))

(defun rdesktop (host)
  (interactive "shost: ")
  (if (not (equal host ""))
      (call-process
       "rdesktop"
       nil 0 nil
       "-f"
       "-d" "MERCURY"
       "-u" "pfisher"
       host)
    (error "no remote host specified")))

(defun prepare-checkins ()
  (interactive)
  (split-window-vertically)
  (shell)
  (rename-buffer "*ci*")
  (insert "cd /vobs/mcos")
  (comint-send-input)
  (other-window 1)
  (shell)
  (rename-buffer "*crify*")
  (insert (format "cd %s/text/CR" homedir))
  (comint-send-input))

(defun mk-tags ()
  (interactive)
  (let* ((view
          (if (not (file-exists-p "/usr/atria/bin/cleartool"))
              (read-buffer "View: ")
            (shell-command-to-string "/usr/atria/bin/cleartool pwv -short")))
         (bname "*mk-tags*")
         (buf (get-buffer-create bname)))
    (if (shell-command
         (concat "~/scripts/mk_cat.mcp -v " view "&")
         bname)
        (progn
          (pop-to-buffer buf)
          (other-window -1)))))

;; telnet to test machine

(defvar test-machine-telnet-target "pcg-vxs-2")

(defun test-machine-telnet-helper (target addr)
  (let ((bname (concat "*" target "*")))
    (switch-to-buffer bname)
    (if (not (string-equal mode-name "Telnet"))
        (progn
          (kill-buffer bname)
          (telnet addr)
          (rename-buffer bname)))))

(defmacro test-machine-context (&rest args)
  (declare (indent 0) (debug t))
  `(let ((target (read-buffer "test host: " test-machine-telnet-target))
         (fname "~/elisp/test-machines")
         beg)
     (when (not (file-exists-p fname))
       (error (concat fname " missing")))
     (with-temp-buffer
       (insert-file-contents fname)
       (when (not (re-search-forward (concat "^[^\\s-#]?.*" target) nil t))
         (error (concat "couldn't find specified target '" target "'")))
       (forward-line)
       (setq beg (point))
       (while (and (not (eobp)) (looking-at "^\\(\\s-\\|#\\)"))
         (forward-line))
       (narrow-to-region beg (point))
       ,@args)))

(defun test-machine-telnet ()
  (interactive)
  (test-machine-context
    (let ((found))
      (goto-char (point-min))
      (while (and (not found)
                  (not (eobp)))
        (when (looking-at
               "^\\s-+telnet\\(\\[s\\([0-9]+\\)\\]\\)?:\\s-*\\(.*\\)\\s-*$")
          (if (or (not (match-string 1))
                  (y-or-n-p (concat "slot "
                                    (buffer-substring
                                     (match-beginning 2)
                                     (match-end 2))
                                    " ")))
              (let ((tinfo (buffer-substring
                            (match-beginning 3)
                            (match-end 3))))
                (message (concat
                          "telnet info for "
                          target
                          ": "
                      tinfo))
                (test-machine-telnet-helper target tinfo)
                (setq found t))))
        (forward-line))
      (if found
          (setq test-machine-telnet-target target)
        (error (concat
                "couldn't find telnet info for target '"
                target
                "'"))))))

(defun test-machine-power ()
  (interactive)
  (test-machine-context
    (when (not (= (call-process-shell-command
                   "ping"
                   nil nil nil
                   "-c" "1" "sqa-marvin")
                  0))
      (error "Host sqa-marvin not responding to ping"))
    (goto-char (point-min))
    (let (cmd)
      (while (not (eobp))
        (when (looking-at "^\\s-+power:\\s-*\\(.*\\)\\s-*$")
          (let ((pinfo (buffer-substring
                        (match-beginning 1)
                        (match-end 1))))
            (setq cmd (concat cmd pinfo "; "))))
        (forward-line))
      (if (not cmd)
          (error (concat
                  "couldn't find power-cycle info for target '"
                  target
                  "'"))
        (setq cmd (concat "ssh sqa-marvin \'" cmd "' &"))
        (shell-command cmd)
        (setq shell-command-history
              (append (list cmd) shell-command-history))))))
(defalias 'power 'test-machine-power)

(defun rdb-aux (dir gdb)
  (if (not (file-exists-p dir))
      (error (concat dir " missing")))
  (let ((LDLIBPATH (getenv "LD_LIBRARY_PATH"))
        (process-environment (copy-sequence process-environment))
        (cmd
         (read-from-minibuffer
          "Run rdb (like this): "
          (concat gdb " --fullname -x .gdbinit"))))
    (setenv "LD_LIBRARY_PATH" (concat LDLIBPATH ":" dir))
    (let ((exec-path (append (list dir) exec-path)))
      (gud-gdb cmd))))

(defun rdb ()
  (interactive)
  (let ((gdbdef ".gdbinit.def")
        (gdbinit ".gdbinit")
        (dir (read-from-minibuffer "run in directory: " default-directory)))
    (unless (file-exists-p dir)
      (error (concat dir " does not exist")))
    (cd dir)
    (unless (file-exists-p gdbdef)
      (error (concat gdbdef " missing in current directory")))
    (unless (file-exists-p gdbinit)
      (error (concat gdbinit " missing in current directory")))
    (with-temp-buffer
      (insert-file-contents gdbdef)
      (unless (re-search-forward
               "#\\s-*arch:\\s-*\\([^[:space:]]*\\)\\s-*$"
               nil t)
        (error (concat "couldn't find arch line in " gdbdef)))
      (let ((arch (match-string 1)))
        (cond ((equal arch "ppc")
               (rdb-aux "~/mcp/gdb.ppc" "ppc86xx-linux-gdb"))
              ((or (equal arch "x86_64")
                   (equal arch "i386"))
               (let ((fname "/etc/redhat-release")
                     relnum)
                 (when (file-exists-p fname)
                   (with-temp-buffer
                     (insert-file-contents fname)
                     (when
                         (re-search-forward "release \\([0-9]\.[0-9]\\)" nil t)
                       (setq relnum (match-string 1)))))
                 (if (and relnum (equal relnum "6.1"))
                     (rdb-aux "~/mcp/gdb.intel" "gdb.rh61")
                   (rdb-aux "~/mcp/gdb.intel" "gdb.rh55"))))
              (t
               (error (concat "arch type " arch " unknown"))))))))

;; kgdb for ppc kernel debugging
(defun kgdb ()
  (interactive)
  (if (not (comint-check-proc "*kgud*"))
      (let ((endian (read-buffer "Endian (BE/LE): " "BE"))
            (prefix "/vobs/mcos/out/os/system/bin"))
        (cond ((equal endian "LE")
               (split-window)
               (let ((exec "/mcexec-2_2-ppc_e_le/exec.ppc_le"))
                 (gdb (concat
                       "/vobs/gdb/kgdb/kgdb-ppc_e_le "
                       prefix
                       exec
                       (if (file-exists-p (concat prefix exec ".debug"))
                           ".debug")))))
              ((equal endian "BE")
               (split-window)
               (let ((exec "/mcexec-2_2-ppc_e/exec.ppc"))
                 (gdb (concat
                       "/vobs/gdb/kgdb/kgdb-ppc_e "
                       prefix
                       exec
                       (if (file-exists-p (concat prefix exec ".debug"))
                           ".debug")))))
              (t
               (error "Error: you must specify BE or LE")))
        (rename-buffer "*kgud*"))
    (pop-to-buffer "*kgud*")
    (delete-other-windows)))

;; gdb for vxworks etc.

(defvar vxgdb-target-command "none")
(defvar vxgdb-debugger "vxgdb68k")
(defvar vxgdb-breakpoint nil)

(defun remove-from-vxgdb-breakpoint (num new)
  (if (and (>= num 0) (< num (length vxgdb-breakpoint)))
      (let ((bp nil)
            (index 0))
        (while (< index (length vxgdb-breakpoint))
          (if (not (= index num))
              (setq bp (append bp (list (nth index vxgdb-breakpoint))))
            (if new
                (setq bp (append bp (list new)))))
          (setq index (+ index 1)))
        (setq vxgdb-breakpoint bp))))

(defun vxgdb-setup-gud (bp-prompt)
  (let ((target-command "none")
        debugger)
    (if bp-prompt
        (setq target-command (read-buffer
                              "target command: "
                              vxgdb-target-command)
              vxgdb-target-command target-command
              debugger (read-buffer "debugger: " vxgdb-debugger)
              vxgdb-debugger debugger))
    (if (not (comint-check-proc "*gud*"))
        (progn
          (gdb debugger)
          (message (concat "Waiting for " debugger " to start"))
          (sleep-for 2)
          (while (util-nil-specified target-command)
            (setq target-command (read-buffer
                                  "target command "
                                  vxgdb-target-command)
                  vxgdb-target-command target-command)))
      (pop-to-buffer "*gud*")
      (delete-other-windows))
    (end-of-buffer)
    (if (not (util-nil-specified target-command))
        (progn
          (insert target-command)
          (comint-send-input)
          (insert "del")
          (comint-send-input)
          (insert "y")
          (comint-send-input)
          (if bp-prompt
              (update-vxgdb-breakpoint t))))))

(defun vxgdb-for-all-breakpoints (action)
  (let ((index 0))
    (while (< index (length vxgdb-breakpoint))
      (cond ((equal action 'insert)
             (progn
               (insert (concat "break " (nth index vxgdb-breakpoint)))
               (comint-send-input)))
            ((equal action 'list)
             (message (format "breakpoint: %s" (nth index vxgdb-breakpoint)))
             (sleep-for 1))
            ((equal action 'edit)
             (let ((edit nil))
               (while (< index (length vxgdb-breakpoint))
                 (setq edit (read-buffer (format
                                          "breakpoint: %s (-/.) "
                                          (nth index vxgdb-breakpoint))))
                 (cond ((equal edit "-")
                        (if (> index 0)
                            (setq index (- index 1))))
                       ((equal edit ".")
                        (remove-from-vxgdb-breakpoint index nil))
                       ((equal edit "")
                        (setq index (+ index 1)))
                       (t
                        (remove-from-vxgdb-breakpoint index edit)))))))
      (setq index (+ index 1)))))

(defun update-vxgdb-breakpoint (&optional i-not-okay)
  (interactive)
  (let ((prompt "breakpoint command (break/delete/edit/insert/list/reset): ")
        (done nil)
        command)
    (while (and (not done)
                (not (equal (setq command (read-buffer prompt)) "")))
      (cond ((or (equal command "delete") (equal command "d"))
             (setq vxgdb-breakpoint nil))
            ((or (and (or (equal command "insert")
                          (equal command "i"))
                      (not i-not-okay))
                 (equal command "break") (equal command "b"))
             (let ((bp (read-buffer "breakpoint: ")))
               (if (not (equal bp ""))
                   (progn
                     (setq vxgdb-breakpoint
                           (append vxgdb-breakpoint (list bp)))
                     (if (or (equal command "insert") (equal command "i"))
                         (progn
                           (vxgdb-setup-gud nil)
                           (insert (concat "break " bp))
                           (comint-send-input)))))))
            ((or (equal command "reset") (equal command "r"))
             (vxgdb-setup-gud nil)
             (insert "del")
             (comint-send-input)
             (insert "y")
             (comint-send-input)
             (vxgdb-for-all-breakpoints 'insert))
            ((and (or (equal command "insert") (equal command "i"))
                  i-not-okay)
             (vxgdb-for-all-breakpoints 'insert)
             (setq done t))
            ((or (equal command "list") (equal command "l"))
             (vxgdb-for-all-breakpoints 'list))
            ((or (equal command "edit") (equal command "e"))
             (vxgdb-for-all-breakpoints 'edit))
            ((equal command "c")
             (vxgdb-setup-gud nil)
             (insert "cont")
             (comint-send-input)
             (setq done t))))))

(defun vxgdb ()
  (interactive)
  (let ((pos (- (point) 200))
        tid)
    (save-excursion
      (if (search-backward-regexp
           "0x[0-9,a-f][0-9,a-f][0-9,a-f][0-9,a-f][0-9,a-f][0-9,a-f]\\([0-9,a-f][0-9,a-f] \\|[0-9,a-f] \\| \\\)"
           pos
           t)
          (let ((beg (point)))
            (forward-sexp)
            (setq tid (buffer-substring beg (point))))
        (error "Sorry: no Tid")))
    (vxgdb-setup-gud t)
    (insert (concat "attach " tid))
    (comint-send-input)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Misc macros
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(fset 'jtest
   [?\C-x ?1 ?\C-x ?t ?j ?d ?a ?v ?i ?d return end ?\C-x ?2 f5 ?\C-x ?t ?j ?d ?e ?b ?u ?g return end])

(fset 'jboot
   [?\C-x ?1 ?\C-x ?t ?j ?d ?a ?v ?i ?d return end ?\C-x ?2 f5 ?\C-x ?t ?j ?d ?e ?b ?u ?g return end ?[ ?b ?8 ?  ?0 ?0 ?  ?0 ?d ?  ?0 ?a ?  ?4 ?0 ?  ?0 ?0 ?  ?0 ?2 ?] return f5 end])

(fset 'jpower
   [?\C-c ?t ?D ?T ?A ?E ?- ?R ?P ?C ?- ?B ?1 ?0 return])

;;(fset 'itest
;;   [escape ?> ?\C-e return return ?\C-x ?\C-f ?\C-a ?\C-k ?/ ?h ?/ ?p ?h ?i ?l ?/ ?m ?c ?p ?/ ?e ?t ?c ?/ ?s ?e ?t ?u ?p return ?\C-  escape ?> escape ?w ?\C-x ?k return ?\C-y return])

(fset 'itest
   [escape ?> ?\C-e return return ?\C-x ?i ?\C-  ?\C-a ?\C-c ?x ?y ?/ ?h ?/ ?p ?h ?i ?l ?/ ?m ?c ?p ?/ ?e ?t ?c ?/ ?s ?e ?t ?u ?p return escape ?> return])

(fset 'rtp-attach
   [f6 ?* ?g ?u ?d ?* return escape ?> escape ?\C-r ?^ ?0 ?x ?\C-m ?\C-  ?\C-s ?  ?\C-m escape ?w escape ?> ?a ?t ?t ?a ?c ?h ?  ?\C-y return])

