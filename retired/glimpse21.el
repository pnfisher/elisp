;;; To install, merely put this file somewhere GNU Emacs will find it,
;;; then add the following lines to your .emacs file:
;;;
;;;   (autoload 'glimpse "glimpse")
;;;
;;; You may also adjust some customizations variables, below, by defining
;;; them in your .emacs file.

(require 'compile)
(require 'font-lock)
(provide 'glimpse)

(defcustom glimpse-id nil
  "Key to index glimpse-directory-table"
  :group 'my-tools-id)
(defvar glimpse-mode-hook nil)
(defvar glimpse-face-hook nil)
(defvar glimpse-directory-table nil)
(defvar glimpse-history nil)
(defvar glimpse-regexp-alist
  '(("\\([a-zA-Z]?:?[^:( \t\n]+\\)[:( \t]+\\([0-9]+\\)[:) \t]" 1 2))
  "Regexp used to match glimpse hits.  See `compilation-error-regexp-alist'.")
(defvar glimpse-params "-n -y")

(defvar glimpse-font-lock-keywords 
  (font-lock-compile-keywords
   '(("^\\([^\n: ]+\\): \\([0-9]+\\):" 
      (1 font-lock-warning-face)
      (2 font-lock-variable-name-face)))))

(defun glimpse ()
  (interactive)
  (run-hooks 'glimpse-mode-hook)
  (let ((arg (read-from-minibuffer "Find string: "
                                   (cons (or (thing-at-point 'symbol) "") 1)
                                   nil
                                   nil
                                   'glimpse-history))
        (directory (nth 1 (assoc glimpse-id glimpse-directory-table))))
    (if (and directory (not (string-equal arg "")))
        (let ((compilation-process-setup-function 'glimpse-process-setup)
              (compilation-scroll-output nil)
              (compilation-mode-hook
               '(lambda ()
                  (set (make-local-variable 'font-lock-defaults)
                       '(glimpse-font-lock-keywords t))
                  (run-hooks 'glimpse-face-hook))))
          (compilation-start (concat 
                              "glimpse "
                              glimpse-params
                              " -H "
                              directory
                              " "
                              arg)
                             "glimpse"
                             nil 
                             glimpse-regexp-alist)))))

(defun glimpse-process-setup ()
  "Set up `compilation-exit-message-function' for `glimpse'."
  (set (make-local-variable 'compilation-exit-message-function)
       (lambda (status code msg)
         (if (eq status 'exit)
             (cond ((zerop code)
                    '("finished (matches found)\n" . "matched"))
                   ((= code 1)
                    '("finished with no matches found\n" . "no match"))
                   (t
                    (cons msg code)))
           (cons msg code)))))
