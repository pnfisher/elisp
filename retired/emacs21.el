;; -*-EMACS-lisp-*- ;;

;; technically this file can work with older versions of emacs
;; but these functions are 21 specific
(if emacs21
    (progn
      (blink-cursor-mode -1)
      (tooltip-mode -1)
      (set-face-background 'fringe "Gray25")))

;; -*-load-path-*- ;;
(setq load-path (cons "~/src/elisp" load-path))
(if (equal (getenv "OSTYPE") "solaris")
    (setq load-path (cons "/h/phil/solaris/share/emacs/site-lisp" load-path)))

(load-library "my-utils")
;;(load-library "clear-util.el")
(if (equal (getenv "OSTYPE") "solaris")
    (load-library "clear-local")
  (load-library "clear-remote"))

;; -*-file variables-*- ;;
(setq make-backup-files nil)
(setq auto-save-default nil)

;; -*-faces-*- ;;
(set-face-background 'highlight "DarkOliveGreen")
(set-face-background 'region "DarkSlateGray")

(setq isearch (custom-face "LightGray" "DarkOliveGreen"))
(setq isearch-lazy-highlight-face (custom-face "LightGray" "DarkSlateGray"))

(require 'comint)
(custom-set-faces
  ;; custom-set-faces was added by Custom -- don't edit or cut/paste it!
  ;; Your init file should contain only one such instance.
 '(comint-highlight-input ((((class color) (background dark)) (:foreground "Gray85"))))
 '(comint-highlight-prompt ((((class color) (background dark)) (:foreground "Gray89")))))

;; -*-font-lock-*- ;;
(require 'font-lock)

(setq font-lock-maximum-decoration nil)

(defvar c-function-face (custom-face "SkyBlue1" "Black"))
;;(defvar c-function-face (custom-face "LightGray" "SlateBlue4"))
;;(defvar c-function-face (custom-face "Wheat" "Black"))
(defvar c-include-face (custom-face "LightGray" "DarkOliveGreen"))
(defvar c-include-string-face (custom-face "Tan" "Black"))
(defvar c-cpp-face (custom-face "LightGray" "DarkOliveGreen"))
(defvar c-return-face (custom-face "Tan" "Black"))
;;(defvar c-macro-face (custom-face "Wheat" "Black"))
;;(defvar c-macro-face (custom-face "LightSteelBlue" "Black"))
(defvar c-macro-face (custom-face "SkyBlue1" "Black"))
(defvar c-comment-face (custom-face "Salmon" "Black"))

(setq
 c-font-lock-keywords
 (font-lock-compile-keywords
  '(("^\\(\\sw+\\)[ \t]*("
     (1 c-function-face))
    ("^\\(\\(\\sw+\\)[ \t]+\\)+\\(\\sw+\\).*("
     (3 c-function-face))
    ("^#[ \t]*\\(import\\|include\\)[ \t]*\\([<\"][^>\"\n]*[>\"]?\\)"
     (1 c-include-face)
     (2 c-include-string-face t))
    ("^#[ \t]*define[ \t]+\\(\\sw+\\)("
     (1 c-macro-face))
    ("^#[ \t]*\\(define\\)[ \t!]*\\(\\sw+\\)?"
     (1 c-cpp-face)
     (2 c-macro-face nil t))
    ("\\b\\(return\\|goto\\|break\\|continue\\)\\b"
     (1 c-return-face))
    ("^#[ \t]*\\(\\(if\\|el\\|en\\)[^/\n\r]*\\)"
     (1 c-cpp-face t)))))

;; -*-tabs-*- ;;
(setq-default tab-width 2)

;; -*-bell-*- ;;
(setq visible-bell nil)

;; -*-rmail-*- ;;
;;(require 'rmail)
;;(load "rmailsum")
;;(setq mail-signature t)
;;(setq send-mail-function 'smtpmail-send-it)
;;(setq smtpmail-smtp-server "exchange.mc.com")
;;(setq smtpmail-smtp-service 25)
;;(setq smtpmail-local-domain "mc.com")
;;(setq smtpmail-sendto-domain "mc.com")
;;(setq user-mail-address "pfisher@mc.com")
;;(setq rmail-file-name "~/rmail/RMAIL")
;;;;(setq rmail-rile-name "/test/phil/rmail/RMAIL")
;;;;(setq rmail-file-name "/n/inxs7/os/phil/rmail/RMAIL")
;;(setq rmail-default-rmail-file "~/rmail/rmail.current")
;;(setq rmail-delete-after-output t)
;;(define-key rmail-mode-map [?\M-r] 'rmail-search-backwards)
;;(define-key rmail-mode-map "F" 'mime-forward)
;;(define-key rmail-summary-mode-map "F" 'mime-forward)
;;(define-key rmail-mode-map "D" 'detach)
;;(define-key rmail-summary-mode-map "D" 'detach)
;;(define-key rmail-mode-map "O"
;;  '(lambda ()
;;     (interactive nil)
;;     (rmail-output-to-rmail-file "~/rmail/rmail.current")))
;;;;(define-key rmail-mode-map "d"
;;;;  '(lambda ()
;;;;     (interactive nil)
;;;;     (if (equal (buffer-name) "rmail.deleted")
;;;;         (rmail-delete-forward)
;;;;       (rmail-output-to-rmail-file "~/rmail/rmail.deleted"))))
;;
;;(defadvice rmail-resend (before safe-rmail-resend activate compile)
;;  (if (not (string-equal mode-name "RMAIL"))
;;      (error "Sorry: not in RMAIL buffer")))
;;
;;;;(setenv "MAILHOST" "exchange.mc.com")
;;;;(setenv "MAILHOST" "CHM-EMAIL1.ad.mc.com")
;;;;(setq rmail-primary-inbox-list `("po:pfisher")
;;;;      rmail-pop-password-required t)
;;;;(if (string-match "SunOS" (shell-command-to-string "uname"))
;;;;    (setq rmail-movemail-program "/h/phil/solaris/bin/movemail"))
;;;;(load-library "pop-password")
;;
;;(setq rmail-primary-inbox-list `("~/rmail/pop3"))
;;(require 'pop3)
;;(defadvice rmail (before rmail-pop3 activate compile)
;;  (setq pop3-password (rmail-get-pop-password))
;;  (pop3-movemail (car rmail-primary-inbox-list))
;;  (setq pop3-password nil))
;;(setq
;; ;;pop3-mailhost "CHM-EMAIL2.ad.mc.com"
;; ;;pop3-mailhost "chm-email2.mc.com"
;; pop3-mailhost "exchange.mc.com"
;; pop3-maildrop "pfisher"
;; pop3-password-required t
;; pop3-port 110
;; pop3-preserve-inbox nil)
;;(if (equal (getenv "USER") "phil")
;;    (load-library "pop-password"))
;;
;;(defadvice rmail (after rmail-update activate compile)
;;  (disptime-update))
;;
;;(defadvice mail-yank-original
;;  (after mail-yank-original-update activate compile)
;;  (beginning-of-buffer)
;;  (if (search-forward mail-header-separator)
;;      (progn
;;        (newline)
;;        (newline)
;;        (goto-char (- (point) 1)))))

;; -*-w3-* ;;
;;(autoload 'w3 "w3" "WWW Browser" t)

;; -*-gnus-* ;;
;;(setq gnus-nntp-server "jericho")
;;(autoload 'gnus "gnus" "Read network news." t)
;;(autoload 'gnus-post-news "gnuspost" "Post a news." t)
;; Set the service name to "nntp" to not use NFS
;;(setq gnus-nntp-service "nntp")
;; This does the right thing for either NFS or NNTP.
;;(cond (gnus-nntp-service (setq gnus-nntp-server "jericho"))
;;      (t
;;       (setq gnus-nntp-server (system-name))
;;       (setq nnspool-spool-directory "/u/news/spool")
;;       (setq nnspool-active-file  "/u/news/newsctl/active")
;;       (setq nnspool-history-file "/u/news/newsctl/history")))

;;(setq gnus-local-domain "mc.com")
;;(setq gnus-your-domain "mc.com")
;;(setq gnus-your-organization
;;"Mercury Computer Systems, Chelmsford MA 01824")
;;(setq gnus-nntp-service 119)
;;(setq gnus-use-generic-from t)
;;(setq gnus-default-article-saver (function gnus-summary-save-in-file))

;;(require 'info)
;;(setq Info-default-directory-list
;;      (append (list "/h/phil/solaris/info")))

;; -*-extensions-*- ;;
(setq auto-mode-alist (append (list '("\\.c$" . c-mode)
                                    '("\\.h$" . c-mode)
                                    '("\\.cc$" . c++-mode)
                                    '("\\.tex$" . TeX-mode)
                                    '("\\.txi$" . Texinfo-mode)
                                    '("\\.el$" . emacs-lisp-mode)
                                    '("\\.lisp$" . lisp-mode)
                                    '("\\.pl$" . prolog-mode)
                                    '("\\.a$" . c-mode)
                                    '("\\.uk_c$" . c-mode)
                                    '("\\.cpp_h$" . c-mode)
                                    '("\\.hs$" . c-mode)
                                    '("\\.s$" . c-mode)
                                    '("\\.cpp_s$" . c-mode))
                              auto-mode-alist))

;; -*-shell-*- ;;
(require 'shell)
;;(setq shell-file-name "/bin/csh")
;;(setq explicit-shell-file-name "/bin/csh")
(if (equal (getenv "OSTYPE") "solaris")
    (progn
      (setq shell-file-name "/h/phil/solaris/bin/tcsh")
      (setq explicit-shell-file-name "/h/phil/solaris/bin/tcsh"))
  (setq shell-file-name "/bin/tcsh")
  (setq explicit-shell-file-name "/bin/tcsh"))

;; -*-printing-*- ;;
;;(setq lpr-switches '("-Psweng_hp"))
(setq printer-name "sweng_hp")
(setq lpr-command "lpr")

;; -*-dired-*- ;;
(require 'dired)
(setq dired-dwim-target t)
(setq find-grep-options "-q -s")
(if (eq window-system 'x)
    (require 'dired-x)
  ;; otherwise do in .Xdefaults
  (menu-bar-mode -1))

;;(require 'dired-a)
;;(setq dired-recursive-copies t)
;;(setq dired-recursive-deletes t)
;;(require 'find-dired)
;;(define-key dired-mode-map "b" 'vtree-browse)

;;(setq dired-view-alist (append (list '("\\.pdf$" "acroread"))
;;                               dired-view-alist))

(defcustom tools-id "mcp"
  "variable to control source code tools")

(setq etags-directory-table
      '(("mcos" "/vobs/mcos")
        ("fm" "/vobs/os_components/L0FabricManager")
        ("mcp" "/vobs/os_components")
        ("fm.phil_3" "/view/phil_3/vobs/os_components/L0FabricManager")
        ("mcp.phil_3" "/view/phil_3/vobs/os_components")
        ("mcp.phil_4" "/view/phil_4/vobs/os_components")
;;        ("linux.pasemi"
;;         "/home/phil/build_output/kernels/linux-2.6.24.2_pasemi")
        ("linux.sbc"
         "/home/phil/build_output/kernels/linux-2.6.11.1_yos_SBC")
        ("linux.hcd"
         "/home/phil/build_output/kernels/linux-2.6.15.7_yos_HCD")
        ("linux.vpa200"
         "/home/phil/build_output/kernels/linux-2.6.12.6_VPA200")
        ("linux.mpc102"
         "/home/phil/build/kernels/mpc102")
        ("new_tests" "/vobs/mcos/new_tests")
        ("cfe" "/h/phil/code/cfe-2.0.25-source-20080229")
        ("." ".")))

(setq global-c-indent-level 2)

;; -*-misc-* ;;
(custom-set-variables
  ;; custom-set-variables was added by Custom -- don't edit or cut/paste it!
  ;; Your init file should contain only one such instance.
 '(Man-notify-method (quote bully))
 '(c-basic-offset global-c-indent-level)
 '(compilation-scroll-output t)
 '(cursor-in-non-selected-windows nil)
 '(display-hourglass nil)
 '(indent-tabs-mode nil)
 '(mail-self-blind t t)
 '(mail-yank-ignored-headers "^via:\\|^mail-from:\\|^origin:\\|^status:\\|^remailed\\|^received:\\|^message-id:\\|^summary-line:\\|^in-reply-to:\\|^return-path:\\|^Content-class:\\|^X-.*:\\|^Thread-.*:\\|^Resent-.*:\\|Reply-.*:\\|^Sender.*:\\|^To:\\|^CC:\\|^\\|^1, detached,," t)
 '(perl-indent-level 2)
 '(tags-apropos-verbose t)
 '(tags-case-fold-search nil)
 '(tags-table-list (list (nth 1 (assoc tools-id etags-directory-table))))
 '(tcl-indent-level 2))

(require 'glimpse)
(setq glimpse-directory-table etags-directory-table)
(defvar glimpse-file-name-face (custom-face "Cyan" "Black"))
(defvar glimpse-line-number-face (custom-face "Gray90" "Black"))

(setq gid-id-table etags-directory-table)
(defvar gid-file-name-face (custom-face "Cyan" "Black"))
(defvar gid-line-number-face (custom-face "Gray90" "Black"))

(require 'cscope)
(setq cscope-reffile-table etags-directory-table)
(defvar cscope-file-name-face (custom-face "Cyan" "Black"))
(defvar cscope-line-number-face (custom-face "Gray90" "Black"))
(defvar cscope-function-name-face (custom-face "LightSalmon" "Black"))
(defvar cscope-global-face (custom-face "MediumSpringGreen" "Black"))
(defvar cscope-unknown-face (custom-face "LightGoldenRod" "Black"))

(require 'etach)
(require 'c-util)

;;(load-library "eshell-util")

(require 'compile)
(setq compilation-error-regexp-alist
      (cons '("E \"\\([^,\" \n\t]+\\)\",L\\([0-9]+\\)" 1 2)
            compilation-error-regexp-alist))
(setq compilation-error-regexp-alist
      (cons '("w \"\\([^,\" \n\t]+\\)\",L\\([0-9]+\\)" 1 2)
            compilation-error-regexp-alist))

(setq transient-mark-mode t)
(setq inhibit-startup-message t)

(set-scroll-bar-mode 'right)

;;(setq display-time-24hr-format t)
;;(display-time)

;;new improved display time functionality with pop account checkin
;;for Mail mode line indicator; if Mail mode line indicator is
;;Mail?, then examine disptime variable
;;
;;   disptime-count-mail-data
;;
;;to see what went wrong
(require 'disptime)
(if (equal 0 (shell-command "which expect"))
    (setq disptime-count-mail-program "expect"
          disptime-count-mail-program-arguments
          '("-f" "/h/phil/scripts/pop3stat.exp")
          disptime-show-mail-p 'count
          disptime-mail-file 'remote
          disptime-update-interval 60))
(setq disptime-show-time-24hr-p t
      disptime-show-load-average-p t)
;;(setq disptime-count-mail-program "/h/phil/solaris/bin/fetchmail"
;;      disptime-count-mail-program-arguments '("--check")
;;      disptime-show-mail-p 'count
;;      disptime-mail-file 'remote
;;      disptime-show-time-24hr-p t
;;      disptime-show-load-average-p t)
(disptime-set-mail-string)
(disptime-enable)

(line-number-mode t)
(auto-compression-mode)
;;(put 'downcase-region 'disabled nil)
(put 'upcase-region 'disabled nil)

;; don't allow the *scratch* buffer to ever be killed
(scratch-protect)

(load "ssh")

;; HOOK SETTINGS ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(setq text-mode-hook 'turn-on-auto-fill)

(defvar compilation-file-name-face (custom-face "tomato" "Black"))
(defvar compilation-number-face (custom-face "Gray95" "Black"))
(defvar compilation-make-face (custom-face "White" "Black"))
(defvar compilation-warning-face (custom-face "LightGoldenRod" "Black"))

(add-hook 'compilation-mode-hook
          '(lambda ()
             (turn-on-font-lock)
             (set (make-local-variable 'font-lock-function-name-face)
                  compilation-make-face)
             (set (make-local-variable 'font-lock-warning-face)
                  compilation-file-name-face)
             (set (make-local-variable 'font-lock-variable-name-face)
                  compilation-number-face)
             (setq font-lock-keywords
                   (append font-lock-keywords
                           (list '("^\\([^:\n]+\\):\\([0-9]+\\): warning:"
                                   (1 compilation-warning-face t)
                                   (2 font-lock-variable-name-face)))))))

(defvar dired-directory-face (custom-face "White" "Black"))
(defvar dired-subdir-face (custom-face "White" "Black"))
(defvar dired-symlink-face (custom-face "LightSteelBlue" "Black"))
(defvar dired-marks-face (custom-face "MediumSpringGreen" "Black"))
(defvar dired-marks-file-face (custom-face "MediumSpringGreen" "Black"))

(add-hook 'dired-mode-hook
          '(lambda ()
             (turn-on-font-lock)
             (set (make-local-variable 'font-lock-function-name-face)
                  dired-subdir-face)
             (set (make-local-variable 'font-lock-type-face)
                  dired-directory-face)
             (set (make-local-variable 'font-lock-constant-face)
                  dired-marks-face)
             (set (make-local-variable 'font-lock-warning-face)
                  dired-marks-file-face)
             (set (make-local-variable 'font-lock-keyword-face)
                  dired-symlink-face)
             (set (make-local-variable 'font-lock-string-face)
                  nil)))

(add-hook 'glimpse-mode-hook
          '(lambda ()
             (set (make-local-variable 'glimpse-id) tools-id)))

(add-hook 'glimpse-face-hook
          '(lambda ()
             (set (make-local-variable 'font-lock-warning-face)
                  glimpse-file-name-face)
             (set (make-local-variable 'font-lock-variable-name-face)
                  glimpse-line-number-face)
             (turn-on-font-lock)))

(add-hook 'gid-mode-hook
          '(lambda ()
             (set (make-local-variable 'gid-id) tools-id)))

(add-hook 'gid-face-hook
          '(lambda ()
             (set (make-local-variable 'font-lock-warning-face)
                  gid-file-name-face)
             (set (make-local-variable 'font-lock-variable-name-face)
                  gid-line-number-face)
             (turn-on-font-lock)))

(add-hook 'cscope-mode-hook
          '(lambda ()
             (set (make-local-variable 'cscope-id) tools-id)))

(add-hook 'cscope-face-hook
          '(lambda ()
             (set (make-local-variable 'font-lock-warning-face)
                  cscope-file-name-face)
             (set (make-local-variable 'font-lock-variable-name-face)
                  cscope-line-number-face)
             (set (make-local-variable 'font-lock-function-name-face)
                  cscope-function-name-face)
             (set (make-local-variable 'font-lock-keyword-face)
                  cscope-global-face)
             (set (make-local-variable 'font-lock-constant-face)
                  cscope-unknown-face)
             (turn-on-font-lock)))

(add-hook 'comint-mode-hook
          '(lambda ()
             (setq tab-width 8)))

(add-hook 'mail-mode-hook
          '(lambda ()
             ;;(define-key mail-mode-map [f7] 'ispell-message)
             (setq mail-yank-prefix "> ")))

(add-hook 'ediff-load-hook
          '(lambda ()
             (setq ediff-window-setup-function 'ediff-setup-windows-plain)
             (setq-default ediff-auto-refine 'off)))

(add-hook 'ediff-startup-hook
          '(lambda ()
             (ediff-jump-to-difference 1)))

(add-hook 'telnet-mode-hook
          '(lambda ()
             (define-key telnet-mode-map "\t" 'comint-dynamic-complete)))

(setq sh-mode-hook
          '(lambda ()
             (setq sh-shell-file "/bin/sh")
             (setq sh-indent-after-function '++)
             (setq sh-indent-after-if '++)
             (setq sh-indent-for-else 0)
             (setq sh-indent-after-loop-construct '++)
             (setq sh-indent-for-done 0)
             (setq sh-indent-for-fi 0)
             (setq sh-indent-for-continuation 0)
             (setq sh-indent-after-open '++)
             (setq sh-indent-for-case-label '+)
             (setq sh-indent-for-case-alt '++)
             (setq sh-indent-comment t)
             (setq sh-basic-offset 1)))

(add-hook 'c-mode-hook
          '(lambda ()
             (turn-on-font-lock)
             ;;(setq c-macro-preprocessor "/usr/ccs/lib/cpp -C")
             ;;(setq c-macro-shrink-window-flag t)
             ;;(setq c-macro-cppflags "")
             (set (make-local-variable 'font-lock-comment-face)
                  c-comment-face)
             (setq c-indent-level global-c-indent-level)
             (setq tab-width global-c-indent-level)
             (c-set-offset 'case-label '+)
             (c-set-offset 'arglist-intro global-c-indent-level)
             (c-set-offset 'substatement-open 0)
             ;;(define-key c-mode-map [f7] 'ispell-comments-and-strings)
             (define-key c-mode-map [?\C-c ?a] 'align-params)
             (define-key c-mode-map [?\C-c ?u] 'unalign-params)
             (define-key c-mode-map [C-f1] 'cpp-highlight-buffer)
             (define-key c-mode-map [?\C-c ?C-p] 'find-next-procedure)
             (define-key c-mode-map [?\C-c ?e] 'c-comment-edit)))

(setq c++-mode-hook c-mode-hook)

;; KEY-BINDINGS ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(if (eq window-system 'x)
    (global-set-key [f1] 'font-lock-mode)
  (global-unset-key [?\C-z])
  (global-set-key [?\C-z ?M ?A ?R] 'forward-word)
  (global-set-key [?\C-z ?M ?A ?L] 'backward-word)
  (global-set-key [?\C-z ?N ?P ?U] 'scroll-down)
  (global-set-key [?\C-z ?N ?P ?D] 'scroll-up))

;; -*-function keys-*- ;;
(global-set-key [f2] 'save-buffer)
(global-set-key [f3] 'find-file)
(global-set-key
 (if (equal window-system 'x)
     [C-f3]
   [?\C-z ?C ?F ?3])
 'revert-buffer)
(global-set-key [f4] 'enlarge-window)
(global-set-key
 (if (equal window-system 'x)
     [C-f4]
   [?\C-z ?C ?F ?4])
 'shrink-window)
(global-set-key [f5] 'other-window)
(global-set-key [f6] 'switch-to-buffer)
(global-set-key
 (if (equal window-system 'x)
     [C-f6]
   [?\C-z ?C ?F ?6])
 'bury-buffer)
(global-set-key [f7] 'myspell)
;;(global-set-key [f7] 'ispell-buffer)
(global-set-key
 (if (equal window-system 'x)
     [C-f7]
   [?\C-z ?C ?F ?7])
 'ispell-region)
(global-set-key [f8] 'rename-uniquely)
;;(global-set-key [C-f8] 'delete-other-windows)
(global-set-key [f9] 'compile)
(global-set-key
 (if (equal window-system 'x)
     [C-f9]
   [?\C-z ?C ?F ?9])
 'shell)
(global-set-key
 (if (equal window-system 'x)
     [C-f10]
   [?\C-z ?C ?F ?1 ?0])
 'do-term)
(global-set-key [f11] 'view-term)
(global-set-key
 (if (equal window-system 'x)
     [C-f11]
   [?\C-z ?C ?F ?1 ?1])
 'view-shell)

;; -*-various keys-*- ;;
(global-set-key
 (if (equal window-system 'x)
     [home]
   [?\C-z ?N ?P ?H])
 'beginning-of-buffer)
(global-set-key
 (if (equal window-system 'x)
     [end]
   [?\C-z ?N ?P ?E])
 'end-of-buffer)
(global-set-key [?\M-g] 'goto-line)
(global-set-key [?\M-s] 'isearch-forward-regexp)
(global-set-key [C-right] 'forward-word)
(global-set-key [C-left] 'backward-word)
(global-set-key [C-next] 'scroll-other-window)
(global-set-key [C-prior] 'scroll-other-window-down)
(global-set-key [C-end] 'scroll-other-window-n-lines-ahead)
(global-set-key [C-home] 'scroll-other-window-n-lines-behind)
(global-set-key [?\C-c ?a] 'ediff-buffers)
(global-set-key [?\C-c ?b] 'rename-buffer)
(global-set-key [?\C-c ?\C-w] 'set-tab-width)
(global-set-key [?\C-c ?t] 'telnet)
(global-set-key [?\C-x ?t] 'vxgo)
(global-set-key [?\C-x ?w] 'vxgdb)
(global-set-key [?\C-x ?p] 'update-vxgdb-breakpoint)
(global-set-key [?\C-c ?\C-r] 'rsh)
(global-set-key [?\C-c ?r] 'rmail)
(global-set-key [?\C-c ?s] 'shell)
(global-set-key [?\C-c ?\g] 'grep)
(global-set-key [?\C-c ?\C-g] 'grep-find)
(global-set-key [?\C-x ?\C-g] 'find-grep-dired)
(global-set-key [?\C-c ?f] 'find-dired)
(global-set-key [?\C-c ?\C-f] 'find-name-dired)
(global-set-key [?\C-c ?m] 'man)
(global-set-key [?\C-c ?i] 'glimpse)
(global-set-key [?\C-c ?\C-i] 'gid)
(global-set-key [?\C-c ?c] 'cscope)
(global-set-key [?\C-c ?0] 'cscope-find-c-symbol)
(global-set-key [?\C-c ?1] 'cscope-find-global-definition)
(global-set-key [?\C-c ?3] 'cscope-find-functions-calling)
(global-set-key [?\C-c ?7] 'cscope-find-file)
(global-set-key [?\C-c ?8] 'cscope-find-files-including)
(global-set-key [?\C-c ?\C-m] 'buffer-menu)
(global-set-key [?\C-c ?\C-j] 'find-file-at-point)
(global-set-key [?\C-c ?v] 'unscroll)

;; -*-keys-bound-to-my-functions-*-
(global-set-key [?\C-c ?\C-b] 'ring-a-ding)
(global-set-key [?\C-c ?\u] 'disptime-update-aux)
(global-set-key [?\C-c ?w] 'delete-vertical-whitespace)
(global-set-key [?\C-c ?n] 'delete-until-next-text)
(global-set-key [?\C-c ?p] 'delete-until-previous-text)
(global-set-key [?\C-c ?k] 'kill-line-backward)
(global-set-key [?\C-c ?l] 'copy-line)
(global-set-key [?\C-c ?o] 'open-compile-window)
(global-set-key [?\C-x ?r?q] 'check-spaces)
(global-set-key
 (if (equal window-system 'x)
     [C-up]
   [?\C-z ?C ?A ?U])
 'scroll-n-lines-behind)
(global-set-key
 (if (equal window-system 'x)
     [C-down]
   [?\C-z ?C ?A ?D])
 'scroll-n-lines-ahead)
(global-set-key [?\C-c ?>] 'prefix-lines)
(global-set-key [?\C-c ?<] 'unprefix-lines)
(global-set-key [?\C-c ?q] 'ediff-pred)
(global-set-key [?\C-c ?z] 'ediff-view)
(global-set-key [?\C-c ?x] 'delete-region-aux)
(global-set-key [?\C-c ?d] 'mercury-associates-directory)

;; The END ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;(set-face-foreground 'scroll-bar "gray80")
;;(set-face-foreground 'default "gray80")
;;(set-face-background 'scroll-bar "black")
;;(set-face-background 'default "black")
