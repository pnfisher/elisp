(setq user-mail-address "pfisher@mc.com")
(setq user-full-name "Philip Fisher")
(setq gnus-select-method
      '(nntp "news.eternal-september.org" (nntp-port-number 80)))
(setq send-mail-function 'smtpmail-send-it)
(setq smtpmail-default-smtp-server "exchange.mc.com")
