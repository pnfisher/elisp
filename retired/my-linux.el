(defvar my-linux-src-dir "~/src3/linux-stable")

(defun my-linux-validate (fname)
  (when (not (file-exists-p fname))
    (error (concat "sorry, can't find " fname))))

(defun my-linux-read-only (fname)
  (my-linux-validate fname)
  (find-file fname)
  (unless buffer-read-only (with-no-warnings (if (functionp 'read-only-mode)
                                                 (read-only-mode)
                                               (toggle-read-only)))))

(defun linux-syscalls ()
  (interactive)
  (my-linux-read-only
   (concat my-linux-src-dir "/arch/x86/syscalls/syscall_64.tbl")))

(defun linux-src ()
  (interactive)
  (my-linux-read-only my-linux-src-dir))

(defun linux-config ()
  (interactive)
  (my-linux-read-only (concat my-linux-src-dir "/.config")))

