;; see http://web-mode.org for docs
(provide 'my-web-mode)

(require 'my-submodules)
(require 'web-mode)

(add-to-list 'auto-mode-alist '("\\.html?\\'" . web-mode))

(add-hook
 'web-mode-hook
 '(lambda ()
    (set (make-local-variable 'web-mode-markup-indent-offset) 2)
    (set (make-local-variable 'web-mode-css-inded-offset 2))
    (set (make-local-variable 'web-mode-code-inded-offset 2))))

;;(set-face-attribute 'web-mode-html-tag-face nil :foreground "OrangeRed")
