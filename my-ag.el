(provide 'my-ag)

(require 'my-tools)
(require 'my-grep)

(eval-and-compile (defconst my-ag-binary "ag"))
(defvar my-ag-default-command (format "%s --nogroup" my-ag-binary))
;; 01;31, same as grep so grep-filter will know what to do
(defvar my-ag-color-params "--color --color-match '01;31'")
(defvar my-ag-history nil)
(defvar my-agrc-top-directory "/")

(eval-and-compile
  (my-grep-define-compilation-mode
    'my-ag-mode
    my-ag-binary
    ;; highlight matches
    t))

(cl-assert my-ag-highlight-matches)

(defun my-ag-types ()
  (interactive)
  (when (not (file-exists-p "/usr/bin/ack-grep"))
    (error "/usr/bin/ack-grep missing"))
  (shell-command "/usr/bin/ack-grep --help-types"))
(defalias 'my-ag-filters 'my-ag-types)

(defun my-ag-find-agrc (path)
  (let ((dir (directory-file-name (expand-file-name path)))
        (p nil))
    (if (string-equal dir my-agrc-top-directory)
        nil
      (setq p (concat dir "/.agrc"))
      (if (file-exists-p p)
          p
        (my-ag-find-agrc
         (directory-file-name
          (file-name-directory
           (directory-file-name dir))))))))

(defun my-ag-get-params-agrc (agrc)
  (let (params)
    (when agrc
      (with-temp-buffer
        (insert-file-contents agrc)
        (goto-char (point-min))
        (while (not (eobp))
          (beginning-of-line)
          (when (and (not (looking-at "\\s-*#"))
                     (looking-at "\\s-*\\(.*\\)"))
            (setq params (concat params " " (match-string 1))))
          (forward-line 1))))
    params))

(defun my-ag ()
  (interactive)
  (let* ((backup (nth 1 (assoc my-tools-id my-tags-directory-table)))
         (agrc (my-ag-find-agrc default-directory))
         (agdir (if agrc (file-name-directory agrc) nil))
         (params (my-ag-get-params-agrc agrc))
         (default
           (replace-regexp-in-string
            "\\s-\\s-+"
            " "
            (concat my-ag-binary " " params " ")))
         (index (+ (string-bytes default) 1))
         cmd)
    (when (not (file-exists-p backup))
      (setq backup nil))
    (when (or (not agdir) current-prefix-arg)
      (setq agdir (read-directory-name
                   "directory: "
                   backup
                   backup)))
    (setq cmd
          (replace-regexp-in-string
           (format "^%s" my-ag-binary)
           (concat my-ag-default-command
                   (when my-ag-highlight-matches
                     (concat " " my-ag-color-params)))
           (read-from-minibuffer
            (format "Run %s (like this): " my-ag-binary)
            (cons (concat default (thing-at-point 'symbol)) index)
            nil
            nil
            'ag-history)))
    (with-temp-buffer
      (cd agdir)
      (compilation-start
       cmd
       'my-ag-mode
       #'(lambda (mode) (concat "*" my-ag-binary "*"))))))
