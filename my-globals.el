(provide 'my-globals)

(require 'files)

;; class containing file variables that can be used to automatically
;; make files in designated directories read-only
(dir-locals-set-class-variables
 'my-elisp-source-class
 '((nil . ((tab-width . 8)
           (buffer-read-only . t)))))

(defconst my-cygport (file-exists-p "/etc/cygport.conf"))

;; must come before first require of psvn
(when my-cygport
  (custom-set-variables
   '(temporary-file-directory "/tmp")))

;; because .emacs did a cd to the current source directory before
;; requiring my-compile-stubs, default-directory will be set to the
;; current source directory (this way only .emacs will ever have a
;; literal reference to the current source directory (in case we
;; want to change it down the line)
(defconst my-elisp-directory (expand-file-name default-directory))
(defconst my-hostname
  (or (getenv "HOST")
      (getenv "HOSTNAME")
      (substring (shell-command-to-string
                  (if my-cygport "/usr/bin/hostname" "hostname")) 0 -1)))
(defconst my-username
  (or (getenv "USER")
      (getenv "USERNAME")))
(eval-and-compile
  (defconst my-home-directory (getenv "HOME"))
  (defconst my-opt-install-path
    (let ((path (cond ((file-exists-p "/etc/lsb-release")
                       (with-temp-buffer
                         (insert-file-contents "/etc/lsb-release")
                         (when (re-search-forward
                                "^DISTRIB_CODENAME=\\(.*\\)"
                                nil t)
                           (match-string 1))))
                      ((file-exists-p "/etc/fedora-release")
                       (let (id version-id)
                         (with-temp-buffer
                           (insert-file-contents "/etc/os-release")
                           (when (re-search-forward "^ID=\\(.*\\)" nil t)
                             (setq id (match-string 1)))
                           (goto-char (point-min))
                           (when (re-search-forward "^VERSION_ID=\\(.*\\)")
                             (setq version-id (match-string 1))))
                         (when (and id version-id)
                           (concat id version-id))))
                      (t nil))))
      (when path
        (concat
         my-home-directory
         "/opt/"
         path
         "/"
         (substring (shell-command-to-string "uname -m") 0 -1))))))
