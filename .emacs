;; -*-EMACS-lisp-*- ;;

(let ((elisp-directory (expand-file-name "~/src/elisp")))

  (unless (file-exists-p elisp-directory)
    (error "elisp directory '%s' missing" elisp-directory))

  (when (< (string-to-number emacs-version) 24.4)
    (error
     "%s init files require emacs version 24.4 or greater"
     elisp-directory))

  (add-to-list 'load-path elisp-directory)
  (with-temp-buffer
    ;; so my-globals can defconst a my-elisp-directory variable using
    ;; default-directory instead of having to "know" the real elisp
    ;; src directory (so if we want to change the src directory down
    ;; the road, this is the only literal reference to it)
    (cd elisp-directory)
    (require 'my-globals)))

(defun my-require-advice (feature &optional filename noerror)
  (when (not (featurep feature))
    (message (concat ">> Requiring " (symbol-name feature)))))
(advice-add #'require :before #'my-require-advice)

(unwind-protect
    (require 'my-emacs)
  (advice-remove #'require #'my-require-advice))
