(provide 'my-themes)

(require 'my-globals)
(require 'cl-lib)

(defconst my-theme-directory
  (expand-file-name (concat my-elisp-directory "/my-themes")))
(defconst my-default-themes '(my-basic3 my-basic4 my-basic5))
(defconst my-minimal-theme 'my-basic2)
(defvar my-themes-builtin
  (append
   (cdr my-default-themes)
   (mapcar
    #'(lambda (theme-file)
        (intern (replace-regexp-in-string "-theme.el" "" theme-file)))
    (directory-files
     (concat data-directory "themes")
     nil
     ".*-theme.el"))
   (list (car my-default-themes))))

(add-to-list 'custom-theme-load-path my-theme-directory)
(add-to-list 'load-path my-theme-directory)

;; load my themes
(dolist (theme-file (directory-files my-theme-directory nil ".*-theme.el"))
  (let ((theme (intern (replace-regexp-in-string "-theme.el" "" theme-file))))
    (load-theme `,theme t t)))

;; load some others
(load-theme 'manoj-dark t t)
(load-theme 'wheatgrass t t)
;; enable my default theme
(enable-theme `,(car my-default-themes))

(defun my-disable-all-themes ()
  (interactive)
  (dolist (enabled-theme custom-enabled-themes)
    (disable-theme `,enabled-theme)))

(defun my-enable-theme (theme)
  ;; interactive part copied from enable-theme
  (interactive
   (list (intern (completing-read
                  "My enable custom theme: "
                  obarray (lambda (sym) (get sym 'theme-settings)) t))))
  (when (not (custom-theme-p theme))
    (error "Undefined Custom theme %s" theme))
  (my-disable-all-themes)
  (enable-theme `,theme)
  (when current-prefix-arg
    (enable-theme `,my-minimal-theme)))

(defun my-load-theme (theme)
  ;; interactive part copied directly from load-theme
  (interactive
   (list
    (intern (completing-read
             "My load custom theme: "
             (mapcar 'symbol-name (custom-available-themes))))))
  (unless (custom-theme-name-valid-p theme)
    (error "Invalid theme name `%s'" theme))
  (my-disable-all-themes)
  (load-theme `,theme t)
  (when current-prefix-arg
    (enable-theme `,my-minimal-theme)))

(defun my-cycle-themes (&optional backward)
  (interactive)
  (let ((current-theme (car (last my-themes-builtin)))
        next-theme enabled)
    (if current-prefix-arg
        (progn
          (if (custom-theme-enabled-p `,my-minimal-theme)
              (disable-theme `,my-minimal-theme)
            (enable-theme `,my-minimal-theme)
            (setq enabled t))
          (message
           "%s theme %s on top of theme %s"
           my-minimal-theme
           (if enabled "enabled" "disabled")
           (if (custom-theme-enabled-p `,current-theme)
               current-theme
             "unknown")))
      (if backward
          (setq
           next-theme (car (last my-themes-builtin 2))
           my-themes-builtin
           ;; rotate list backward
           (nconc (last my-themes-builtin) (butlast my-themes-builtin 1)))
        (setq
         next-theme (car my-themes-builtin)
         ;; rotate list forward
         my-themes-builtin (nconc (cdr my-themes-builtin) (list next-theme))))
      (if (not (member next-theme custom-known-themes))
          (my-load-theme next-theme)
        (my-enable-theme next-theme)
        (setq enabled t))
      (message "%s theme is %s" (if enabled "enabled" "loaded") next-theme))))

(defun my-uncycle-themes ()
  (interactive)
  (my-cycle-themes t))
