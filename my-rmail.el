(provide 'my-rmail)

(require 'my-globals)
(require 'my-keymaps)

(require 'shr)
(require 'rmail)
(require 'rmailsum)
(require 'smtpmail)
(require 'pop3)

;; etach.el seems to be out-of-date and not working properly
;; (eval-and-compile
;;   (let ((storage-dir (concat my-elisp-directory "/storage")))
;;     (add-to-list 'load-path storage-dir)))
;; (require 'etach)
;; (setq etach-prompt-me-for-file-names t)
;; (setq etach-detachment-default-directory "~/rmail/detached")
;; (setq
;;  etach-mime-type-alist
;;  (cl-remove-if
;;   #'(lambda (elt)
;;       (let ((type (car elt)))
;;         (or (string-equal type "html")
;;             (string-equal type "htm"))))
;;   etach-mime-type-alist))

;;(autoload 'rhtml2txt "rhtml2txt" nil t)

(defvar my-rmail-approved-host
  (or (string-equal my-hostname "brome")
      (string-equal my-hostname "TP-20141493")
      (string-equal my-hostname "arm.dev.ecobee.com")))
(defvar my-rmail-fetch-mail-from-server t)
(defvar my-rmail-strip-html t)
;; (defvar my-rmail-wrap-lines t)
;; (defvar my-rmail-html-mime-unhide t)

;; (if my-rmail-at-work
;;     (custom-set-variables
;;      '(pop3-leave-mail-on-server nil)
;;      '(pop3-mailhost "exchange.mc.com")
;;      '(pop3-maildrop "pfisher")
;;      '(pop3-password-required t)
;;      '(pop3-port 110))
;;   (custom-set-variables
;;    '(pop3-leave-mail-on-server t)
;;    '(pop3-mailhost "localhost")
;;    '(pop3-maildrop "pfisher")
;;    '(pop3-password-required t)
;;    '(pop3-port 2110)))

;; (if (equal (getenv "USER") "phil")
;;     (load-library "pop-password"))

;; makes things more readable on windows
(when (boundp 'shr-use-fonts)
  (custom-set-variables
   '(shr-use-fonts nil)))

;;(defvar smtpmail-auth-credentials (expand-file-name "~/.authinfo.gpg"))
;;(defvar smtpmail-starttls-credentials '(("smtp.gmail.com" 587 nil nil)))
(custom-set-variables
 '(pop3-uidl-file "~/rmail/pop3-uidl")
 '(pop3-mailhost "outlook.office365.com")
 '(pop3-maildrop "philipf@ecobee.com")
 '(pop3-leave-mail-on-server t)
 '(pop3-password-required t)
 '(pop3-port 995)
 ;; '(pop3-stream-type 'tls)
 '(rmail-preserve-inbox nil)
 '(rmail-primary-inbox-list `("~/rmail/pop3"))
 ;;'(rmail-default-rmail-file "~/rmail/rmail.current")
 '(rmail-delete-after-output nil)
 '(rmail-file-name "~/rmail/RMAIL")
 ;;'(rmail-highlight-face emacs22-line-face)
 '(rmail-mime-attachment-dirs-alist
   `(("text/.*" "~/rmail/detached")
    ("image/.*" "~/rmail/detached")
    (".*" "~/rmail/detached")))
 '(smtpmail-smtp-user "philipf@ecobee.com")
 '(smtpmail-default-smtp-server "smtp.office365.com")
 '(smtpmail-smtp-server "smtp.office365.com")
 '(smtpmail-smtp-service 587)
 '(smtpmail-local-domain "ecobee.com")
 '(smtpmail-sendto-domain "ecobee.com")
 ;;'(smtpmail-debug-info t)
 '(send-mail-function 'smtpmail-send-it)
 '(message-send-mail-function 'smtpmail-send-it)
 ;; '(message-yank-prefix "> ")
 '(message-default-mail-headers "CC: \nBCC: philipf@ecobee.com\n")
 '(message-directory "~/rmail/mail")
 '(message-auto-save-directory "~/rmail/mail/drafts")
 '(user-mail-address "philipf@ecobee.com")
 '(user-full-name "Philip Fisher")
 ;; default mode is now messange-user-agent; it does not understand
 ;; mail-self-blind, so don't set that unless using old
 ;; sendmail-user-agent
 ;; '(mail-user-agent 'sendmail-user-agent)
 ;; '(mail-user-agent 'message-user-agent)
 ;; '(mail-self-blind t)
 '(mail-yank-ignored-headers nil)
 '(mail-signature t)
 '(mail-yank-prefix nil)
 '(mail-indentation-spaces 0))

(defun my-rmail-html2text-force ()
  (interactive)
  (save-excursion
    (goto-char (point-min))
    (when (re-search-forward "^\\s-*$" nil t)
      (read-only-mode -1)
      (shr-render-region (point) (point-max))
      (read-only-mode))))

(defun my-rmail-html2text ()
  (interactive)
  (save-excursion
    (when (re-search-forward "<html" nil t)
      (forward-char -5)
      (let ((p1 (point)))
        (when (re-search-forward "</html>" nil t)
          (let ((p2 (point)))
            (read-only-mode -1)
            (shr-render-region p1 p2)
            (read-only-mode)))))))

;; (if my-rmail-at-work
;;     (custom-set-variables
;;      '(smtpmail-smtp-server "exchange.mc.com")
;;      '(smtpmail-smtp-service 25))
;;   (custom-set-variables
;;    '(smtpmail-smtp-server "localhost")
;;    '(smtpmail-smtp-service 2025)))

(defun my-rmail-toggle-html ()
  (interactive)
  (setq my-rmail-strip-html (not my-rmail-strip-html))
  (when (equal major-mode 'rmail-mode)
    (rmail-show-message)))

;; (defun my-rmail-toggle-wrap ()
;;   (interactive)
;;   (setq rmail-wrap-message (not rmail-wrap-message)))

;; (defun my-rmail-toggle-mime-unhide ()
;;   (interactive)
;;   (setq my-rmail-html-mime-unhide (not my-rmail-html-mime-unhide)))

(defun my-rmail-wrap-lines ()
  (interactive)
  (save-excursion
    (set (make-local-variable 'fill-column) (- (window-width) 1))
    (goto-char (point-min))
    (let ((buffer-read-only))
      (when (re-search-forward "^\\s-*$")
        (while (not (eobp))
          (forward-line)
          (beginning-of-line)
          (let ((p1 (point)))
            (end-of-line)
            (let ((p2 (point)))
              (save-excursion
                (fill-region p1 p2 nil t)))))))))

;; (defadvice rmail-mime-toggle-button
;;   (around html-rmail-toggle-button-advice activate compile)
;;   (if my-rmail-strip-html
;;       (let ((pos (point)))
;;         (end-of-line)
;;         (let ((end (point)))
;;           (beginning-of-line)
;;           (let* ((beg (point))
;;                  (tag (buffer-substring beg end)))
;;             ;; find the mime tag without the actual button
;;             ;; (i.e. Hide or Show)
;;             (if (not (string-match "^\\(\\[[^:]*:[^ ]* \\)" tag))
;;                 (error (concat "search for sub-tag within " tag " failed")))
;;             (let ((tag2 (match-string 1 tag))
;;                   ;; suppress html stripping for the next call
;;                   ;; to rmail-show-message... rmailmm.el needs
;;                   ;; to see the buffer in it's non-stripped state
;;                   ;; to carry-out the next button push
;;                   (my-rmail-strip-html nil))
;;               (rmail-show-message)
;;               ;; search for the tag again, the call to
;;               ;; rmail-show-message may have changed the state of the
;;               ;; button (i.e.  from Hide to Show) because we've made a
;;               ;; previous call to rmail-do-html-mime-unhide which has
;;               ;; altered the buffer's default state, in which case
;;               ;; we're finished.  If the button is still in its
;;               ;; original state then we need to push it
;;               (if (search-forward tag nil t)
;;                   (progn
;;                     (forward-char (- pos end))
;;                     (push-button))
;;                 ;; the button has already changed state, so just
;;                 ;; search for the original tag without the button in
;;                 ;; order to get us back to our original position in
;;                 ;; the buffer (the call to rmail-show-message above
;;                 ;; always takes us back to the start of the buffer)
;;                 (if (not (search-forward tag2 nil t))
;;                     (error (concat "search for " tag2 " failed"))))
;;               (my-rmail-html2text)
;;               (my-rmail-do-wrap-lines)
;;               (beginning-of-line)
;;               (forward-char (- pos beg))))))
;;     ad-do-it
;;     (when my-rmail-strip-html
;;       (error "xxx how did we get here?")
;;       (my-rmail-html2text)
;;       (my-rmail-do-wrap-lines))))

;; (defadvice rmail-mime
;;   (before rmail-mime-advice activate compile)
;;   (when my-rmail-strip-html
;;     (let ((my-rmail-strip-html nil))
;;       (rmail-show-message))))

;; (defun my-rmail-do-html-mime-unhide ()
;;   (if my-rmail-html-mime-unhide
;;       (save-excursion
;;         (goto-char (point-min))
;;         (while (re-search-forward
;;                 "^\\[[^:]*:text/\\(html Show\\|calendar Hide\\)\\]"
;;                 nil
;;                 t)
;;           (beginning-of-line)
;;           (forward-button 1)
;;           (let ((my-rmail-strip-html nil))
;;             (push-button))
;;           (end-of-line)))))

;; (defun my-rmail-do-strip-html ()
;;   ;; (my-rmail-do-html-mime-unhide)
;;   (my-rmail-html2text)
;;   ;; (my-rmail-do-wrap-lines)
;;   )

(defadvice rmail-summary-by-regexp
  (around my-rmail-summary-by-regexp-advice activate compile)
  (let ((rmail-enable-mime nil))
    ad-do-it))

(defadvice rmail-show-message-1
  (after my-rmail-show-message-1 activate compile)
  (if my-rmail-strip-html
      (my-rmail-html2text)))

(defadvice rmail-resend (around my-rmail-resend-safe activate compile)
  (if (not (string-equal mode-name "RMAIL"))
      (error "Sorry: not in RMAIL buffer"))
  (let ((pos (point)))
    (let ((rmail-enable-mime nil)
          (my-rmail-strip-html nil))
      (rmail-show-message)
      ad-do-it)
    (rmail-show-message)
    (goto-char pos)))

(defun my-rmail-resend ()
  (interactive)
  (let ((addr (read-buffer "rmail-resend address: " "philipf@ecobee.com")))
    (rmail-resend addr)))

(defun my-rmail-without-fetch ()
  (interactive)
  (let ((my-rmail-fetch-mail-from-server nil))
    (rmail)))

;;(defadvice sendmail-user-agent-compose
;;  (after sendmail-user-agent-advice activate compile)
;;  (auto-fill-mode nil)
;;  (longlines-mode t))

;;(defadvice compose-mail (after compose-mail-advice activate compile)
;;  (auto-fill-mode nil)
;;  (longlines-mode t))

;;(defadvice mail-send (before mail-send-advice activate compile)
;;  (longlines-mode nil))

;; there seems to be a bug in 23.4's rmail. When it deletes the last
;; message in the RMAIL file, it leaves a blank line at the start of
;; the file; for rmail-convert-file-maybe, this means the file is
;; using an invalid mbox format
(defadvice rmail-convert-file-maybe
  (before my-rmail-convert-file-maybe-advice activate compile)
  (widen)
  (goto-char (point-min))
  (when (and (looking-at "\\(^\\s-*$\\)\n")
             (re-search-forward "\\(^\\s-*$\\)\n" nil t))
    (let ((buffer-read-only nil))
      (replace-match "")
      (goto-char (point-min))))
  (when (and (file-exists-p rmail-file-name)
             (not (equal (file-modes rmail-file-name) #o600)))
    (chmod rmail-file-name #o600)))

(defadvice rmail-input (before rmail-input-advice activate compile)
  (interactive
   (let ((my-rmail-fetch-mail-from-server nil))
     (list (read-file-name "RMAIL file: "
                           "~/rmail/"
                           nil
                           nil
                           "rmail.retired")))))

;; (defadvice rmail (before my-rmail-advice activate compile)
;;   (when (y-or-n-p "Fetch new mail from outlook office365: ")
;;     (let ((pop3-password (rmail-get-remote-password nil)))
;;       (if my-rmail-approved-host
;;           (pop3-movemail (car rmail-primary-inbox-list))
;;         (let ((pop3-leave-mail-on-server
;;                (not (y-or-n-p "Delete mail on server: "))))
;;           (pop3-movemail (car rmail-primary-inbox-list)))))))

(defadvice rmail (before my-rmail-advice activate compile)
  (when (y-or-n-p "Fetch new mail from outlook office365: ")
    (let ((pop3-password (rmail-get-remote-password nil)))
      (if my-rmail-approved-host
          (pop3-movemail (car rmail-primary-inbox-list))
        (let ((pop3-leave-mail-on-server t))
          (pop3-movemail (car rmail-primary-inbox-list)))))))

;; (defadvice rmail (after rmail-update-advice activate compile)
;;   (disptime-update))

(defadvice mail-yank-original
  (around my-mail-yank-original-advice activate compile)
  (goto-char (point-max))
  (insert "\n\n-----Original Message-----\n")
  (let ((header (point)))
    ad-do-it
    (goto-char header))
  (let ((case-fold-search t))
    (while (not (looking-at "^\\s-*$"))
      (if (not (looking-at "\\(from\\|to\\|subject\\|date\\|cc\\): "))
          (let ((p1 (point)))
            (forward-line)
            (delete-region p1 (point)))
        (forward-line)
        (while (and (not (looking-at "\\s-*$"))
                    (looking-at "^\\s-"))
          (forward-line))))
    (goto-char (point-min))
    (when (search-forward mail-header-separator nil t)
      (forward-char 1))))

;;(define-key rmail-summary-mode-map "F" 'mime-forward)
;;(define-key rmail-summary-mode-map "D" 'detach)
(define-key rmail-mode-map [?\M-r] 'rmail-search-backwards)
;;(define-key rmail-mode-map "F" 'mime-forward)
;;(define-key rmail-mode-map "D" 'detach)
;;(define-key rmail-mode-map "N" 'find-next-detach-label)
;;(define-key rmail-mode-map "P" 'find-previous-detach-label)
;;(define-key rmail-mode-map "X" 'delete-detach-label)
(define-key rmail-mode-map "W" 'my-rmail-wrap-lines)
(define-key rmail-mode-map "H" 'my-rmail-html2text-force)
;;(define-key rmail-mode-map [?\C-c ?h] 'html2txt-detach-label)
;;(define-key rmail-mode-map [?\C-c ?H] 'html2txt-detach-label)
(define-key rmail-mode-map "R" 'my-rmail-resend)
(define-key rmail-mode-map "L" 'rmail-add-label)
(define-key rmail-summary-mode-map "L" 'rmail-summary-add-label)
(define-key rmail-mode-map "V" 'my-rmail-toggle-html)
(define-key rmail-mode-map "O" 'rmail-output-body-to-file)
(define-key rmail-summary-mode-map "O" 'rmail-output-body-to-file)

(define-key rmail-mode-map "J"
  '(lambda ()
     (interactive nil)
     (let ((p (point)))
       (rmail-show-message rmail-current-message)
       (goto-char p))))

(define-key rmail-summary-mode-map "I"
  '(lambda ()
     (interactive nil)
     (rmail-summary-add-label "NB")))

(define-key rmail-mode-map "I"
  '(lambda ()
     (interactive nil)
     (rmail-add-label "NB")))

(define-key rmail-summary-mode-map "T"
  '(lambda ()
     (interactive nil)
     (rmail-summary-add-label "TEMP")))

(define-key rmail-mode-map "T"
  '(lambda ()
     (interactive nil)
     (rmail-add-label "TEMP")))

(define-key rmail-summary-mode-map "M"
  '(lambda ()
     (interactive nil)
     (rmail-summary-add-label "Attachments")))

(define-key rmail-mode-map "M"
  '(lambda ()
     (interactive nil)
     (rmail-add-label "Attachments")))

(define-key rmail-summary-mode-map "A"
  '(lambda ()
     (interactive nil)
     (let ((my-rmail-fetch-mail-from-server nil))
       (rmail-summary-input "~/rmail/rmail.archive"))))

(define-key rmail-mode-map "A"
  '(lambda ()
     (interactive nil)
     (let ((my-rmail-fetch-mail-from-server nil))
       (rmail-input "~/rmail/rmail.archive"))))

(define-key rmail-summary-mode-map "D"
  '(lambda ()
     (interactive nil)
     (let ((my-rmail-fetch-mail-from-server nil))
       (rmail-summary-input "~/rmail/rmail.deleted"))))

(define-key rmail-mode-map "D"
  '(lambda ()
     (interactive nil)
     (let ((my-rmail-fetch-mail-from-server nil))
       (rmail-input "~/rmail/rmail.deleted"))))

(define-key rmail-summary-mode-map "a"
  '(lambda ()
     (interactive nil)
     (let ((rmail-delete-after-output t)
           (my-rmail-html-mime-unhide nil))
       (rmail-summary-output "~/rmail/rmail.archive" 1))))

(define-key rmail-mode-map "a"
  '(lambda ()
     (interactive nil)
     (if (equal (buffer-name) "rmail.archive")
         (rmail-delete-forward)
       (let ((rmail-delete-after-output t)
             (my-rmail-html-mime-unhide nil))
         (rmail-output "~/rmail/rmail.archive")))))

(define-key rmail-summary-mode-map "d"
  '(lambda ()
     (interactive nil)
     (let ((rmail-delete-after-output t)
           (my-rmail-html-mime-unhide nil))
       (rmail-summary-output "~/rmail/rmail.deleted" 1))))

(define-key rmail-mode-map "d"
  '(lambda ()
     (interactive nil)
     (if (equal (buffer-name) "rmail.deleted")
         (rmail-delete-forward)
       (let ((rmail-delete-after-output t)
             (my-rmail-html-mime-unhide nil))
         (rmail-output "~/rmail/rmail.deleted")))))

(my-global-set-key (kbd "C-c r") 'rmail)
