(provide 'my-bookmark)

(require 'bookmark)
(require 'cl-lib)

(defvar my-bookmark-current nil)

(cl-defun my-bookmark-cycle (&optional reverse)
  (when (not bookmark-alist)
    (cl-return-from my-bookmark-cycle))
  (let* ((alist
          (sort
           (cl-copy-list bookmark-alist)
           #'(lambda (item1 item2)
               (string-lessp (car item1) (car item2)))))
         (pos (cl-position my-bookmark-current alist))
         (len (length alist)))
    (if (not pos)
        (setq my-bookmark-current (car alist))
      (if reverse
          (setq pos (- (if (= pos 0) len pos) 1))
        (setq pos (if (= (+ pos 1) len) 0 (+ pos 1))))
      (setq my-bookmark-current (nth pos alist))))
  (bookmark-jump (car my-bookmark-current)))

(defun my-bookmark-cycle-forward ()
  (interactive)
  (my-bookmark-cycle nil))

(defun my-bookmark-cycle-reverse ()
  (interactive)
  (my-bookmark-cycle t))

(defun my-bookmark-set-advice (ofunc &optional name &rest args)
  (when (not name)
    (setq name (read-from-minibuffer
                "Set bookmark: "
                (concat (bookmark-buffer-name)
                        "@"
                        (number-to-string (line-number-at-pos)))
                bookmark-minibuffer-read-name-map)))
  (apply ofunc (cons name args)))
(advice-add #'bookmark-set :around #'my-bookmark-set-advice)

(defun my-bookmark-clear ()
  (interactive)
  (let ((bfile "~/.emacs.d/bookmarks"))
    (when (file-exists-p bfile)
      (delete-file bfile)))
  (setq bookmark-alist nil))

(custom-set-variables
 '(bookmark-default-file "~/.emacs.d/bookmarks")
 '(bookmark-save-flag 1))
