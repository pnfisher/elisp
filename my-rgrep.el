(provide 'my-rgrep)

(require 'my-tools)

(require 'compile)
(require 'grep)
(require 'cl-lib)

;; force use of --color in all cases. This is technically unnecessary
;; on emacs versions using older versions of grep.el (which make use
;; of GREP_OPTIONS), but with versions of grep.el that don't support
;; GREP_OPTIONS, if you don't explicitly force the use of --color the
;; call to grep won't generate the desired colorized output
(eval-and-compile
  (cl-letf (((symbol-function 'message) #'ignore))
    (if (= (shell-command "which rgrep >& /dev/null") 0)
        (progn
          (defvar my-rgrep-binary "rgrep --color")
          (defvar my-rgrep-params "-HnIEe"))
      (defvar my-rgrep-binary "grep --color")
      (defvar my-rgrep-params "-rHnIEe"))))
(defvar my-rgrep-mode-hook nil)
(defvar my-rgrep-history nil)
(defvar my-rgreprc-top-directory "/")
(defconst my-rgreprc-file ".rgreprc")

(defun find-rgreprc (path)
  (let ((dir (directory-file-name (expand-file-name path)))
        (p nil))
    (if (string-equal dir my-rgreprc-top-directory)
        nil
      (setq p (concat dir "/" my-rgreprc-file))
      (if (file-exists-p p)
          p
        (find-rgreprc
         (directory-file-name
          (file-name-directory
           (directory-file-name dir))))))))

(defun my-rgrep-get-params-rgreprc (rgreprc)
  (let (params)
    (when rgreprc
      (with-temp-buffer
        (insert-file-contents rgreprc)
        (goto-char (point-min))
        (while (not (eobp))
          (beginning-of-line)
          (when (and (not (looking-at "\\s-*#"))
                     (looking-at "\\s-*\\(.*\\)"))
            (setq params (concat params " " (match-string 1))))
          (forward-line 1))))
    params))

(defun my-rgrep ()
  (interactive)
  (run-hooks 'my-rgrep-mode-hook)
  (let* (
         (backup (nth 1 (assoc my-tools-id my-tags-directory-table)))
         (rgreprc (find-rgreprc default-directory))
         (rgrepdir (if rgreprc (file-name-directory rgreprc) nil))
         (params (my-rgrep-get-params-rgreprc rgreprc))
         (default
           (replace-regexp-in-string
            "\\s-\\s-+"
            " "
            (concat my-rgrep-binary " " params " " my-rgrep-params " ")))
         (index (+ (string-bytes default) 1))
         (grep-use-null-device nil)
         cmd)
    (when (not (file-exists-p backup))
      (setq backup nil))
    (when (or (not rgrepdir) current-prefix-arg)
      (setq rgrepdir (read-directory-name
                   "directory: "
                   backup
                   backup)))
    (setq cmd
          (read-from-minibuffer
            "Run rgrep (like this): "
            (cons (concat default (thing-at-point 'symbol)) index)
            nil
            nil
            'my-rgrep-history))
    (with-temp-buffer
      (cd rgrepdir)
      (grep cmd))))
