(provide 'my-remember)

(require 'my-globals)
(require 'my-keymaps)

(require 'remember)
(require 'grep)

(custom-set-variables
 '(remember-data-file (concat my-elisp-directory "/remember"))
 '(remember-notes-initial-major-mode 'text-mode))

(advice-add
 'remember-notes
 :after
 '(lambda (&rest args)
    (view-mode)))

(defun my-remember-grep (regexp)
  (interactive "sRegex: ")
  (with-temp-buffer
    (cd my-elisp-directory)
    (set (make-local-variable 'grep-use-null-device) nil)
    (grep (concat
           "git --no-pager grep --no-color -nI "
           regexp
           " -- "
           remember-data-file))))
