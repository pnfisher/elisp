(provide 'my-frame)

(require 'my-utils)
(require 'my-funckeys)

(require 'frame)
(require 'cl-lib)

(defvar my-frame-height
  (with-temp-buffer
    (insert (shell-command-to-string "xwininfo -root -shape"))
    (when (re-search-backward "^\\s-*Height:\\s-*\\([0-9]+\\)" nil t)
      (string-to-number (match-string 1)))))
(defvar my-frame-width
  (with-temp-buffer
    (insert (shell-command-to-string "xwininfo -root -shape"))
    (when (re-search-backward "^\\s-*Width:\\s-*\\([0-9]+\\)" nil t)
      (string-to-number (match-string 1)))))
;; If we're forgetting gravity, then we'll assume we're cygwin X11,
;; otherwise we'll assume linux
(defvar my-frame-forget-gravity
  (with-temp-buffer
    (insert (shell-command-to-string "xwininfo -root -bits"))
    (when (re-search-backward "\\s-*Bit gravity: ForgetGravity" nil t)
      t)))
;; X11CLIENT set by myemacs script or my-emacs func
(defvar my-frame-x11client (getenv "X11CLIENT"))
(when (not my-frame-x11client)
  (setq my-frame-x11client my-hostname))
(defvar my-frame-height-position
  (cond
   ;; assuming cygwin
   (my-frame-forget-gravity 35)
   ;; linux, but brome is weird
   ((string-equal my-frame-x11client "brome") 0)
   ;; assume "normal" linux x11
   (t 8)))
(defvar my-frame-height-adjust 3)

(defun my-frame-init ()
  ;; not sure why I have to do this, but when I run using emacsclient,
  ;; style defaults to 'ascii
  ;; (add-hook
  ;;  'ediff-startup-hook
  ;;  '(lambda ()
  ;;     (setq ediff-use-faces            t
  ;;           ediff-highlighting-style  'face
  ;;           ediff-highlight-all-diffs  t)))
  (menu-bar-mode -1)
  (tool-bar-mode -1)
  (set-frame-font "9x15")
  (set-frame-width (selected-frame) 80)
  ;; overset the height and it will default to the full height of
  ;; the current frame, it seems
  ;;(set-frame-height (selected-frame) 1000)
  (if (not my-frame-height)
      (set-frame-height (selected-frame) 1000)
    (let ((h (- (/ my-frame-height 15) my-frame-height-adjust)))
      (if (> h 0)
          (set-frame-height (selected-frame) h)
        (set-frame-height (selected-frame) 1000))))
  (when my-frame-width
    (let ((w (- (/ my-frame-width 2) (/ (* 80 9) 2))))
      (cl-assert (> w 0))
      (message
       "window geometry: width %d, height %d"
       my-frame-width my-frame-height)
      (message
       "frame position: xoffset %d, yoffset %d"
       w my-frame-height-position)
      (set-frame-position (selected-frame) w my-frame-height-position)))
  (my-funckeys-set-control-funckeys)
  (call-process-shell-command "xset b off"))

(defun my-frame-init-w32 ()
  (menu-bar-mode -1)
  (tool-bar-mode -1)
  (set-frame-width (selected-frame) 80)
  (my-funckeys-set-control-funckeys))

(cond ((eq window-system 'x) (my-frame-init))
      ((eq window-system 'w32) (my-frame-init-w32))
      (t (my-funckeys-set-control-x-funckeys)))

(when (eq window-system 'x)
  (defun my-frame-resize ()
    (interactive)
    (my-frame-init)))

;; so new frames (whether created by emacsclient or C-x 5 2 take on
;; the correct settings x window settings)
(add-hook
 'after-make-frame-functions
 #'(lambda (frame)
     (select-frame-set-input-focus frame)
     (my-frame-init)))
