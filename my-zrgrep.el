(provide 'my-zrgrep)

(require 'cl-lib)
(require 'grep)
(require 'files)

(defconst my-zrgrep-emacs-lisp-directory
  (expand-file-name
   (concat
    (car (cl-remove-if-not
          #'(lambda (elt)
              (string-match
               (format
                "%d.%d/lisp$"
                emacs-major-version
                emacs-minor-version)
               elt))
          load-path))
    "/")))

(defun my-zrgrep ()
  (interactive)
  (let* ((grep-setup-hook #'my-zrgrep-process-setup)
         (file-name-history
          (if (member my-zrgrep-emacs-lisp-directory file-name-history)
              file-name-history
            (cons my-zrgrep-emacs-lisp-directory file-name-history))))
    (call-interactively #'zrgrep)))

(defun my-zrgrep-process-setup ()
  (set
   (make-local-variable 'compilation-exit-message-function)
   #'(lambda (status code msg)
       (if (eq status 'exit)
           ;; there appears to be a bug in zgrep in that it returns 1
           ;; even if matches were found, so we're going to rely on
           ;; buffer-modified-p to determine if matches were found
           (cond ((and (< code 2) (buffer-modified-p))
                  '("finished (matches found)\n" . "matched"))
                 ((and (< code 2) (not (buffer-modified-p)))
                  '("finished with no matches found\n" . "no match"))
                 (t
                  (cons msg code)))))))
