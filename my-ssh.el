(provide 'my-ssh)

(require 'my-submodules)

(add-to-history 'ssh-history my-hostname)

(defun my-ssh-directory-tracking-mode-message ()
  (message (concat "ssh-directory-tracking-mode set to "
                   (prin1-to-string ssh-directory-tracking-mode)
                   " (use 'C-c C-t' to toggle)")))

(defun my-toggle-ssh-directory-tracking-mode ()
  (interactive)
  (if (equal ssh-directory-tracking-mode 'local)
      (ssh-directory-tracking-mode)
    (ssh-directory-tracking-mode 1))
  (my-ssh-directory-tracking-mode-message))

(add-hook
 'ssh-mode-hook
 #'(lambda ()
     (my-set-process-query-on-exit-flag nil)
     (my-ssh-directory-tracking-mode-message)))

(define-key ssh-mode-map (kbd "C-c C-t") 'my-toggle-ssh-directory-tracking-mode)
