(provide 'my-grep)

(require 'my-globals)

(require 'cl-lib)
(require 'compile)
(require 'grep)

(when my-cygport
  (cl-assert (boundp 'find-program))
  ;; works around a problem when running emacs from within visual
  ;; studio 2015
  (setq find-program "/usr/bin/find"))

;; define a compilation mode using `define-compile-mode' with most of
;; the settings copied verbatim from `grep-mode' usage of the same
;; defmacro. In other words, we want to leverage `grep-mode'
;; facilities into doing most of our heavy lifting.
(defun my-grep-define-compilation-mode
    (mode binary &optional highlight-matches)
  (let* ((mode-name
          (replace-regexp-in-string "-mode" "" (symbol-name mode)))
         (mode-regexp-alist
          (intern (concat mode-name "-regexp-alist")))
         (mode-highlight-matches
          (intern (concat mode-name "-highlight-matches")))
         (mode-last-buffer
          (intern (concat mode-name "-last-buffer")))
         (mode-scroll-output
          (intern (concat mode-name "-scroll-output")))
         (mode-font-lock-keywords
          (intern (concat mode-name "-mode-font-lock-keywords"))))
    ;; the minimum set of variables our custom mode needs to ensure
    ;; its use of `compilation-start' will behave the way we want
    (eval `(defcustom ,mode-highlight-matches highlight-matches
             ,(format
               "Use special markers to highlight %s matches."
               binary)))
    (eval `(defcustom ,mode-scroll-output nil
             ,(format
               "Non-nil to scroll the %s output buffer as output appears."
               binary)))
    (eval `(defvar ,mode-last-buffer nil))
    (eval `(defvar ,mode-font-lock-keywords
             (mapcar
              #'(lambda (e1)
                  (mapcar
                   #'(lambda (e2)
                       (if (stringp e2)
                           (replace-regexp-in-string "Grep" ,binary e2 t)
                         e2))
                   e1))
              grep-mode-font-lock-keywords)))
    ;; this stuff cut-and-pasted from grep-mode
    (eval
     `(define-compilation-mode ,mode ,binary
        ;; if the binary supports highlighting matches (i.e. grep-like
        ;; escape character generation to mark the beginning and end
        ;; of a match), then let's set `grep-highlight-matches'
        ;; accordingly so `compilation-mode' will (using
        ;; `grep-regexp-alist' -- see below) know at what column the
        ;; first match begins (useful when calling `next-error' etc.)
        (set (make-local-variable 'grep-highlight-matches)
             ,mode-highlight-matches)
        (setq ,mode-last-buffer (current-buffer))
        (set (make-local-variable 'tool-bar-map) grep-mode-tool-bar-map)
        (set (make-local-variable 'compilation-error-face) grep-hit-face)
        ;; if the caller has defined a custom regexp alist
        ;; (e.g. `my-glimpse-regexp-alist') for use as a
        ;; `compilation-error-regexp-alist' then use it, otherwise use
        ;; `grep-regexp-alist'
        (set (make-local-variable 'compilation-error-regexp-alist)
             ,(if (boundp mode-regexp-alist)
                  mode-regexp-alist
                (unintern mode-regexp-alist nil)
                'grep-regexp-alist))
        (set (make-local-variable 'compilation-directory-matcher) '("\\`a\\`"))
        (set (make-local-variable 'compilation-process-setup-function)
             'grep-process-setup)
        (set (make-local-variable 'compilation-disable-input) t)
        (set (make-local-variable 'compilation-error-screen-columns)
             grep-error-screen-columns)
        (add-hook 'compilation-filter-hook 'grep-filter nil t)))))
