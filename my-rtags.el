(provide 'my-rtags)

(require 'my-globals)
(require 'my-utils)

(eval-and-compile
  (when my-opt-install-path
    (let ((rtags-dir (concat my-opt-install-path
                             "/share/emacs/site-lisp/rtags")))
      (when (file-exists-p rtags-dir)
        (add-to-list 'load-path rtags-dir)
        (dir-locals-set-directory-class
         rtags-dir
         'my-elisp-source-class)))))

(require 'rtags nil :noerror)
;; for company-mode stuff and helm stuff etc.
(require 'my-submodules)
(require 'cc-mode)
(require 'make-mode)

(eval-when-compile
  (when (not (boundp 'rtags-mode-map))
    ;; for when we are compiling
    (message "WARNING: my-rtags.el failed to load rtags during compilation")
    ;; suppress compiler warnings when rtags.el fails to load
    (defun rtags-enable-standard-keybindings (&rest args) nil)
    (defun rtags-diagnostics (&rest args) nil)
    (defun rtags-is-indexed (&rest args) nil)
    (defun rtags-find-symbol (&rest args) nil)
    (defun rtags-location-stack-back (&rest args) nil)
    (defvar rtags-autostart-diagnostics nil)
    (defvar rtags-completions-enabled nil)
    (defvar rtags-use-helm nil)))

(if (not (boundp 'rtags-mode-map))
    ;; for when we are interpreting
    (message
     "WARNING: my-rtags.el failed to load rtags, skipping my-rtags setup")
  (if (or (> (call-process-shell-command "which rc") 0)
          (> (call-process-shell-command "rc --version") 0))
      ;; for when we are interpreting
      (message
       "WARNING: my-rtags.el couldn't find rc binary, skipping my-rtags setup")

    (rtags-enable-standard-keybindings c-mode-base-map)
    (rtags-enable-standard-keybindings makefile-mode-map)

    ;;(setq rtags-use-bookmarks nil)
    (setq rtags-autostart-diagnostics t)
    (rtags-diagnostics)
    (setq rtags-completions-enabled t)
    (push 'company-rtags company-backends)
    ;; (global-company-mode t)
    (add-hook 'c-mode-common-hook 'rtags-start-process-unless-running)
    (add-hook 'makefile-mode-hook 'rtags-start-process-unless-running)

    (defun my-tags-find-symbol ()
      (interactive)
      (if (rtags-is-indexed)
          (rtags-find-symbol)
        (call-interactively 'find-tag)))

    (defun my-tags-pop ()
      (interactive)
      (if (rtags-is-indexed)
          (rtags-location-stack-back)
        (pop-tag-mark)))

    (when (require 'helm-config nil :noerror)
      (setq rtags-use-helm t)
      (advice-add 'rtags-handle-results-buffer
                  :around
                  #'(lambda (ofunc &rest args)
                      (let ((helm-autoresize-max-height 50))
                        (apply ofunc args)))))

    ;; won't be useful when rtags-use-helm enabled, but set anyway
    ;; in case we unset rtags-use-helm dynamically
    (define-key c-mode-base-map (kbd "C-,") 'rtags-previous-match)
    (define-key c-mode-base-map (kbd "C-.") 'rtags-next-match)
    ;; basic key settings
    (define-key c-mode-base-map (kbd "C-c r *") 'my-tags-pop)
    ;;(define-key c-mode-base-map (kbd "M-.") 'my-tags-find-symbol)
    (define-key c-mode-base-map (kbd "C-<") 'rtags-location-stack-back)
    (define-key c-mode-base-map (kbd "C->") 'rtags-location-stack-forward)
    (define-key c-mode-base-map (kbd "C-c r i") 'rtags-include-file)))
