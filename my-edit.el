(provide 'my-edit)

(require 'my-keymaps)

(defun my-vertical-delete ()
  (just-one-space)
  (let ((current (point)) (beg))
    (beginning-of-line)
    (setq beg (point))
    (goto-char current)
    (if (= beg (1- current))
        (progn
          (backward-delete-char-untabify 2)
          (my-vertical-delete)))))

(defun my-delete-all-whitespace-but-one ()
  (interactive)
  (let ((beg (point)))
    (re-search-forward "[!-~]")
    (forward-char -1)
    (if (not (= beg (point)))
        (progn
          (delete-region (point) beg)
          (my-vertical-delete)
          (forward-char -1)))))

(defun my-delete-all-whitespace ()
  (interactive)
  (my-delete-all-whitespace-but-one)
  (delete-region (point) (+ (point) 1)))

(defun my-delete-until-next-text ()
  "Delete from the current point until the next block of text."
  (interactive)
  (save-excursion
    (let ((beg (point)))
      (re-search-forward "[!-~]")
      (delete-region (1- (point)) beg))))

(defun my-delete-until-previous-text ()
  "Delete from the current point back to the previous block of text."
  (interactive)
  (let ((beg (point)))
    (re-search-backward "[!-~]")
    (forward-char 1)
    (delete-region (point) beg)))

(defun my-kill-line-backward ()
  "Kills line from point to beginning of line."
  (interactive)
  (let ((beg (point)))
    (beginning-of-line)
    (delete-region (point) beg)))

(defun my-copy-line ()
  (interactive)
  (save-excursion
    (beginning-of-line)
    (let ((beg (point)))
      (end-of-line)
      (copy-region-as-kill beg (point)))))

(defun my-delete-region (top bottom)
  (interactive "r")
  (if (y-or-n-p "delete: ")
      (progn
        (delete-region top bottom)
        (goto-char (point-max)))))

(my-global-set-key (kbd "C-c w") 'my-delete-all-whitespace-but-one)
(my-global-set-key (kbd "C-c W") 'my-delete-all-whitespace)
(my-global-set-key (kbd "C-c n") 'my-delete-until-next-text)
(my-global-set-key (kbd "C-c p") 'my-delete-until-previous-text)
(my-global-set-key (kbd "C-c k") 'my-kill-line-backward)
(my-global-set-key (kbd "C-c l") 'my-copy-line)
