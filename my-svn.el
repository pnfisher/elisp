(provide 'my-svn)

(require 'my-globals)
(require 'my-submodules)

(require 'psvn)
(require 'view)
(require 'shell)
(require 'cl-lib)

(defvar my-svn-help-history nil)
(defvar my-svn-file-history nil)
(defvar my-svn-branch-history nil)
(defvar my-svn-status-history nil)
(defvar my-svn-revision-history nil)
(defvar my-svn-log-flags-history nil)
(defvar my-svn-relative-url-history nil)
(defvar my-svn-extract-directory ".~@@~")
(defvar my-svn-default-num-extract-revisions 10)

(custom-set-variables
 '(svn-status-hide-unmodified t)
 '(svn-status-hide-externals t)
 '(svn-status-hide-unknown t)
 '(svn-status-sort-status-buffer nil)
 '(svn-status-svn-executable
   (if (and my-cygport (file-exists-p "~/bin/mypsvn"))
       ;; script which should set LC_ALL appropriately for use with
       ;; cygwin svn client and then call /usr/bin/svn (the cygwin svn
       ;; client)
       "~/bin/mypsvn"
     "svn"))
 '(svn-status-prefix-key (kbd "C-c s"))
 '(svn-status-display-full-path t)
 '(svn-log-edit-show-diff-for-commit t)
 '(svn-status-ediff-delete-temporary-files :ask))

;; redifinition of psvn function in order to filter out not just
;; unknown files (status ?), but to filter out obstructed files
;; (status ~) as well
(when my-cygport
  (defun svn-status-line-info->hide-because-unknown (line-info)
    (and svn-status-hide-unknown
         (or (eq (svn-status-line-info->filemark line-info) ??)
             (eq (svn-status-line-info->filemark line-info) ?~)))))

(defun my-svn-valid-repository ()
  (unless (svn-version-controlled-dir-p (expand-file-name default-directory))
    (error "Not currently in a valid svn repository")))

(defun my-svn-log-file-name-at-point ()
  (when (not (eq major-mode 'svn-log-view-mode))
    (error "not in log view mode"))
  (car (split-string (svn-log-file-name-at-point nil))))

(defun my-svn-status ()
  (interactive)
  (let ((status-chars (read-from-minibuffer
                       "svn status characters: "
                       "AM"
                       nil nil nil
                       'my-svn-status-history)))
    (switch-to-buffer "*my-svn-status*")
    (read-only-mode -1)
    (erase-buffer)
    (insert (shell-command-to-string
             (concat svn-status-svn-executable
                     (concat
                      " status"
                      (when (not (string-match "^\\s-*$" status-chars))
                        (concat
                         " | egrep '^["
                         status-chars
                         "]'"))))))
    (goto-char (point-min))
    (view-mode)))

(defun my-svn-help ()
  (interactive)
  (let ((command (read-from-minibuffer
                  "svn command: "
                  nil nil nil
                  'my-svn-help-history)))
    (switch-to-buffer "*my-svn-help*")
    (read-only-mode -1)
    (erase-buffer)
    (insert (shell-command-to-string (concat svn-status-svn-executable
                                             " help "
                                             command)))
    (goto-char (point-min))
    (set (make-local-variable 'font-lock-defaults)
         '(shell-font-lock-keywords t))
    (setq-local ansi-color-apply-face-function #'shell-apply-ansi-color)
    (shell-reapply-ansi-color)
    (font-lock-mode t)
    (view-mode)))

(defun my-svn-cat ()
  (interactive)
  (let* ((filename (my-svn-log-file-name-at-point))
         file-info rev)
    (unless (and filename (file-name-directory filename))
      (error "current line contains no valid filename"))
    (save-excursion
      (when (not (re-search-backward "^\\(r[0-9]+\\)" nil t))
        (error "couldn't find revision for filename %s" filename))
      (setq rev (match-string 1)))
    (switch-to-buffer "*my-svn-cat*")
    (read-only-mode -1)
    (erase-buffer)
    (insert (shell-command-to-string (concat
                                      svn-status-svn-executable
                                      " cat -r "
                                      rev
                                      " "
                                      "^/"
                                      filename)))
    (goto-char (point-min))
    (view-mode)))

(defun my-svn-extract-file-revision (rev file-info)
  (let* ((url (cdr (assoc 'url file-info)))
         (filepath (cdr (assoc 'file-path file-info)))
         (filename (cdr (assoc 'file-name file-info)))
         (svn-path (cdr (assoc 'svn-path file-info)))
         (wc-root (cdr (assoc 'wc-root file-info)))
         (xfilename filename)
         catname revname)
    (cl-assert rev)
    (with-temp-buffer
      (cd (concat wc-root "/" filepath))
      (when (not (string-match "$\\s-*r?[0-9]+\\s-*$" rev))
        (insert (shell-command-to-string
                 (concat
                  svn-status-svn-executable
                  " info -r "
                  rev
                  " "
                  svn-path)))
        (goto-char (point-min))
        (when (not (re-search-forward
                    "^Revision: \\([0-9]+\\)+$"
                    nil t))
          (error "couldn't find revision for filename %s, rev %s"
                 svn-path
                 rev))
        (setq rev (match-string 1)))
      (setq revname (concat xfilename "@" rev)
            catname (concat my-svn-extract-directory "/" revname))
      (unless (file-exists-p my-svn-extract-directory)
        (mkdir my-svn-extract-directory t))
      (let ((rc 0))
        (unless (file-exists-p catname)
          (setq rc (call-process-shell-command (concat
                                                svn-status-svn-executable
                                                " cat "
                                                (when rev
                                                  (concat "-r " rev " "))
                                                svn-path
                                                " > "
                                                catname))))
        (when (> rc 0)
          (delete-file catname)
          (setq catname nil)))
      (if catname
          (concat default-directory catname)
        nil))))

(defun my-svn-get-repo-info (filename)
  (let ((rc (call-process-shell-command
             (concat svn-status-svn-executable " info --show-item wc-root")))
        url relative-url repo-root wc-root)
    (if (> rc 0)
        (with-temp-buffer
          (let ((filename (if (or (not filename) (file-exists-p filename))
                              filename
                            (concat "^/" filename))))
            (insert
             (my-shell-command-to-string
              (concat svn-status-svn-executable
                      " info "
                      (when filename
                        filename)))))
          (goto-char (point-min))
          (when (not (re-search-forward "^URL: \\(.*\\)$" nil t))
            (error "couldn't find svn info URL entry"))
          (setq url (match-string 1))
          (when (not (re-search-forward "^Repository Root: \\(.*\\)$" nil t))
            (error "couldn't find svn info Repo Root entry"))
          (setq repo-root (match-string 1))
          (when (not (string-match (concat "^" repo-root "\\(.*\\)$") url))
            (error "Repo Root not part of URL"))
          (setq relative-url (concat "^" (match-string 1 url)))
          (when (not (file-exists-p ".svn"))
            (error "We don't appear to be in an svn repository"))
          (cl-loop
           (when (not (file-exists-p ".svn"))
             (cl-return nil))
           (setq wc-root default-directory)
           (cd "..")))
      (setq
       wc-root
       (my-shell-command-to-string (concat
                                    svn-status-svn-executable
                                    " info --show-item wc-root "
                                    (when filename
                                      filename)))
       relative-url
       (my-shell-command-to-string (concat
                                    svn-status-svn-executable
                                    " info --show-item relative-url "
                                    (when filename
                                      filename)))))
    (list wc-root relative-url)))

(defun my-svn-get-file-info (filename &optional noprompt)
  (let ((original-filename filename)
        (original-dir default-directory)
        (repo-info (my-svn-get-repo-info filename))
        default-url url filepath)
    (when (file-exists-p filename)
      (setq filename (substring (car (cdr repo-info)) 2)))
    (with-temp-buffer
      (cd (car repo-info))
      (setq repo-info (my-svn-get-repo-info nil))
      (setq default-url (car (cdr repo-info)))
      (let ((regexp (concat "^" (substring default-url 2) "/\\(.*\\)")))
        (when (string-match regexp filename)
          (setq filename (concat (match-string 1 filename))))
        (when (not (file-exists-p filename))
          (setq url default-url)))
      (setq url (if noprompt
                    (if url url default-url)
                  (read-from-minibuffer
                   "svn file: "
                   (if url url default-url)
                   nil nil
                   'my-svn-relative-url-history)))
      (when (string-match "\\(.*\\)/+$" url)
        (setq url (match-string 1 url)))
      (when (and (setq filepath (file-name-directory filename))
                 (string-match "\\(.*\\)/+$" filepath))
        (setq filepath (match-string 1 filepath)))
      (list
       `(url . ,url)
       `(wc-root . ,(replace-regexp-in-string "/+$" "" default-directory))
       `(file-path . ,filepath)
       `(file-name . ,(file-name-nondirectory filename))
       `(svn-path . ,(if (string-equal default-url url)
                         (file-name-nondirectory filename)
                       (concat url "/" filename)))))))

(defun my-svn-extract-file-revisions ()
  (interactive)
  (my-svn-valid-repository)
  (let* ((filename (read-from-minibuffer
                    "svn file: "
                    (nth 3 (svn-status-get-line-information))
                    nil nil
                    'my-svn-file-history))
         (file-info (my-svn-get-file-info filename))
         (svn-path (cdr (assoc 'svn-path file-info)))
         (numrevs (string-to-number
                   (read-from-minibuffer
                    "svn limit: "
                    (number-to-string
                     my-svn-default-num-extract-revisions))))
         (current-dir default-directory))
    (when (file-directory-p filename)
      (error "%s is a directory" filename))
    (with-temp-buffer
      (unwind-protect
          (let ((file-path (cdr (assoc 'file-path file-info)))
                (wc-root (cdr (assoc 'wc-root file-info))))
            (cd (concat wc-root "/" file-path))
            (insert (shell-command-to-string
                     (concat
                      svn-status-svn-executable
                      " log -q "
                      (when (> numrevs 0)
                        (format "-l %d " numrevs))
                      svn-path))))
        (cd current-dir))
      (goto-char (point-min))
      (while (re-search-forward "^r[0-9]+" nil t)
        (my-svn-extract-file-revision (match-string 0) file-info)))))

(defun my-svn-ebw-adjust-prompt (prompt)
  (when (string-match
         "\\(main\\|os\\)/branches/\\(.*\\)/\\(embedded\\|eos\\)/\\(.*\\)"
         prompt)
    (let ((path (concat
                 (match-string 1 prompt)
                 "/trunk/"
                 (match-string 3 prompt)
                 "/"
                 (match-string 4 prompt))))
      (setq
       my-svn-file-history
       (cons path (remove path my-svn-file-history))))
    (let ((path (concat
                 (match-string 1 prompt)
                 "/branches/v4.2.0/"
                 (match-string 3 prompt)
                 "/"
                 (match-string 4 prompt))))
      (setq
       my-svn-file-history
       (cons path (remove path my-svn-file-history)))))
  (when (and current-prefix-arg
             (string-match
              "\\(main\\|os\\)/\\(.*\\)/\\(embedded\\|eos\\)/\\(.*\\)"
              prompt))
    (setq prompt (concat
                  (match-string 1 prompt)
                  "/"
                  (read-from-minibuffer
                   "branch: "
                   (match-string 2 prompt)
                   nil nil
                   'my-svn-branch-history)
                  "/"
                  (match-string 3 prompt)
                  "/"
                  (match-string 4 prompt))))
  prompt)

(defun my-svn-file-revision-to-buffer-aux (mode)
  (interactive)
  (cl-assert (not (eq major-mode 'svn-log-view-mode)))
  (let ((filename (cond ((eq major-mode 'dired-mode)
                         (thing-at-point 'filename))
                        ((eq major-mode 'svn-status-mode)
                         (nth 3 (svn-status-get-line-information)))
                        (t (file-name-nondirectory (buffer-file-name)))))
        (rev "PREV")
        prompt original-filename revname1 revname2 repo-info basedir url)
    (unless filename
      (error "filename unknown"))
    (when (string-match "\\(.*?\\)\\(<\\|:\\).*" filename)
      (setq filename (match-string 1 filename)))
    (when (string-match "\\(.*?\\)@r[0-9]+$" filename)
      (setq filename (match-string 1 filename)))
    (setq original-filename filename)
    (setq basedir (if (string-match "\\(.*?\\)\.~@@.*" default-directory)
                      (match-string 1 default-directory)
                    default-directory))
    (with-temp-buffer
      (cd basedir)
      (my-svn-valid-repository)
      (unless (file-exists-p filename)
        (error "filename %s doesn't exist" filename))
      (when (file-directory-p filename)
        (error "filename %s is a directory" filename))
      (find-file filename)
      (setq revname1 (buffer-name)
            repo-info (my-svn-get-repo-info filename)
            prompt (substring (car (cdr repo-info)) 2)
            filename (read-from-minibuffer
                      (format "file to %s: " (if (eq mode 'ediff)
                                                 "ediff"
                                               "extract"))
                      (my-svn-ebw-adjust-prompt prompt)
                      nil nil
                      'my-svn-file-history))
      (if (string-match
           (concat "\\s-*" prompt "\\s-*")
           filename)
          (with-temp-buffer
            (insert (my-shell-command-to-string
                     (concat "svn status " original-filename)))
            (goto-char (point-min))
            (when (looking-at (concat "^[MCR~][M ]\\s-*" original-filename))
              (setq rev "HEAD"))
            (setq url original-filename))
        (setq rev "HEAD"
              url (concat "^/" filename)))
      (setq rev (read-from-minibuffer
                 (format "file rev to %s: " (if (eq mode 'ediff)
                                                "ediff"
                                              "extract"))
                 rev
                 nil nil
                 'my-svn-revision-history)
            revname2 (concat (file-name-nondirectory filename) "@" rev))
      (if (not (or (string-equal rev "FILE")
                   (string-equal rev "")))
          (my-svn-extract-file-revision-to-buffer revname2 rev url)
        (setq filename (concat my-home-directory "/" filename))
        (when (not (file-exists-p filename))
          (error (concat "file " filename " missing")))
        (find-file filename)
        (setq revname2 (buffer-name))))
    (when (eq mode 'ediff)
      (ediff-buffers revname1 revname2))))

(defun my-svn-ediff-revision ()
  (interactive)
  (my-svn-file-revision-to-buffer-aux 'ediff))

(defun my-svn-file-revision-to-buffer ()
  (interactive)
  (my-svn-file-revision-to-buffer-aux 'extract))

(defun my-svn-show-log (args)
  (interactive
   (progn
     (my-svn-valid-repository)
     (list (split-string (read-from-minibuffer
                          "svn log flags: "
                          (concat
                           (mapconcat
                            #'identity
                            svn-status-default-log-arguments
                            " ")
                           " ")
                          nil nil
                          'my-svn-log-flags-history)))))
  (let ((svn-status-default-log-arguments args)
        (filename (cond ((eq major-mode 'dired-mode)
                         (thing-at-point 'filename))
                        ((eq major-mode 'svn-status-mode)
                         (nth 3 (svn-status-get-line-information)))
                        (t (file-name-nondirectory (buffer-file-name)))))
        original-filename basedir prompt)
    (unless filename
      (error "filename unknown"))
    (when (string-match "\\(.*?\\)\\(<\\|:\\).*" filename)
      (setq filename (match-string 1 filename)))
    (when (string-match "\\(.*?\\)@r[0-9]+$" filename)
      (setq filename (match-string 1 filename)))
    (setq original-filename filename)
    (setq basedir (if (string-match "\\(.*?\\)\.~@@.*" default-directory)
                      (match-string 1 default-directory)
                    default-directory))
    (with-temp-buffer
      (cd basedir)
      (my-svn-valid-repository)
      (unless (file-exists-p filename)
        (error "filename %s doesn't exist" filename))
      (when (file-directory-p filename)
        (error "filename %s is a directory" filename))
      (let ((repo-info (my-svn-get-repo-info filename)))
        (setq prompt (substring (car (cdr repo-info)) 2)
              filename (read-from-minibuffer
                        "file to log: "
                        (my-svn-ebw-adjust-prompt prompt)
                        nil nil
                        'my-svn-file-history)))
      (if (string-match (concat "\\s-*" prompt "\\s-*") filename)
          (setq filename original-filename)
        (setq filename (concat "^/" filename)))
      (cl-letf (((symbol-function 'svn-status-line-info->filename)
                 #'(lambda (line-info)
                     filename)))
        (call-interactively #'svn-status-show-svn-log)))))

(defun my-svn-status-show-svn-log (args)
  (interactive
   (progn
     (my-svn-valid-repository)
     (list (split-string (read-from-minibuffer
                          "svn log flags: "
                          (concat
                           (mapconcat
                            #'identity
                            svn-status-default-log-arguments
                            " ")
                           " ")
                          nil nil
                          'my-svn-log-flags-history)))))
  (my-svn-valid-repository)
  (let ((svn-status-default-log-arguments args)
        (filename (nth 3 (svn-status-get-line-information)))
        original-filename prompt)
    (unless filename
      (error "filename unknown"))
    (setq original-filename filename)
    (unless (file-exists-p filename)
      (error "filename %s doesn't exist" filename))
    (let ((repo-info (my-svn-get-repo-info filename)))
      (setq prompt (substring (car (cdr repo-info)) 2)
            filename (read-from-minibuffer
                      "file to log: "
                      (my-svn-ebw-adjust-prompt prompt)
                      nil nil
                      'my-svn-file-history)))
    (if (string-match (concat "\\s-*" prompt "\\s-*") filename)
        (setq filename original-filename)
      (setq filename (concat "^/" filename)))
    (cl-letf (((symbol-function 'svn-status-line-info->filename)
               #'(lambda (line-info)
                   filename)))
      (call-interactively #'svn-status-show-svn-log))))

(cl-defun my-svn-switch-worktree (branch &optional ignore-ancestry)
  (interactive "sbranch: ")
  (my-svn-valid-repository)
  (switch-to-buffer "*my-svn-switch*")
  (read-only-mode -1)
  (erase-buffer)
  (when (> (call-process-shell-command (concat
                                        svn-status-svn-executable
                                        " switch "
                                        (when ignore-ancestry
                                          "--ignore-ancestry ")
                                        (when (not current-prefix-arg)
                                          "-q ")
                                        branch) nil t) 0)
    (goto-char (point-min))
    (when (and (looking-at "^svn: E195012:")
               (y-or-n-p "run with --ignore-ancestry? "))
      (cl-return-from my-svn-switch-worktree (my-svn-switch-worktree branch t)))
    (view-mode)
    (error "svn switch failed"))
  (goto-char (point-min))
  (let ((rev-message
         (save-excursion
           (when (re-search-forward "^\\(Updated to\\|At\\) revision" nil t)
             (buffer-substring (save-excursion (beginning-of-line) (point))
                               (save-excursion (end-of-line) (point)))))))
    (view-mode)
    (svn-status-update default-directory)
    (when rev-message
      (message rev-message))))

;; (defun my-svn-status-show-svn-log ()
;;   (interactive)
;;   (let ((svn-status-edit-svn-command t))
;;     (call-interactively #'svn-status-show-svn-log)))

(defun my-svn-log-view-next-file (&optional backward)
  (interactive)
  (my-svn-valid-repository)
  (let ((regexp "^\\s-+[ACDGM]\\s-+.*"))
    (when (if (not backward)
              (re-search-forward regexp nil t)
            (re-search-backward regexp nil t))
      (beginning-of-line)
      (re-search-forward "^\\s-+"))))

(defun my-svn-log-view-previous-file ()
  (interactive)
  (my-svn-log-view-next-file t))

(defun my-svn-find-dired-tmp-files ()
  (interactive)
  (find-name-dired "." "*.~*~"))

(defun my-svn-log-view-first ()
  (interactive)
  (goto-char (point-min))
  (svn-log-view-next))

(defun my-svn-log-view-last ()
  (interactive)
  (goto-char (point-max))
  ;; svn-log-view-prev always searches backwards twice (because it
  ;; assumes it's already on a match)
  (svn-log-view-prev)
  (svn-log-view-next))

(defun my-svn-log-view-revision (rev)
  (interactive "nrevision: ")
  (my-svn-valid-repository)
  (let ((regexp (format "^r%d\\s-+|" rev)))
    (when (not (re-search-forward regexp nil t))
      (when (not (re-search-backward regexp nil t))
        (error "couldn't find log entry for revision %d" rev)))
    (beginning-of-line)
    (svn-log-view-next)))

(defun my-svn-wc-root ()
  (let ((prev default-directory))
    (cl-loop
     (when (not (file-exists-p ".svn"))
       (cl-return prev))
     (setq prev default-directory)
     (cd ".."))))

(defun my-svn-cd-file-directory (filename)
  (let ((tmp filename))
    (cd (my-svn-wc-root))
    (cl-loop
     (when (file-exists-p tmp)
       (cd (file-name-directory tmp))
       (cl-return))
     (when (= (length tmp) 0)
       (setq tmp nil)
       (cl-return))
     (setq tmp (mapconcat #'identity
                          (cdr (split-string tmp "/"))
                          "/")))
    tmp))

(defun my-svn-extract-file-revision-to-buffer (revname rev url)
  (let ((buf (current-buffer))
        (rc 0))
    (switch-to-buffer revname)
    (read-only-mode -1)
    (erase-buffer)
    (switch-to-buffer buf)
    (setq rc (call-process-shell-command (concat
                                          svn-status-svn-executable
                                          " cat -r "
                                          rev
                                          " "
                                          url)
                                         nil
                                         (get-buffer revname)
                                         t))
    (if (= rc 0)
        (progn
          (switch-to-buffer revname)
          (goto-char (point-min))
          (view-mode))
      (kill-buffer revname)
      (error "extraction failed"))))

(cl-defun my-svn-extract-file-revision-log-view ()
  (interactive)
  (let* ((filename (my-svn-log-file-name-at-point))
         (url (concat "^/" filename))
         (rev
          (save-excursion
            (when (not (re-search-backward "^\\(r[0-9]+\\)" nil t))
              (error "couldn't find revision for filename %s" filename))
            (match-string 1))))
    (with-temp-buffer
      (let* ((revname (concat (file-name-nondirectory filename) "@" rev))
             (catname (concat my-svn-extract-directory "/" revname))
             (tmp (my-svn-cd-file-directory filename))
             (rc 0))
        (when (not tmp)
          (my-svn-extract-file-revision-to-buffer revname rev url)
          (cl-return-from my-svn-extract-file-revision-log-view revname))
        (when (not (y-or-n-p (format "filename %s" tmp)))
          (my-svn-extract-file-revision-to-buffer revname rev url)
          (cl-return-from my-svn-extract-file-revision-log-view revname))
        (setq filename (file-name-nondirectory tmp))
        (unless (file-exists-p catname)
          (unless (file-exists-p my-svn-extract-directory)
            (mkdir my-svn-extract-directory t))
          (setq rc (call-process-shell-command (concat
                                                svn-status-svn-executable
                                                " cat -r "
                                                rev
                                                " "
                                                filename
                                                " > "
                                                catname)))
          (when (> rc 0)
            (delete-file catname)
            (setq rc (call-process-shell-command (concat
                                                  svn-status-svn-executable
                                                  " cat -r "
                                                  rev
                                                  " "
                                                  url
                                                  " > "
                                                  catname)))
            (when (> rc 0)
              (delete-file catname)
              (setq catname nil))))
        (if (not catname)
            (error "extraction failed")
          (find-file catname))
        revname))))

(defun my-svn-adjust-revision (filename rev)
  (if (string-match "$\\s-*r?[0-9]+\\s-*$" rev)
      rev
    (with-temp-buffer
      (insert (shell-command-to-string
               (concat
                svn-status-svn-executable
                " info -r "
                rev
                " "
                (concat "^/" filename))))
      (goto-char (point-min))
      (when (not (re-search-forward
                  "^Revision: \\([0-9]+\\)+$"
                  nil t))
        (error "couldn't find revision for filename %s, rev %s"
               filename
               rev))
      (match-string 1))))

(defun my-svn-ediff-file-revision-log-view ()
  (interactive)
  (let* ((buf (current-buffer))
         (filename (my-svn-log-file-name-at-point))
         (url (concat "^/" filename))
         (rev
          (save-excursion
            (when (not (re-search-backward "^\\(r[0-9]+\\)" nil t))
              (error "couldn't find revision for filename %s" filename))
            (match-string 1)))
         (revname1 (concat (file-name-nondirectory filename) "@" rev))
         revname2)
    (my-svn-extract-file-revision-to-buffer revname1 rev url)
    (switch-to-buffer buf)
    (setq rev "HEAD")
    (save-excursion
      (forward-line 1)
      (when (re-search-forward filename nil t)
        (when (re-search-backward "^\\(r[0-9]+\\)" nil t)
          (setq rev (match-string 1)))))
    (setq filename (read-from-minibuffer "svn second file: "
                                         filename
                                         nil nil
                                         'my-svn-file-history)
          rev (read-from-minibuffer
               "svn rev: "
               rev
               nil nil
               'my-svn-revision-history)
          url (concat "^/" filename)
          revname2 (concat (file-name-nondirectory filename) "@" rev))
    (my-svn-extract-file-revision-to-buffer revname2 rev url)
    (ediff-buffers revname1 revname2)))

(define-key svn-global-keymap (kbd "H") #'my-svn-help)
(define-key svn-global-keymap (kbd "L") #'my-svn-show-log)
(define-key svn-global-keymap (kbd "@") #'my-svn-file-revision-to-buffer)

(define-key svn-status-mode-map (kbd "L") #'my-svn-status-show-svn-log)
(define-key svn-status-mode-map (kbd "S") #'my-svn-switch-worktree)
(define-key svn-status-mode-map (kbd "@") #'my-svn-extract-file-revisions)
(define-key svn-status-mode-map (kbd "t") #'my-svn-find-dired-tmp-files)
(define-key svn-status-mode-map (kbd "Q") #'my-svn-ediff-revision)
(define-key svn-status-mode-map (kbd "!") #'my-svn-ediff-revision)

(define-key svn-log-view-mode-map (kbd "c") #'my-svn-cat)
(define-key svn-log-view-mode-map (kbd "j") #'my-svn-log-view-next-file)
(define-key svn-log-view-mode-map (kbd "J") #'my-svn-log-view-previous-file)
(define-key svn-log-view-mode-map (kbd "<") #'my-svn-log-view-first)
(define-key svn-log-view-mode-map (kbd ">") #'my-svn-log-view-last)
(define-key svn-log-view-mode-map (kbd "r") #'my-svn-log-view-revision)
(define-key svn-log-view-mode-map (kbd "s") #'svn-status-update)
(define-key svn-log-view-mode-map (kbd "Q")
  #'my-svn-ediff-file-revision-log-view)
(define-key svn-log-view-mode-map (kbd "!")
  #'my-svn-ediff-file-revision-log-view)
(define-key svn-log-view-mode-map (kbd "@")
  #'my-svn-extract-file-revision-log-view)
(define-key svn-log-view-mode-map (kbd "/") #'View-search-regexp-forward)
(define-key svn-log-view-mode-map (kbd "N") #'View-search-last-regexp-forward)
(define-key svn-log-view-mode-map (kbd "P") #'View-search-last-regexp-backward)
