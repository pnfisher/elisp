#!/bin/bash

ln -sf $(pwd)/.emacs ${HOME}/.emacs

[ ! -d submodules ] && ./submodules.sh
