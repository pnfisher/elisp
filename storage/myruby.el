;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; ruby
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(if emacs22
    (progn
      (let ((rdir "/usr/share/emacs/site-lisp/ruby1.8-elisp"))
        (if (file-accessible-directory-p rdir)
            (setq load-path (cons rdir load-path))))
      ;; must be in path before ruby1.8-elisp since it is a newer,
      ;; better version
      (setq load-path (cons "~/elisp/rdebug" load-path))
      (require 'rdebug)
      (defun rdebug-window-layout-basic (src-buf name)
        (delete-other-windows)
        (split-window nil)
        (set-window-buffer
         (selected-window) src-buf)
        (other-window 1)
        (set-window-buffer
         (selected-window) (rdebug-get-buffer "variables" name))
        (split-window nil)
        (other-window 1)
        (set-window-buffer
         (selected-window) (rdebug-get-buffer "cmd" name))
        (goto-char (point-max)))
      ;;(setq rdebug-many-windows nil)
      (setq rdebug-use-separate-io-buffer nil)
      (setq rdebug-populate-common-keys-function nil)
      (setq rdebug-window-layout-function 'rdebug-window-layout-basic)))

;;(if emacs22
;;    (progn
;;      (let ((rdir "~/elisp/rails"))
;;        (if (file-accessible-directory-p rdir)
;;            (progn
;;              (setq load-path (cons rdir load-path))
;;              (require 'rails))))))

(defun ri (term)
  (interactive "sterm: ")
  (split-window)
  (other-window 1)
  (let ((buf (switch-to-buffer (get-buffer-create (concat "*ri<" term ">*")))))
    (if (= (buffer-size) 0)
        (progn
          (erase-buffer)
          (use-local-map (append (make-sparse-keymap) (current-local-map)))
          (define-key (current-local-map) " " 'scroll-up)
          (define-key (current-local-map) "\M- " 'scroll-down)
          (define-key (current-local-map) "q" 'quit-window)
          (define-key (current-local-map) "<" 'beginning-of-buffer)
          (define-key (current-local-map) ">" 'end-of-buffer)
          (define-key (current-local-map) "/" 'search-forward-regexp)
          (shell-command (concat "ri " term) buf)
          (setq buffer-read-only t)))))
