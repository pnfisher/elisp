;; Semantic
(semantic-mode t)
(global-semantic-idle-completions-mode)
(global-semantic-idle-summary-mode)
(global-semantic-decoration-mode)
;;(global-semantic-highlight-func-mode t)
;;(global-semantic-show-unmatched-syntax-mode t)





;; Autocomplete
;;(require 'auto-complete-config)
;;(add-to-list 'ac-dictionary-directories (expand-file-name
;;             "~/.emacs.d/elpa/auto-complete-1.4.20110207/dict"))
;;(setq ac-comphist-file (expand-file-name
;;             "~/.emacs.d/ac-comphist.dat"))
;;(ac-config-default)