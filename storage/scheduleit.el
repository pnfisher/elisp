(require 'view)
(require 'cl)

(defvar schedit-python-app "/h/phil/code/python/scheduleit.py")
(defvar schedit-buffer-name-prefix "*Schedit")
(defvar schedit-buffer-alist nil)

(defvar schedit-mode-map
  (let ((map (make-sparse-keymap)))
    (suppress-keymap map)
    (set-keymap-parent map view-mode-map)
    (define-key map "D" '(lambda ()
                           (interactive)
                           (schedit-jump t)))
    (define-key map "U" '(lambda ()
                           (interactive)
                           (schedit-jump nil)))
    (define-key map "j" '(lambda ()
                           (interactive)
                           (schedit-jump t)))
    (define-key map "u" '(lambda ()
                           (interactive)
                           (schedit-jump nil)))
    (define-key map "n" 'schedit-next)
    (define-key map "N" '(lambda ()
                           (interactive)
                           (schedit-next-buffer t)))
    (define-key map "p" 'schedit-prev)
    (define-key map "P" '(lambda ()
                           (interactive)
                           (schedit-next-buffer nil)))
    (define-key map "i" 'schedit-info)
    (define-key map "c" 'schedit-create-reservation)
    (define-key map "d" 'schedit-del-reservation)
    (define-key map "m" 'schedit-modify-reservation)
    (define-key map "l" 'schedit-list-reservations)
    ;;(define-key map "\n" 'schedit)
    (define-key map "\r" 'schedit)
    (define-key map "g" 'schedit-reload)
    (define-key map "f" '(lambda ()
                           (interactive)
                           (when view-last-regexp
                             (re-search-forward view-last-regexp nil t))))
    (define-key map "r" '(lambda ()
                           (interactive)
                           (when view-last-regexp
                             (re-search-backward view-last-regexp nil t))))
    map))

(defun schedit-mode ()
  (kill-all-local-variables)
  (setq mode-name "Schedit")
  (setq major-mode 'schedit-mode)
  ;;(use-local-map schedit-mode-map)
  (view-mode)
  (set (make-local-variable 'view-no-disable-on-exit) t)
  ;; With Emacs 22 `view-exit-action' could delete the selected window
  ;; disregarding whether the help buffer was shown in that window at
  ;; all.  Since `view-exit-action' is called with the help buffer as
  ;; argument it seems more appropriate to have it work on the buffer
  ;; only and leave it to `view-mode-exit' to delete any associated
  ;; window(s).
  (setq view-exit-action
        (lambda (buffer)
          ;; Use `with-current-buffer' to make sure that `bury-buffer'
          ;; also removes BUFFER from the selected window.
          (with-current-buffer buffer
            (bury-buffer))))
  (set (make-local-variable 'minor-mode-overriding-map-alist)
       (list (cons 'view-mode schedit-mode-map))))

(defun schedit-insertion-filter (proc string)
  ;;(display-buffer (process-buffer proc))
  (with-current-buffer (process-buffer proc)
    (save-excursion
      ;; Insert the text, advancing the process marker.
      (goto-char (process-mark proc))
      (let ((buffer-read-only nil))
        (insert string))
      (set-marker (process-mark proc) (point)))))

(defun schedit-insertion-sentinel (proc msg)
  (if (memq (process-status proc) '(exit signal))
      (let ((buffer (process-buffer proc)))
        (if (null (buffer-name buffer))
            ;; buffer killed
            (set-process-buffer proc nil)
          (with-current-buffer buffer
            ;;(goto-char (point-min))
            ;;(setq buffer-read-only t)
            (delete-process proc))))))

(defun schedit-jump (fwd)
  (interactive)
  (let ((bname (buffer-name (current-buffer))))
    (if (string-match "#i>\\*$" bname)
        (schedit-find t)
      (let ((sfunc (if fwd 're-search-forward 're-search-backward))
            (msg "no more test resources")
            (regexp "^[^R].* reservations for \\(.*\\)$")
            (p (point)))
    (if (funcall sfunc regexp nil t)
        (goto-char (match-beginning 1))
      (goto-char p)
      (message msg))))))

(defun* schedit-find (fwd)
  (let ((bname (buffer-name (current-buffer)))
        (sfunc (if fwd 're-search-forward 're-search-backward))
        (p (point))
        msg regexp)
    (cond ((string-match "#i>\\*$" bname)
           (setq msg "no more test resources"
                 regexp "^## Resource \\[\\(.*?\\)\\] ###"))
          ((string-match "short#l>\\*$" bname)
           (setq msg "no more reservations"
                 regexp
                 (concat
                  "^\\* [0-9][0-9]/[0-9][0-9]/[0-9][0-9][0-9][0-9] "
                  "@ "
                  "\\([0-9]?[0-9]:[03]0[ap]m\\) ")))
          (t
           (setq msg "no more reservations"
                 regexp "^\\* \\([0-9]?[0-9]:[03]0[ap]m\\) ")))
    (when (string-match "<short#i>\\*$" bname)
      (forward-line (if fwd 1 -1))
      (return-from schedit-find))
    (if (funcall sfunc regexp nil t)
        (goto-char (match-beginning 1))
      (goto-char p)
      (message msg))))

(defun schedit-next ()
  (interactive)
  (schedit-find t))

(defun schedit-prev ()
  (interactive)
  (schedit-find nil))

(defun schedit-encode-date (day month year)
  (apply 'encode-time (list 0 0 0 day month year 0 0 (current-time-zone))))

(defun schedit-days-to-reservation (rsv)
  (let ((ctime (parse-time-string (current-time-string)))
        (rtime (parse-time-string rsv)))
    (format
     "%d"
     (-
      (time-to-days (schedit-encode-date (nth 3 rtime)
                                         (nth 4 rtime)
                                         (nth 5 rtime)))
      (time-to-days (schedit-encode-date (nth 3 ctime)
                                         (nth 4 ctime)
                                         (nth 5 ctime)))))))

;; if you want to specify no tmachine and you can't just enter
;; return because you're being prompted with a default
(defun schedit-no-tmachine (tmachine)
  (if (string-match "^\\(\.\\|-\\)?$" tmachine) t nil))

;; use of ' incase resource name contains ( or )
(defun schedit-munge-tmachine (tmachine)
  (concat "'" tmachine "'"))

;; remove use of ' for use of name in buffer name
(defun schedit-unmunge-tmachine (tmachine)
  (if (and (> (length tmachine) 1)
           (equal (substring tmachine 0 1) "'")
           (equal (substring tmachine -1) "'"))
      (substring tmachine 1 -1)
    tmachine))

(defun schedit-next-buffer (fwd)
  (setq schedit-buffer-alist
        (loop for item in schedit-buffer-alist
              if (buffer-name (car item))
              collect item))
  (let ((len (length schedit-buffer-alist)))
    (when (> len 1)
      (let ((i (position (current-buffer) schedit-buffer-alist :key 'car)))
        (if fwd
            (when (= (setq i (+ i 1)) len)
              (setq i 0))
          (when (= (setq i (- i 1)) -1)
            (setq i (- len 1))))
        (switch-to-buffer (car (nth i schedit-buffer-alist)))))))

(defun schedit-add-buffer-alist (args buf)
  (setq schedit-buffer-alist
        (sort
         (cons
          (cons buf args)
          (loop for item in schedit-buffer-alist
                if (and (buffer-name (car item))
                        (not (eq (car item) buf)))
                collect item))
         '(lambda (item1 item2)
            (let ((s1 (buffer-name (car item1)))
                  (s2 (buffer-name (car item2))))
              (string-lessp s1 s2))))))

(defun schedit-command (args bname)
  (let ((buf (get-buffer bname)))
    (if buf
        (switch-to-buffer buf)
      (schedit-start-command args (get-buffer-create bname)))))

(defun schedit-start-command (args buf)
  (schedit-add-buffer-alist args buf)
  (switch-to-buffer buf)
  (goto-char (point-min))
  (setq buffer-read-only nil)
  (delete-region (point-min) (point-max))
  (let ((proc (start-process-shell-command
               "schedit"
               (current-buffer)
               (concat
                schedit-python-app
                args))))
    (set-process-filter proc 'schedit-insertion-filter)
    (set-process-sentinel proc 'schedit-insertion-sentinel)
    (schedit-mode)))

(defun schedit-reload ()
  (interactive)
  (let* ((buf (current-buffer))
         (args (cdr (assoc buf schedit-buffer-alist))))
    (when (not args)
      (error "couldn't find args for current buffer"))
    (schedit-start-command args buf)))

(defun* schedit-get-buffer-info ()
  (save-excursion
    (let ((args (cdr (assoc (current-buffer) schedit-buffer-alist)))
          tmachine rsvtime rsvdate day-offset endtime)
      (flet ((return-buffer-info ()
               (return-from schedit-get-buffer-info
                 (list day-offset rsvtime tmachine endtime))))
        (when (not args)
          (return-buffer-info))
        ;; list buffer with short option
        (when (string-match "-l.*?-s\\s-*" args)
          (beginning-of-line)
          (when (not (looking-at
                      (concat
                       "\\* \\([0-9][0-9]\\)/"
                       "\\([0-9][0-9]\\)/"
                       "\\([0-9][0-9][0-9][0-9]\\) @ "
                       "\\([0-9]?[0-9]:[03]0[ap]m\\)\\s-*"
                       "\\(.*?\\)\\s-*\\[To "
                       "\\([0-9]?[0-9]:[03]0[ap]m\\)\\]")))
            (return-buffer-info))
          (setq day-offset
                (schedit-days-to-reservation
                 (format-time-string
                  "%b %d, %Y" ;; Jun 02, 2012
                  (schedit-encode-date
                   ;; month
                   (string-to-number (match-string 2))
                   ;; day
                   (string-to-number (match-string 1))
                   ;; year
                   (string-to-number (match-string 3)))))
                rsvtime (match-string 4)
                tmachine (schedit-munge-tmachine (match-string 5))
                endtime (match-string 6))
          (return-buffer-info))
        ;; list buffer, no short option
        (when (string-match "-l\\s-*" args)
          (when (string-match "-o\\s-*?\\([0-9]+\\)" args)
            (setq day-offset (match-string 1 args)))
          (when (looking-at (concat "\\([0-9]?[0-9]:[03]0[ap]m\\) "
                                    ".*? \\[To "
                                    "\\([0-9]?[0-9]:[03]0[ap]m\\)"
                                    "\\]"))
            (setq rsvtime (match-string 1))
            (setq endtime (match-string 2))
            (when (not (re-search-backward " for \\(.*\\)$" nil t))
              (return-buffer-info)))
          (beginning-of-line)
          ;; don't remove the space at the start of the next regexp;
          ;; it's how we distinguish between an actual reservation for a
          ;; resource, and a buffer's first-line header
          (when (not (re-search-forward " reservations for \\(.*\\)$" nil t))
            (return-buffer-info))
          (setq tmachine (schedit-munge-tmachine (match-string 1)))
          ;; now we're looking for the buffer's first line header
          (when (not (re-search-backward
                      "^Reservations for ... \\(.*\\)$"
                      nil t))
            (return-buffer-info))
          (setq rsvdate (match-string 1))
          (when (not day-offset)
            (setq day-offset (schedit-days-to-reservation rsvdate)))
          (return-buffer-info))
        ;; info buffer
        (when (string-match "-i\\s-*" args)
          ;; with short option?
          (cond ((string-match "-s\\s-*" args)
                 (beginning-of-line)
                 (let ((p (point)))
                   (end-of-line)
                   (setq tmachine (schedit-munge-tmachine (buffer-substring
                                                           p
                                                           (point))))))
                ;; or not short option?
                (t
                 (forward-line 1)
                 (beginning-of-line)
                 (when (re-search-backward
                        "^## Resource \\[\\(.*?\\)\\] ###"
                        nil t)
                   (setq tmachine
                         (schedit-munge-tmachine (match-string 1)))))))
        (return-buffer-info)))))

(defun schedit-info()
  (interactive)
  (let* ((buffer-info (schedit-get-buffer-info))
         (bname (concat schedit-buffer-name-prefix "<"))
         (tmachine (nth 2 buffer-info))
         (args " -i"))
    (when (not tmachine)
      (setq tmachine (read-buffer "system: " (thing-at-point 'symbol))))
    (when (schedit-no-tmachine tmachine)
      (setq tmachine ""))
    (if (not (equal tmachine ""))
        (setq args (concat args " -n " tmachine)
              bname (concat bname (schedit-unmunge-tmachine tmachine)))
      (if (y-or-n-p "just names: ")
          (setq args (concat args " -s")
                bname (concat bname "short"))
        (setq bname (concat bname "all"))))
    (setq bname (concat bname "#i>*"))
    (schedit-command args bname)))

(defun schedit-list-reservations ()
  (interactive)
  (let* ((buffer-info (schedit-get-buffer-info))
         (bname (concat schedit-buffer-name-prefix "<"))
         (tmachine (nth 2 buffer-info))
         (day-offset (nth 0 buffer-info))
         (args " -l"))
    (setq tmachine
          (read-buffer "system: "
                       (if tmachine
                           (schedit-unmunge-tmachine tmachine)
                         (thing-at-point 'symbol))))
    (when (equal tmachine "*")
      (setq tmachine "all"))
    (when (not day-offset)
      (setq day-offset "0"))
    (if (schedit-no-tmachine tmachine)
        (setq args (concat args " -s")
              bname (concat bname "short"))
      (setq args (concat args " -n " (schedit-munge-tmachine tmachine))
            bname (concat bname (schedit-unmunge-tmachine tmachine)))
      (when (not (equal tmachine "my"))
        (setq day-offset (read-buffer "days in future: " day-offset))
        (when (not (equal day-offset "0"))
            (setq args (concat args " -o " day-offset)))))
    (setq bname (concat
                 bname
                 "#l"
                 (when (not (string-match
                             (concat
                              "^\\"
                              schedit-buffer-name-prefix
                              "<\\(my\\|short\\)")
                             bname))
                   day-offset)
                 ">*"))
    (schedit-command args bname)))

(defun schedit-create-reservation ()
  (interactive)
  (let* ((buffer-info (schedit-get-buffer-info))
         (tmachine (nth 2 buffer-info))
         (day-offset (nth 0 buffer-info))
         (args "-c"))
    (when (not tmachine)
      (setq tmachine (read-buffer "system: " (thing-at-point 'symbol))))
    (if (schedit-no-tmachine tmachine)
        (error "no test machine specified"))
    (setq args (concat args " -n " tmachine))
    (when (not day-offset)
      (setq day-offset "0"))
    (let ((begin (read-from-minibuffer "begin time: ")))
      (if (equal begin "")
          (error "begin time missing")
        (setq args (concat args " -b " begin))))
    (let ((end (read-from-minibuffer "end time: ")))
      (if (equal end "")
          (error "end time missing")
        (setq args (concat args " -e " end))))
    (setq day-offset  (read-buffer "days in future: " day-offset))
    (when (not (equal day-offset "0"))
      (setq args (concat args " -o " day-offset)))
    (shell-command (concat "scheduleit " args " &"))))

(defun schedit-del-reservation ()
  (interactive)
  (let* ((buffer-info (schedit-get-buffer-info))
         (tmachine (nth 2 buffer-info))
         (rtime (nth 1 buffer-info))
         (day-offset (nth 0 buffer-info))
         (args "-d"))
    (when (not tmachine)
      (setq tmachine (read-buffer "system: " (thing-at-point 'symbol))))
    (when (schedit-no-tmachine tmachine)
      (error "no test machine specified"))
    (when (not day-offset)
      (setq day-offset "0"))
    (when (not rtime)
      (setq rtime (read-from-minibuffer "reservation start time: ")))
    (when (equal rtime "")
      (error "no reservation start time specified"))
    (setq args (concat args " " rtime " -n " tmachine))
    (setq day-offset (read-buffer "days in future: " day-offset))
    (when (not (equal day-offset "0"))
      (setq args (concat args " -o " day-offset)))
    (shell-command (concat "scheduleit " args " &"))))

(defun schedit-modify-reservation ()
  (interactive)
  (let* ((buffer-info (schedit-get-buffer-info))
         (endtime (nth 3 buffer-info))
         (tmachine (nth 2 buffer-info))
         (rtime (nth 1 buffer-info))
         (day-offset (nth 0 buffer-info))
         (args "-m"))
    (when (not tmachine)
      (setq tmachine (read-buffer "system: " (thing-at-point 'symbol))))
    (when (schedit-no-tmachine tmachine)
      (error "no test machine specified"))
    (when (not day-offset)
      (setq day-offset "0"))
    (when (not rtime)
      (setq rtime (read-from-minibuffer "reservation start time: ")))
    (when (equal rtime "")
      (error "no reservation start time specified"))
    (setq args (concat args " " rtime " -n " tmachine))
    (let ((begin (read-buffer "begin time: " rtime)))
      (when (not (equal begin ""))
        (setq args (concat args " -b " begin))))
    (let ((end (read-buffer "end time: " endtime)))
      (when (not (equal end ""))
        (setq args (concat args " -e " end))))
    (when (not (string-match "-[be] " args))
      (error "no begin or end time specified -- at least one required"))
    (setq day-offset (read-buffer "days in future: " day-offset))
    (when (not (equal day-offset "0"))
      (setq args (concat args " -o " day-offset)))
    (shell-command (concat "scheduleit " args " &"))))

(defun schedit ()
  (interactive)
  (let ((mode (read-buffer "info, list, create, modify, or del " "list")))
    (cond ((or (equal mode "list") (equal mode "l"))
           (schedit-list-reservations))
          ((or (equal mode "create") (equal mode "c"))
           (schedit-create-reservation))
          ((or (equal mode "del") (equal mode "d"))
           (schedit-del-reservation))
          ((or (equal mode "modify") (equal mode "m"))
           (schedit-modify-reservation))
          ((or (equal mode "info") (equal mode "i"))
           (schedit-info))
          (t
           (error "invalid command")))))
