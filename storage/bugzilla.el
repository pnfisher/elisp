(require 'view)
(require 'cl)

(defvar bz-python-app "/h/phil/code/python/bugzilla.py")
(defvar bz-buffer-name-prefix "*Bugs")
(defvar bz-buffer-alist nil)

(defvar bz-mode-map
  (let ((map (make-sparse-keymap)))
    (suppress-keymap map)
    (set-keymap-parent map view-mode-map)
    (define-key map "n" 'bz-next)
    (define-key map "N" '(lambda ()
                           (interactive)
                           (bz-next-buffer t)))
    (define-key map "p" 'bz-prev)
    (define-key map "P" '(lambda ()
                           (interactive)
                           (bz-next-buffer nil)))
    (define-key map "\n" 'bz-go)
    (define-key map "\r" 'bz-go)
    (define-key map "v" 'bz-go)
    (define-key map "g" 'bz-reload)
    (define-key map "s" 'bz-search)
    (define-key map "b" 'bz)
    (define-key map "f" '(lambda ()
                           (interactive)
                           (when view-last-regexp
                             (re-search-forward view-last-regexp nil t))))
    (define-key map "r" '(lambda ()
                           (interactive)
                           (when view-last-regexp
                             (re-search-backward view-last-regexp nil t))))
    map))

(defun bz-mode ()
  (kill-all-local-variables)
  (setq mode-name "Bugzilla")
  (setq major-mode 'bz-mode)
  ;;(use-local-map bz-mode-map)
  (view-mode)
  (set (make-local-variable 'view-no-disable-on-exit) t)
  ;; With Emacs 22 `view-exit-action' could delete the selected window
  ;; disregarding whether the help buffer was shown in that window at
  ;; all.  Since `view-exit-action' is called with the help buffer as
  ;; argument it seems more appropriate to have it work on the buffer
  ;; only and leave it to `view-mode-exit' to delete any associated
  ;; window(s).
  (setq view-exit-action
        (lambda (buffer)
          ;; Use `with-current-buffer' to make sure that `bury-buffer'
          ;; also removes BUFFER from the selected window.
          (with-current-buffer buffer
            (bury-buffer))))
  (set (make-local-variable 'minor-mode-overriding-map-alist)
       (list (cons 'view-mode bz-mode-map))))

(defun bz-find (fwd)
  (let ((bname (buffer-name (current-buffer)))
        (sfunc (if fwd 're-search-forward 're-search-backward))
        (p (point))
        msg regexp)
    (if (equal bname (concat bz-buffer-name-prefix "*"))
        (setq msg "no more bugs"
              regexp "^[0-9][0-9][0-9]+ ")
      (setq msg "no more comments"
            regexp
             (concat
              "^\\(Description \\|Comment [0-9]+ \\) .*? "
              "2[0-9][0-9][0-9]-[0-9][0-9]-[0-9][0-9] "
              "[0-9][0-9]:[0-9][0-9]:[0-9][0-9]")))
    (when (and fwd (looking-at regexp))
      (forward-char 1))
    (if (funcall sfunc regexp nil t)
        (beginning-of-line)
      (goto-char p)
      (message msg))))

(defun bz-next ()
  (interactive)
  (bz-find t))

(defun bz-prev ()
  (interactive)
  (bz-find nil))

(defun bz-insertion-filter (proc string)
  ;;(display-buffer (process-buffer proc))
  (with-current-buffer (process-buffer proc)
    (save-excursion
      ;; Insert the text, advancing the process marker.
      (goto-char (process-mark proc))
      (let ((buffer-read-only nil))
        (insert string))
      (set-marker (process-mark proc) (point)))))

(defun bz-insertion-sentinel (proc msg)
  (if (memq (process-status proc) '(exit signal))
      (let ((buffer (process-buffer proc)))
        (if (null (buffer-name buffer))
            ;; buffer killed
            (set-process-buffer proc nil)
          (with-current-buffer buffer
            ;;(save-excursion
            ;;  (goto-char (process-mark proc))
            ;;  (let ((buffer-read-only nil))
            ;;    (insert "\n" (substring (current-time-string) 0 19))))
            (delete-process proc))))))

;; if you want to specify no bug and you can't just enter
;; return because you're being prompted with a default
(defun bz-no-bug (bug)
  (if (string-match "^\\(\.\\|-\\)?$" bug) t nil))

(defun bz-next-buffer (fwd)
  (setq bz-buffer-alist
        (loop for item in bz-buffer-alist
              if (buffer-name (car item))
              collect item))
  (let ((len (length bz-buffer-alist)))
    (when (> len 1)
      (let ((i (position (current-buffer) bz-buffer-alist :key 'car)))
        (if fwd
            (when (= (setq i (+ i 1)) len)
              (setq i 0))
          (when (= (setq i (- i 1)) -1)
            (setq i (- len 1))))
        (switch-to-buffer (car (nth i bz-buffer-alist)))))))

(defun bz-add-buffer-alist (args buf)
  (setq bz-buffer-alist
        (sort
         (cons
          (cons buf args)
          (loop for item in bz-buffer-alist
                if (and (buffer-name (car item))
                        (not (eq (car item) buf)))
                collect item))
         '(lambda (item1 item2)
            (let ((s1 (buffer-name (car item1)))
                  (s2 (buffer-name (car item2))))
              (string-lessp s1 s2))))))

(defun bz-reload ()
  (interactive)
  (let* ((buf (current-buffer))
         (args (cdr (assoc buf bz-buffer-alist))))
    (when (not args)
      (error "couldn't find args for current buffer"))
    (bz-start-command args buf)))

(defun bz-start-command (args buf)
  (bz-add-buffer-alist args buf)
  (switch-to-buffer buf)
  (goto-char (point-min))
  (setq buffer-read-only nil)
  (delete-region (point-min) (point-max))
  (let ((proc (start-process-shell-command
               "bz"
               (current-buffer)
               (concat
                bz-python-app
                args))))
    (set-process-filter proc 'bz-insertion-filter)
    (set-process-sentinel proc 'bz-insertion-sentinel)
    (bz-mode)))

(defun bz-command (bug)
  (let (args bname buf)
    (if (bz-no-bug bug)
        (setq bname (concat bz-buffer-name-prefix "*")
              args (let ((user (read-from-minibuffer
                                "user (default me): ")))
                     (when (not (equal user ""))
                       (concat " -u " user))))
      (setq bname (concat bz-buffer-name-prefix "<" bug ">*")
            args (concat " -b "
                         bug
                         (when (not (y-or-n-p "basic: "))
                           " -f"))))
    ;; most basic bugzilla command can have no args, in which case
    ;; args will be nil; but let's force args to be an empty string in
    ;; such cases so that bz-reload will always find a non-nil set of
    ;; args for any valid bugzilla buffer
    (when (not args)
      (setq args ""))
    (if (and (setq buf (get-buffer bname))
             ;; let's force reload of existing buffer if existing
             ;; buffer using different args from current request
             (equal args (cdr (assoc buf bz-buffer-alist))))
        (switch-to-buffer buf)
      (bz-start-command args (get-buffer-create bname)))))

(defun bz ()
  (interactive)
  (let ((bug (read-buffer "Bug: " (thing-at-point 'word))))
    (bz-command bug)))

(defun bz-go ()
  (interactive)
  (let ((bug (thing-at-point 'word)))
    (when (string-match "^[0-9][0-9][0-9][0-9]+$" bug)
      (bz-command bug))))

(defun bz-search ()
  (interactive)
  (let* ((stext (read-buffer "search text: " (thing-at-point 'word)))
         (bname (concat bz-buffer-name-prefix "<search>*"))
         (args (concat " -s \'" search "\'")))
    (bz-start-command args (get-buffer-create bname))))
