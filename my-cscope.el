(provide 'my-cscope)

(require 'my-tools)
(require 'my-gtags-cscope)
(require 'my-grep)

(require 'cl-lib)

(eval-and-compile (defconst my-cscope-binary "cscope"))
(defvar my-cscope-history nil)
(defvar my-cscope-top-root my-home-directory)
;; gtags-cscope doesn't support [a]ssigned, so slip it in here
(defvar my-cscope-prompt (concat "[a]ssigned " my-gtags-cscope-prompt))
(defvar my-cscope-command-table
  (append
   my-gtags-cscope-command-table
   '((?a . ("9" . "assigned to")))))
(defvar my-cscope-regexp-alist my-gtags-cscope-regexp-alist)

(eval-and-compile
  (my-grep-define-compilation-mode
    'my-cscope-mode
    my-cscope-binary))

(cl-assert (not my-cscope-highlight-matches))

(defun my-cscope-find-root (path)
  (let ((p (directory-file-name (expand-file-name path))))
    (if (string-equal p my-cscope-top-root)
        nil
      (if (file-exists-p (concat p "/cscope.out"))
          t
        (my-cscope-find-root
         (directory-file-name
          (file-name-directory
           (directory-file-name p))))))))

(defun my-cscope ()
  (interactive)
  (cl-loop
   (message my-cscope-prompt)
   (let* ((arg (read-char))
          (entry (cdr (assoc arg my-cscope-command-table))))
     (when (char-equal arg ?q)
       (cl-return))
     (when entry
       (my-cscope-aux (car entry) (cdr entry))
       (cl-return)))))

(defun my-cscope-aux (command prompt)
  (let* ((token
          (read-from-minibuffer
           (concat "Find " prompt ": ")
           (cons (or (thing-at-point 'symbol) "") 1)
           nil
           nil
           'my-cscope-history))
         (backup (nth 1 (assoc my-tools-id my-tags-directory-table)))
         (cddir (if (or (not (my-cscope-find-root default-directory))
                        current-prefix-arg)
                    (read-file-name
                     "directory: "
                     backup
                     backup
                     nil
                     nil
                     nil)
                  nil)))
    (when (not (string-match "^\\s-*\"" token))
      (setq token (concat "\"" token "\"")))
    (with-temp-buffer
      (when cddir (cd cddir))
      (compilation-start
       (concat
        my-cscope-binary
        " -f cscope.out -d -L"
        command
        " "
        token)
       'my-cscope-mode
       #'(lambda (mode) "*cscope*")))))
