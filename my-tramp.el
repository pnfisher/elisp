(provide 'my-tramp)

(defun my-tramp-aux (host)
  (let ((fname
         (if (eq major-mode 'dired-mode)
             (thing-at-point 'filename)
           (let* ((bname (buffer-name))
                  (match (string-match "<\\|:" bname 0)))
             (if match
                 (substring bname 0 match)
               bname)))))
    (find-file
     (concat "/ssh:"
             host
             ":/"
             (expand-file-name default-directory)
             "/"
             (when (file-exists-p fname) fname)))))

(defun my-tramp-root ()
  (interactive)
  (my-tramp-aux "root@localhost"))

(defun my-tramp-host ()
  (interactive)
  (let ((host (read-string "target host: ")))
    (if (string-equal host "root")
        (my-tramp-root)
      (my-tramp-aux host))))
