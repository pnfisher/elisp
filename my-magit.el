(provide 'my-magit)

(require 'my-globals)
(require 'my-submodules)
(require 'my-keymaps)
(require 'my-text)

(require 'cl-lib)
(require 'vc-hooks)
(require 'ispell)

(defvar my-magit-log-all-args
  ;;'("-n256" "--graph" "--decorate "--follow")
  ;;'("-n256" "--graph" "--decorate" "--simplify-by-decoration")
  '("-n256" "--graph" "--decorate"))
(defvar my-magit-grep-default-args "-nI" )
(defvar my-magit-grep-history nil)
(defvar my-magit-overlay nil)
(defface my-magit-highlight-face
  '((t (:inherit 'isearch)))
  "Face for my-magit-highlight-section-search-string"
  :group 'isearch)
(set-face-background 'my-magit-highlight-face "magenta3")
(set-face-foreground 'my-magit-highlight-face "white")
(defvar my-magit-popup-customizations
  '(
    (:actions magit-reset-popup ?c "checkout file" magit-checkout-file)
    (:switches magit-patch-popup ?S "signoff" "--signoff")
    (:switches magit-log-popup ?F "full history" "--full-history")
    (:switches magit-pull-popup ?t "tags" "--tags")
    )
  )

;; turn off mode line messages about git
(custom-set-variables
 '(magit-ediff-quit-hook
   '(magit-ediff-cleanup-auxiliary-buffers delete-other-windows))
 '(magit-log-margin '(nil age-abbreviated magit-log-margin-width t 18))
 '(git-commit-fill-column 69)
 '(vc-handled-backends (delq 'Git vc-handled-backends)))

;; (when my-cygport
;;   (defun magit-status-refresh-buffer ()
;;     (let ((magit-git-executable "/cygdrive/c/Program Files/Git/mingw64/libexec/git-core/git.exe"))
;;       (magit-git-exit-code "update-index" "--refresh")
;;       (magit-insert-section (status)
;;         (magit-run-section-hook 'magit-status-sections-hook))
;;       (run-hooks 'magit-status-refresh-hook))))

;; (when my-cygport
;;   (custom-set-variables
;;    '(magit-git-executable "/usr/bin/git")))

(add-hook
 'git-commit-setup-hook
 #'(lambda ()
     (my-magit-insert-staged-files)
     (set (make-local-variable 'ispell-check-comments) nil)
     (set (make-local-variable 'column-number-mode) t)))

;; process my-magit-popup-customizations
(dolist (custom my-magit-popup-customizations)
  (let* ((type (cl-first custom))
         (popup (cl-second custom))
         (ch (cl-third custom))
         (current (plist-get (eval popup) type)))
    (if (cl-member-if
         #'(lambda (entry)
             (let ((case-fold-search nil))
               (and entry (char-equal entry ch))))
         (if (stringp (cl-first current)) (cdr current) current)
         :key 'car)
        (message
         (concat "WARNING: my-magit.el did not define a new "
                 (symbol-name popup)
                 " "
                 (symbol-name type)
                 " for key '"
                 (char-to-string ch)
                 "' (key already in use)"))
      (cond ((eq type :actions)
             (apply #'magit-define-popup-action (cdr custom)))
            ((eq type :switches)
             (apply #'magit-define-popup-switch (cdr custom)))
            (t
             (error (concat "my-magit.el: unrecognized type "
                            (symbol-name type))))))))

;; there's a bug in git, where listing worktrees segfaults if
;; --no-pager is specified, so let's skip doing worktree stuff until
;; it is fixed
;; (add-hook 'magit-status-sections-hook 'magit-insert-worktrees)

(defun my-magit-insert-staged-files ()
  (when (y-or-n-p (format "insert staged files into %s? " (buffer-name)))
    (goto-char (point-min))
    (insert
     (mapconcat
      #'identity
      (magit-staged-files)
      "\n"))
    ;; force squashed message down two lines
    (insert "\n\n")
    (goto-char (point-min))))

(defun my-magit-git-grep ()
  (interactive)
  (let ((git-dir (magit-toplevel default-directory)))
    (if (or (not git-dir) current-prefix-arg)
        (command-execute #'grep)
      (let* ((prompt (concat "git grep " my-magit-grep-default-args " "))
             (cmd
              (read-from-minibuffer
               "Run git grep (like this): "
               (cons
                (concat prompt (thing-at-point 'symbol))
                (+ (string-bytes prompt) 1))
               nil
               nil
               'my-magit-grep-history)))
        (with-temp-buffer
          (cd git-dir)
          (set (make-local-variable 'grep-use-null-device) nil)
          ;; replace 'git grep ...' with 'git --no-pager grep ...'
          (grep (concat "git --no-pager" (substring cmd 3))))))))

(defun my-magit-log-all (&optional file)
  (interactive)
  (if current-prefix-arg
      (magit-log-buffer-file-popup)
    (when (not file)
      (setq file (magit-current-file)))
    (magit-log-all
     my-magit-log-all-args
     (if file (list file) nil))))

(defun my-magit-jump-to-file-log ()
  (interactive)
  (let ((file (or (magit-current-file)
                  (magit-file-at-point)
                  (magit-read-file "Jump to log for file" t))))
    (when (not file)
      (error "unrecognized file"))
    (my-magit-log-all file)))

(defun my-magit-visit-file ()
  (interactive)
  (let ((file (or (magit-current-file)
                  (magit-read-file "File to visit" t)))
        (rev (magit-branch-or-commit-at-point)))
    (when (not file)
      (error "no git repo file associated with current buffer"))
    (when (not rev)
      (setq rev (magit-read-branch-or-commit "Revision")))
    (if (magit-rev-verify rev)
        (magit-find-file rev file)
      (message (concat
                rev
                " doesn't appear to be a legitimate revision")))))

(defun my-magit-file-status-to-string ()
  (interactive)
  (let ((file (magit-current-file)))
    (when (not file)
      (error "no git repo file associated with current buffer"))
    (let ((entry (magit-file-status (file-name-nondirectory file))))
      (if entry
          (concat (char-to-string (cl-third (car entry)))
                  (char-to-string (cl-fourth (car entry))))
        nil))))

(defun my-magit-file-status-display ()
  (interactive)
  (let ((status (my-magit-file-status-to-string)))
    (message (concat "git status: '"
                     (if status
                         status
                       "up-to-date")
                     "'"))))

;; (defun my-magit-file-status-display ()
;;   (interactive)
;;   (message (concat "git status: "
;;                    (symbol-name
;;                     (vc-git-state (buffer-file-name (current-buffer)))))))

(defun my-magit-ediff-current-buffer ()
  (interactive)
  (if current-prefix-arg
      (call-interactively 'magit-ediff-compare)
    (let ((file (magit-current-file))
          (status (my-magit-file-status-to-string)))
      (if status
          (if (or (string-equal status "??")
                  (string-equal status "R ")
                  (string-equal status "A "))
              (error (concat file " has inappropriate status (" status ")"))
            (magit-ediff-compare "HEAD" nil file file))
        (when (y-or-n-p "Buffer not staged or modified. Jump to log? ")
          (my-magit-log-all))))))

(defun my-magit-ediff-log-revision-at-point-with-head ()
  (interactive)
  (when (not (derived-mode-p 'magit-log-mode))
    (error "magit-log-mode not active"))
  (let ((file (or (magit-current-file)
                  (magit-read-file "File to compare" t)))
        (rev (magit-branch-or-commit-at-point)))
    (when (not file)
      (error
       "no file specified or associated with this magit-log-mode session"))
    (when (not rev)
      (error "no revision or branch at point"))
    (magit-ediff-compare "HEAD" rev file file)))

(defun my-magit-diff-log-revision-at-point-with-head ()
  (interactive)
  (when (not (derived-mode-p 'magit-log-mode))
    (error "magit-log-mode not active"))
  (let ((rev (magit-branch-or-commit-at-point)))
    (when (not rev)
      (error "no revision or branch at point"))
    (magit-diff rev)))

;; modified version of isearch-overlay (from isearch.el)
(defun my-magit-highlight (beg end)
  (if my-magit-overlay
      ;; Overlay already exists, just move it.
      (move-overlay my-magit-overlay beg end (current-buffer))
    ;; Overlay doesn't exist, create it.
    (setq my-magit-overlay (make-overlay beg end))
    ;; 1001 is higher than lazy's 1000 and ediff's 100+
    (overlay-put my-magit-overlay 'priority 1001)
    (overlay-put my-magit-overlay 'face 'my-magit-highlight-face)))

(defun my-magit-jump-to-diffstat-or-diff ()
  (magit-section-show-level-3-all)
  (magit-jump-to-diffstat-or-diff))

(defun my-magit-show-commit-other-window ()
  (interactive)
  (call-interactively 'magit-show-commit)
  (my-magit-jump-to-diffstat-or-diff)
  (other-window -1))

(defun my-magit-show-commit-other-window-with-search ()
  (interactive)
  ;;(apply #'magit-show-commit (magit-commit-at-point) (magit-diff-arguments))
  (call-interactively 'magit-show-commit)
  (let ((laps 0)
        regex found)
    (dolist (item (car (magit-log-arguments)) regex)
      (when (string-match "^\-[GS]\\s-*\\(.*\\)" item)
        (setq regex (match-string 1 item))))
    (if (not regex)
        (my-magit-jump-to-diffstat-or-diff)
      ;; make sure the whole commit is visible for searching
      (magit-section-show-level-4-all)
      ;; this is a bit hacky, but technically it'll do the right thing
      ;; and break from the loop if for some unexplained reason we
      ;; can't find our search item anywhere in a valid diff hunk in
      ;; the entirety of the current commit (which shouldn't be
      ;; possible, or why did git return the search results it did).
      (while (not (or found (= laps 2)))
        (setq found (re-search-forward regex nil t))
        (when (not found)
          (goto-char (point-min))
          (setq laps (1+ laps))
          (setq found (re-search-forward regex nil t)))
        (when found
          (setq found nil)
          (if (eq (magit-section-type (magit-current-section)) 'hunk)
              (save-excursion
                (beginning-of-line)
                (when (looking-at "^[+-]")
                  (setq found t)))
            (magit-section-forward))))
      ;; if for some unexplained reason we broke from the above while
      ;; loop without ever finding our search item in any valid diff
      ;; hunk in the commit, then print a warning message and fall
      ;; back to jumping to the first diff in the commit
      (if (not found)
          (progn
            (message (concat "Search for expression " regex " unsuccessful"))
            (my-magit-jump-to-diffstat-or-diff))
        ;; otherwise, highlight the search result
        (save-excursion
          (let ((p1 (point)))
            (re-search-backward regex nil t)
            (my-magit-highlight p1 (point))))
        (recenter)))
    (other-window -1)))

(global-magit-file-mode)

(define-key git-commit-mode-map (kbd "TAB") 'my-indent-for-tab-command)
(define-key magit-mode-map (kbd "J") 'my-magit-jump-to-file-log)
(define-key magit-log-mode-map (kbd "j")
  'my-magit-show-commit-other-window-with-search)
(define-key magit-log-mode-map (kbd "J") 'my-magit-show-commit-other-window)
(define-key magit-log-mode-map (kbd "C") 'magit-checkout-file)
(define-key magit-log-mode-map (kbd "/") 'my-magit-visit-file)
(define-key magit-log-mode-map (kbd ".")
  'my-magit-ediff-log-revision-at-point-with-head)
(define-key magit-log-mode-map (kbd ",")
  'my-magit-diff-log-revision-at-point-with-head)

;; we might as well do this, since magit itself will do this once it
;; runs the first time
(my-global-set-key (kbd "C-x g") 'magit-status)
