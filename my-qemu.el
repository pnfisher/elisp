(provide 'my-qemu)

(require 'my-globals)
(require 'my-term)
(require 'comint)

(defun my-qemu-boot-telnet-try-again ()
  (y-or-n-p-with-timeout
   "telnet connection failed, try again? "
   4
   t))

(defun my-qemu-boot-telnet (port bname)
  (telnet "localhost " port)
  (rename-buffer (concat "*" bname "*"))
  (sleep-for 0.1)
  (if (not (re-search-backward "^Connected to localhost." nil t))
      (not (my-qemu-boot-telnet-try-again))
    (goto-char (point-max))
    t))

(defun my-qemu-boot-term (port bname)
  (my-term-aux nil ;;don't kill buffer on exit
               "telnet"
               (concat "localhost " port)
               bname)
  (sleep-for 0.1)
  (if (not (comint-check-proc (concat "*" bname "*")))
      (not (my-qemu-boot-telnet-try-again))
    t))

(defun my-qemu-boot-aux (host)
  (let ((fname "~/qemu/qemu-hosts")
        port args)
    (when (not (file-exists-p fname))
      (error (concat fname " missing")))
    (with-temp-buffer
      (insert-file-contents fname)
      (when (not
             (re-search-forward
              (concat "^" host ":[ ]+-t[ ]+\\([0-9]+\\)[ ].*$")))
        (error (concat
                "couldn't find specified host '"
                host
                "' telnet port")))
      (setq port (match-string 1))
      (goto-char (point-min))
      (when (not
             (re-search-forward
              (concat "^" host ":[ ]+\\(-.*\\)[ ]*$")
              nil t))
        (error (concat "couldn't find specified host '" host "'")))
      (setq args (read-from-minibuffer
                  "args: "
                  (concat (match-string 1) " "))))
      (let ((match (string-match "#[0-9]+$" host)))
        (when match
          (setq host (substring host 0 match))))
    (let ((telnet-bufname (concat host "<telnet:" port ">"))
          (shell-bufname (concat "*" host "<shell>*"))
          (use-term-mode (y-or-n-p-with-timeout
                          "term-mode "
                          2
                          t))
          (success nil))
      (when (get-buffer shell-bufname)
        (kill-buffer shell-bufname))
      (shell-command
       (concat "boot-qemu -h " host " " args " &") shell-bufname)
      (while (not success)
        (kill-buffer (get-buffer-create (concat "*" telnet-bufname "*")))
        (sleep-for 0.5)
        (setq success (if use-term-mode
                          (my-qemu-boot-term port telnet-bufname)
                        (my-qemu-boot-telnet port telnet-bufname))))
      (switch-to-buffer (concat "*" telnet-bufname "*"))
      (let ((qdir (concat my-home-directory "/qemu/" host)))
        (when (file-exists-p qdir)
          (message (concat "changing directory to " qdir))
          (cd qdir))))))

(defun my-qemu-boot-sugarloaf ()
  (interactive)
  (my-qemu-boot-aux "sugarloaf"))
(defalias 'qboot-sugarloaf 'my-qemu-boot-sugarloaf)

(defun my-qemu-boot-sugarbush ()
  (interactive)
  (my-qemu-boot-aux "sugarbush"))
(defalias 'qboot-sugarbush 'my-qemu-boot-sugarloaf)

(defun my-qemu-host-is-up (host)
  (call-process "ping" nil nil nil "-c" "1"
                ;;"-W" "1"
                host))

(defun my-qemu-boot ()
  (interactive)
  (let ((host (read-from-minibuffer "boot host: ")))
    (my-qemu-boot-aux host)))
(defalias 'qboot 'my-qemu-boot)

(defun my-qemu-shutdown ()
  (interactive)
  (let ((bname (buffer-name)))
    (when (not (string-match "\*\\(.*\\)<telnet:\\([0-9]+\\)>\*" bname 0))
      (error "sorry, couldn't determine target host name from buffer"))
    (let ((host (substring bname (match-beginning 1) (match-end 1))))
      (when (not (= (my-qemu-host-is-up host) 0))
        (error (concat "sorry, couldn't ping host " host)))
      (call-process
       "ssh"
       nil 0 nil
       ;;nil bname t
       "-f" "-n"
       host
       "sudo sh -c "
       "'systemctl stop user@1000.service && "
       " systemctl stop user@0.service && "
       " sudo shutdown -P now'")
      (message (concat
                "shutting down "
                host
                " now (with systemctl stop user@ hack)")))))
(defalias 'qshutdown 'my-qemu-shutdown)

(defun my-qemu-reboot ()
  (interactive)
  (let ((bname (buffer-name)))
    (when (not (string-match "\*\\(.*\\)<telnet:\\([0-9]+\\)>\*" bname 0))
      (error "sorry, couldn't determine target host name from buffer"))
    (let ((host (substring bname (match-beginning 1) (match-end 1))))
      (when (not (= (my-qemu-host-is-up host) 0))
        (error (concat "sorry, couldn't ping host " host)))
      (call-process
       "ssh"
       nil 0 nil
       "-f" "-n"
       host
       "sudo sh -c "
       "'systemctl stop user@1000.service && "
       " systemctl stop user@0.service && "
       " reboot'")
      (message (concat
                "rebooting "
                host
                " (with systemctl stop user@ hack)")))))
(defalias 'qreboot 'my-qemu-reboot)
