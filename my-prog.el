(provide 'my-prog)

(require 'my-ag)
(require 'my-rg)
(require 'my-ccprog)
(require 'my-cscope)
(require 'my-fontlock)
(require 'my-gdb)
(require 'my-glimpse)
(require 'my-gtags)
(require 'my-gtags-cscope)
(require 'my-idutils)
(require 'my-jdb)
(require 'my-makefile)
(require 'my-python)
(require 'my-rgrep)
;; (require 'my-rtags)
(require 'my-tools)
(require 'my-utils)

(require 'sh-script)
(require 'compile)
(require 'etags)
(require 'go-mode)

(defalias 'gud 'gud-gdb)

(require 'font-lock)
(font-lock-set-defaults)
(global-font-lock-mode 0)
(setq font-lock-maximum-decoration nil)

;;(load-library "chrome")
;;(load-library "bugzilla")
;;(load-library "scheduleit")
;;(load-library "linuxce")
;;(autoload 'javascript-mode "javascript" nil t)
;;(autoload 'css-mode "css-mode" nil t)

;;(auto-compression-mode)
;;(setq inferior-lisp-program "lein repl")
;;(require 'clojure-mode)
;;(require 'slime)
;;(slime-setup)

(defvar global-c-indent-level 2)

(custom-set-variables
 '(c-basic-offset global-c-indent-level)
 '(compilation-scroll-output t)
  ;; current error always at top of buffer
 '(compilation-context-lines 0)
 '(tags-apropos-verbose t)
 '(tags-case-fold-search nil)
 '(tcl-indent-level 2)
 '(magit-log-cutoff-length 1000)
 '(python-check-command "flake8")
 ;; '(ffap-alist
 ;;   (cons
 ;;    (cons
 ;;     "^/vobs/"
 ;;     (defun ffap-vobs (name)
 ;;       (interactive)
 ;;       (let ((view (read-buffer "view: ")))
 ;;         (concat
 ;;          "/view/" view name))))
 ;;    ffap-alist))
 '(interpreter-mode-alist
   (append
    (list
     '("ruby" . ruby-mode))
    interpreter-mode-alist))
 )

(add-hook
 'ruby-mode-hook
 '(lambda ()
    (font-lock-mode t)))

(add-hook
 'compilation-mode-hook
 '(lambda ()
    (font-lock-mode t)))

(define-key compilation-minor-mode-map (kbd "n") 'compilation-next-error)
(define-key compilation-minor-mode-map (kbd "p") 'compilation-previous-error)
(define-key compilation-minor-mode-map (kbd "v") 'compilation-display-error)

(add-hook
 'css-mode-hook
 '(lambda ()
    (set
     (make-local-variable 'css-indent-level)
     global-c-indent-level)))

(add-hook
 'sh-mode-hook
 '(lambda ()
    (font-lock-mode t)
    (smie-config-local '((2 :after "then" nil)))
    ;; seems to control indenting after line-continuation
    (smie-config-local '((2 :elem basic 4)))
    (set (make-local-variable 'sh-basic-offset) 2)))

(define-key sh-mode-map (kbd "C-c C-c") 'comment-region)
(define-key go-mode-map (kbd "C-c C-c") 'comment-region)

;; funcs

(defun my-set-compilation-search-path ()
  (interactive)
  (let ((dir (read-directory-name "dir: " default-directory)))
    (custom-set-variables
     '(compilation-search-path
       `(,dir)))
    (pp compilation-search-path)))

(defun my-open-compile-window (&optional arg)
  (interactive "P")
  (let ((lines (or arg 5))
        (buffer (get-buffer "*compilation*")))
    (unless buffer
      (error "no compiliation buffer"))
    (delete-other-windows)
    (split-window-vertically)
    (other-window 1)
    (switch-to-buffer buffer)
    (balance-windows)
    (shrink-window (- (window-height) lines))
    (goto-char (point-max))
    (other-window 1)))

(defun my-prog-tidy-comments-in-region (p1 p2 tidy-fn &optional progress-msg)

  (cl-assert (and (not mark-active) (< p1 p2)))

  (save-restriction

    (narrow-to-region p1 p2)

    (let ((total-line-count 0)
          start-point start-line-number
          comment-list is-comment
          state progress-reporter)

      (goto-char (point-min))

      ;; loop through entire buffer keeping a list of the start and
      ;; end of every comment encountered
      (cl-loop

       (when (eobp)
         (cl-return))

       ;; find the next syntatic expression
       (setq state (parse-partial-sexp
                    (point) (point-max)
                    nil nil
                    state 'syntax-table))

       ;; bail if not a comment or string
       (when (not (or (nth 3 state) (nth 4 state)))
         (cl-return nil))

       (setq
        ;; did we find a comment?
        is-comment (nth 4 state)
        start-point (point)
        start-line-number (line-number-at-pos)
        ;; move to the end of the current comment or string (by
        ;; finding the beginning of the next syntatic expression)
        state (parse-partial-sexp
               start-point (point-max)
               nil nil
               state 'syntax-table))

       ;; if we found a comment, track it in our list
       (when is-comment
         (let ((line-count (+ (line-number-at-pos) 1)))
           (push
            (list
             (save-excursion (goto-char start-point) (point-marker))
             (save-excursion (forward-char -1) (point-marker))
             (setq line-count (- line-count start-line-number)))
            comment-list)
           (setq total-line-count (+ total-line-count line-count))))

       ;; error condition caused by unbalanced comment demarkers
       (when (or (nth 3 state) (nth 4 state))
         ;; just bail out
         (cl-return nil)))

      (when progress-msg
        (setq progress-reporter
              (make-progress-reporter progress-msg 1 total-line-count))
        (setq total-line-count 0))

      ;; now work backward through the buffer, dealing with each
      ;; comment (last comment first, first comment last)
      (dolist (comment comment-list)
        (when progress-reporter
          (progress-reporter-update
           progress-reporter
           (setq total-line-count (+ total-line-count (cl-third comment)))))
        (funcall tidy-fn (cl-first comment) (cl-second comment)))

      (when progress-reporter
        (progress-reporter-done progress-reporter))

      )
    )
  )

(defun my-prog-check-mode ()
  (when (not (or (string-equal (substring mode-name 0 1) "C")
                 (string-equal (substring mode-name 0 3) "C++")
                 (string-equal mode-name "Emacs-Lisp")
                 (string-equal mode-name "Python")))
    (error "sorry, current mode '%s' is not supported" mode-name)))

(cl-defun my-prog-tidy-region (&optional start end)

  (interactive)
  (my-prog-check-mode)
  (cl-assert (or (and start end) (not (or start end))))

  ;; handle special case (region selected)
  (when (and transient-mark-mode mark-active)
    (cl-assert (not (or start end)))
    (let ((mark-active nil))
      (my-prog-tidy-region (region-beginning) (region-end)))
    (cl-return-from my-prog-tidy-region))

  (let ((start-mark (set-marker (make-marker) (if start start (point-min))))
        (end-mark (set-marker (make-marker) (if end end (point-max))))
        noindent)

    (when (my-overlay-and-prompt
           (marker-position start-mark) (marker-position end-mark)
           "indent region? ")
      (indent-region (marker-position start-mark) (marker-position end-mark))
      (setq noindent t))

    (when (not (my-overlay-and-prompt
                (marker-position start-mark) (marker-position end-mark)
                "refill all comments in region? "))
      (cl-return-from my-prog-tidy-region))

    (save-excursion

      (unless noindent
        ;; go through buffer re-indenting all comments
        (my-prog-tidy-comments-in-region
         (marker-position start-mark) (marker-position end-mark)
         #'(lambda (start-mark-for-comment end-mark-for-comment)
             (cl-letf (((symbol-function 'message) #'ignore))
               (indent-region
                (marker-position start-mark-for-comment)
                (marker-position end-mark-for-comment))))
         "Indenting comments... "))

      ;; go through buffer refilling all comments
      (my-prog-tidy-comments-in-region
       (marker-position start-mark) (marker-position end-mark)
       #'(lambda (start-mark-for-comment end-mark-for-comment)
           (goto-char (marker-position end-mark-for-comment))
           (while (> (point) (marker-position start-mark-for-comment))
             (fill-paragraph)
             (beginning-of-line)
             (when (not (bobp))
               (forward-line -1))))
       "Refilling comments... ")
      )
    )
  )

(cl-defun my-prog-tidy-function ()

  (interactive)
  (my-prog-check-mode)

  (let ((mark-active nil))
    (save-excursion
      (beginning-of-defun)
      (my-prog-tidy-region
       (point)
       (progn (end-of-defun) (forward-char -1) (point)))
      )
    )
  )
