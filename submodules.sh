#!/bin/bash

pname=$(basename $0)

[ "$1" = "clean" ] && rm -rf submodules
mkdir -p submodules || {
  echo "$pname: error, couldn't make submodules dir" 1>&2;
  exit 1;
}

(
  set -ex
  cd submodules

  # magit
  if [ ! -d with-editor ]; then
    git clone https://github.com/magit/with-editor.git
    (cd with-editor && git checkout v2.7.0)
  fi
  if [ ! -d dash.el ]; then
    git clone https://github.com/magnars/dash.el.git
    (cd dash.el && git checkout 2.13.0)
  fi
  if [ ! -d magit ]; then
    git clone https://github.com/magit/magit.git
    (cd magit && git checkout 2.11.0)
    rm -f magit/config.mk
    cat <<-EOF > magit/config.mk
		LOAD_PATH  = -L ${PWD}/magit/lisp
		LOAD_PATH += -L ${PWD}/dash.el
		LOAD_PATH += -L ${PWD}/with-editor
		EOF
    (cd magit && make clean && make)
  fi

  # magit-svn
  if [ ! -d magit-svn ]; then
    git clone https://github.com/magit/magit-svn.git
  fi

  # markdown
  if [ ! -d markdown-mode ]; then
    git clone https://github.com/jrblevin/markdown-mode.git    
    (cd markdown-mode && git checkout v2.3)
  fi

  # web-mode
  if [ ! -d web-mode ]; then
    git clone https://github.com/fxbois/web-mode.git
    (cd web-mode && git checkout v14)
  fi

  # ssh
  if [ ! -d ssh-el ]; then
    git clone https://github.com/ieure/ssh-el.git
  fi

  # google c style guide
  if [ ! -d styleguide ]; then
    git clone https://github.com/google/styleguide.git
  fi

  # yasnippet
  if [ ! -d yasnippet ]; then
    git clone --recursive https://github.com/joaotavora/yasnippet.git
    (cd yasnippet && git checkout 0.10.0)
  fi

  # helm
  if [ ! -d emacs-async ]; then
    git clone https://github.com/jwiegley/emacs-async.git
    (cd emacs-async && git checkout v1.9)
  fi
  if [ ! -d helm ]; then
    git clone https://github.com/emacs-helm/helm.git
    (cd helm && git checkout v1.9.8 && make)
  fi
  if [ ! -d helm-ls-git ]; then
    git clone https://github.com/emacs-helm/helm-ls-git.git
    (cd helm-ls-git && git checkout v1.8.0)
  fi
  if [ ! -d helm-descbinds ]; then
    git clone https://github.com/emacs-helm/helm-descbinds.git
    (cd helm-descbinds && git checkout 1.12)
  fi
  if [ ! -d emacs-helm-ag ]; then
    git clone https://github.com/syohex/emacs-helm-ag.git
    (cd emacs-helm-ag && git checkout 0.56)
  fi
  # if [ ! -d helm-rg ]; then
  #   git clone https://github.com/cosmicexplorer/helm-rg.git
  #   (cd helm-rg && git checkout 0.1)
  # fi
  if [ ! -d go-mode.el ]; then
    git clone https://github.com/dominikh/go-mode.el.git
    (cd go-mode.el && git checkout v1.5.0)
  fi
  if [ ! -d emacs-helm-gtags ]; then
    git clone https://github.com/syohex/emacs-helm-gtags.git
    (cd emacs-helm-gtags && git checkout 1.5.6)
  fi

  # # dtrt-indent (guess current buffer indentation scheme)
  # git clone https://github.com/jscheid/dtrt-indent.git

  # # flycheck (support builtin in emacs 25)
  # git clone https://github.com/flycheck/flycheck.git
  # (cd flycheck && git checkout 28)
  # (cd flycheck && \
  #     wget -O let-alist.el http://elpa.gnu.org/packages/let-alist-1.0.4.el)
  # git clone https://github.com/magnars/dash.el.git
  # (cd dash.el && git checkout 2.12.1)
  # git clone https://github.com/NicolasPetton/seq.el.git
  # (cd seq.el && git checkout 1.11)

  ##
  ## using company mode instead of auto-complete
  ##

  # # auto-complete
  # git clone https://github.com/auto-complete/auto-complete.git
  # (cd auto-complete && git checkout v1.5.1)
  # pop-up
  # git clone https://github.com/auto-complete/popup-el.git
  # (cd popup-el && git checkout v0.5.3)

  # company-mode
  if [ ! -d company-mode ]; then
    git clone https://github.com/company-mode/company-mode.git
    (cd company-mode && git checkout 0.9.0)
  fi

  # psvn
  if [ ! -e psvn ]; then
    if [ ! -f ../psvn/psvn.el ]; then
      (cd .. && echo "$pname: error, $(pwd)/psvn/psvn.el missing" 1>&2)
      exit 1
    fi
    ln -s ../psvn .
  fi

  # smartabs
  if [ ! -e smartabs ]; then
    git clone https://github.com/jcsalomon/smarttabs.git
    (cd smarttabs && git checkout v1.0)
  fi

)
if (($? != 0)); then
  echo "$pname: error, when cloning and setting up submodules" 1>&2
  exit 1
fi

echo "$pname: submodules setup complete"
