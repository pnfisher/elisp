(provide 'my-tools)

(defcustom my-tags-directory-table
  '(
    ("jboss" "/home/phil/src/java/jboss")
    ("goosync" "/home/phil/src/goosync")
    ("." "."))
  "table of various etags locations"
  :group 'my-tools-id)

(defcustom my-tools-id
  "jboss"
  "variable to control source code tools"
  :group 'my-tools-id)

(defun my-reset-tools-id-file ()
  (let ((fname "~/.emacs.d/my-tools-id.el"))
    (if (file-readable-p fname)
        (progn
          (load-file fname)
          (add-to-list
           'compilation-search-path
           (nth 1 (assoc my-tools-id my-tags-directory-table))))
    fname)))

(defun my-reset-tools-id ()
  (interactive)
  (let* ((fname (my-reset-tools-id-file))
         (ddir (substring default-directory 0 -1))
         (id (read-buffer "tools-id: " my-tools-id))
         (entry (nth 1 (assoc id my-tags-directory-table)))
         (nosave (nth 2 (assoc id my-tags-directory-table))))
    (if (equal entry ".")
        (let ((id (read-buffer
                   "label: "
                   (file-name-nondirectory
                    (directory-file-name default-directory)))))
          (if (equal id "")
              (setq id ddir))
          (if (not (nth 1 (assoc id my-tags-directory-table)))
              (setq
               my-tags-directory-table
               (append
                (list (list id ddir t))
                my-tags-directory-table)))
          (setq my-tools-id id)
          (add-to-list 'compilation-search-path ddir)
          (message (format "tools-id %s" my-tools-id)))
      (if entry
          (progn
            (setq my-tools-id id)
            (add-to-list 'compilation-search-path entry)
            (message (format "tools-id %s" my-tools-id)))
        (let ((prompt "")
              (p nil)
              (i 0))
          (while (setq p (nth 0 (nth i my-tags-directory-table)))
            (setq prompt (format "%s\"%s\", " prompt p))
            (setq i (+ 1 i)))
          (error
           (format
            "Invalid tools-id: valid choices are %s"
            (substring prompt 0 -2)))))
      (if (not nosave)
          (progn
            (if (file-readable-p fname)
                (delete-file fname))
            (write-region
             (format "(setq my-tools-id \"%s\")" my-tools-id) nil fname))))))

;; must come after setup of my-tags-directory-table and my-tools-id
;; above
(my-reset-tools-id-file)
