(provide 'my-pycalc)

(require 'my-utils)

(require 'comint)
(require 'cl-lib)

(defvar my-pycalc-python-binary "python")
(defvar my-pycalc-result-variable-form "_r")
(defvar my-pycalc-process-send-string nil)
(defvar my-pycalc-result-stack-size 100)
(defvar my-pycalc-buffer-name "pycalc")
(defvar my-pycalc-default-mode "pfxd")
(defvar my-pycalc-result-index 0)
(defvar my-pycalc-debug nil)
(defvar my-pycalc-marker (make-marker))

(set-marker-insertion-type my-pycalc-marker nil)

(defun my-pycalc-wait-for-prompt (proc prompt)
  (let ((bound (marker-position my-pycalc-marker)))
    (cl-loop
     repeat 25 do
     ;; give up if process has died
     (when (not (comint-check-proc (current-buffer)))
       (cl-return nil))
     (goto-char (process-mark proc))
     ;; bound our prompt search to ensure we don't find an old,
     ;; out-of-date prompt
     (when (looking-back prompt bound)
       (cl-return t))
     (sleep-for 0.1))))

(define-derived-mode my-pycalc-mode comint-mode "pycalc"
  (my-set-process-query-on-exit-flag nil)
  (let ((proc (get-buffer-process (current-buffer))))
    (unless (my-pycalc-wait-for-prompt proc "^>>> ")
      (error "failure during python startup"))
    ;; delete the prompt we found
    (forward-line 0)
    (delete-region (point) (point-max))
    (set-marker my-pycalc-marker (point))
    (process-send-string proc "import math\n")
    (unless (my-pycalc-wait-for-prompt proc "^>>> ")
      (error "failure while importing math library"))
    ;; delete the prompt we found
    (forward-line 0)
    (delete-region (point) (point-max))
    (set-marker my-pycalc-marker (point))
    ;; let's update the python prompt so my-pycalc-mode has its own
    ;; distinctive feel
    (process-send-string proc "sys.ps1 = '-> ' \n")
    (unless (my-pycalc-wait-for-prompt proc "^-> ")
      (error "failure while updating python prompt"))
    ;; clean out some of the python start-up messaging
    (if (not (re-search-backward "^Python" nil t))
        (forward-line 0)
      (forward-line 1))
    (delete-region (point) (marker-position my-pycalc-marker))
    ;; no longer needed
    (set-marker my-pycalc-marker nil)
    ;; put a space between python start-up messaging and our first
    ;; prompt
    (insert "Python math library imported\n\n")
    (goto-char (process-mark proc))
    (set (make-local-variable 'comint-input-sender) #'my-pycalc-send)
    (set (make-local-variable 'comint-prompt-regexp) "-> ")
    (set-process-filter proc #'my-pycalc-output-filter)
    (setq my-pycalc-result-index 0)))

(cl-defun my-pycalc ()
  (interactive)
  (switch-to-buffer (concat "*" my-pycalc-buffer-name "*"))
  (goto-char (point-max))
  ;; for use in my-pycalc-mode setup. Basically, we'll use it to
  ;; ensure our searches for the most recent python prompt don't go
  ;; back too far and find an invalid, older, out-of-date prompt
  (set-marker my-pycalc-marker (point))
  (let ((proc (get-buffer-process (current-buffer))))
    (when proc
      (goto-char (process-mark proc))
      (message "my-pycalc alreay running")
      (cl-return-from my-pycalc))
    (when (> (point) 1)
      (insert "\n\n"))
    (insert "Starting my-pycalc\n")
    (apply #'make-comint my-pycalc-buffer-name my-pycalc-python-binary nil)
    (my-pycalc-mode)))
(defalias 'py-calc 'my-pycalc)

(cl-defun my-pycalc-output-filter (proc string)
  (unless my-pycalc-debug
    (setq string (replace-regexp-in-string
                  "\\s-*\\(Traceback\\|File\\)\\s-.*\n?"
                  ""
                  string)))
  (when (string-match "^\$\\([0-9]+\\) = " string)
    (let ((rindex (string-to-number (match-string 1 string))))
      (when (= (setq my-pycalc-result-index (+ rindex 1))
               my-pycalc-result-stack-size)
        (setq my-pycalc-result-index 0))))
  (comint-output-filter proc string))

(defun my-pycalc-process-send-string (proc string)
  (ignore-errors
    (process-send-string proc string))
  (setq my-pycalc-process-send-string
        (concat my-pycalc-process-send-string string))
  (when (and my-pycalc-debug (string-match "\n$" string))
    (print (concat
            "\nmy-pycalc-process-send-string sent:\n"
            my-pycalc-process-send-string))))

(defun my-pycalc-print-print-mode (proc)
  (my-pycalc-process-send-string proc (concat
                                     "print("
                                     "'print mode = "
                                     (substring my-pycalc-default-mode 1)
                                     "')\n")))

(cl-defun my-pycalc-send (proc string)

  (let ((result (format "%s%d"
                        my-pycalc-result-variable-form
                        my-pycalc-result-index))
        (print-mode my-pycalc-default-mode)
        (print-modes "bfgxd"))

    (setq my-pycalc-process-send-string nil)

    ;; handle $[0-9]+ variable substitution
    (setq string (replace-regexp-in-string "\$\\([0-9]+\\)"
                                           (concat
                                            my-pycalc-result-variable-form
                                            "\\1")
                                           string))

    ;; special py mode
    (when (string-match "^\\s-*py\\s-*\\(.*\\)\\s-*$" string)
      (my-pycalc-process-send-string proc
                                      (concat (match-string 1 string) "\n"))
      (cl-return-from my-pycalc-send))

    ;; exit or quit
    (when (string-match "^\\s-*\\(exit\\|quit\\)" string)
      (ignore-errors (process-send-eof proc))
      ;; (kill-buffer (current-buffer))
      (cl-return-from my-pycalc-send))

    ;; blank lines
    (when (string-match "^\\s-*$" string)
      (my-pycalc-process-send-string proc "pass\n")
      (cl-return-from my-pycalc-send))

    ;; handle special print all print mode
    (when (string-match "^\\s-*pa?\\(\\s-*\\|\\s-+\\(.*?\\)\\s-*\\)$" string)
      (setq string (concat "pbfgxd " (match-string 2 string))))

    ;; handle special print the current mode request
    (when (string-match "^\\s-*pm\\s-*$" string)
      (my-pycalc-print-print-mode proc)
      (cl-return-from my-pycalc-send))

    ;; set print mode
    (when (string-match (concat "^\\s-*\\(p/?\\(["
                                print-modes
                                "]+\\)\\)\\s-*$")
                        string)
      (setq my-pycalc-default-mode
            (concat "p"
                    (mapconcat
                     #'identity
                     (delete-dups (split-string (match-string 2 string) "" t))
                     "")))
      (my-pycalc-print-print-mode proc)
      (cl-return-from my-pycalc-send))

    ;; handle p/x type print requests by removing /
    (when (string-match (concat "^p/\\([" print-modes "]+\\)\\s-+") string)
      (setq string (concat "p" (substring string 2))))
    ;; handle print request without a 'p' prefix (by prepending a 'p')
    (when (string-match (concat "^[" print-modes "]+\\s-+") string)
      (setq string (concat "p" string)))

    ;; split string into print mode request and expression (with
    ;; expression stored in string)
    (when (string-match (concat "^\\(p["
                                print-modes
                                "]+\\)\\s-+\\(.*\\)\\s-*$")
                        string)
      (let ((m1 (match-string 1 string)))
        (setq string (replace-regexp-in-string
                      "\\s-+"
                      " "
                      (match-string 2 string))
              print-mode (mapconcat
                          #'identity
                          (delete-dups (split-string m1 "" t))
                          ""))))
    ;; process calculation and store result in result
    (setq string (concat result " = " string "; "))
    (my-pycalc-process-send-string proc string)

    ;; now process print mode request (now guaranteed to be in
    ;; standard form, i.e. (concat "p[" print-modes "]+")
    (let* ((print-prefix (format "print('\$%d = " my-pycalc-result-index))
           (chars (split-string (substring print-mode 1) "" t))
           (last (car (last chars))))

      ;; handle each print mode in sequence
      (dolist (ch chars)
        (cond ((string-equal ch "b")
               (setq string (concat print-prefix
                                    "%s' % bin(int("
                                    result
                                    ")))")))
              ((string-equal ch "d")
               (setq string (concat print-prefix "%d' % int(" result "))")))
              ((string-equal ch "f")
               (setq string (concat print-prefix "%f' % float(" result "))")))
              ((string-equal ch "g")
               (setq string (concat print-prefix "%g' % " result ")")))
              ((string-equal ch "x")
               (setq string (concat print-prefix "0x%x' % int(" result "))")))
              (t
               (error "my-pycalc, not possible")))
        (my-pycalc-process-send-string proc string)
        (when (not (string-equal ch last))
          (my-pycalc-process-send-string proc "; ")))

      (my-pycalc-process-send-string proc "\n"))))
