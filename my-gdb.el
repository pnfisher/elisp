(provide 'my-gdb)

(require 'my-utils)

(require 'gud)
(require 'gdb-mi)
(require 'comint)
(require 'cl-lib)

;; (defadvice gdb (before pdb-advice activate compile)
;;   (let* ((fname (car (cdr (split-string-and-unquote command-line))))
;;          (bname (concat "*gud-" fname "*"))
;;          (proc (get-buffer-process bname)))
;;     (when (and proc
;;                (y-or-n-p (format "%s already exists. Restart? " bname)))
;;       (switch-to-buffer bname)
;;       (let* ((src gud-target-name)
;;              (buf (find-buffer-visiting src)))
;;         (when buf
;;           (switch-to-buffer buf))
;;         (kill-process proc)
;;         (while (get-buffer-process bname)
;;           (sleep-for 0.1))
;;         (kill-buffer bname)))))

(custom-set-variables
 '(gdb-show-main t)
 '(gdb-non-stop-setting nil))

(defun my-gdb-gud-filter-advice (proc &rest args)
  (ignore-errors
    (when (and
           ;; don't do `gdb-restore-windows' when gud-minor-mode is
           ;; 'gdbmi. If we do, the restore will clobber the doings of
           ;; `my-gdb-inferior-filter-advice' which runs when using
           ;; gdbmi (instead of the old gud-gdb interface)
           (not (eq
                 (buffer-local-value 'gud-minor-mode gud-comint-buffer)
                 'gdbmi))
           ;; if gdb-main-file not set, then gdb-restore-windows isn't
           ;; going to do the right thing.
           gdb-main-file
           ;; only do gdb-restore-windows when we finally get the gdb
           ;; prompt
           (save-excursion
             (goto-char (process-mark proc))
             (looking-back comint-prompt-regexp nil)))
      (gdb-restore-windows))))
(advice-add #'gud-filter :after #'my-gdb-gud-filter-advice)

;; reconfigure windows when gdbmi creates a third window for
;; displaying input/output
(defun my-gdb-inferior-filter-advice (&rest args)
  (when (not gdb-many-windows)
    (ignore-errors
      (gdb-restore-windows))
    (let ((sz (window-size)))
      (split-window-below)
      (gdb-display-buffer (gdb-get-buffer-create 'gdb-inferior-io))
      ;;(balance-windows)
      (other-window -1)
      (let ((diff (- sz (window-size))))
        (when (> diff 0)
          (enlarge-window (- diff 1))))
      (other-window 1))))
(advice-add #'gdb-inferior-filter :after #'my-gdb-inferior-filter-advice)

;; normally interned by gud-query-cmdline, but I need to suppress
;; compilation warnings
(defvar gud-gud-gdb-history)

(cl-defun my-gdb-get-source-file ()
  (cl-assert (and (boundp 'gud-gud-gdb-history) gud-gud-gdb-history))
  (let ((gdb-binary (car (split-string-and-unquote (car gud-gud-gdb-history))))
        (gdb-prompt-regexp comint-prompt-regexp)
        (program gud-target-name))
    ;; (when (or (> (call-process-shell-command
    ;;               (concat gdb-binary " -i=mi -batch"))
    ;;              0)
    ;;           (not program))
    ;;   (cl-return-from my-gdb-get-source-file nil))
    (with-temp-buffer
      (comint-mode)
      (comint-exec (current-buffer)
                   (concat "my-gdb-process-" (buffer-name))
                   gdb-binary
                   nil
                   `("-i=mi" ,program))
      ;; ignore-errors in case process has already died
      (ignore-errors
        (my-set-process-query-on-exit-flag nil))
      ;; wait for prompt, give up if it never appears
      (when (not (cl-loop
                  repeat 10 do
                  (goto-char (point-min))
                  (when (re-search-forward gdb-prompt-regexp nil t)
                    (cl-return t))
                  ;; give up if process has died
                  (when (not (comint-check-proc (current-buffer)))
                    (cl-return nil))
                  (sleep-for 0.1)))
        (cl-return-from my-gdb-get-source-file nil))
      (erase-buffer)
      ;; ignore-errors in case process has died unexpectedly
      (ignore-errors
        (process-send-string
         (get-buffer-process (current-buffer))
         "-file-list-exec-source-file\n"))
      ;; wait for prompt, give up if it never appears
      (when (not (cl-loop
                  repeat 10 do
                  (goto-char (point-min))
                  (when (re-search-forward gdb-prompt-regexp nil t)
                    (cl-return t))
                  ;; give up if process has died
                  (when (not (comint-check-proc (current-buffer)))
                    (cl-return nil))
                  (sleep-for 0.1)))
        (cl-return-from my-gdb-get-source-file nil))
      (goto-char (point-min))
      (when (re-search-forward gdb-source-file-regexp nil t)
        (read (match-string 1))))))

(add-hook
 'gud-gdb-mode-hook
 ;; copied from gdb-mi.el
 '(lambda ()
    (my-set-process-query-on-exit-flag nil)
    (when gdb-show-main
      (setq gdb-main-file (my-gdb-get-source-file)))
    (when gdb-main-file
      (ignore-errors
        (gdb-restore-windows)))
    (when (ring-empty-p comint-input-ring) ; cf shell-mode
      (let ((hfile (expand-file-name (or (getenv "GDBHISTFILE")
                                         ".gdb_history")))
            ;; gdb defaults to 256, but we'll default to
            ;; comint-input-ring-size.
            (hsize (getenv "HISTSIZE")))
        (dolist (file (append '("~/.gdbinit")
                              (unless (string-equal (expand-file-name ".")
                                                    (expand-file-name "~"))
                                '(".gdbinit"))))
          (if (file-readable-p (setq file (expand-file-name file)))
              (with-temp-buffer
                (insert-file-contents file)
                ;; TODO? check for "set history save\\( *on\\)?" and
                ;; do not use history otherwise?
                (while (re-search-forward
                        "^ *set history \\(filename\\|size\\)  *\\(.*\\)" nil t)
                  (cond ((string-equal (match-string 1) "filename")
                         (setq hfile (expand-file-name
                                      (match-string 2)
                                      (file-name-directory file))))
                        ((string-equal (match-string 1) "size")
                         (setq hsize (match-string 2))))))))
        (and (stringp hsize)
             (integerp (setq hsize (string-to-number hsize)))
             (> hsize 0)
             (set (make-local-variable 'comint-input-ring-size) hsize))
        (if (stringp hfile)
            (set (make-local-variable 'comint-input-ring-file-name) hfile))
        (comint-read-input-ring t)))))

;; ;; old
;; (add-hook
;;  'gud-gdb-mode-hook
;;  '(lambda ()
;;     (gud-def gud-proc "info proc" "\C-q" "info proc")))

;; new
(add-hook
 'gdb-mode-hook
 '(lambda ()
    (my-gdb-process-query-on-exit-flag nil)))
;;     (gud-def gud-proc "info proc" "\C-q" "info proc")))
