(provide 'my-spell)

(require 'cl-lib)
(require 'ispell)

(cl-defun my-spell ()

  (interactive)

  ;; spell check high-lighted region?
  (when (and transient-mark-mode mark-active)
    (ispell-region (region-beginning) (region-end))
    (cl-return-from my-spell))

  (cond

   ((or (string-equal (substring mode-name 0 1) "C")
        (string-equal (substring mode-name 0 3) "C++")
        (string-equal mode-name "Emacs-Lisp")
        (string-equal mode-name "Python"))
    (let ((current (point))
          (start nil)
          (state nil)
          (checked nil))
      (save-excursion
        (goto-char (point-min))
        (cl-loop
         while t
         do

         ;; find the next syntatic expression
         (setq state (parse-partial-sexp
                      (point) (point-max)
                      nil nil
                      state 'syntax-table))

         ;; is it a comment or string?
         (when (not (or (nth 3 state) (nth 4 state)))
           (cl-return nil))

         ;; track start of current comment
         (setq start (point))

         ;; move to the end of the current
         ;; comment or string (by finding the
         ;; beginning of the next syntatic
         ;; expression)
         (setq state (parse-partial-sexp
                      start (point-max)
                      nil nil
                      state 'syntax-table))

         ;; error condition caused by unbalanced
         ;; comment demarkers
         (when (or (nth 3 state) (nth 4 state))
           ;; just bail out and see what happens if we try to ispell
           ;; all comments and strings in the buffer
           (cl-return nil))

         ;; is the current comment past the point
         ;; we originally occupied?
         (when (> (- start (if (nth 3 state) 1 2)) current)
           ;; if so break from loop
           (cl-return nil))

         ;; if not, is our original starting point
         ;; contained in the current comment?
         (when (> (point) current)
           ;; if so spell check it
           (ispell-region start (point))
           (setq checked t)
           (cl-return t)))

        (when (not checked)
          ;; original point not in a comment or string, so
          ;; spell check all the comments or strings in the
          ;; buffer
          (ispell-comments-and-strings)))))

   ((string-equal mode-name "Mail")
    (ispell-message))
   (t
    (ispell-buffer))))
