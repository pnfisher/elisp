BATCH  = emacs -batch -Q -L .
# BATCHC = $(BATCH) \
# --eval "(require 'my-globals)" \
# -f batch-byte-compile
BATCHC = $(BATCH) -f batch-byte-compile

#SHELL=/bin/bash -x

sources := $(wildcard *.el)
#sources := $(filter-out my-globals.el,$(sources))
objects := $(subst .el,.elc,$(sources))

%: %.el
	@make --no-print-directory $@.elc

%.elc: %.el
	@echo ">> byte compiling $<"
	@$(BATCHC) $<

.phony: all
all: $(objects)

.phony: test
test: clean all
	make clean

.phony: clean
clean:
	rm -f *.elc
