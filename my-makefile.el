(provide 'my-makefile)

(require 'make-mode)

(add-hook
 'makefile-mode-hook
 '(lambda ()
    (font-lock-mode t)))
