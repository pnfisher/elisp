(provide 'my-scratch)

(require 'cl-lib)

(defun my-scratch-protect (&optional buf)
  (let ((buffer (if buf
                    buf
                  (get-buffer "*scratch*"))))
    (if (bufferp buffer)
        (with-current-buffer buffer
          (lisp-interaction-mode)
          (make-local-variable 'kill-buffer-query-functions)
          (add-hook 'kill-buffer-query-functions '(lambda () nil))))))

(my-scratch-protect)

(cl-defun my-scratch ()
  (interactive)
  (when current-prefix-arg
    (my-scratch-insert)
    (cl-return-from my-scratch))
  (my-scratch-protect (switch-to-buffer (get-buffer-create "*scratch*"))))

(defun my-scratch-insert ()
  (interactive)
  (when (not (and transient-mark-mode mark-active))
    (error "no region selected"))
  (let* ((beg (region-beginning))
         (end (region-end))
         (buf (current-buffer)))
    (with-current-buffer "*scratch*"
      (save-excursion
        (goto-char (point-max))
        (insert "\n\n@@ from my-scratch-insert\n\n")
        (insert-buffer-substring buf beg end)))))
