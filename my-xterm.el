(provide 'my-xterm)

(defun my-xterm-aux (fg bg fn geo)
  (call-process "xterm"
		nil 0 nil
		"-sb"
		"-sl" "500"
		"-j"
		"-geometry" geo
		"-fn" fn
		"-fg" fg
		"-bg" bg))

(defun my-xterm ()
  (interactive)
  (let ((bg (read-buffer "Background: " "Black"))
        (fg (read-buffer "Foreground: " "Gray80"))
        (fn (read-buffer "Font: " "9x15"))
        (geo (read-buffer "Geometry: " "80x25")))
    (my-xterm-aux fg bg fn geo)))

(defun my-dterm ()
  (interactive)
  (my-xterm-aux "gray80" "black" "9x15" "80x25"))
(defalias 'xterm 'my-dterm)

;; (defun dterm ()
;;   (interactive)
;;   (xterm-aux "gray80" "gray5" "9x15" "80x25"))

;; (defun gterm ()
;;   (interactive)
;;   (xterm-aux "white" "darkolivegreen" "9x15" "80x25"))

;; (defun bterm ()
;;   (interactive)
;;   (xterm-aux "white" "darkslateblue" "9x15" "80x25"))

;; (defun rterm ()
;;   (interactive)
;;   (xterm-aux "white" "brown" "9x15" "80x25"))

;; (defun pterm ()
;;   (interactive)
;;   (xterm-aux "black" "peachpuff" "9x15" "80x25"))

(defun my-debugterm ()
  (interactive)
  (call-process "xterm"
                nil 0 nil
                "-geometry" "80x55"
                "-fn" "9x15"
                "-fg" "grey90"
                "-bg" "black"))
