(provide 'my-dired)

(require 'dired)
(require 'find-dired)

(setq dired-dwim-target t)
(setq find-grep-options "-q -s")
(setq dired-listing-switches "-alh")
(setq dired-recursive-copies t)
(setq dired-recursive-deletes 'always)

(add-hook
 'dired-mode-hook
 '(lambda ()
    (turn-on-font-lock)
    (set
     (make-local-variable 'dired-header-face)
     dired-directory-face)
    ;; (font-lock-add-keywords
    ;;  nil
    ;; ;; let's not treat files ending in colon as directory headers
    ;; '(("^\\(.* [0-9][0-9]:[0-9][0-9] [^ ]+:\\)$"
    ;;    (1 emacs22p-default-face))))
    ))
