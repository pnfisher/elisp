(provide 'my-gtags)

(require 'my-globals)
(require 'my-submodules)
(require 'my-tools)
(require 'my-grep)

(require 'cl-lib)

(eval-and-compile (defconst my-gtags-binary "global"))
(defvar my-gtags-default-command
  (format "%s --path-style=absolute --result=grep" my-gtags-binary))
(defvar my-gtags-color-params "--color=auto")
(defvar my-gtags-highlight-matches t)
(defvar my-gtags-history nil)
(defvar my-gtags-top-root my-home-directory)
(defvar my-gtags-prompt nil)
(defvar my-gtags-arg-table nil)

(eval-and-compile
  (my-grep-define-compilation-mode
   'my-gtags-mode
   my-gtags-binary
   ;; highlight matches
   t))

(cl-assert my-gtags-highlight-matches)

(if (require 'helm-gtags nil :noerror)
    (progn
      (setq my-gtags-prompt
            (concat "[d]ef "
                    "[D]wim "
                    "[Ee][g]rep [Ff]ile [i]dutils [Rr]tag "
                    "se[L]ect "
                    "[Ss]ym [Tt]ag"))
      (setq my-gtags-arg-table
            '(
              (?d . ("-do" . "definition"))
              (?D . (#'helm-gtags-dwim . nil))
              (?E . ("-goE" . "extended grep"))
              (?e . ("-goe" . "egrep"))
              (?f . ("-Po" . "find file"))
              (?F . (#'helm-gtags-find-files . nil))
              (?L . (#'helm-gtags-select . nil))
              (?r . ("-r" . "rtag"))
              (?R . (#'helm-gtags-find-rtag . nil))
              (?s . ("-s" . "symbol"))
              (?S . (#'helm-gtags-find-symbol . nil))
              (?t . ("-t" . "tag"))
              (?T . (#'helm-gtags-find-tag . nil))
              (?g . ("-go" . "grep"))
              (?i . ("-I" . "idutils")))))
  (setq my-gtags-prompt
        "[d]ef [Ee][g]rep [f]ile [i]dutils [r]tag [s]ym [t]ag")
  (setq my-gtags-arg-table
        '(
          (?d . ("-do" . "definition"))
          (?E . ("-goE" . "extended grep"))
          (?e . ("-goe" . "egrep"))
          (?f . ("-Po" . "find file"))
          (?r . ("-r" . "rtag"))
          (?s . ("-s" . "symbol"))
          (?t . ("-t" . "tag"))
          (?g . ("-go" . "grep"))
          (?i . ("-I" . "idutils")))))

(defun my-gtags ()
  (interactive)
  (cl-loop
   (message my-gtags-prompt)
   (let* ((arg (read-char))
          (entry (cdr (assoc arg my-gtags-arg-table))))
     (when (char-equal arg ?q)
       (cl-return))
     (when entry
       (let ((e1 (car entry))
             (e2 (cdr entry)))
         (if (not (or (stringp e1) e2))
             (let ((func (eval e1)))
               (when (not (functionp func))
                 (error (concat "unrecognized function " (symbol-name func))))
               (call-interactively func))
           (my-gtags-aux e1 e2)))
       (cl-return)))))

(defun my-gtags-find-root (path)
  (let ((p (directory-file-name (expand-file-name path))))
    (if (string-equal p my-gtags-top-root)
        nil
      (if (file-exists-p (concat p "/GTAGS"))
          t
        (my-gtags-find-root
         (directory-file-name
          (file-name-directory
           (directory-file-name p))))))))

(defun my-gtags-aux (command-args prompt)
  (let* ((default (concat
                   command-args
                   (when (not (string-equal command-args "")) " ")))
         (index (+ (string-bytes default) 1))
         (args
          (read-from-minibuffer
           (concat "Run gtags " prompt " with these args: ")
           (cons (concat default (thing-at-point 'symbol)) index)
           nil
           nil
           'my-gtags-history))
         (backup (nth 1 (assoc my-tools-id my-tags-directory-table)))
         (cddir (if (or (not (my-gtags-find-root default-directory))
                        current-prefix-arg)
                    (read-file-name
                     "directory: "
                     backup
                     backup
                     nil
                     nil
                     nil)
                  nil)))
    (when (and (not (string-match "\\(-L\\|-S\\)" args))
               (string-match "\\(-?[^\s]*\\)\\s-+\\([^-]*\\)$" args))
      (setq index (match-beginning 2))
      (when (not (string-match "^\"" (substring args index)))
        (setq args (concat
                    (substring args 0 index)
                    "\""
                    (substring args index)
                    "\""))))
    (with-temp-buffer
      (when cddir (cd cddir))
      (compilation-start
       (concat my-gtags-default-command
               (when my-gtags-highlight-matches
                 (concat " " my-gtags-color-params))
               " " args)
       'my-gtags-mode
       #'(lambda (mode) "*gtags*")))))
