(provide 'my-utils)

(require 'my-globals)
(require 'my-keymaps)

(require 'compile)

(defun my-shell-command-to-string (cmd)
  (let ((result (shell-command-to-string cmd)))
    (if (string-match "\n$" result)
        (substring result 0 -1)
      result)))

(defun my-stash ()
  (interactive)
  (find-file "~/gDrive/stash/"))

(defun my-set-title ()
  (interactive)
  (let ((title (read-from-minibuffer
                "title: "
                (frame-parameter (selected-frame) 'title))))
    (set-frame-parameter (selected-frame) 'title title)))

(defun my-shell-command-output-other-window ()
  (interactive)
  (let ((buffer (get-buffer "*Shell Command Output*")))
    (unless buffer
      (error "no shell command output buffer"))
    (delete-other-windows)
    (pop-to-buffer buffer)
    (other-window -1)))

(defun my-async-shell-command-output-other-window ()
  (interactive)
  (let ((buffer (get-buffer "*Async Shell Command*")))
    (unless buffer
      (error "no async shell command output buffer"))
    (delete-other-windows)
    (pop-to-buffer buffer)
    (other-window -1)))

(defun my-append-to-junk ()
  (interactive)
  (let ((fname
         (if (y-or-n-p "current directory: ")
             ".JUNK"
           (concat my-home-directory "/.emacs.d/.JUNK"))))
    (write-region (concat "\n@@ " (current-time-string) " @@\n\n") nil fname t)
    (write-region (region-beginning) (region-end) fname t)
    (write-region "\n@@ End @@\n" nil fname t)))

(when my-cygport
  (defun my-open-visualstudio ()
    (interactive)
    (let ((fname (buffer-file-name)))
      (if (not fname)
          (error "buffer has no file associated with it")
        (setq fname (file-name-nondirectory fname))
        (when (>
               (call-process-shell-command
                (concat "devenv.exe /edit "
                        fname
                        " /command \"Edit.goto "
                        (number-to-string (line-number-at-pos))
                        "\""))
               0)
          (error "couldn't open %s in visual studio" fname))))))

(defun my-native-buffer ()
  "Set end-of-line to native mode"
  (interactive)
  (set-buffer-file-coding-system
   ;;default-buffer-file-coding-system
   buffer-file-coding-system
   t))

(defun my-toggle-dos-eol ()
  (interactive)
  (let ((setting nil))
    (if buffer-display-table
        (setq setting (aref buffer-display-table ?\^M))
      (setq buffer-display-table (make-display-table)))
    (if setting
        (aset buffer-display-table ?\^M nil)
      (aset buffer-display-table ?\^M []))
    (force-window-update)))

(defun my-set-tab-width (w)
  (interactive "ntab-width: ")
  (set-variable 'tab-width w))

(defun my-check-spaces ()
  (interactive)
  (if current-prefix-arg
      (let ((delete-trailing-lines t))
        (delete-trailing-whitespace))
    (save-excursion
      (goto-char (point-min))
      (if (re-search-forward "[ \t]+$" nil t)
          (query-replace-regexp
           "[ \t]+$"
           ""
           nil (point-min) (point-max))
        (message "no trailing whitespace"))
      (sleep-for 1)
      (goto-char (point-min))
      (if (not (re-search-forward "\\(\\(^$\\)\n\\)\\(\\(^$\\)\n\\)+" nil t))
          (message "no multiple lines ")
        (query-replace-regexp
         "\\(\\(^$\\)\n\\)\\(\\(^$\\)\n\\)+"
         "\n"
         nil (point-min) (point-max)))
      (goto-char (point-max))
      (forward-line -1)
      (when (and (looking-at "^$")
                 (y-or-n-p "delete trailing blank lines "))
        (delete-blank-lines)))))

(defun my-kill-buffer-other-window ()
  (interactive)
  (let ((buf (current-buffer)))
    (other-window 1)
    (when (not (equal (current-buffer) buf))
      (kill-buffer (current-buffer))
      (other-window -1))))

(defun my-open-help-button ()
  (interactive)
  (let ((original-buffer (current-buffer))
        (help-buffer (get-buffer "*Help*")))
    (unless help-buffer
      (error "no *Help* buffer"))
    (switch-to-buffer help-buffer t)
    (goto-char (point-min))
    (if (forward-button 1)
        (progn
          (push-button (point))
          (bury-buffer help-buffer)
          (delete-other-windows))
      (switch-to-buffer original-buffer)
      (message "no button in *Help* buffer"))))

(defun my-overlay-and-prompt (start end prompt)
  (let ((ol (make-overlay start end))
        continue)
    (save-excursion
      (goto-char start)
      (overlay-put ol 'face 'highlight)
      (unwind-protect
          (setq continue (y-or-n-p prompt))
        (delete-overlay ol)))
      ;; ;; handle keyboard quit so we can ensure overlay is always deleted
      ;; (condition-case nil
      ;;     (setq continue (y-or-n-p prompt))
      ;;   (quit
      ;;    (delete-overlay ol)
      ;;    (keyboard-quit)))
      ;; (delete-overlay ol))
    continue))

(defun my-first-error-other-window ()
  (interactive)
  (let ((current-buf (current-buffer))
        (error-buf (next-error-find-buffer)))
    (when (and error-buf (bufferp error-buf))
      (switch-to-buffer-other-window error-buf)
      (compilation-next-error 1 nil (point-min))
      (recenter 0)
      (when (not current-prefix-arg)
        (switch-to-buffer-other-window current-buf)))))

(defun my-rotate-window-buffers-forward ()
  (interactive)
  (let ((bl (mapcar #'window-buffer (window-list))))
    (when (> (length bl) 1)
      (dolist (b (append (last bl) (butlast bl 1)))
        (switch-to-buffer b nil t)
        (other-window 1)))))

(defun my-rotate-window-buffers-backward ()
  (interactive)
  (let ((bl (mapcar #'window-buffer (window-list))))
    (when (> (length bl) 1)
      (dolist (b (append (cdr bl) (list (car bl))))
        (switch-to-buffer b nil t)
        (other-window 1)))))

(defun my-set-process-query-on-exit-flag (flag)
  (set-process-query-on-exit-flag (get-buffer-process (current-buffer)) flag))

(my-global-set-key (kbd "C-c TAB") 'my-first-error-other-window)
(my-global-set-key (kbd "C-c C-j") 'my-open-help-button)
(my-global-set-key (kbd "C-c j") 'my-append-to-junk)
