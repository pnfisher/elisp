(provide 'my-gtags-cscope)

(require 'my-tools)
(require 'my-gtags)
(require 'my-grep)

(require 'cl-lib)

(eval-and-compile (defconst my-gtags-cscope-binary "gtags-cscope"))
(defvar my-gtags-cscope-history nil)
(defvar my-gtags-cscope-prompt
  "called-[b]y [c]alling [d]ef [e]grep [f]ile #[i]nc [s]ym [t]ext")
(defvar my-gtags-cscope-command-table
  '((?s . ("0" . "symbol"))
    (?d . ("1" . "global definition"))
    (?b . ("2" . "functions called by"))
    (?c . ("3" . "locations calling"))
    (?t . ("4" . "text string"))
    ;; (?g . ("5" . "grep pattern"))
    (?e . ("6" . "egrep pattern"))
    (?f . ("7" . "file"))
    (?i . ("8" . "file including"))))
(defvar my-gtags-cscope-regexp-alist
  '(("^\\([^ ]+\\) \\([^ ]+\\) \\([0-9]+\\)"
     1
     3
     nil
     nil
     0
     ;; (2 grep-context-face)
     ;; (2 compilation-leave-directory-face)
     (2 compilation-enter-directory-face)
     )))

(eval-and-compile
  (my-grep-define-compilation-mode
    'my-gtags-cscope-mode
    my-gtags-cscope-binary))

(cl-assert (not my-gtags-cscope-highlight-matches))

(defun my-gtags-cscope ()
  (interactive)
  (cl-loop
   (message my-gtags-cscope-prompt)
   (let* ((arg (read-char))
          (entry (cdr (assoc arg my-gtags-cscope-command-table))))
     (when (char-equal arg ?q)
       (cl-return))
     (when entry
       (my-gtags-cscope-aux (car entry) (cdr entry))
       (cl-return)))))

(defun my-gtags-cscope-aux (command prompt)
  (let* ((token
          (read-from-minibuffer
           (concat "Find " prompt ": ")
           (cons (or (thing-at-point 'symbol) "") 1)
           nil
           nil
           'my-gtags-cscope-history))
         (backup (nth 1 (assoc my-tools-id my-tags-directory-table)))
         (cddir (if (or (not (my-gtags-find-root default-directory))
                        current-prefix-arg)
                    (read-file-name
                     "directory: "
                     backup
                     backup
                     nil
                     nil
                     nil)
                  nil))
         )
    (when (not (string-match "^\\s-*\"" token))
      (setq token (concat "\"" token "\"")))
    (with-temp-buffer
      (when cddir (cd cddir))
      (compilation-start
       (concat
        my-gtags-cscope-binary
        " -a -d -L"
        command
        " "
        token)
       'my-gtags-cscope-mode
       #'(lambda (mode) (concat "*" my-gtags-cscope-binary "*"))))))
