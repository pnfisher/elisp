(provide 'my-emacs)

;; require my stuff (globals and keymaps first because they are
;; actually needed by this file)
(require 'my-globals)
(require 'my-keymaps)

(require 'my-bookmark)
(require 'my-comint)
;; just save desktops manually if you feel like it
;; (require 'my-desktop)
(require 'my-dired)
(require 'my-ediff)
(require 'my-elisp)
(require 'my-frame)
(require 'my-funckeys)
;; (require 'my-gnus)
(require 'my-hacks)
(require 'my-ido)
(require 'my-prog)
(require 'my-pycalc)
(require 'my-qemu)
(require 'my-remember)
(require 'my-rgrep)
(require 'my-rmail)
(require 'my-scratch)
(require 'my-scroll)
(require 'my-shell)
(require 'my-spell)
(require 'my-ssh)
(require 'my-submodules)
(require 'my-subprocs)
(require 'my-term)
(require 'my-text)
(require 'my-themes)
(require 'my-time)
(require 'my-timers)
(require 'my-tramp)
(require 'my-uniquify)
(require 'my-utils)
(require 'my-view)
(require 'my-xterm)
(require 'my-zrgrep)

(require 'files)

;; apply the elisp read-only class set in my-globals.el to all elisp
;; source directories (except .el files in current directory and
;; ./my-themes directory)
;;
;; WARNING: the following must be executed after 'load-path has been
;; modified by all elisp init files, or those paths added to
;; 'load-path after this point won't get protected
(cl-flet ((normalize-path (path) (expand-file-name (concat path "/"))))
  (dolist (path (cl-set-difference
                 (mapcar #'normalize-path load-path)
                 (mapcar #'normalize-path (list
                                           my-elisp-directory
                                           my-theme-directory))
                 :test #'string-equal))
    (dir-locals-set-directory-class path 'my-elisp-source-class)))
;; let's also protect the c source files
(dir-locals-set-directory-class source-directory 'my-elisp-source-class)

;; convenient for easy reloading of saved desktop
(add-to-history 'extended-command-history "desktop-read")

(require 'elec-pair)
(setq-default blink-matching-paren 'jump)
(electric-pair-mode 1)
(setq-default electric-pair-mode nil)

(or (boundp 'isearch-mode-map)
    (load-library "isearch"))

;; not needed, set by .Xdefaults
(when (and (window-system) nil)
  (set-default-font
   "-misc-fixed-medium-r-normal--18-120-100-100-c-90-iso8859-1")
  (menu-bar-mode 0)
  (tool-bar-mode 0))

(setq-default tab-width 2)

;; rmail stuff
;;(load-library "rmail-setup.el")

;; printing stuff
;;(setq lpr-switches '("-Psweng_hp"))
;;(setq printer-name "sweng_hp")
;;(setq lpr-command "lpr")

(custom-set-variables
 ;;'(desktop-restore-in-current-display t)
 ;; I prefer this one to the one above. The one above resizes the
 ;; emacs session when running on a different sized display from the
 ;; original
 '(history-delete-duplicates t)
 '(desktop-restore-frames nil)
 '(use-dialog-box nil)
 '(warning-suppress-types '((undo discard-info)))
 '(make-backup-files nil)
 '(auto-save-default nil)
 '(visible-bell nil)
 '(Man-notify-method (quote bully))
 '(cursor-in-non-selected-windows nil)
 '(display-hourglass nil)
 '(indent-tabs-mode nil)
 '(mode-line-in-non-selected-windows nil)
 '(perl-indent-level 2)
 '(large-file-warning-threshold nil)
 '(large-file-warning-threshold nil)
 '(x-select-enable-clipboard t)
 '(transient-mark-mode t)
 '(inhibit-startup-message t)
 '(inhibit-eol-conversion t)
 '(ansi-color-names-vector
   ["black" "red3" "green3" "yellow3" "RoyalBlue1" "magenta3" "cyan3" "White"])
 '(mode-line-format
    '("%e"
      "-"
      mode-line-mule-info
      mode-line-modified
      mode-line-frame-identification
      ;;mode-line-buffer-identification
      "%b "
      global-mode-string
      " "
      mode-line-modes
      ;;minor-mode-alist
      (which-function-mode ("" which-func-format " "))
      mode-line-position
      vc-mode
      ;; create a dashed line at tail of mode line
      ;;" -%-"
      ))
 ;; '(mode-line-position
 ;;   '((-3
 ;;      #("%p" 0 2 nil))
 ;;     (size-indication-mode
 ;;      (8
 ;;       #(" of %I" 0 6 nil)))
 ;;     (line-number-mode
 ;;      ((column-number-mode
 ;;        (10
 ;;         #(" (%l,%c)" 0 8 nil))
 ;;        (3
 ;;         #(" L%l" 0 4 nil))))
 ;;      ((column-number-mode
 ;;        (5
 ;;         #(" C%c" 0 4 nil)))))))
 )

;; turn off the bell sound generated by errors
(setq-default ring-bell-function #'ignore)

;; 'meta if you'd prefer to swith with alt key
(windmove-default-keybindings 'shift)
(set-scroll-bar-mode 'right)
(blink-cursor-mode -1)
(tooltip-mode -1)
(line-number-mode t)
;;(put 'downcase-region 'disabled nil)
(put 'upcase-region 'disabled nil)
;;(load-library "eshell-util")

;; ;; don't use new smie stuff for indenting in sh-mode
;; (require 'sh-script)
;; (setq sh-use-smie nil)

;; (add-hook
;;  'nxml-mode-hook
;;  '(lambda ()
;;     (font-lock-mode t)))

;; (add-hook
;;  'org-mode-hook
;;  '(lambda ()
;;     (turn-on-font-lock)))

(my-global-set-key (kbd "C-<right>") 'forward-word)
(my-global-set-key (kbd "C-<left>") 'backward-word)
;; (my-global-set-key (kbd "C-c C-j") 'find-file-at-point)
(my-global-set-key (kbd "C-x r S") 'string-insert-rectangle)
(when my-cygport
  (my-global-set-key (kbd "M-*") 'pop-tag-mark))
