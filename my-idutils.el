(provide 'my-idutils)

(require 'my-globals)
(require 'my-tools)
(require 'my-grep)

(require 'cl-lib)

(eval-and-compile (defconst my-gid-binary "gid"))
(defvar my-idutils-history nil)
(defvar my-idutils-top-root my-home-directory)

(eval-and-compile
  (my-grep-define-compilation-mode
    'my-gid-mode
    my-gid-binary))

(cl-assert (not my-gid-highlight-matches))

(add-to-list
 'compilation-error-regexp-alist-alist
 '(fnid . ("^\\([^ \n]+\\)\n" 1)))

(defun my-idutils-find-root (path)
  (let ((p (directory-file-name (expand-file-name path))))
    (if (string-equal p my-idutils-top-root)
        nil
      (if (file-exists-p (concat p "/ID"))
          p
        (my-idutils-find-root
         (directory-file-name
          (file-name-directory
           (directory-file-name p))))))))

(defun my-idutils-helper (prompt cmd error-alist)
  (let* ((token (read-from-minibuffer
                 prompt
                 (cons (or (thing-at-point 'symbol) "") 1)
                 nil
                 nil
                 'my-idutils-history))
         (backup (nth 1 (assoc my-tools-id my-tags-directory-table)))
         (iddir (my-idutils-find-root default-directory))
         (rundir iddir)
         (name-fn #'(lambda (mode) "*idutils*")))
    (when (or (not iddir) current-prefix-arg)
      (setq rundir (read-file-name
                    "directory: "
                    backup
                    backup
                    nil
                    nil
                    nil)
            iddir (my-idutils-find-root rundir))
      (when (not iddir)
        (error "error, invalid idutils directory specified")))
    (setq cmd (concat cmd (format " %s/ID " iddir) "\"" token "\""))
    (with-temp-buffer
      (cd rundir)
      (if error-alist
          (let ((compilation-scroll-output nil)
                (compilation-start-hook
                 #'(lambda (process)
                     (set
                      (make-local-variable 'compilation-error-regexp-alist)
                      error-alist))))
            (compilation-start cmd 'compilation-mode name-fn))
        (compilation-start cmd 'my-gid-mode name-fn)))))

(defun my-gid ()
  (interactive)
  ;; (my-idutils-helper "Find token: " (concat my-gid-binary " -f") '(gnu))
  (my-idutils-helper "Find token: " (concat my-gid-binary " -f") nil))

(defun my-fnid ()
  (interactive)
  (my-idutils-helper "Find file: " "fnid -S newline -f" '(fnid)))
