(provide 'my-glimpse)

(require 'my-globals)
(require 'my-tools)
(require 'my-grep)

(require 'cl-lib)

(eval-and-compile (defconst my-glimpse-binary "glimpse"))
(defvar my-glimpse-history nil)
(defvar my-glimpse-params "-n -y -i")
(defvar my-glimpse-top-root my-home-directory)
(defvar my-glimpse-regexp-alist
  '(("^\\([^:(\\s-\n]+\\): \\([0-9]+\\):" 1 2)))

(eval-and-compile
  (my-grep-define-compilation-mode
    'my-glimpse-mode
    my-glimpse-binary))

(cl-assert (not my-glimpse-highlight-matches))

(defun my-glimpse-find-root (path)
  (let ((p (directory-file-name (expand-file-name path))))
    (if (string-equal p my-glimpse-top-root)
        nil
      (if (file-exists-p (concat p "/.glimpse_index"))
          p
        (my-glimpse-find-root
         (directory-file-name
          (file-name-directory
           (directory-file-name p))))))))

(defun my-glimpse (&optional my-glimpse-directory)
  (interactive)
  (let* ((default (concat my-glimpse-binary " " my-glimpse-params " "))
         (index (+ (string-bytes default) 1))
         (args
          (replace-regexp-in-string
           (concat "^" my-glimpse-binary " ")
           ""
           (read-from-minibuffer
            (format "Run %s (like this): " my-glimpse-binary)
            (cons (concat default (thing-at-point 'symbol)) index)
            nil
            nil
            'my-glimpse-history)))
         (backup (nth 1 (assoc my-tools-id my-tags-directory-table)))
         (gdir (if my-glimpse-directory
                   my-glimpse-directory
                 (my-glimpse-find-root default-directory))))
    (when (or (not gdir) current-prefix-arg)
      (setq gdir (my-glimpse-find-root (read-file-name
                                        "directory: "
                                        backup
                                        backup
                                        nil
                                        nil
                                        nil)))
      (when (not gdir)
        (error "error, invalid glimpse directory specified")))
    (compilation-start
     (concat my-glimpse-binary " -H " gdir " " args)
     'my-glimpse-mode
     #'(lambda (mode) (concat "*" my-glimpse-binary "*")))))
