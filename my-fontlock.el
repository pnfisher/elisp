(provide 'my-fontlock)

(require 'font-lock)

;; (defun my-custom-face (foreground &optional background)
;;   "Return a face with background set to bg, and foreground set to fg"
;;   (let* ((fg (downcase foreground))
;;          (bg (if (null background)
;;                  "black"
;;                (downcase background)))
;;          (face-name (intern (concat "my-custom-" fg "-" bg "-face"))))
;;     (when (not (facep face-name))
;;       ;; (let ((face (make-face face-name)))
;;       ;;   (modify-face face fg bg)
;;       ;;   face)
;;       (modify-face
;;        (custom-declare-face
;;         face-name
;;         '((t :inherit default))
;;         (concat "My custom face using foreground " fg ", background " bg)
;;         :group 'my-custom-faces-group)
;;        fg
;;        bg))
;;     face-name))

(defun my-fontlock-set-level (level)
  (interactive "nlevel: ")
  (setq font-lock-maximum-decoration level)
  (let ((filename (buffer-file-name)))
    (when filename (revert-buffer t t))))

(mapc
 (lambda (mode)
   (font-lock-add-keywords
    mode
    '(("\\(xxx\\|XXX\\|FIXME\\|TODO\\)" 1 'font-lock-warning-face t))))
 '(
   c-mode
   c++-mode
   shell-script-mode
   emacs-lisp-mode
   java-mode
   javascript-mode
   python-mode
   makefile-mode
   ))
