(deftheme my-basic2)

(require 'my-basic-faces)

(eval
 `(custom-theme-set-faces
   'my-basic2
   ,@my-basic1-faces
   ,@my-basic2-faces
   )
 )

(provide-theme 'my-basic2)
