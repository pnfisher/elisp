;; the bare minimum of font-lock changes needed to make font-lock
;; usable across various modes

(deftheme my-basic3)

(require 'my-basic-faces)

(eval
 `(custom-theme-set-faces
   'my-basic3
   ,@my-basic1-faces
   ,@my-basic2-faces
   ,@my-basic3-faces
   )
 )

(provide-theme 'my-basic3)
