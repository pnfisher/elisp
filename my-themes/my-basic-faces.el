(provide 'my-basic-faces)

(defconst my-basic1-faces
  '(
    '(default ((t (:foreground "Gray80" :background "Black"))))
    ))

(defconst my-basic2-faces
  '(
    ;; font-lock rtags
    '(rtags-skippedline ((t (:background "Gray40"))))
    ;; font-lock company-mode
    '(company-tooltip ((t (:foreground "Black" :background "Gray80"))))
    '(company-tooltip-selection
      ((t (:foreground "White" :background "DarkOliveGreen"))))
    '(company-tooltip-common ((t (:foreground "Green" :background "Gray40"))))
    '(company-preview-common ((t (:foreground "Green"))))
    '(company-preview ((t (:background "Black"))))
    '(company-scrollbar-bg ((t (:background "White"))))
    '(company-scrollbar-fg ((t (:background "Gray60"))))
    ))

(defconst my-basic3-faces
  '(
    '(isearch ((t (:foreground "White" :background "DarkOliveGreen"))))
    '(highlight ((t (:background "DarkOliveGreen"))))
    '(region ((t (:background "DarkSlateGray"))))
    '(fringe ((t (:background "Gray25"))))
    '(scroll-bar ((t (:background "Gray60"))))
    ;; '(cursor ((t (:background "BlanchedAlmond"))))
    '(cursor ((t (:background "Moccasin"))))
    '(comint-highlight-prompt ((t (:foreground "White"))))
    '(minibuffer-prompt ((t (:inherit default))))
    ;; '(query-replace
    ;;   ((t (:foreground "White" :background "DarkOliveGreen"))))

    ;; helm
    '(helm-match ((t (:foreground "SteelBlue2"))))
    '(helm-selection ((t (:background "DarkOliveGreen"))))
    '(helm-candidate-number ((t (:background "Gray80"))))

    ;; font-lock stuff
    '(font-lock-warning-face ((t (:foreground "Red1"))))
    ))

(defconst my-basic4-faces
  '(
    ;;
    ;; more font-lock stuff
    ;;

    ;; shell-mode
    '(sh-heredoc-face ((t (:foreground "PaleGreen1"))))

    ;; general
    '(font-lock-variable-name-face
      ((t (:foreground "Gray80"))))
    '(font-lock-preprocessor-face
      ((t (:foreground "LightGray" :background "DarkOliveGreen"))))
    '(font-lock-keyword-face
      ((t (:foreground "Gray80" :background "Gray20"))))
    '(font-lock-type-face
      ((t (:foreground "Gray80" :background "Gray20"))))
    '(font-lock-constant-face
      ((t (:foreground "White"))))
    ;; '(font-lock-warning-face ((t (:foreground "Red1"))))
    ))

(defconst my-basic5-faces
  '(
    '(font-lock-comment-face ((t (:foreground "White"))))
    '(font-lock-string-face ((t (:foreground "White"))))
    '(font-lock-function-name-face ((t (:foreground "Gray90"))))
    ;; '(font-lock-comment-face ((t (:foreground "Coral1"))))
    ;; '(font-lock-string-face ((t (:foreground "Coral2"))))
    ;; '(font-lock-string-face ((t (:foreground "Plum4"))))
    ;; '(font-lock-string-face ((t (:foreground "RosyBrown1"))))
    ;; '(font-lock-string-face ((t (:foreground "DarkKhaki"))))
    ;; '(font-lock-string-face ((t (:foreground "BurlyWood"))))
    ;; '(font-lock-string-face ((t (:foreground "FireBrick2"))))
    ))
