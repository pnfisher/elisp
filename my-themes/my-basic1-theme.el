(deftheme my-basic1)

(require 'my-basic-faces)

(eval
 `(custom-theme-set-faces
   'my-basic1
   ,@my-basic1-faces
   )
 )

(provide-theme 'my-basic1)
