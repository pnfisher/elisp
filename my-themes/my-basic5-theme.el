;; more elaborate font-lock face changes to soften some of the
;; brightness of the default settings

(deftheme my-basic5)

(require 'my-basic-faces)

(eval
 `(custom-theme-set-faces
   'my-basic5
   ,@my-basic1-faces
   ,@my-basic2-faces
   ,@my-basic3-faces
   ,@my-basic4-faces
   ,@my-basic5-faces
   )
 )

(provide-theme 'my-basic5)
