(provide 'my-elisp)

(require 'my-utils)
(require 'my-rgrep)
(require 'my-zrgrep)

(require 'cl-lib)
(require 'pp)

(defconst my-elisp-practice-file
  (expand-file-name (concat my-elisp-directory "/emacs-lisp")))
(defvar my-elisp-grep-history nil)

(cl-defun my-elisp ()
  (interactive)
  (when current-prefix-arg
    (my-elisp-insert)
    (cl-return-from my-elisp))
  (when (not (file-exists-p my-elisp-practice-file))
    (error "%s is missing" my-elisp-practice-file))
  (find-file my-elisp-practice-file)
  (when (= (line-number-at-pos) 1)
    (re-search-forward "^\\([^;]\\|$\\)" nil t)))

(defun my-elisp-insert ()
  (interactive)
  (when (not (and transient-mark-mode mark-active))
    (error "no region selected"))
  (when (not (file-exists-p my-elisp-practice-file))
    (error "%s is missing" my-elisp-practice-file))
  (write-region "\n" nil my-elisp-practice-file t)
  (write-region (region-beginning) (region-end) my-elisp-practice-file t)
  (write-region "\n" nil my-elisp-practice-file t))

(defun my-elisp-find-symbol-at-point ()
  (interactive)
  (point-to-register (string-to-char ".") t)
  (let ((sym (symbol-at-point)))
    (if (and sym (symbol-file sym))
        (cond
         ((fboundp sym)
          (find-function sym))
         ((boundp sym)
          (find-variable sym))
         ((facep sym)
          (find-face-definition sym))
         (t
          (error "unrecognized symbol type")))
      (message (concat "couldn't find a symbol named " (symbol-name sym))))))

(defun my-elisp-zrgrep ()
  (interactive)
  (when (not (file-exists-p my-zrgrep-emacs-lisp-directory))
    (error "No such directory '%s'" my-zrgrep-emacs-lisp-directory))
  (let ((tap (thing-at-point 'symbol))
        (grep-files-history (list "*.gz" "all" "*.el"))
        (grep-setup-hook #'my-zrgrep-process-setup))
    ;; temp buffer so rzgrep doesn't try to use the extension of the
    ;; file associated with our current buffer but instead defaults to
    ;; *.gz since that will be the first element in grep-files-history
    ;; (see above)
    (with-temp-buffer
      ;; so zrgrep will have the thing that was at point in our
      ;; original buffer as its default search item
      (when tap (insert tap))
      (cd my-zrgrep-emacs-lisp-directory)
      (call-interactively #'zrgrep))))

;; this function depends upon ~/src/elisp/.rgreprc for its proper
;; functioning
(defun my-elisp-grep ()
  (interactive)
  (let ((current-directory default-directory)
        (my-rgrep-history my-elisp-grep-history)
        (rgreprc (concat my-elisp-directory my-rgreprc-file)))
    (when (not (file-exists-p rgreprc))
      (error "%s is missing" rgreprc))
    (unwind-protect
        (progn
          (cd my-elisp-directory)
          (my-rgrep))
      (cd current-directory))
    (setq my-elisp-grep-history my-rgrep-history)))

(defun my-elisp-pp-eval-last-sexp ()
  (interactive)
  (let (expression)
    (if current-prefix-arg
        (save-excursion
          (beginning-of-defun)
          (forward-sexp)
          (setq expression (pp-last-sexp)))
      (setq expression (pp-last-sexp)))
    (setq values (cons (eval expression lexical-binding) values))    
    (pop-to-buffer "*pp*")
    (read-only-mode -1)
    (erase-buffer)
    (cl-letf (((symbol-function 'message) #'ignore))
      (pp-display-expression (car values) (buffer-name)))
    (save-excursion
      (goto-char (point-max))
      (insert "\n"))
    (view-mode)))

(add-hook
 'emacs-lisp-mode-hook
 '(lambda ()
    (set (make-local-variable 'font-lock-maximum-decoration) 3)
    (font-lock-mode t)))

(define-key emacs-lisp-mode-map (kbd "C-c C-c") 'comment-region)
(my-global-set-key (kbd "C-c C-p") 'my-elisp-find-symbol-at-point)
