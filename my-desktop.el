(provide 'my-desktop)

(require 'desktop)

;; use only one desktop
(setq desktop-path '("~/.emacs.d"))
(setq desktop-dirname "~/.emacs.d")
(setq desktop-base-file-name "desktop")

;; remove desktop after it's been read
(add-hook
 'desktop-after-read-hook
 '(lambda ()
    ;; desktop-remove clears desktop-dirname
    (let ((tmp desktop-dirname))
      (desktop-remove)
      (setq desktop-dirname tmp))))

(defun my-desktop-saved-session ()
  (file-exists-p (concat desktop-dirname "/" desktop-base-file-name)))

(defun my-desktop-restore ()
  "Restore a saved emacs session."
  (interactive)
  (if (my-desktop-saved-session)
      (desktop-read)
    (error "no saved desktop session found")))

(defun my-desktop-save ()
  "Save an emacs session."
  (interactive)
  (unless noninteractive
    (if (my-desktop-saved-session)
        (when (y-or-n-p "overwrite existing desktop? ")
          (desktop-save-in-desktop-dir))
      (when (y-or-n-p "save current desktop? ")
        (desktop-save-in-desktop-dir)))))

(add-hook 'kill-emacs-hook 'my-desktop-save)

;;;; ask user whether to restore desktop at start-up
;;(add-hook
;; 'after-init-hook
;; '(lambda ()
;;    (when (and (my-desktop-saved-session)
;;               (y-or-n-p "restore saved desktop? "))
;;      (my-desktop-restore))))
