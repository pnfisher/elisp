;; advice for jdb which lets us use -a instead of -attach to specify
;; an attach request. We need this to workaround an annoying feature
;; in the gud jdb implementation which when it sees -attach in the jdb
;; command line always issues a classpath command to jdb. It then uses
;; whatever is returned as the classpath in which to search for source
;; files. This doesn't work for end-users when they are attaching to a
;; java ee server and need to feed gud jdb a special classpath in
;; order to debug a java ee application running in the ee server.
;;
;; End-user can use -sourcepath in conjunction with -a, then gud jdb
;; will treat the specified sourcepath as the classpath in which to
;; look for source files (sourcepath should be specified relative to
;; the current working directory or as an absolute path). Or end-user
;; can specify sourcepaths in their .jdbrc file, which this file
;; will read looking for the sourcepath keyword

(provide 'my-jdb)

(require 'my-globals)
(require 'gud)
(require 'cl-lib)

;; turn our "special" jdb -a argument into a "real" jdb -attach
;; argument. (At this point, gud jdb will have been fooled into
;; thinking end-user has not specified a real -attach argument; which
;; is good, because then gud jdb won't issue a classpath command to
;; the running jdb process in order to determine the directories in
;; which it should look for source code files. Instead it will use
;; whatever we've fed it while looking for sourcepath keyword in an
;; existing .jdbrc file, or it will use the current directory if a
;; .jdbrc file doesn't exist -- and if we've also specified a
;; -sourcepath arg, then it will use that as well
(defun my-gud-jdb-massage-args (orig-fun &rest args)
  (insert "gud classpaths in use are: ")
  (if gud-jdb-classpath
      (dolist (cp gud-jdb-classpath)
        (insert "\n" cp))
    (insert "none"))
  (insert "\n")
  (insert "gud sourcepaths in use are: ")
  (if gud-jdb-sourcepath
      (dolist (cp gud-jdb-sourcepath)
        (insert "\n" cp))
    (insert "none"))
  (insert "\n")
  (cl-map
   'list
   '(lambda (entry)
      (progn
        (if (equal entry "-a")
            "-attach"
          entry)))
   (apply orig-fun args)))

(advice-add 'gud-jdb-massage-args :around #'my-gud-jdb-massage-args)

(defvar jdbrcfile-top-directory my-home-directory)

(defun find-jdbrcfile (path)
  (let ((dir (directory-file-name (expand-file-name path)))
        (p nil))
    (if (string-equal dir jdbrcfile-top-directory)
        nil
      (setq p (concat dir "/.jdbrc"))
      (if (file-exists-p p)
          p
        (find-jdbrcfile
         (directory-file-name
          (file-name-directory
           (directory-file-name dir))))))))

(defun my-jdb-read-jdbrcfile ()
  (let ((sourcepath nil)
        (jdbrcfile (find-jdbrcfile default-directory))
        done line rstart)
    (when jdbrcfile
      (with-temp-buffer
        (insert-file-contents jdbrcfile)
        (goto-char (point-min))
        (while (not done)
          (beginning-of-line)
          (when (looking-at "^\\s-*sourcepath\\s-*\\(.*\\)")
            (setq line (match-string 1))
            (if sourcepath
                (setq sourcepath (concat sourcepath ":" line))
              (setq sourcepath line)))
          (when (> (forward-line 1) 0)
            (setq done t)))))
    sourcepath))

(defadvice jdb (before my-jdb-advice compile activate)
  ;; gud jdb will read the CLASSPATH env variable in order to
  ;; determine where it should look for source code files; if end-user
  ;; specifies a -sourcepath argument, the sourcepath paths will be
  ;; appended to the CLASSPATH paths when gud jdb looks for source
  ;; code files. If we find sourcepath entries in a .jdbrc file, then
  ;; we'll set CLASSPATH env variable to whatever we entries we found
  (setenv "CLASSPATH" (my-jdb-read-jdbrcfile)))
