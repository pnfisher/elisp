(provide 'my-time)

(require 'time)

(custom-set-variables
 '(display-time-24hr-format t)
 '(display-time-default-load-average nil)
 '(display-time-world-list
   (append
    display-time-world-list
    '(("Etc/UTC" "UTC"))))
 )

(display-time-mode 1)
