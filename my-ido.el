(provide 'my-ido)

(require 'my-globals)
(require 'my-utils)

(require 'ido)

;; to avoid problems with nfs mounted home directories
(setq ido-save-directory-list-file
      (concat user-emacs-directory "ido.last." my-hostname))

;; 'buffer or 'file or 'both
(ido-mode 'both)

(custom-set-variables
 '(ido-enable-flex-matching t))

;; technically, setting ido-mode to 'both above has already done this,
;; but my-helm may override our setting above and set ido-mode to
;; 'file. But even if it does, I'd like to keep this particular remap
;; in effect.
(global-set-key [remap kill-buffer] 'ido-kill-buffer)
