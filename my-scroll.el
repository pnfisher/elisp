(provide 'my-scroll)

(require 'my-keymaps)

(defvar my-unscroll-point (make-marker))
(defvar my-unscroll-window-start (make-marker))

(put 'scroll-up 'unscrollable t)
(put 'scroll-down 'unscrollable t)

(defun my-unscroll-maybe-remember (&rest args)
  (if (not (get last-command 'unscrollable))
      (progn
        (set-marker my-unscroll-point (point))
        (set-marker my-unscroll-window-start (window-start)))))

(advice-add #'scroll-up :before #'my-unscroll-maybe-remember)
(advice-add #'scroll-down :before #'my-unscroll-maybe-remember)

(defun my-unscroll ()
  (interactive)
  (goto-char my-unscroll-point)
  (set-window-start nil my-unscroll-window-start))

(defun my-scroll-n-lines-ahead (&optional n)
  (interactive "P")
  (scroll-up (prefix-numeric-value n)))

(defun my-scroll-n-lines-behind (&optional n)
  (interactive "P")
  (scroll-down (prefix-numeric-value n)))

(defun my-scroll-other-window-n-lines-ahead (&optional n)
  (interactive "P")
  (scroll-other-window (prefix-numeric-value n)))

(defun my-scroll-other-window-n-lines-behind (&optional n)
  (interactive "P")
  (scroll-other-window-down (prefix-numeric-value n)))

(my-global-set-key (kbd "C-c v") 'my-unscroll)
(my-global-set-key (kbd "M-<down>") 'my-scroll-other-window-n-lines-ahead)
(my-global-set-key (kbd "M-<up>") 'my-scroll-other-window-n-lines-behind)
