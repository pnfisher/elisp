(provide 'my-compile)

(require 'my-globals)

(require 'compile)

(defun my-compile-compilation-find-file-advice
    (ofunc marker filename &rest args)
  (setq filename
        (replace-regexp-in-string
         "\\s-+" "\\\\ "
         (replace-regexp-in-string "\\\\" "/" filename)))
  (if (string-match (concat "^[Cc]:/cygwin64"
                            my-home-directory
                            "\\(.*\\)")
                    filename)
      (setq filename (concat "~" (match-string 1 filename)))
    (if (string-match "^[Cc]:/cygwin64/home\\(.*\\)" filename)
        (setq filename (concat "/home" (match-string 1 filename)))
      (when (string-match "^[Cc]:\\(.*\\)" filename)
        (setq filename (concat "/cygdrive/c" (match-string 1 filename))))))
  (apply ofunc marker filename args))

(when my-cygport
  (advice-add #'compilation-find-file
              :around
              #'my-compile-compilation-find-file-advice))
