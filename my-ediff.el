(provide 'my-ediff)

(require 'my-magit)
(require 'my-svn)

(require 'cl-lib)

;; <section> the following section taken from
;; https://www.emacswiki.org/emacs/EdiffMode
(defvar my-ediff-begin-win-config nil)
(defvar my-ediff-begin-win-reg ?.)
(defvar my-ediff-after-win-config nil)
(defvar my-ediff-after-win-reg ?e)

(defun my-ediff-before-setup-hook ()
  (setq my-ediff-begin-win-config (current-window-configuration))
  (cl-assert (characterp my-ediff-begin-win-reg))
  (set-register
   my-ediff-begin-win-reg
   (list my-ediff-begin-win-config (point-marker))))
(add-hook 'ediff-before-setup-hook 'my-ediff-before-setup-hook)

(defun my-ediff-after-setup-hook ()
  (setq my-ediff-after-win-config (current-window-configuration))
  (cl-assert (characterp my-ediff-after-win-reg))
  (set-register
   my-ediff-after-win-reg
   (list my-ediff-after-win-config (point-marker))))
(add-hook 'ediff-after-setup-windows-hook 'my-ediff-after-setup-hook 'append)

(defun my-ediff-quit-hook ()
  (ediff-cleanup-mess)
  (when my-ediff-begin-win-config
    (set-window-configuration my-ediff-begin-win-config)))
(add-hook 'ediff-quit-hook 'my-ediff-quit-hook)
;; <section/>

;; (defun my-ediff-vc (arg)
;;   (interactive "P")
;;   (cond ((magit-toplevel default-directory)
;;          (if current-prefix-arg
;;              (call-interactively #'magit-ediff-compare)
;;            (my-magit-ediff-current-buffer)))
;;         ((svn-version-controlled-dir-p (expand-file-name default-directory))
;;          (cl-letf (((symbol-function 'svn-ediff-startup-hook) #'ignore))
;;            (svn-file-show-svn-ediff arg)))
;;         (t
;;          (error "unknown version control system"))))

;; (defun my-ediff-vc-rev ()
;;   (interactive)
;;   (cond ((magit-toplevel default-directory)
;;          (call-interactively #'magit-ediff-compare))
;;         ((svn-version-controlled-dir-p (expand-file-name default-directory))
;;          (cl-letf (((symbol-function 'svn-ediff-startup-hook) #'ignore))
;;            (svn-file-show-svn-ediff t)))
;;         (t
;;          (error "unknown version control system"))))

(defun my-ediff-vc (arg)
  (interactive "P")
  (cond ((magit-toplevel default-directory)
         (if current-prefix-arg
             (call-interactively #'magit-ediff-compare)
           (my-magit-ediff-current-buffer)))
        ((svn-version-controlled-dir-p (expand-file-name default-directory))
         (my-svn-ediff-revision))
        (t
         (error "unknown version control system"))))

(defun my-ediff-vc-rev ()
  (interactive)
  (cond ((magit-toplevel default-directory)
         (call-interactively #'magit-ediff-compare))
        ((svn-version-controlled-dir-p (expand-file-name default-directory))
         (my-svn-ediff-revision))
        (t
         (error "unknown version control system"))))

(defun my-ediff-host ()
  (interactive)
  (let ((host (read-string "target host: "))
        (fname
         (if (eq major-mode 'dired-mode)
             (thing-at-point 'filename)
           (let* ((bname (buffer-name))
                  (match (string-match "<\\|:" bname 0)))
             (if match
                 (substring bname 0 match)
               bname)))))
    (ediff-files
     fname
     (concat "/ssh:"
             host
             ":/"
             default-directory
             "/"
             fname))))

(custom-set-variables
 '(ediff-window-setup-function (quote ediff-setup-windows-plain)))

(setq-default ediff-auto-refine 'on)

(add-hook
 'ediff-startup-hook
 '(lambda ()
    (ediff-jump-to-difference 1)))
