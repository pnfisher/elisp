(provide 'my-shell)

(require 'my-utils)

(require 'shell)

(setq explicit-shell-file-name "/bin/bash")

(add-hook
 'shell-mode-hook
 '(lambda ()
    (delete-other-windows)
    (my-set-process-query-on-exit-flag nil)
    (font-lock-mode t)
    (set (make-local-variable 'ansi-color-apply-face-function)
         'ansi-color-apply-overlay-face)))
